//
//  ErrorAlertableViewModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import RxSwift

protocol ErrorAlertableViewModelType {

  func errorToAlertable(error: Error) -> (title: String, message: String)
  var error: PublishSubject<(title: String, message: String)> { get set }
}

extension ErrorAlertableViewModelType {

  func errorToAlertable(error: Error) -> (title: String, message: String) {
    if let apiServiceError = error as? APIServiceErrorType {
      return (title: apiServiceError.name, message: apiServiceError.message)
    }
    return (title: "", message: error.localizedDescription)
  }
}
