//
//  BaseCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import RxSwift
import Foundation

class BaseCoordinator<ResultType> {
  
  typealias CoordinationResult = ResultType
  
  let disposeBag = DisposeBag()
  
  private let identifier = UUID()
  
  public var childCoordinators = [UUID: Any]()
  
  internal let window: UIWindow
  
  internal let baseViewController: UIViewController
  
  internal let transitionType: CoordinatorTransitionType
  
  internal let animated: Bool
//
//  internal let baseViewModel: BaseViewModel = BaseViewModel()
  
  init(window: UIWindow, baseViewController: UIViewController = UIViewController(), transitionType: CoordinatorTransitionType = .rootWindow, animated: Bool = true) {
    self.window = window
    self.baseViewController = baseViewController
    self.transitionType = transitionType
    self.animated = animated
    
  }
  
  private func store<T>(coordinator: BaseCoordinator<T>) {
    childCoordinators[coordinator.identifier] = coordinator
  }
  
  private func free<T>(coordinator: BaseCoordinator<T>) {
    childCoordinators[coordinator.identifier] = nil
  }

  func coordinate<T>(to coordinator: BaseCoordinator<T>) -> Observable<T> {
    store(coordinator: coordinator)
    return coordinator.start()
      .do(onNext: { [weak self] _ in self?.free(coordinator: coordinator) })
  }

  func start() -> Observable<ResultType> {
    fatalError("Start method should be implemented.")
  }
  
  
  func navigateToTarget<T>(target: BaseCoordinator<T>) -> Observable<T> {
    return coordinate(to: target)
  }
  
}

