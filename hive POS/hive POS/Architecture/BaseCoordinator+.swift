//
//  BaseCoordinator+.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import UIKit

enum CoordinatorTransitionType {
  case push
  case modal
  case rootWindow
  case rootNavigation
  case rootInNavigation
}

extension BaseCoordinator {
  
  func transition(to destinationViewController: UIViewController) {
    switch transitionType {
    case .push: baseViewController.navigationController?.pushViewController(destinationViewController, animated: animated)
    case .modal:
      destinationViewController.modalPresentationStyle = .formSheet
      baseViewController.present(destinationViewController, animated: animated, completion: nil)
    case .rootWindow:
      window.rootViewController = destinationViewController
      window.makeKeyAndVisible()
    case .rootNavigation:
      (baseViewController as! UINavigationController).pushViewController(destinationViewController, animated: false)
    case .rootInNavigation:
      baseViewController.navigationController?.setViewControllers([destinationViewController], animated: false)
    }
  }
  
  func navigateBack() {
    switch transitionType {
    case .push:
      baseViewController.navigationController?.popViewController(animated: animated)
    case .modal:
      baseViewController.dismiss(animated: animated, completion: nil)
    case .rootWindow:
      break
    case .rootNavigation:
      break
    case .rootInNavigation:
      break
    }
  }
  
  func navigateBackToRoot() {
    switch transitionType {
    case .push:
      baseViewController.navigationController?.popToRootViewController(animated: animated)
    case .modal:
      baseViewController.dismiss(animated: animated, completion: nil)
    case .rootWindow:
      break
    case .rootNavigation:
      break
    case .rootInNavigation:
      break
    }
  }
  
}


