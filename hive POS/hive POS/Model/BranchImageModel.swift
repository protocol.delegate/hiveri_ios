//
//  BranchImageModel.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class BranchImageModel: NSObject, Mappable{
    
    var branchImageId: String = ""
    var branchImageUrl: String = ""
    var profileImage: Bool = false
    var ID: Int = -1
    
    required convenience init?(map: Map) {
      self.init()
    }
    
    func mapping(map: Map) {
        branchImageId <- map["imageId"]
        branchImageUrl <- map["imageUrl"]
        profileImage <- map["profileImage"]
        ID <- map["id"]
    }
}
