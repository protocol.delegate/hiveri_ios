//
//  BranchHolidayDateModel.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class BranchHolidayDateModel: NSObject, Mappable {
    
    var day: String = ""
  var week: String = ""
    
    required convenience init?(map: Map) {
      self.init()
    }
    
    func mapping(map: Map) {
      day <- map["day"]
      week <- map["week"]
    }
    
}
