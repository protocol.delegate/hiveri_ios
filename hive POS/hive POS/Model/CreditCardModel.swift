//
//  CreditCardModel.swift
//  hive POS
//
//  Created by Space-C4ts on 16/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class CreditCardModel: Mappable {

  var cardID: String = ""
  var cardName: String = ""
  var cardToken: String = ""
  var cardPan: String = ""

  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    cardID <- map["cardId"]
    cardName <- map["cardNickname"]
    cardToken <- map["cardToken"]
    cardPan <- map["cardNo"]
  }
}
