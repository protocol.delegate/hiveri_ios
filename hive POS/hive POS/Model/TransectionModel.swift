//
//  TransectionModel.swift
//  hive POS
//
//  Created by Space-C4ts on 16/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class TransectionModel: Mappable {

  var appType: String = ""
  var appVersion: String = ""
  var cardID: String = ""
  var customerID: String = ""
  var status: String = ""

  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    appType <- map["appType"]
    cardID <- map["cardId"]
    status <- map["totalAmount invalid"]
  }
}
