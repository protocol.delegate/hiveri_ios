//
//  BranchModel.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class BranchModel: Mappable {
    var branchName: String = ""
    var branchNo: String = ""
    var branchDetail: String = ""
    var qrcode: String = ""
    var branchImageUrl: String = ""
    var branchImageID: String = ""
    var hasDelivery: Bool = false
    var placeLatitude: Double = -1
    var placeLongitude: Double = -1
    var email: String = ""
    var websiteUrl: String = ""
    var lineId: String = ""
    var facebookName: String = ""
    var instagramName: String = ""
    var preOrderMinute: String = ""
    var priceSince: Int = 0
    var priceTo: Int = 0
    var openDays: BranchOpenDayModel = BranchOpenDayModel()
    var brandID: Int = -1
    var telephones: [BranchTelephoneModel] = []
    var onlineOnly: Bool = false
    var foodCategorys: [BranchFoodCategoryModel] = []
    var holidayDays: [BranchHolidayDayModel] = []
    var holidayDates: [BranchHolidayDateModel] = []
    var ID: Int = -1
    var foodImgs: [BranchImageModel] = []
    var menuImgs: [BranchImageModel] = []
    var viewImgs: [BranchImageModel] = []
    
    var branchImageProfileID: String = ""
    var branchImageProfileUrl: String = ""
    var branchImageCoverID: String = ""
    var branchImageCoverUrl: String = ""
    
    var building: String = ""
    var roomNumber: String = ""
    var floor: String = ""
    var villageName: String = ""
    var placeNumber: String = ""
    var moo: String = ""
    var placeAlley: String = ""
    var placeRoad: String = ""
    var placeArea: Int = -1
    var placeDistrict: Int = -1
    var placeProvince: Int = -1
    var zipcode: String = ""
    var spoonWalkID: String = ""

    
    required convenience init?(map: Map) {
      self.init()
    }
    
    func mapping(map: Map) {
        branchName <- map["branchName"]
        branchNo <- map["branchNo"]
        branchDetail <- map["branchDetail"]
        qrcode <- map["qrcode"]
        branchImageUrl <- map["branchImageUrl"]
        branchImageID <- map["branchImageId"]
        hasDelivery <- map["hasDelivery"]
        placeLatitude <- map["placeLatitude"]
        placeLongitude <- map["placeLongitude"]
        email <- map["email"]
        websiteUrl <- map["websiteUrl"]
        lineId <- map["lineId"]
        facebookName <- map["facebookName"]
        instagramName <- map["instagramName"]
        preOrderMinute <- map["preOrderMinute"]
        priceSince <- map["priceSince"]
        priceTo <- map["priceTo"]
        openDays <- map["openDays"]
        brandID <- map["brandId"]
        telephones <- map["telephones"]
        foodImgs <- map["foodImgs"]
        menuImgs <- map["menuImgs"]
        viewImgs <- map["viewImgs"]
        branchImageProfileID <- map["branchImageProfileId"]
        branchImageProfileUrl <- map["branchImageProfileUrl"]
        branchImageCoverID <- map["branchImageCoverId"]
        branchImageCoverUrl <- map["branchImageCoverUrl"]
        onlineOnly <- map["onlineOnly"]
        foodCategorys <- map["foodCategorys"]
        holidayDays <- map["holidayDays"]
        holidayDates <- map["holidayDates"]
        ID <- map["id"]
        building <- map["building"]
        roomNumber <- map["roomNumber"]
        floor <- map["floor"]
        villageName <- map["villageName"]
        placeNumber <- map["placeNumber"]
        moo <- map["moo"]
        placeAlley <- map["placeAlley"]
        placeRoad <- map["placeRoad"]
        placeArea <- map["placeAreaId"]
        placeDistrict <- map["placeDistrictId"]
        placeProvince <- map["placeProvinceId"]
        zipcode <- map["zipcode"]
        spoonWalkID <- map["spoonwalkId"]
    }
}
