//
//  ListDistrictModel.swift
//  hive POS
//
//  Created by Space-C4ts on 18/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import ObjectMapper

class ListDistrictModel: Mappable {

  var list: [DistrictModel] = []
  
  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    list <- map["list"]
  }
}
