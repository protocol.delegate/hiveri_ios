//
//  ProvinceModel.swift
//  hive POS
//
//  Created by Space-C4ts on 18/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import ObjectMapper

class ProvinceModel: Mappable {

  var provinceID: Int = 0
  var nameEN: String = ""
  var nameTH: String = ""
  var status: String = ""

  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    provinceID <- map["id"]
    nameEN <- map["name_en"]
    nameTH <- map["name_th"]
    status <- map["status"]
  }
}

