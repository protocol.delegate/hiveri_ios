//
//  BrandTelephoneModel.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class BrandTelephoneModel: Mappable{
    
    var phoneNumber: String = ""
    var ID: Int = -1
    
    required convenience init?(map: Map) {
      self.init()
    }
    
    func mapping(map: Map) {
        phoneNumber <- map["phoneNumber"]
        ID <- map["id"]
    }
}
