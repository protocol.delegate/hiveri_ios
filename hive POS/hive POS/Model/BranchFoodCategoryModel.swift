//
//  BranchFoodCategoryModel.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class BranchFoodCategoryModel: NSObject, Mappable  {
    
    var categoryName: String = ""
    
    required convenience init?(map: Map) {
      self.init()
    }
    
    func mapping(map: Map) {
        categoryName <- map["categoryName"]
    }
}
