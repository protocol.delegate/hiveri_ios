//
//  FoodCategoryModel.swift
//  hive POS
//
//  Created by Space-C4ts on 6/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class FoodCategoryModel: NSObject {
    
    var id : String = ""
    var thaiName: String = ""
    var engName: String = ""
    var disclosureIndicator: Bool = false
    var forendKey: String? = nil
    var isSelect: Bool = false
    
}
