//
//  ShopModel.swift
//  hive POS
//
//  Created by Space-C4ts on 18/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import ObjectMapper

class ShopModel: Mappable {

  var shopID: String = ""
  var brandID: Int = -1
  var branchID: Int = -1
  
  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    self.shopID <- map["id"]
    self.brandID <- map["brand_id"]
    self.branchID <- map["branch_id"]
  }
}
