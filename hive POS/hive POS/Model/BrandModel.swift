//
//  BrandModel.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import ObjectMapper

class BrandModel: Mappable{
    
    var brandName: String = ""
    var email: String = ""
    var website: String = ""
    var lineName: String = ""
    var facebookUrl: String = ""
    var instagramName: String = ""
    var brandImageUrl: String = ""
    var brandImageID: String = ""
    var brandDetail: String = ""
    var brandTelephones: [BrandTelephoneModel] = []
    var ID: Int = -1
    var building: String = ""
    var roomNumber: String = ""
    var floor: String = ""
    var village: String = ""
    var placeNumber: String = ""
    var moo: String = ""
    var alley: String = ""
    var road: String = ""
    var area: Int = -1
    var district: Int = -1
    var province: Int = -1
    var zipcode: String = ""
    var placeLatitude: Double = -1
    var placeLongitude: Double = -1
    var contactPrefix: String = ""
    var contactFirstname: String = ""
    var contactLastname: String = ""
    var contactEmail: String = ""
    var contactTelephones: [BrandContactTelephonesModel] = []
    var isSelect: Bool = true
    
    required convenience init?(map: Map) {
      self.init()
    }
    
    func mapping(map: Map) {
        brandName <- map["brandName"]
        email <- map["email"]
        website <- map["website"]
        lineName <- map["lineName"]
        facebookUrl <- map["facebookUrl"]
        instagramName <- map["instagramName"]
        brandImageUrl <- map["brandImageUrl"]
        brandImageID <- map["brandImageId"]
        brandDetail <- map["brandDetail"]
        brandTelephones <- map["brandTelephones"]
        ID <- map["id"]
        building <- map["building"]
        roomNumber <- map["roomNumber"]
        floor <- map["floor"]
        village <- map["village"]
        placeNumber <- map["placeNumber"]
        moo <- map["moo"]
        alley <- map["alley"]
        road <- map["road"]
        area <- map["areaId"]
        district <- map["districtId"]
        province <- map["provinceId"]
        zipcode <- map["zipcode"]
        placeLatitude <- map["placeLatitude"]
        placeLongitude <- map["placeLongitude"]
        contactPrefix <- map["contactPrefix"]
        contactFirstname <- map["contactFirstname"]
        contactLastname <- map["contactLastname"]
        contactEmail <- map["contactEmail"]
        contactTelephones <- map["contactTelephones"]
    }
}
