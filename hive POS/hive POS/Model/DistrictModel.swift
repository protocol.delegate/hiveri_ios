//
//  DistrictModel.swift
//  hive POS
//
//  Created by Space-C4ts on 18/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class DistrictModel: Mappable {

  var districtID: Int = 0
  var provinceID: Int = 0
  var nameEN: String = ""
  var nameTH: String = ""
  var status: String = ""

  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    districtID <- map["id"]
    provinceID <- map["province_id"]
    nameEN <- map["name_en"]
    nameTH <- map["name_th"]
    status <- map["status"]
  }
}
