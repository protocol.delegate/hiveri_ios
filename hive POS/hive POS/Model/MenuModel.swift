//
//  MenuModel.swift
//  hive POS
//
//  Created by Space-C4ts on 19/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class MenuModel: Mappable {

  var menuID: String = ""
  var menuNO: String = ""
  var brandID: Int = -1
  var branchID: Int = -1
  var categoryID: String = ""
  var imagePath: String = ""
  var nameTH: String = ""
  var nameEN: String = ""
  var detailTH: String = ""
  var detailEN: String = ""
  var price: Double = 0
  var priceType: Int = -1
  var priceDetail: Int = -1
  var menuOption: [Int] = []
  var menuType: [Int] = []
  var timeToCook: Int = 0
  var isPublish: Bool = false
  
  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    self.menuID <- map["id"]
    self.menuNO <- map["menu_id"]
    self.brandID <- map["brand_id"]
    self.branchID <- map["branch_id"]
    self.categoryID <- map["category_id"]
    self.imagePath <- map["image"]
    self.nameTH <- map["name_th"]
    self.nameEN <- map["name_en"]
    self.detailTH <- map["desc_th"]
    self.detailEN <- map["desc_en"]
    self.price <- map ["price"]
    self.priceType <- map["price_method_type"]
    self.priceDetail <- map["price_method_detail"]
    self.menuOption <- map["option"]
    self.menuType <- map["type"]
    self.timeToCook <- map["time_to_cook"]
    self.isPublish <- map["isPublish"]
  }
}
