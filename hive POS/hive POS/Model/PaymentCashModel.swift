//
//  PaymentCashModel.swift
//  hive POS
//
//  Created by Space-C4ts on 16/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class PaymentCashModel: Mappable {

  var docPrefix: String = ""
  var docNo: String = ""

  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    docPrefix <- map["docPrefix"]
    docNo <- map["docNo"]
  }
}
