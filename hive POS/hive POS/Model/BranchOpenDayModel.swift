//
//  BranchOpenDayModel.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class BranchOpenDayModel: Mappable{
    
    var everyDay: [TimeOpenModel] = []
    var monday: [TimeOpenModel] = []
    var tuesday: [TimeOpenModel] = []
    var wednesday: [TimeOpenModel] = []
    var thursday: [TimeOpenModel] = []
    var friday: [TimeOpenModel] = []
    var saturday: [TimeOpenModel] = []
    var sunday: [TimeOpenModel] = []
    var specialHoliday: [TimeOpenModel] = []
    
    required convenience init?(map: Map) {
      self.init()
    }
    
    func mapping(map: Map) {
        everyDay <- map["everyDay"]
        monday <- map["monday"]
        tuesday <- map["tuesday"]
        wednesday <- map["wednesday"]
        thursday <- map["thursday"]
        friday <- map["friday"]
        saturday <- map["saturday"]
        sunday <- map["sunday"]
        specialHoliday <- map["specialHoliday"]
    }
}
