//
//  UserModel.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import ObjectMapper

class UserModel: Mappable {

  var firstname: String = ""
  var lastname: String = ""
  var profileImageUrl: String = ""
  var profileImageId: String = ""
  var countryCode: String = ""
  var prefix: String = ""
  var firebaseUid: String = ""
  var phoneNumber: String = ""

  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    firstname <- map["firstname"]
    lastname <- map["lastname"]
    profileImageUrl <- map["profileImageUrl"]
    profileImageId <- map["profileImageId"]
    countryCode <- map["countryCode"]
    prefix <- map["prefix"]
    firebaseUid <- map["firebaseUid"]
    phoneNumber <- map["phoneNumber"]
  }
}
