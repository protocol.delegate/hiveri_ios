//
//  TableModel.swift
//  hive POS
//
//  Created by Space-C4ts on 25/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class TableModel: Mappable {

  var tableID: String = ""
  var brandID: Int = -1
  var branchID: Int = -1
  var spoonWalkID: String = ""
  var isEnable: Bool = false
  var capacity: Int = 0
  var tableName: String = ""
  
  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    tableID <- map["id"]
    brandID <- map["brand_id"]
    branchID    <- map["branch_id"]
    spoonWalkID <- map["restaurant_id"]
    capacity  <- map["capacity"]
    isEnable  <- map["has_enable"]
    tableName <- map["table_name"]
  }
}
