//
//  TimeOpenModel.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class TimeOpenModel: NSObject, Mappable {
    
    var timeStart: String = ""
    var timeClose: String = ""
    
    required override init() {
    }
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        timeStart <- map["startTime"]
        timeClose <- map["endTime"]
    }
    
}
