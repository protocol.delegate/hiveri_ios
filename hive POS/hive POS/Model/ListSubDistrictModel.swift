//
//  ListSubDistrictModel.swift
//  hive POS
//
//  Created by Space-C4ts on 18/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import ObjectMapper

class ListSubDistrictModel: Mappable {

  var list: [SubDistrictModel] = []
  
  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    list <- map["list"]
  }
}
