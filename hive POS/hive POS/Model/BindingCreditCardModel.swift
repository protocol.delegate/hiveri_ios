//
//  BindingCreditCardModel.swift
//  hive POS
//
//  Created by Space-C4ts on 15/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class BindingCreditCardModel: Mappable {
  
  var deepLink: String = ""
  var reference: String = ""
  
  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    deepLink <- map["deepLink"]
    reference <- map["referenceNumber"]
  }
}
