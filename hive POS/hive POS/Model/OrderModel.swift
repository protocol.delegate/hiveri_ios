//
//  OrderModel.swift
//  hive POS
//
//  Created by Space-C4ts on 25/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class OrderModel: Mappable {
  
  var restaurantID: String = ""
  var type: String = ""
  var status: String = ""
  var menu: [OrderMenuModel] = []
  var paymentMethod: String = ""
  var timeToCook: String = ""
  
  required init?(map: Map) {
    
  }
  
  func mapping(map: Map) {
    restaurantID    <- map["restaurant_id"]
    type            <- map["type"]
    status          <- map["status"]
    menu           <- map["order"]
    paymentMethod   <- map["payment_method"]
    timeToCook      <- map["time_to_cook"]
  }
}

class OrderMenuModel: Mappable {
  var menuID: String = ""
  var categoryID: String = ""
  var menuImage: String = ""
  var menuNameTH: String = ""
  var menuNameEN: String = ""
  var menuDescTH: String = ""
  var menuDescEN: String = ""
  var price: Double = 0
  var amount: Int = 0
  
  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    menuID      <- map["id"]
    categoryID  <- map["category_id"]
    menuImage   <- map["image"]
    menuNameTH  <- map["name_th"]
    menuNameEN  <- map["name_en"]
    menuDescTH  <- map["desc_th"]
    menuDescEN  <- map["desc_en"]
    price       <- map["price"]
    amount      <- map["amount"]
  }
}
