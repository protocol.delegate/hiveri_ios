//
//  SubDistrictModdel.swift
//  hive POS
//
//  Created by Space-C4ts on 18/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class SubDistrictModel: Mappable {

  var districtID: Int = 0
  var subDistricID: Int = 0
  var nameEN: String = ""
  var nameTH: String = ""
  var status: String = ""

  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    subDistricID <- map["id"]
    districtID <- map["district_id"]
    nameEN <- map["name_en"]
    nameTH <- map["name_th"]
    status <- map["status"]
  }
}
