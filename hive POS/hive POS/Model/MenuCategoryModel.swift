//
//  MenuCategoryModel.swift
//  hive POS
//
//  Created by Space-C4ts on 19/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class MenuCategoryModel: Mappable {

  var categoryID: String = ""
  var categoryName: String = ""
  
  required init?(map: Map) {
    
  }

  func mapping(map: Map) {
    self.categoryID <- map["id"]
    self.categoryName <- map["name"]
  }
}
