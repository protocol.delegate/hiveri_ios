//
//  PopoverViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import UIKit

protocol uiViewDelegate {
    func getStrView(name: String)
}

class PopoverViewController: UIViewController {

    var getNameView: uiViewDelegate?
    let logout = "logout"
//    @IBOutlet weak var changeCompanyButton: UIButton!
//    @IBOutlet weak var settingUserButton: UIButton!
//    @IBOutlet weak var settingCompanyButton: UIButton!
//    @IBOutlet weak var helpButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
//    @IBOutlet weak var changeUserButton: UIButton!
//    @IBOutlet weak var howToUseButton: UIButton!
    @IBOutlet weak var settingButton: UIButton!
//    @IBOutlet weak var drawMoneyButton: UIButton!
//    @IBOutlet weak var clockinButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func settingUserButtonTapped(_ sender: Any) {
        showAlert(message: "ขออภัย ข้อมูลยังไม่ครบ กรุณาระบุข้อมูลเพิ่มเติมด้วยค่ะ")
        print("settingUserButtonTapped")
        
    }
    @IBAction func settingCompanyButtonTapped(_ sender: Any) {
        //showAlert(message: "ขออภัย ข้อมูลยังไม่ครบ กรุณาระบุข้อมูลเพิ่มเติมด้วยค่ะ")
        self.getNameView?.getStrView(name: "settingCompany")
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func clockinButtonTapped(_ sender: Any) {
        showAlert(message: "ขออภัย ข้อมูลยังไม่ครบ กรุณาระบุข้อมูลเพิ่มเติมด้วยค่ะ")
        print("clockinButtonTapped")
    }
    @IBAction func drawMoneyButtonTapped(_ sender: Any) {
        self.getNameView?.getStrView(name: "drawMoney")
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func openTurnOffTheShiftButtonTapped(_ sender: Any) {
        self.getNameView?.getStrView(name: "openTurnOffTheShift")
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func settingButtonTapped(_ sender: Any) {
        self.getNameView?.getStrView(name: "setting")
        self.dismiss(animated: true, completion: nil)
        print("settingButtonTapped")
    }
    @IBAction func howToUseButtonTapped(_ sender: Any) {
        showAlert(message: "ขออภัย ข้อมูลยังไม่ครบ กรุณาระบุข้อมูลเพิ่มเติมด้วยค่ะ")
        print("howToUseButtonTapped")
    }
    @IBAction func helpButtonTapped(_ sender: Any) {
        showAlert(message: "ขออภัย ข้อมูลยังไม่ครบ กรุณาระบุข้อมูลเพิ่มเติมด้วยค่ะ")
        print("helpButtonTapped")
    }
    @IBAction func changeCompanyButtonTapped(_ sender: Any) {
        showAlert(message: "ขออภัย ข้อมูลยังไม่ครบ กรุณาระบุข้อมูลเพิ่มเติมด้วยค่ะ")
        print("changeCompanyButtonTapped")
    }
    @IBAction func changeUserButtonTapped(_ sender: Any) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "logout") , object: self)
        self.getNameView?.getStrView(name: "logout")
        print("changeUserButtonTapped")
    }
    @IBAction func logoutButtonTapped(_ sender: Any) {
      NotificationCenter.default.post(name: .didClickOnSignout , object: self)
    }
}
