//
//  EmptyBrandTitleButtonNavigationBarCustom.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import UIKit

class EmptyBrandTitleButtonNavigationBarCustom: UIView {
  
  @IBOutlet weak var brandNameLabel: UILabel!
  @IBOutlet weak var branchNameLabel: UILabel!
  
  var brandName: String?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
    }
  
  func configure(brandName: String) {
      let view = UINib(nibName: EmptyBrandTitleButtonNavigationBarCustom.className, bundle: Bundle(for:type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.flexibleWidth.rawValue | UIView.AutoresizingMask.flexibleHeight.rawValue)
        addSubview(view)
        view.isUserInteractionEnabled = true
      
      if brandName == "" {
        branchNameLabel.text = "brandbranch_empty_branch".localized()
      } else {
        branchNameLabel.text = brandName
      }
      
  }
  
  func setupL10N() {
//    brandNameLabel.text = "brandbranch_empty_company".localized()
//    branchNameLabel.text = "brandbranch_empty_branch".localized()
  }
    
}
