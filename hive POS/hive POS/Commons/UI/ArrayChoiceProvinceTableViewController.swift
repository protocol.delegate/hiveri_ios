//
//  ArrayChoiceProvinceTableViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 18/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

class ArrayChoiceProvinceTableViewController<Element> : UITableViewController {
    
    typealias SelectionHandler = (ProvinceModel) -> Void
    
    private let provinceValues: [ProvinceModel]
    private let onSelect: SelectionHandler?
    
    init(_ provinceValues : [ProvinceModel], onSelect : SelectionHandler? = nil) {
        self.provinceValues = provinceValues
        self.onSelect = onSelect
        super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return provinceValues.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
      cell.textLabel?.font = UIFont(name: "Kanit-Regular", size: 15)
      cell.textLabel?.text = provinceValues[indexPath.row].nameTH
      return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      self.dismiss(animated: true)
      onSelect?(provinceValues[indexPath.row])
    }
}
