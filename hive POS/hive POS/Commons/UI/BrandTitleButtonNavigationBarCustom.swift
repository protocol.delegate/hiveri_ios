//
//  BrandTitleButtonNavigationBarCustom.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

@IBDesignable class BrandTitleButtonNavigationBarCustom: UIView {
    
    @IBOutlet weak var manageBrandButton: UIButton!
    @IBOutlet weak var brandImageView: UIImageView!
    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var branchNameLabel: UILabel!
    @IBOutlet weak var branchTitleLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        setup()
    }
    func setup() {
      let view = UINib(nibName: BrandTitleButtonNavigationBarCustom.className, bundle: Bundle(for:type(of: self))).instantiate(withOwner: self, options: nil)[0] as! UIView
        view.frame = bounds
        view.autoresizingMask = UIView.AutoresizingMask(rawValue: UIView.AutoresizingMask.flexibleWidth.rawValue | UIView.AutoresizingMask.flexibleHeight.rawValue)
        addSubview(view)
        view.isUserInteractionEnabled = true
      branchTitleLabel.text = "brandbranch_unit".localized() + " "
      brandImageView.layer.cornerRadius = brandImageView.frame.height / 2
    }
}

