//
//  ArrayChoiceOrderTableTableViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 25/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import UIKit

class ArrayChoiceOrderTableTableViewController<T> : UITableViewController {
    
    typealias SelectionHandler = (DisplayTableData) -> Void
    
    private let values: [DisplayTableData]
    private let onSelect: SelectionHandler?
    
    init(_ values : [DisplayTableData], onSelect : SelectionHandler? = nil) {
      self.values = values
      self.onSelect = onSelect
      super.init(style: .plain)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return values.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = UITableViewCell(style: .default, reuseIdentifier: nil)
      cell.textLabel?.font = UIFont(name: "Kanit-Regular", size: 15)
      cell.textLabel?.text = values[indexPath.row].tableName
      return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      self.dismiss(animated: true)
      onSelect?(values[indexPath.row])
    }
}
