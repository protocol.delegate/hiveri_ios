//
//  AlwaysPresentAsPopover.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import UIKit

/**
 By default, when you use:
 
 ```
 controller.modalPresentationStyle = .popover
 ```
 
 in a horizontally compact environment (iPhone in portrait mode), this option behaves the same as fullScreen.
 You can make it to always show a popover by using:
 
 ```
 let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
 ```
 */
class AlwaysPresentAsPopover : NSObject, UIPopoverPresentationControllerDelegate {
  
  // `sharedInstance` because the delegate property is weak - the delegate instance needs to be retained.
  private static let sharedInstance = AlwaysPresentAsPopover()
  
  private override init() {
    super.init()
  }
  
  func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
    return .none
  }
  
  static func configurePresentation(forController controller : UIViewController) -> UIPopoverPresentationController {
    controller.modalPresentationStyle = .popover
    let presentationController = controller.presentationController as! UIPopoverPresentationController
    presentationController.delegate = AlwaysPresentAsPopover.sharedInstance
    presentationController.backgroundColor = .white
    return presentationController
  }
  
}
