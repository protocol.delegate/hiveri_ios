//
//  BranchCollectionViewFlowLayout.swift
//  hive POS
//
//  Created by Space-C4ts on 14/6/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import UIKit

class BranchCollectionViewFlowLayout: UICollectionViewFlowLayout {
  
  let spacingOffset: CGFloat    = 0
  let numberOfColumns: CGFloat  = 4
  
  override init() {
    super.init()
    setupLayout()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setupLayout()
  }
  
  override var itemSize: CGSize {
    set {}
    get {
      let itemWidth = (self.collectionView!.frame.width - (spacingOffset*(numberOfColumns - 1)) - (spacingOffset*2)) / numberOfColumns
      return CGSize(width: itemWidth, height: itemWidth*1.3)
    }
  }
  
  func setupLayout() {
    minimumInteritemSpacing = spacingOffset
    minimumLineSpacing      = spacingOffset
    sectionInset            = UIEdgeInsets(top:     16,
                                           left:    spacingOffset,
                                           bottom:  0,
                                           right:   spacingOffset)
    scrollDirection         = .vertical
  }
}

