//
//  LoadingActivityView.swift
//  hive POS
//
//  Created by Space-C4ts on 21/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import SnapKit

class LoadingActivityView: UIView {
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    
    let bgView = UIView(frame: .zero)
    bgView.backgroundColor = .black
    bgView.alpha = 0
    self.addSubview(bgView)
    bgView.snp.makeConstraints { (maker) in
      maker.edges.equalToSuperview()
    }
    
    let blurEffect = UIBlurEffect(style: .prominent)
    let blurredEffectView = UIVisualEffectView(effect: blurEffect)
    self.addSubview(blurredEffectView)
    blurredEffectView.snp.makeConstraints { (maker) in
      maker.center.equalToSuperview()
      maker.width.height.equalTo(80)
    }
  
    let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    activityIndicator.color = UIColor(hex: 0x3B5A87)
    activityIndicator.startAnimating()
    blurredEffectView.contentView.addSubview(activityIndicator)
    activityIndicator.snp.makeConstraints { (maker) in
      maker.center.equalToSuperview()
    }
    
    blurredEffectView.layer.cornerRadius = 5
    blurredEffectView.clipsToBounds = true
  }
  
  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension LoadingActivityView {
  
  static func action(in view: UIView, isShow: Bool) {
    if isShow {
      show(in: view)
    } else {
      hide(in: view)
    }
  }
  
  static func show(in view: UIView) {
    guard view.viewWithTag(9999) == nil else { return }
    let indicatorView = LoadingActivityView(frame: view.frame)
    indicatorView.tag = 9999
    view.addSubview(indicatorView)
  }
  
  static func hide(in view: UIView) {
    if let indicator = view.viewWithTag(9999) as? LoadingActivityView {
      indicator.removeFromSuperview()
    }
  }
}

