//
//  StoryboardInitializable.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

protocol StoryboardInitializable {
  static var storyboardIdentifier: String { get }
}

extension StoryboardInitializable where Self: UIViewController {
  
  static var storyboardIdentifier: String {
    return String(describing: Self.self)
  }
  
  static func initFromStoryboard(name: String = "Authentication") -> Self {
    let storyboard = UIStoryboard(name: name, bundle: Bundle.main)
    return storyboard.instantiateViewController(withIdentifier: storyboardIdentifier) as! Self
  }
}
