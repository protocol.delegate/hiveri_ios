//
//  NSObject+ClassName.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

extension NSObject {
  var className: String {
    return String(describing: type(of: self))
  }
  
  class var className: String {
    return String(describing: self)
  }
  
  class var classNameWithNav: String {
    return "\(className)Nav"
  }
}
