//
//  Notification+.swift
//  hive POS
//
//  Created by Space-C4ts on 15/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

extension Notification.Name {
  static let didClickOnSignout = Notification.Name("didClickOnSignout")
}
