//
//  Helper+.swift
//  hive POS
//
//  Created by Space-C4ts on 1/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ZSwiftKit
import Kingfisher

extension Helper {
  static func openURLInBrowser(urlPath: String) {
    //let urlPath = stringWithHttpPrefix(urlPath: urlPath)
    guard let encodedUrlString = urlPath.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
      else { return }
    if let url = URL(string: encodedUrlString) {
      //UIApplication.shared.openURL(url)
      
      if #available(iOS 10.0, *) {
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
      } else {
        UIApplication.shared.openURL(url)
      }
    }
  }
  
  static func setImageURL(for imageView: UIImageView, urlString: String, placeholder: UIImage? = nil, options: KingfisherOptionsInfo? = nil) {
    setImageURL(for: imageView, urlString: urlString, placeholder: placeholder, options: options, completion: nil)
  }
  
  static func setImageURL(for imageView: UIImageView, urlString: String, placeholder: UIImage? = nil, options: KingfisherOptionsInfo? = nil, completion: ((Result<RetrieveImageResult, KingfisherError>) -> Void)?) {
    guard let encodedUrlString = urlString.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
      else { return }
    
    if let url = URL(string: encodedUrlString) {
      imageView.kf.indicatorType = .activity
      imageView.kf.setImage(with: url,
                            placeholder: placeholder,
                            options: options ?? [.transition(.fade(0.2)), .keepCurrentImageWhileLoading, .onlyLoadFirstFrame],
                            completionHandler: completion)
    }
  }
}
