//
//  String+.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

extension String {
    
    func localized(bundle: Bundle = .main, tableName: String = "Localizable") -> String {
        
        let langCode = getLanguageSelectedKey()
        let path = bundle.path(forResource: langCode, ofType: "lproj")
        
        let languageBundle = Bundle(path: path!)
        let str = languageBundle?.localizedString(forKey: self, value: "**\(self)**", table: nil)
        
        return str!
    }
    
    public func getLanguageSelectedKey() -> String {
        let userDef = UserDefaults.standard
        let langCode = userDef.string(forKey: "DefaultLanguage")
        return langCode == nil ? "en" : langCode!
    }
    
}
