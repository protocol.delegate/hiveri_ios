//
//  UIImage+.swift
//  hive POS
//
//  Created by Space-C4ts on 29/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

extension UIImage {

    func resize(newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / self.size.width
        let newHeight = self.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        self.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
}
