//
//  UIColor+.swift
//  hive POS
//
//  Created by Space-C4ts on 21/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(hex: Int, alpha: CGFloat = 1) {
        self.init(red: CGFloat((hex >> 16) & 0xff) / CGFloat(255),
                  green: CGFloat((hex >> 8) & 0xff) / CGFloat(255),
                  blue: CGFloat((hex) & 0xff) / CGFloat(255),
                  alpha: alpha)
    }
}
