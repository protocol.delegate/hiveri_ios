//
//  UIViewController+.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import Kingfisher
import Firebase

extension UIViewController {
  
  //MARK: - CustomNavigation

  func setNavigationBarItem(brandName: String = "",brandImageUrl: String = "", branchName: String = "", titlePageName: String, isBackButton: Bool = false) {
    let brand = LocalDataService.getSelectBrand()
    let branch = BranchSignService.shared.getBranchData()
    setupRemainingItem()
    setupRightNavItem(brandName: brand?.brandName ?? "",brandImageUrl: brand?.brandImageUrl ?? "" , branchName: branch?.branchName ?? "")
    if isBackButton {
      setLeftNavWithBackButton(title: titlePageName)
    } else {
      setupLeftNavItem(title: titlePageName)
    }
  }
  
  
  func setupRemainingItem() {
          let label = UIImageView(frame: CGRect(x: 0, y: 0, width: 32, height: 32))
          navigationItem.titleView = label
          navigationController?.navigationBar.backgroundColor = .white
          navigationController?.navigationBar.isTranslucent = false
      }
  
  func setupRightNavItem(brandName: String,brandImageUrl: String, branchName: String) {
    let notificationButtonViewRect:CGRect = CGRect.init(origin: CGPoint.init(x: 0, y: 0), size: CGSize.init(width: 34, height: 34))
    let notificationButtonView:UIView = UIView.init(frame: notificationButtonViewRect)
    let notificationButton = UIButton(type: .system)
    notificationButton.setImage(UIImage(named: "ic_notification")?.withRenderingMode(.alwaysOriginal), for: .normal)
    notificationButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
    notificationButton.contentMode = .scaleAspectFit
    notificationButton.addTarget(self, action: #selector(notificationButtonItemClicked), for: .touchUpInside)
    notificationButtonView.addSubview(notificationButton)
    let userButtonView:UIView = UIView()
    userButtonView.frame = CGRect(x: 0, y: 0, width: 100, height: 34)
  //                userButtonView.backgroundColor = .green
    let userButton = UIButton(type: .system)
    userButton.setImage(UIImage(named: "ic_user_navigation")?.withRenderingMode(.alwaysOriginal), for: .normal)
    userButton.frame = CGRect(x: 0, y: 0, width: 100, height: 34)
    userButton.titleEdgeInsets.right = 10
    if LocalDataService.getUser() != nil {
      let name = LocalDataService.getUser()
      if name != nil {
        userButton.setTitle("\(name!.firstname)", for: .normal)
      } else {
        userButton.setTitle("", for: .normal)
          do { try Auth.auth().signOut() } catch {}
              let alert = UIAlertController(title: "แจ้งเตือน", message: "กรุณาเข้าสู่ระบบเพื่อดำเนินการต่อ", preferredStyle: .alert)
              alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                //TODO
//              self.performSegue(withIdentifier: "gotoLoginStoryboard", sender: nil)
              }))
            self.present(alert, animated: true, completion: nil)
            }
          } else {
              userButton.setTitle("", for: .normal)
              do { try Auth.auth().signOut() } catch {}
              let alert = UIAlertController(title: "แจ้งเตือน", message: "กรุณาเข้าสู่ระบบเพื่อดำเนินการต่อ", preferredStyle: .alert)
              alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                //TODO
//                  self.performSegue(withIdentifier: "gotoLoginStoryboard", sender: nil)
              }))
              self.present(alert, animated: true, completion: nil)
          }
          userButton.setTitleColor(UIColor(hex: 0x454F63), for: .normal)
          userButton.titleLabel?.font = UIFont(name: "Kanit", size: 12)
          userButton.semanticContentAttribute = .forceRightToLeft
          userButton.contentMode = .scaleAspectFit
          //TODO
          userButton.addTarget(self, action: #selector(optionAppButton), for: .touchUpInside)
          userButtonView.addSubview(userButton)

          if (brandName != "") {
              let branchButton = BrandTitleButtonNavigationBarCustom(frame: CGRect(x: 0, y: 0, width: 150, height: 34))
              if (brandImageUrl != "") {
                  branchButton.frame.size = CGSize(width: 150, height: 34)
                  if (branchName != "") {
                      branchButton.brandNameLabel.text = brandName
                      branchButton.branchNameLabel.text = branchName
                  } else {
                      branchButton.brandNameLabel.text = brandName
                      branchButton.branchNameLabel.text = "ทั้งหมด"
                  }
                  let url = URL(string: brandImageUrl)
                  branchButton.brandImageView.kf.setImage(with: url)
                  branchButton.manageBrandButton.addTarget(self, action: #selector(brachButtonTabpped), for: .touchUpInside)
              } else {
                  branchButton.frame.size = CGSize(width: 150, height: 34)
                  if (branchName.count) >= 1 {
                      branchButton.brandNameLabel.text = brandName
                      branchButton.branchNameLabel.text = branchName
                  } else {
                      branchButton.brandNameLabel.text = brandName
                      branchButton.branchNameLabel.text = "ทั้งหมด"
                  }
                  branchButton.brandImageView.image = UIImage(named: "ic_default_brandbranch")
                  branchButton.manageBrandButton.addTarget(self, action: #selector(brachButtonTabpped), for: .touchUpInside)
              }
              navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: notificationButtonView), UIBarButtonItem(customView: userButtonView), UIBarButtonItem(customView: branchButton)]
          } else {
              let branchButton = EmptyBrandTitleButtonNavigationBarCustom(frame: CGRect(x: 0, y: 0, width: 150, height: 34))
              branchButton.configure(brandName: brandName)
              navigationItem.rightBarButtonItems = [UIBarButtonItem(customView: notificationButtonView), UIBarButtonItem(customView: userButtonView), UIBarButtonItem(customView: branchButton)]
          }
      }
  
  func setLeftNavWithBackButton(title: String) {
    let titlelabel = UILabel()
    titlelabel.text = title
    titlelabel.textColor = UIColor(hex: 0x454F63)
    titlelabel.font = UIFont(name:"Kanit-SemiBold", size: 21)
    let titleLabelUIView = UIBarButtonItem.init(customView: titlelabel)
    let backImage = UIImage(named: "ic_authen_back")
    let backButton   = UIBarButtonItem(image: backImage,  style: .plain, target: self, action: nil)
    backButton.width = 60
    backButton.tag = 33
    backButton.tintColor = UIColor(hex: 0x454F63)
    navigationItem.leftBarButtonItems = [backButton, titleLabelUIView]
  }
  
      func setupLeftNavItem(title: String!) {
          let logoImage = UIImage.init(named: "ic_hive_navigation_title")
          let logoImageView = UIImageView.init(image: logoImage)
          logoImageView.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
          logoImageView.contentMode = .scaleAspectFit
          let imageItem = UIBarButtonItem.init(customView: logoImageView)
          let negativeSpacer = UIBarButtonItem.init(barButtonSystemItem: .fixedSpace, target: nil, action: nil)
          negativeSpacer.width = -25
          
          let separatorLine = UIView()
          separatorLine.frame = CGRect(x: 0, y: 0, width: 2, height: 30)
          separatorLine.backgroundColor = UIColor(hex: 0xBFBFBF)
          let separatorLineUIView = UIBarButtonItem.init(customView: separatorLine)
          
          let titlelabel = UILabel()
          titlelabel.text = title!
          titlelabel.textColor = UIColor(hex: 0x454F63)
          titlelabel.font = UIFont(name:"Kanit-SemiBold", size: 21)
          let titleLabelUIView = UIBarButtonItem.init(customView: titlelabel)
          
          navigationItem.leftBarButtonItems = [imageItem, separatorLineUIView, titleLabelUIView]
          
          //        let manageBrandButton = UIButton(type: .system)
          //        manageBrandButton.setImage(UIImage(named: "bin_icon.png")?.withRenderingMode(.alwaysOriginal), for: .normal)
          //        manageBrandButton.frame = CGRect(x: 0, y: 0, width: 34, height: 34)
          //        manageBrandButton.contentMode = .scaleAspectFit
          //        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: manageBrandButton)
          
      }
  
  //MARK: - @objc func
      
      @objc func notificationButtonItemClicked(_ sender: Any)
      {
//          let titleNameData = ["12/08/2018 - 12:39\n\n",
//                               "12/08/2018 - 12:39\nจองโต๊ะ A008\nมีการจองโต๊ะ A008 ลูกค้าจำนวน 4 ท่าน",
//                               "12/08/2018 - 12:39\nจองโต๊ะ A010\nมีการจองโต๊ะ A010 ลูกค้าจำนวน 6 ท่าน",
//                               "12/08/2018 - 12:39\nจองโต๊ะ A010\nมีการจองโต๊ะ A010 ลูกค้าจำนวน 6 ท่าน",
//                               "12/08/2018 - 12:39\nจองโต๊ะ A010\nมีการจองโต๊ะ A010 ลูกค้าจำนวน 6 ท่าน",
//                               "12/08/2018 - 12:39\nจองโต๊ะ A010\nมีการจองโต๊ะ A010 ลูกค้าจำนวน 6 ท่าน",
//                               "12/08/2018 - 12:39\nจองโต๊ะ A010\nมีการจองโต๊ะ A010 ลูกค้าจำนวน 6 ท่าน"]
        let titleNameData:[String] = []
        let viewsDict = [
                      "date" : "12/08/2018 - 12:39",
                      "title" : "วัตถุดิบ เนื้อหมูสด ใกล้หมด",
                      "detail" : "วัตถุดิบในกองคลัง เนื้อหมู ใกล้หมด",
                      ]
          self.callPopover(titleNameData, UIButton(type: .custom), sender)
          print("notification button clicked")
      }
      
      @objc func optionAppButton(_ sender: Any) {
         
  //        self.uiShowPopView(UIButton(type: .custom))
          self.uiCallPopup(UIButton(type: .custom), sender)
      }
      
      @objc func logoutButtonTabpped(sender: UIButton!) {
          do { try Auth.auth().signOut() } catch {}
          print("log Out")
          self.performSegue(withIdentifier: "gotoLoginStoryboard", sender: nil)
      }
      @objc func brachButtonTabpped(sender: UIButton!) {
          print("brachButtonTabpped")
      }
      @objc func hideKeyboard() {
          view.endEditing(true)
      }
  
  // MARK: - ui
      
      private func callPopover(_ data: Array<Any>, _ button: UIButton, _ sender: Any) {
          let controller = ArrayChoiceTableViewController(data) { (name) in
              let color = UIColor(red:0, green:0, blue:0, alpha: 1)
              button.setTitleColor(color, for: .normal)
              button.setTitle("\(data)", for: .normal)
              print("\(data)")
          }
          controller.preferredContentSize = CGSize(width: 380, height: 580)
          showPopup(controller, sourceView: sender as! UIView)
      }
      private func showPopup(_ controller: UIViewController, sourceView: UIView) {
          let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
          presentationController.sourceView = sourceView
          presentationController.sourceRect = sourceView.bounds
          presentationController.permittedArrowDirections = [.down, .up]
          self.present(controller, animated: true)
      }
      
      private func uiCallPopup(_ button: UIButton, _ sender: Any ) {
        let popView = PopoverViewController(nibName: PopoverViewController.className, bundle: nil)
          popView.getNameView = self
          popView.preferredContentSize = CGSize(width: 220, height: 60)
          uiShowPopup(popView, sourceView: sender as! UIView)
          
      }
      
      private func uiShowPopup(_ controller: UIViewController, sourceView: UIView) {
          let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
          presentationController.sourceView = sourceView
          presentationController.sourceRect = sourceView.bounds
          presentationController.permittedArrowDirections = [.down, .up]
          self.present(controller, animated: true)
      }
      
//      func toGoSettingCompanyVC(id: Int) {
//          if let vc = UIStoryboard(name: "Home", bundle: nil).instantiateViewController(withIdentifier: "CompanyVCId") as? CompanyViewController {
//              vc.brandId = id
//              if let navigator = navigationController {
//                  navigator.pushViewController(vc, animated: true)
//              }
//          }
//      }
//
//      func toGoWithDrawMoneyVC() {
//          let vc = UIStoryboard(name: "PopupWithdrawMoney", bundle: nil).instantiateViewController(withIdentifier: "PopupWithdrawMoneyViewController") as! PopupWithdrawMoneyViewController
//          self.addChild(vc)
//          self.view.addSubview(vc.view)
//          vc.didMove(toParent: self)
//      }
//
//      func toGoOpenTurnOffTheShiftVC() {
//          let vc = UIStoryboard(name: "PopupOpenTurnOffTheShift", bundle: nil).instantiateViewController(withIdentifier: "OpenTurnOffTheShiftViewController") as!  OpenTurnOffTheShiftViewController
//          self.addChild(vc)
//          self.view.addSubview(vc.view)
//          vc.didMove(toParent: self)
//      }
      
      func toGoSettingVC() {
         self.tabBarController?.selectedIndex = 7
      }
      
      func logout() {
  //        do { try Auth.auth().signOut() } catch {}
  //        print("log Out")
          self.performSegue(withIdentifier: "gotoLoginStoryboard", sender: nil)
          
      }
      
      func getStrView(name: String) {
          if name == "settingCompany" {
              if let brand = LocalDataService.getBrands() {
//                  toGoSettingCompanyVC(id: brand[0].id!)
              } else {
                  showAlert(message: "กรุณาCreate Brand ")
              }
          } else if name == "setting"{
              toGoSettingVC()
          } else if name == "drawMoney"{
//              toGoWithDrawMoneyVC()
          } else if name == "openTurnOffTheShift"{
//              toGoOpenTurnOffTheShiftVC()
          } else {
              logout()
          }
      }
      
      func showSolfwareLicense(onView : UIView) {
          let screenSize = UIScreen.main.bounds
          let screenHeight = screenSize.height
          let screenWidth = screenSize.width
          let softWareLabel = UILabel(frame: CGRect(x: 16, y: screenHeight - 40, width: screenWidth / 2 - 16, height: 30))
          softWareLabel.textAlignment = .left
          softWareLabel.textColor = .white
          softWareLabel.text = "© 2018 Hive up Company All rights reserved."
          softWareLabel.font = UIFont(name: "Kanit-Regular", size: 12)
          self.view.addSubview(softWareLabel)
      }
      
      func showSoftWareVersion(onView : UIView) {
          let appVersion = Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String
          let screenSize = UIScreen.main.bounds
          let screenHeight = screenSize.height
          let screenWidth = screenSize.width
          let softWareLabel = UILabel(frame: CGRect(x: screenWidth / 2 - 16, y: screenHeight - 40, width: screenWidth / 2 - 16, height: 30))
          softWareLabel.textAlignment = .right
          softWareLabel.textColor = .white
          softWareLabel.text = "V " + appVersion!
          softWareLabel.font = UIFont(name: "Kanit-Regular", size: 12)
          self.view.addSubview(softWareLabel)
      }
}

@IBDesignable
extension UIViewController {
    
    func showAlert(message: String) {
        let alert = UIAlertController(title: "แจ้งเตือน", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ตกลง", style: UIAlertAction.Style.default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}
extension UICollectionViewCell {
    func showAlertCollectionViewCell(message: String) {
        let alert = UIAlertController(title: "แจ้งเตือน", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ตกลง", style: UIAlertAction.Style.default, handler: nil))
        self.window?.rootViewController?.present(alert, animated: true, completion: nil)
    }
}

extension UIViewController: uiViewDelegate {
  
}
