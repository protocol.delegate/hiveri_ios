//
//  LocalizationManager.swift
//  hive POS
//
//  Created by Space-C4ts on 21/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

enum LanguageType: String {
  case th = "th"
  case en = "en"
}

class LocalizationManager {
  static let share = LocalizationManager()
  
  func setDefautLanguage(language: LanguageType) {
    let userDefaults = UserDefaults.standard
    userDefaults.set(language.rawValue, forKey: "DefaultLanguage")
    userDefaults.synchronize()
  }
  
  func getDefaultLanguage() -> LanguageType {
    let userDefaults = UserDefaults.standard
    guard let language = userDefaults.string(forKey: "DefaultLanguage") else { return LanguageType.en }
    guard let languageType = LanguageType(rawValue: language) else { return LanguageType.en }
    return languageType
  }
}
