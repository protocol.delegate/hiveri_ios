//
//  FoodCategoryManagement.swift
//  hive POS
//
//  Created by Space-C4ts on 6/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import UIKit

class FoodCategoryManagement: NSObject {
    
    static func getFoodCategoryAndSpecial() -> [FoodCategoryModel] {
        var result: [FoodCategoryModel] = [FoodCategoryModel]()
        
        var nsArray: NSArray?
        if let path = Bundle.main.path(forResource: "FoodCategoryAndSpecial", ofType: "plist") {
            nsArray = NSArray(contentsOfFile: path)
            if let foodCategorys = nsArray! as? Array<NSDictionary>{
                for foodCategory in foodCategorys {
                    let newItem: FoodCategoryModel = FoodCategoryModel()
                  newItem.id = foodCategory.object(forKey: "id") as? String ?? ""
                  newItem.thaiName = foodCategory.object(forKey: "thaiName") as? String ?? ""
                  newItem.engName = foodCategory.object(forKey: "engName") as? String ?? ""
                  newItem.isSelect = foodCategory.object(forKey: "isSelect") as? Bool ?? false
                  result.append(newItem)
                }
            }
        }
        
        return result
    }
    
    static func getSubFoodCategoryAndSpecial() -> [FoodCategoryModel] {
        var result: [FoodCategoryModel] = [FoodCategoryModel]()
        
        var nsArray: NSArray?
        if let path = Bundle.main.path(forResource: "SubFoodCategoryAndSpecial", ofType: "plist") {
            nsArray = NSArray(contentsOfFile: path)
            if let subFoodCategorys = nsArray! as? Array<NSDictionary>{
                for subFoodCategory in subFoodCategorys {
                    let newItem: FoodCategoryModel = FoodCategoryModel()
                  newItem.id = subFoodCategory.object(forKey: "id") as? String ?? ""
                  newItem.thaiName = subFoodCategory.object(forKey: "thaiName") as? String ?? ""
                  newItem.engName = subFoodCategory.object(forKey: "engName") as? String ?? ""
                  newItem.disclosureIndicator = subFoodCategory.object(forKey: "disclosureIndicator") as? Bool ?? false
                  newItem.forendKey = subFoodCategory.object(forKey: "fkId") as? String
                  newItem.isSelect = subFoodCategory.object(forKey: "isSelect") as? Bool ?? false
                  result.append(newItem)
                }
            }
        }
        
        return result
    }
    
    static func getFoodTypeAndNationality() -> [FoodCategoryModel] {
        var result: [FoodCategoryModel] = [FoodCategoryModel]()
        
        var nsArray: NSArray?
        if let path = Bundle.main.path(forResource: "FoodTypeAndNationality", ofType: "plist") {
            nsArray = NSArray(contentsOfFile: path)
            if let subFoodCategorys = nsArray! as? Array<NSDictionary>{
                for subFoodCategory in subFoodCategorys {
                    let newItem: FoodCategoryModel = FoodCategoryModel()
                  newItem.id = subFoodCategory.object(forKey: "id") as? String ?? ""
                  newItem.thaiName = subFoodCategory.object(forKey: "thaiName") as? String ?? ""
                  newItem.engName = subFoodCategory.object(forKey: "engName") as? String ?? ""
                  newItem.disclosureIndicator = subFoodCategory.object(forKey: "disclosureIndicator") as? Bool ?? false
                    newItem.forendKey = subFoodCategory.object(forKey: "fkId") as? String
                  newItem.isSelect = subFoodCategory.object(forKey: "isSelect") as? Bool ?? false
                    result.append(newItem)
                }
            }
        }
        
        return result
    }
    
    static func getSubFoodNationality() -> [FoodCategoryModel] {
        var result: [FoodCategoryModel] = [FoodCategoryModel]()
        
        var nsArray: NSArray?
        if let path = Bundle.main.path(forResource: "SubFoodNationality", ofType: "plist") {
            nsArray = NSArray(contentsOfFile: path)
            if let subFoodNationalitys = nsArray! as? Array<NSDictionary>{
                for subFoodNationality in subFoodNationalitys {
                    let newItem: FoodCategoryModel = FoodCategoryModel()
                  newItem.id = subFoodNationality.object(forKey: "id") as? String ?? ""
                  newItem.thaiName = subFoodNationality.object(forKey: "thaiName") as? String ?? ""
                  newItem.engName = subFoodNationality.object(forKey: "engName") as? String ?? ""
                  newItem.disclosureIndicator = subFoodNationality.object(forKey: "disclosureIndicator") as? Bool ?? false
                    newItem.forendKey = subFoodNationality.object(forKey: "fkId") as? String
                  newItem.isSelect = subFoodNationality.object(forKey: "isSelect") as? Bool ?? false
                    result.append(newItem)
                }
            }
        }
        
        return result
    }
}
