//
//  WebSocketService.swift
//  hive POS
//
//  Created by Space-C4ts on 14/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import RxWebSocket
import RxSwift
import RxCocoa

class WebSocketService {
  
  private init() { }
  static let shared = WebSocketService()
  private let socket = RxWebSocket(url: URL(string: "wss://msg.spoonwalk.com/message")!)
  private let disposeBag = DisposeBag()
  private var observDataTrigger = PublishSubject<Void>()
  
  var isDisconnect = false
  
  func connect(){
    
    socket.rx.connect
      .subscribe(onNext: { [unowned self] _ in
        print("Socket was connect")
        self.isDisconnect = false
        self.observDataTrigger.onNext(())})
      .disposed(by: disposeBag)
    
    socket.rx.disconnect
      .subscribe(onNext: { error in
        self.socket.connect()
        print("Socket was disconnect ", error?.localizedDescription ?? "")
      })
      .disposed(by: disposeBag)
    
    socket.rx.data
      .subscribe(onNext: { data in
        print("Socket data : ", data)})
      .disposed(by: disposeBag)
    
    socket.rx.text
      .subscribe(onNext: { text in
        print("Socket text", text)})
      .disposed(by: disposeBag)
    
    socket.rx.stream
      .subscribe(onNext: { event in
        print("Socket event-----> ", event)
      })
      .disposed(by: disposeBag)
  }
  
  func write(text: String) {
    if isDisconnect {
      self.connectWithWrite(text: text)
    } else {
      socket.rx.text.onNext(text)
    }
  }
  
  func disConnect() {
    socket.rx.disconnect
    .subscribe(onNext: { error in
      self.isDisconnect = true
      print("Web socket Error ", error?.localizedDescription ?? "")
    })
    .disposed(by: disposeBag)
  }
  
  func connectWithWrite(text: String) {
    socket.connect()
    socket.rx.connect
    .subscribe(onNext: { [unowned self] _ in
      print("Socket was connect")
      self.isDisconnect = false
      self.socket.rx.text.onNext(text)
    })
    .disposed(by: disposeBag)
  }
  
  func updateData(data: Data) {
    Observable.just(data)
      .bind(to: socket.rx.data)
      .disposed(by: disposeBag)
  }
  
  func writeWithoutReactiveX(text: String) {
    socket.write(string: text)
  }
  
}
