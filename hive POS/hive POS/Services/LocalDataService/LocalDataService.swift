//
//  LocalDataService.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import ObjectMapper

class LocalDataService {
  
  static func setUserToken(token: String) {
    let userDefault = UserDefaults.standard
    userDefault.set(token, forKey: "userToken")
    userDefault.synchronize()
  }
  
  static func getUserToken() -> String? {
    let userDefault = UserDefaults.standard
    let token = userDefault.string(forKey: "userToken")
    return token
  }
  
  static func deleteUserToken() {
    let userDefault = UserDefaults.standard
    userDefault.removeObject(forKey: "userToken")
    userDefault.synchronize()
  }
  
  static func setRememberMe() {
    let userDefault = UserDefaults.standard
    userDefault.set(true, forKey: "rememberMe")
    userDefault.synchronize()
  }
  
  static func getRememberMe() -> Bool {
    let userDefault = UserDefaults.standard
    let isRemember = userDefault.bool(forKey: "rememberMe")
    return isRemember
  }
  
  static func deleteRememberMe() {
    let userDefault = UserDefaults.standard
    userDefault.removeObject(forKey: "rememberMe")
    userDefault.synchronize()
  }
  
  static func setUser(_ userModel: UserModel) {
    let userDefault = UserDefaults.standard
    let jsonString = userModel.toJSONString()
    userDefault.set(jsonString, forKey: "userModel")
    userDefault.synchronize()
   }
   
   static func getUser() -> UserModel? {
      let userDefault = UserDefaults.standard
      guard let jsonString = userDefault.string(forKey: "userModel") else { return nil }
      let mappableModel = Mapper<UserModel>().map(JSONString: jsonString)
      return mappableModel
   }
  
  static func deleteUser() {
    let userDefault = UserDefaults.standard
    userDefault.removeObject(forKey: "userModel")
    userDefault.synchronize()
  }
  
  static func setBrands(_ brandModel: [BrandModel]) {
      let userDefault = UserDefaults.standard
      let jsonString = brandModel.toJSONString()
      userDefault.set(jsonString, forKey: "brandModels")
      userDefault.synchronize()
  }
  
  static func getBrands() -> [BrandModel]? {
    let userDefault = UserDefaults.standard
    guard let jsonString = userDefault.string(forKey: "brandModels") else { return nil }
    let mappableModel = Mapper<BrandModel>().mapArray(JSONString: jsonString)
    return mappableModel
  }
  
  static func setSelectBrand(_ brandModel: BrandModel) {
      let userDefault = UserDefaults.standard
      let jsonString = brandModel.toJSONString()
      userDefault.set(jsonString, forKey: "brandModel")
      userDefault.synchronize()
  }
  
  static func getSelectBrand() -> BrandModel? {
      let userDefault = UserDefaults.standard
      guard let jsonString = userDefault.string(forKey: "brandModel") else { return nil }
      let mappableModel = Mapper<BrandModel>().map(JSONString: jsonString)
      return mappableModel
  }
  
  static func deleteSelectBrand() {
    let userDefault = UserDefaults.standard
    userDefault.removeObject(forKey: "brandModel")
    userDefault.synchronize()
  }
  
  static func setAuthen(email: String, password: String) {
    let userDefault = UserDefaults.standard
    let dic = ["email" : email, "password" : password]
    userDefault.set(dic, forKey: "auth")
    userDefault.synchronize()
  }
  
  static func getAuthen() -> [String : Any]? {
    let userDefault = UserDefaults.standard
    let dic = userDefault.dictionary(forKey: "auth")
    return dic
  }
  
  static func deleteAuthen() {
    let userDefault = UserDefaults.standard
    userDefault.removeObject(forKey: "auth")
    userDefault.synchronize()
  }
  
  static func getProvince() -> ListProvinceModel? {
    if let path = Bundle.main.path(forResource: "province", ofType: "json") {
      do {
        let jsonString = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
        let mappableModel = Mapper<ListProvinceModel>().map(JSONString: jsonString)
        return mappableModel
      } catch {
        print("Invalid filename/path.")
        return nil
      }
    } else {
      print("Error map model")
      return nil
    }
  }
  
  static func getDistrict() -> ListDistrictModel? {
    if let path = Bundle.main.path(forResource: "district", ofType: "json") {
      do {
        let jsonString = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
        let mappableModel = Mapper<ListDistrictModel>().map(JSONString: jsonString)
        return mappableModel
      } catch {
        print("Invalid filename/path.")
        return nil
      }
    } else {
      print("Error map model")
      return nil
    }
  }
  
  static func getSubDistrict() -> ListSubDistrictModel? {
    if let path = Bundle.main.path(forResource: "sub_district", ofType: "json") {
      do {
        let jsonString = try String(contentsOfFile: path, encoding: String.Encoding.utf8)
        let mappableModel = Mapper<ListSubDistrictModel>().map(JSONString: jsonString)
        return mappableModel
      } catch {
        print("Invalid filename/path.")
        return nil
      }
    } else {
      print("Error map model")
      return nil
    }
  }
}
