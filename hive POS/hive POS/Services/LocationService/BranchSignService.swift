//
//  BranchSignService.swift
//  hive POS
//
//  Created by Space-C4ts on 8/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class BranchSignService {
  
  private init() { }
  static let shared = BranchSignService()
  var branchData: BranchModel?
  
  func setBranch(branchData: BranchModel) {
    self.branchData = branchData
  }
  
  func getBranchData() -> BranchModel? {
    return self.branchData
  }
  
  func deleteBranchData() {
    branchData = nil
  }
}
