//
//  LocationService.swift
//  hive POS
//
//  Created by Space-C4ts on 28/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import CoreLocation
import RxCoreLocation
import RxSwift
import RxCocoa

class LocationService: LocationServiceType {
  
  private init() { }
  static let shared = LocationService()
  
  var shareLocation: CLLocation?
  let disposeBag = DisposeBag()
  
  lazy var manager: CLLocationManager = {
    return CLLocationManager()
  }()
  
  func didUpdateLocationSingle() -> Observable<CLLocation?> {
    return manager.rx.location
  }
  
  func didUpdateLocations() -> ControlEvent<CLLocationsEvent> {
    return manager.rx.didUpdateLocations
  }
  
  func didRequestLocationService() {
    if CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
      CLLocationManager.authorizationStatus() == .authorizedAlways{
      manager.startUpdatingLocation()
      manager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
      manager.rx.location.subscribe(onNext: { [unowned self] location in
        self.shareLocation = location})
      .disposed(by:disposeBag)
    }else{
      manager.requestWhenInUseAuthorization()
    }
  }
}
