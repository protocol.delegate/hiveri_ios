//
//  LocationServiceType.swift
//  hive POS
//
//  Created by Space-C4ts on 28/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation
import RxCocoa
import RxCoreLocation

protocol LocationServiceType {
  func didUpdateLocationSingle() -> Observable<CLLocation?>
  func didUpdateLocations() -> ControlEvent<CLLocationsEvent>
  func didRequestLocationService()
}
