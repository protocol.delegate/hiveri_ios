//
//  APIServiceType.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import RxSwift

protocol APIServiceType {
  func rx_updateUser(requestModel: UpdateUserRequestModelType) -> Observable<UserModel>
  func rx_getBrandList(requestModel: GetBrandListRequestModelType) -> Observable<[BrandModel]>
  func rx_getBranchList(requestModel: GetBranchListRequestModelType) -> Observable<[BranchModel]>
  func rx_createBrand(requestModel: CreateBrandRequestModelType) -> Observable<BrandModel>
  func rx_getBrand(requestModel: GetBrandRequestModelType) -> Observable<BrandModel>
  func rx_updateBrand(requestModel: UpdateBrandRequestModelType) -> Observable<BrandModel>
  func rx_getUser(requestModel: GetUserRequestModelType) -> Observable<UserModel>
  func rx_deleteBrand(requestModel: DeleteBrandRequestModelType) -> Observable<Void>
  func rx_createBranch(requestModel: CreateBranchRequestModelType) -> Observable<BranchModel>
  func rx_getBranch(requestModel: GetBranchRequestModelType) -> Observable<BranchModel>
  func rx_updateBranch(requestModel: UpdateBranchRequestModelType) -> Observable<BranchModel>
  func rx_deleteBranch(requestModel: DeleteBranchRequestModelType) -> Observable<Void>
  func rx_deliveryOrders(requestModel: DeliveryOrdersRequestModelType) -> Observable<Void>
  func rx_bindingCreditCard(requestModel: BindingCreditCardRequestModelType) -> Observable<BindingCreditCardModel>
  func rx_creditCardTransection(requestModel: CreditCardTransectionRequestModelType) -> Observable<TransectionModel>
  func rx_inqueryCreditCard(requestModel: InqueryCreditCardRequestModelType) -> Observable<[CreditCardModel]>
  func rx_paymentCash(requestModel: PaymentCashRequestModelType) -> Observable<PaymentCashModel>
  func rx_createShop(requestModel: CreateShopRequestModelType) -> Observable<ShopModel>
  func rx_createMenuCategory(requestModel: CreateMenuCategoryRequestModelType) -> Observable<MenuCategoryModel>
  func rx_getMenuCategory(requestModel: GetMenuCategoryRequestModelType) -> Observable<[MenuCategoryModel]>
  func rx_updateMenuCategory(requestModel: UpdateCategoryRequestModelType) -> Observable<Void>
  func rx_deleteCategory(requestModel: DeleteCategoryRequestModelType) -> Observable<Void>
  func rx_createMenu(requestModel: CreateMenuRequestModelType) -> Observable<Void>
  func rx_getMenuList(requestModel: GetMenuListRequestModelType) -> Observable<[MenuModel]>
  func rx_editMenu(requestModel: EditMenuRequestModelType) -> Observable<Void>
  func rx_deleteMenu(requestModel: DeleteMenuRequestModelType) -> Observable<Void>
  func rx_getTableList(requestModel: GetTableListRequestModelType) -> Observable<[TableModel]>
  func rx_creatTable(requestModel: CreateTableRequestModelType) -> Observable<Void>
  func rx_getOrderByTable(requestModel: GetOrderByTableRequestModelType) -> Observable<OrderModel>
  func rx_updateOrder(requestModel: UpdateOrderRequestModelType) -> Observable<Void>
  func rx_createOrder(requestModel: CreateOrderRequestModelType) -> Observable<Void>
  func rx_createMultipleTable(requestModel: CreateMultipleTableRequestModelType) -> Observable<Void>
  func rx_updateTable(requestModel: UpdateTableRequestModelType) -> Observable<Void>
  func rx_getOrdersByTaleID(requestModel: GetOrdersByTableIDRequestModelType) -> Observable<[OrderModel]>
}
