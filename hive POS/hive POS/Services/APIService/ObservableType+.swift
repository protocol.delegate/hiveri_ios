//
//  ObservableType+.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import Moya
import RxSwift
import SwiftyJSON
import ObjectMapper

public extension ObservableType where Element == Moya.Response {
  
  /// Maps received data at key path into a SwiftyJSON. If the conversion fails, the signal errors.
  func mapSwiftyJSON() -> Observable<JSON> {
    return self.map { JSON($0.data) }
  }
  
  func filterApiStatusCode() -> Observable<Element> {
    return self.filter{ value in
      print(value.statusCode)
      let json = JSON(value.data)
      if case 200...299 = value.statusCode {
        return true
      } else if value.statusCode == 401 {
          throw
            APIService.Error(
              category: .tokenInvalid,
              name: "Status: \(value.statusCode)",
              message: json["message"].stringValue)
      }
      
      print(json)
      var errorTitleString = "Status: \(value.statusCode)"
      var errorMessgae = json["message"].stringValue
      var errorString = ""
      if json["error"].exists() {
        let errorList = json["error"].arrayValue.compactMap{$0.string}
        errorString = errorList.reduce("", { (result, each) -> String in
          print(result + "\n" + each)
          return result + "\n" + each
        }).trimmingCharacters(in: .whitespacesAndNewlines)
      }
      if json["errors"].exists() {
        let errorList = json["errors"].dictionaryValue.flatMap({ (arg) -> [String] in
          let (_, value) = arg
          return value.arrayValue.compactMap{$0.string}
        })
        errorString = errorList.reduce("", { (result, each) -> String in
          return result + "\n" + each
        }).trimmingCharacters(in: .whitespacesAndNewlines)
      }
      if let errorTitle = json["title"].string {
        errorTitleString = errorTitle
      }
      throw
        APIService.Error(
          name: errorTitleString,
          message: errorMessgae)
    }
    //return self as! Observable<Response>
  }
}

public extension ObservableType where Element == JSON {
  
  func filterApiStatus() -> Observable<Element> {
    return map { json -> Element in
      print(json)
      let dataRef = json
      
      guard let apiStatus = dataRef["Status"].int else { return dataRef }
      guard let error = dataRef["error"].dictionary else { return dataRef }
      
      if error.count > 0 {
        throw
        APIService.Error(
          name: "Error",
          message: error["message"]?.stringValue ?? "")
      }
      
      if apiStatus == 2 {
        throw
        APIService.Error(
          name: "Error",
          message: dataRef["message"].stringValue)
      } else if apiStatus == 3 {
        throw
          APIService.Error(category: .tokenInvalid,
                           name: "Error",
                           message: dataRef["message"].stringValue)
      } else if apiStatus == 4 {
        throw
        APIService.Error(category: .sessionExpired,
                         name: "Error",
                         message: dataRef["message"].stringValue)
      }
            
      return dataRef
    }
  }
  
  func mapApiValue<T>(of type: T.Type,
                      dataPath: [String] = ["data"],
                      isIgnoreOutmostArray: Bool = true) -> Observable<T> where T: Any {
    return map { json -> T in
      var dataRef = json
      
      if isIgnoreOutmostArray
        , let dataRefArray = dataRef.array
        , let dataRefArrayFirstItem = dataRefArray.first {
        dataRef = dataRefArrayFirstItem
      }
      
      for i in 0..<dataPath.count {
        dataRef = dataRef[dataPath[i]]
      }
      guard let mappedModel = dataRef.rawValue as? T
        else {
          throw
            APIService.Error(
              name: "Error: mapApiModel",
              message: "Cannot map object to model.") }
      return mappedModel
    }
  }
  
  func mapApiModel<T>(of type: T.Type,
                             dataPath: [String] = ["data"],
                             isIgnoreOutmostArray: Bool = true) -> Observable<T> where T: Mappable {
    return map { json -> T in
      var dataRef = json
      
      if isIgnoreOutmostArray
        , let dataRefArray = dataRef.array
        , let dataRefArrayFirstItem = dataRefArray.first {
        dataRef = dataRefArrayFirstItem
      }
      
      for i in 0..<dataPath.count {
        dataRef = dataRef[dataPath[i]]
      }
      guard let mappedModel = Mapper<T>().map(JSONObject: dataRef.object)
        else {
          throw
            APIService.Error(
              name: "Error: mapApiModel",
              message: "Cannot map object to model.") }
      return mappedModel
    }
  }
  
  func mapApiModelArray<T>(of type: T.Type,
                           dataPath: [String] = ["data"],
                           isIgnoreOutmostArray: Bool = true) -> Observable<[T]> where T: Mappable {
    return map { json -> [T] in
      var dataRef = json
      
      print(dataRef.rawString())
      
      if isIgnoreOutmostArray
        , let dataRefArray = dataRef.array
        , let dataRefArrayFirstItem = dataRefArray.first {
        dataRef = dataRefArrayFirstItem
      }
      
      for i in 0..<dataPath.count {
        dataRef = dataRef[dataPath[i]]
      }
      
      print(dataRef.rawString())
      
      guard let mappedModel = Mapper<T>().mapArray(JSONObject: dataRef.object)
        else {
          throw
            APIService.Error(
              name: "Error: mapApiModel",
              message: "Cannot map object to model.") }
      return mappedModel
    }
  }
}
