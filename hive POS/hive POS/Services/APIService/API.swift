//
//  API.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

enum API {
  case getUser(requestModel: GetUserRequestModelType)
  case updateUser(requestModel: UpdateUserRequestModelType)
  case getBrandList(requestModel: GetBrandListRequestModelType)
  case getBranchList(requestModel: GetBranchListRequestModelType)
  case createBrand(requestModel: CreateBrandRequestModelType)
  case getBrand(requestModel: GetBrandRequestModelType)
  case updateBrand(requestModel: UpdateBrandRequestModelType)
  case deleteBrand(requestModel: DeleteBrandRequestModelType)
  case createBranch(requestModel: CreateBranchRequestModelType)
  case getBranch(requestModel: GetBranchRequestModelType)
  case updateBranch(requestModel: UpdateBranchRequestModelType)
  case deleteBranch(requestModel: DeleteBranchRequestModelType)
  case deliveryOrders(requestModel: DeliveryOrdersRequestModelType)
  case bindingCreditCard(requestModel: BindingCreditCardRequestModelType)
  case creditCardTransection(requestModel: CreditCardTransectionRequestModelType)
  case inqueryCreditCard(requestModel: InqueryCreditCardRequestModelType)
  case paymentCash(requestModel: PaymentCashRequestModelType)
  case createShop(requestModel: CreateShopRequestModelType)
  case createMenuCategory(requestModel: CreateMenuCategoryRequestModelType)
  case getMenuCategory(requestModel: GetMenuCategoryRequestModelType)
  case updateCategory(requestModel: UpdateCategoryRequestModelType)
  case deleteCategory(requestModel: DeleteCategoryRequestModelType)
  case createMenu(requestModel: CreateMenuRequestModelType)
  case getMenuList(requestModel: GetMenuListRequestModelType)
  case editMenu(requestModel: EditMenuRequestModelType)
  case deleteMenu(requestModel: DeleteMenuRequestModelType)
  case getTableList(requestModel: GetTableListRequestModelType)
  case createTable(requestModel: CreateTableRequestModelType)
  case getOrderByTable(requestModel: GetOrderByTableRequestModelType)
  case updateOrder(requestModel: UpdateOrderRequestModelType)
  case createOrder(requestModel: CreateOrderRequestModelType)
  case createMultipleTable(requestModel: CreateMultipleTableRequestModelType)
  case updateTable(requestModel: UpdateTableRequestModelType)
  case getOrdersByTableID(requestModel: GetOrdersByTableIDRequestModelType)
}
