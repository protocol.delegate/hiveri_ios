//
//  APIService.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Moya
import ObjectMapper
import SwiftyJSON
import RxSwift
import RxCocoa
import RxSwiftExt
import Firebase
import RxFirebaseAuthentication
import RxOptional

public class APIService: APIServiceType {
  
  private var provider: MoyaProvider<API>!
  private let disposeBag = DisposeBag()
  
  init() {
    provider = MoyaProvider<API>()
  }
  
  func rx_updateUser(requestModel: UpdateUserRequestModelType) -> Observable<UserModel> {
    return handleResponseMap(with: .updateUser(requestModel: requestModel),
                             of: UserModel.self,
                             dataPath: [])
  }
  
  func rx_getBrandList(requestModel: GetBrandListRequestModelType) -> Observable<[BrandModel]> {
    return handleResponseMapArray(with: .getBrandList(requestModel: requestModel),
                                  of: BrandModel.self,
                                  dataPath: [])
  }
  
  func rx_getBranchList(requestModel: GetBranchListRequestModelType) -> Observable<[BranchModel]> {
    return handleResponseMapArray(with: .getBranchList(requestModel: requestModel),
                                  of: BranchModel.self,
                                  dataPath: [])
  }
  
  func rx_createBrand(requestModel: CreateBrandRequestModelType) -> Observable<BrandModel> {
    return handleResponseMap(with: .createBrand(requestModel: requestModel),
                             of: BrandModel.self,
                             dataPath: [])
  }
  
  func rx_getBrand(requestModel: GetBrandRequestModelType) -> Observable<BrandModel> {
    return handleResponseMap(with: .getBrand(requestModel: requestModel),
                             of: BrandModel.self,
                             dataPath: [])
  }
  
  func rx_updateBrand(requestModel: UpdateBrandRequestModelType) -> Observable<BrandModel> {
    return handleResponseMap(with: .updateBrand(requestModel: requestModel),
                             of: BrandModel.self,
                             dataPath: [])
  }
  
  func rx_getUser(requestModel: GetUserRequestModelType) -> Observable<UserModel> {
    return handleResponseMap(with: .getUser(requestModel: requestModel),
                             of: UserModel.self,
                             dataPath: [])
  }
  
  func rx_deleteBrand(requestModel: DeleteBrandRequestModelType) -> Observable<Void> {
    return handleResponse(with: .deleteBrand(requestModel: requestModel)).map{ _ in ()}
  }
  
  func rx_createBranch(requestModel: CreateBranchRequestModelType) -> Observable<BranchModel> {
    return handleResponseMap(with: .createBranch(requestModel: requestModel),
                       of: BranchModel.self,
                       dataPath: [])
  }
  
  func rx_getBranch(requestModel: GetBranchRequestModelType) -> Observable<BranchModel> {
    return handleResponseMap(with: .getBranch(requestModel: requestModel),
                             of: BranchModel.self,
                             dataPath: [])
  }
  
  func rx_updateBranch(requestModel: UpdateBranchRequestModelType) -> Observable<BranchModel> {
    return handleResponseMap(with: .updateBranch(requestModel: requestModel),
                             of: BranchModel.self,
                             dataPath: [])
  }
  
  func rx_deleteBranch(requestModel: DeleteBranchRequestModelType) -> Observable<Void> {
    return handleResponseMap(with: .deleteBranch(requestModel: requestModel),
                             of: BranchModel.self,
                             dataPath: []).map{ _ in ()}
  }
  
  func rx_deliveryOrders(requestModel: DeliveryOrdersRequestModelType) -> Observable<Void> {
    return handleResponseValue(with: .deliveryOrders(requestModel: requestModel),
                               of: String.self,
                               dataPath: []).map{ _ in () }
  }
  
  func rx_bindingCreditCard(requestModel: BindingCreditCardRequestModelType) -> Observable<BindingCreditCardModel> {
    return handleResponseMap(with: .bindingCreditCard(requestModel: requestModel),
                             of: BindingCreditCardModel.self,
                             dataPath: [])
  }
  
  func rx_creditCardTransection(requestModel: CreditCardTransectionRequestModelType) -> Observable<TransectionModel> {
    return handleResponseMap(with: .creditCardTransection(requestModel: requestModel),
                               of: TransectionModel.self,
                               dataPath: [])
  }
  
  func rx_inqueryCreditCard(requestModel: InqueryCreditCardRequestModelType) -> Observable<[CreditCardModel]> {
    return handleResponseMapArray(with: .inqueryCreditCard(requestModel: requestModel),
                                  of: CreditCardModel.self,
                                  dataPath: ["creditcardTokenList"])
  }
  
  func rx_paymentCash(requestModel: PaymentCashRequestModelType) -> Observable<PaymentCashModel> {
    return handleResponseMap(with: .paymentCash(requestModel: requestModel),
                             of: PaymentCashModel.self,
                             dataPath: [])
  }
  
  func rx_createShop(requestModel: CreateShopRequestModelType) -> Observable<ShopModel> {
    return handleResponseMap(with: .createShop(requestModel: requestModel),
                             of: ShopModel.self,
                             dataPath: ["data"])
  }
  
  func rx_createMenuCategory(requestModel: CreateMenuCategoryRequestModelType) -> Observable<MenuCategoryModel> {
    return handleResponseMap(with: .createMenuCategory(requestModel: requestModel),
                             of: MenuCategoryModel.self,
                             dataPath: ["data"])
  }
  
  func rx_getMenuCategory(requestModel: GetMenuCategoryRequestModelType) -> Observable<[MenuCategoryModel]> {
    return handleResponseMapArray(with: .getMenuCategory(requestModel: requestModel),
                                  of: MenuCategoryModel.self,
                                  dataPath: ["data"])
  }
  
  func rx_updateMenuCategory(requestModel: UpdateCategoryRequestModelType) -> Observable<Void> {
    return handleResponseValue(with: .updateCategory(requestModel: requestModel),
                               of: String.self,
                               dataPath: ["status"]).map{ _ in () }
  }
  
  func rx_deleteCategory(requestModel: DeleteCategoryRequestModelType) -> Observable<Void> {
    return handleResponseValue(with: .deleteCategory(requestModel: requestModel),
                               of: String.self,
                               dataPath: ["status"]).map{ _ in () }
  }
  
  func rx_createMenu(requestModel: CreateMenuRequestModelType) -> Observable<Void> {
    return handleResponseValue(with: .createMenu(requestModel: requestModel),
                               of: String.self,
                               dataPath: ["status"]).map{ _ in () }
  }
  
  func rx_getMenuList(requestModel: GetMenuListRequestModelType) -> Observable<[MenuModel]> {
    return handleResponseMapArray(with: .getMenuList(requestModel: requestModel),
                                  of: MenuModel.self,
                                  dataPath: ["data"])
  }
  
  func rx_editMenu(requestModel: EditMenuRequestModelType) -> Observable<Void> {
    return handleResponseValue(with: .editMenu(requestModel: requestModel),
                               of: String.self,
                               dataPath: ["status"]).map{ _ in () }
  }
  
  func rx_deleteMenu(requestModel: DeleteMenuRequestModelType) -> Observable<Void> {
    return handleResponseValue(with: .deleteMenu(requestModel: requestModel),
                               of: String.self,
                               dataPath: ["status"]).map{ _ in () }
  }
  
  func rx_getTableList(requestModel: GetTableListRequestModelType) -> Observable<[TableModel]> {
    return handleResponseMapArray(with: .getTableList(requestModel: requestModel),
                                  of: TableModel.self,
                                  dataPath: ["data"])
  }
  
  func rx_creatTable(requestModel: CreateTableRequestModelType) -> Observable<Void> {
    return handleResponseMap(with: .createTable(requestModel: requestModel),
                             of: TableModel.self).map{ _ in () }
  }
  
  func rx_getOrderByTable(requestModel: GetOrderByTableRequestModelType) -> Observable<OrderModel> {
    return handleResponseMap(with: .getOrderByTable(requestModel: requestModel),
                             of: OrderModel.self,
                             dataPath: [])
  }
  
  func rx_updateOrder(requestModel: UpdateOrderRequestModelType) -> Observable<Void> {
    return handleResponseMap(with: .updateOrder(requestModel: requestModel),
                             of: OrderModel.self,
                             dataPath: []).map{ _ in () }
  }
  
  func rx_createOrder(requestModel: CreateOrderRequestModelType) -> Observable<Void> {
    return handleResponseMap(with: .createOrder(requestModel: requestModel),
                             of: OrderModel.self, dataPath: []).map{_ in ()}
  }
  
  func rx_createMultipleTable(requestModel: CreateMultipleTableRequestModelType) -> Observable<Void> {
    return handleResponseMapArray(with: .createMultipleTable(requestModel: requestModel),
                                  of: TableModel.self,
                                  dataPath: []).map{ _ in ()}
  }
  
  func rx_updateTable(requestModel: UpdateTableRequestModelType) -> Observable<Void> {
    return handleResponseMap(with: .updateTable(requestModel: requestModel),
                             of: TableModel.self,
                             dataPath: []).map{ _ in () }
  }
  
  func rx_getOrdersByTaleID(requestModel: GetOrdersByTableIDRequestModelType) -> Observable<[OrderModel]> {
    return handleResponseMapArray(with: .getOrdersByTableID(requestModel: requestModel),
                                  of: OrderModel.self,
                                  dataPath: ["data"])
  }
  
  // MARK: Private
  
  private func handleResponse(with targetType: API) -> Observable<JSON> {
    
    //TODO : Refactor
    
    let user = Auth.auth().currentUser
    guard let getUserSignal = user?.rx.getIDToken().materialize() else {
      return provider
          .rx
          .request(targetType).debug()
          .asObservable()
          .do(onNext:{ (data) in print("handleResponse---->",(try? data.mapJSON()) ?? "-" ) } )
          .filterApiStatusCode()
          .map { JSON($0.data) }
          .filterApiStatus()
      .catchError { throw APIService.Error(error: $0)}
    }
    
    return getUserSignal
      .elements()
      .do(onNext: { token in LocalDataService.setUserToken(token: token) })
      .flatMapLatest{ [unowned self] _ in
        self.provider
            .rx
            .request(targetType).debug()
            .asObservable()
            .do(onNext:{ (data) in print("handleResponse---->",(try? data.mapJSON()) ?? "-" ) } )
            .filterApiStatusCode()
            .map { JSON($0.data) }
            .filterApiStatus()
          .catchError { throw APIService.Error(error: $0) }}
  }
  
  private func handleResponseValue<T>(with targetType: API, of type: T.Type, dataPath: [String] = ["Data"]) -> Observable<T> where T: Any {
    return handleResponse(with: targetType)
      .mapApiValue(of: T.self, dataPath: dataPath, isIgnoreOutmostArray: false)
      .catchError { throw APIService.Error(error: $0) }
  }
  
  private func handleResponseMap<T>(with targetType: API, of type: T.Type, dataPath: [String] = ["Data"]) -> Observable<T> where T: Mappable {
    return handleResponse(with: targetType)
      .mapApiModel(of: T.self, dataPath: dataPath, isIgnoreOutmostArray: false)
      .catchError { throw APIService.Error(error: $0) }
  }
  
  private func handleResponseMapArray<T>(with targetType: API, of type: T.Type, dataPath: [String] = ["Data"]) -> Observable<[T]> where T: Mappable {
    return handleResponse(with: targetType)
      .mapApiModelArray(of: T.self, dataPath: dataPath, isIgnoreOutmostArray: false)
      .catchError { throw APIService.Error(error: $0) }
  }
  
  private func handleResponseArrayWithPrefix<T>(with targetType: API, of type: T.Type, dataPath: [String] = ["data"]) -> Observable<([T], String)> where T: Mappable {
    let sharedResponse = handleResponse(with: targetType).share(replay: 1)
    let arraySignal = sharedResponse
      .mapApiModelArray(of: T.self, dataPath: dataPath)
    let prefixSignal = handleResponse(with: targetType)
      .map { $0["prefix"].stringValue }
    return Observable.zip(arraySignal, prefixSignal)
      .catchError { throw APIService.Error(error: $0) }
  }
}
