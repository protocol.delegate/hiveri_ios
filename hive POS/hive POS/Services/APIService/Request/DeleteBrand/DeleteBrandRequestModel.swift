//
//  DeleteBrandRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 2/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class DeleteBrandRequestModel: DeleteBrandRequestModelType {
  
  var brandID: Int
  
  init(brandID: Int) {
    self.brandID = brandID
  }
}

extension DeleteBrandRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["id" : brandID]
  }
}
