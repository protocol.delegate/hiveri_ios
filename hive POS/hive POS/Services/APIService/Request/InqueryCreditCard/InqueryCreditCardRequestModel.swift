//
//  InqueryCreditCardRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 16/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class InqueryCreditCardRequestModel: InqueryCreditCardRequestModelType {
  
  var platformID: String
  var customerID: String
  
  init(platformID: String, customerID: String) {
    self.platformID = platformID
    self.customerID = customerID
  }
}

extension InqueryCreditCardRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["platformId" :platformID,
            "customerId" : customerID]
  }
}
