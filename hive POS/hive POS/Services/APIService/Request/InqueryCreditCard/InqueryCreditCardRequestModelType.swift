//
//  InqueryCreditCardRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 16/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol InqueryCreditCardRequestModelType: BaseAPIServiceRequestType {
  var platformID: String { get set }
  var customerID: String { get set }
}
