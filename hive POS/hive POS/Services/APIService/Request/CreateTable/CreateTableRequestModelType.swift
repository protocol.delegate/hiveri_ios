//
//  CreateTableRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 25/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
protocol CreateTableRequestModelType: BaseAPIServiceRequestType {
  var branchID: Int { get set }
  var tableName: String { get set }
}
