//
//  CreateTableRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 25/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class CreateTableRequestModel: CreateTableRequestModelType {
  var branchID: Int
  var tableName: String
  
  init(branchID: Int, tableName: String) {
    self.branchID = branchID
    self.tableName = tableName
  }
}

extension CreateTableRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["branchId" : branchID,
            "tableName" : tableName]
  }
}
