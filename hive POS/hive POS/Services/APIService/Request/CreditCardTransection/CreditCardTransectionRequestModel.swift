//
//  CreditCardTransectionRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 15/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class CreditCardTransectionRequestModel: CreditCardTransectionRequestModelType {
  
  var platformID: String
  var cardToken: String
  var cardID: String
  var customerID: String
  var currency: String
  var amount: String
  var transectionType: String
  
  
  init(platformID: String, cardToken: String,
       cardID: String, customerID: String,
       currency: String, amount: String,
       transectionType: String) {
    self.platformID = platformID
    self.cardToken = cardToken
    self.cardID = cardID
    self.customerID = customerID
    self.currency = currency
    self.amount = amount
    self.transectionType = transectionType
  }
}

extension CreditCardTransectionRequestModelType {
  
  func toDictionary() -> [String : Any] {
    return ["platformId" : platformID,
            "customerId" : customerID,
            "cardId" : cardID,
            "cardToken" : cardToken,
            "currency" : currency,
            "amount" : amount,
            "transactionType" : transectionType]
  }
  
}
