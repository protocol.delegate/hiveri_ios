//
//  CreditCardTransectionRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 15/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol CreditCardTransectionRequestModelType: BaseAPIServiceRequestType {
  var platformID: String { get set }
  var cardToken: String { get set }
  var cardID: String { get set }
  var customerID: String { get set }
  var currency: String { get set }
  var amount: String { get set }
  var transectionType: String { get set }
}
