//
//  CreateBranchRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 7/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol CreateBranchRequestModelType: BaseAPIServiceRequestType {
  var branchName: String { get set }
  var branchDetail: String { get set }
  var building: String { get set }
  var room: String { get set }
  var floor: String { get set }
  var village: String { get set }
  var houseNO: String { get set }
  var villageNO: String { get set }
  var soi: String { get set }
  var street: String { get set }
  var subDistrict: Int { get set }
  var district: Int { get set }
  var province: Int { get set }
  var postCode: String { get set }
  var isDelivery: Bool { get set }
  var isOnline: Bool { get set }
  var latitude: Double { get set }
  var longitude: Double { get set }
  var email: String { get set }
  var website: String { get set }
  var line: String { get set }
  var facebook: String { get set }
  var instagram: String { get set }
  var preOrderMinute: Int { get set }
  var priceSince: Int { get set }
  var priceTo: Int { get set }
  var brandID: Int { get set }
  var branchNO: String { get set }
  var phoneNumbers: [[String: Any]] { get set }
  var opendays: [String: Any] { get set }
  var foodImage: [[String: Any]] { get set }
  var menuImages: [[String: Any]] { get set }
  var viewImages: [[String: Any]] { get set }
  var foodCategory: [[String: Any]] { get set }
  var holidayWeeks:[[String: Any]] { get set }
  var holidayDays: [[String: Any]] { get set }
  var branchImageLogoID: String { get set }
  var branchImageLogoURL: String { get set }
  var branchImageBanerID: String { get set }
  var branchImageBanerURL: String { get set }
}
