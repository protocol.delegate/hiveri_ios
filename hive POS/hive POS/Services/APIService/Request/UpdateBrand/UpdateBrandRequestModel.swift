//
//  UpdateBrandRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 1/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class UpdateBrandRequestModel: UpdateBrandRequestModelType {

  var brandID: Int
  var brandName: String
  var brandEmail: String
  var website: String
  var lineID: String
  var facebook: String
  var instagram: String
  var imageURL: String
  var imageID: String
  var brandDetail: String
  var brandPhones: [[String : Any]]
  var building: String
  var roomNO: String
  var floor: String
  var village: String
  var houseNO: String
  var villageNO: String
  var soi : String
  var street: String
  var subDistrict: Int
  var district: Int
  var province: Int
  var postCode: String
  var latitude: String
  var longitude: String
  var prefix: String
  var firstName: String
  var lastName: String
  var phoneNumbers: [[String : Any]]
  var email: String
  
  init(brandID: Int, brandName: String, brandEmail: String, website: String, lineID: String, facebook: String,
       instagram: String, imageURL: String, imageID: String, brandDetail: String, brandPhones: [[String : Any]], building: String,
       roomNO: String, floor: String, village: String, houseNO: String, villageNO: String, soi: String, street: String,
       subDistrict: Int, district: Int, province: Int, postCode: String, latitude: String, longitude: String,
       prefix: String, firstName: String, lastName: String, phoneNumbers: [[String : Any]], email: String) {
    
    self.brandID = brandID
    self.brandName = brandName
    self.brandEmail = brandEmail
    self.website = website
    self.lineID = lineID
    self.facebook = facebook
    self.instagram = instagram
    self.imageURL = imageURL
    self.imageID = imageID
    self.brandDetail = brandDetail
    self.brandPhones = brandPhones
    self.building = building
    self.roomNO = roomNO
    self.floor = floor
    self.village = village
    self.houseNO = houseNO
    self.villageNO = villageNO
    self.soi = soi
    self.street = street
    self.subDistrict = subDistrict
    self.district = district
    self.province = province
    self.postCode = postCode
    self.latitude = latitude
    self.longitude = longitude
    self.prefix = prefix
    self.firstName = firstName
    self.lastName = lastName
    self.phoneNumbers = phoneNumbers
    self.email = email
  }
  
}

extension UpdateBrandRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["id" : brandID,
            "brandName" : brandName,
            "email" : brandEmail,
            "website" : website,
            "lineName" : lineID,
            "facebookUrl" : facebook,
            "instagramName" : instagram,
            "brandImageUrl": imageURL,
            "brandImageId" : imageID,
            "brandDetail" : brandDetail,
            "brandTelephones" : brandPhones,
            "building" : building,
            "roomNumber" : roomNO ,
            "floor" : floor,
            "village" : village,
            "placeNumber" : houseNO,
            "moo" : villageNO,
            "alley" : soi,
            "road" : street,
            "areaId" : subDistrict,
            "districtId" : district,
            "provinceId" : province,
            "zipcode" : postCode,
            "placeLatitude" : latitude,
            "placeLongitude" : longitude,
            "contactPrefix" : prefix,
            "contactFirstname" : firstName,
            "contactLastname" : lastName,
            "contactTelephones" : phoneNumbers,
            "contactEmail" : email]
  }
}
