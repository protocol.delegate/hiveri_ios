//
//  UpdateBrandRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 1/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol UpdateBrandRequestModelType: BaseAPIServiceRequestType {
  
  var brandID: Int { get set }
  var brandName: String { get set }
  var brandEmail: String { get set }
  var website: String { get set }
  var lineID: String { get set }
  var facebook: String { get set }
  var instagram: String { get set }
  var imageURL: String { get set }
  var imageID: String { get set }
  var brandDetail: String { get set }
  var brandPhones: [[String : Any]] { get set }
  var building: String { get set }
  var roomNO: String { get set }
  var floor: String { get set }
  var village: String { get set }
  var houseNO: String { get set }
  var villageNO: String { get set }
  var soi : String { get set }
  var street: String { get set }
  var subDistrict: Int { get set }
  var district: Int { get set }
  var province: Int { get set }
  var postCode: String { get set }
  var latitude: String { get set }
  var longitude: String { get set }
  var prefix: String { get set }
  var firstName: String { get set }
  var lastName: String { get set }
  var phoneNumbers: [[String : Any]] { get set }
  var email: String { get set }
  
}
