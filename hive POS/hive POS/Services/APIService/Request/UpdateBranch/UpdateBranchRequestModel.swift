//
//  UpdateBranchRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 9/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class UpdateBranchRequestModel: UpdateBranchRequestModelType {
  
  var branchID: Int
  var branchName: String
  var branchDetail: String
  var building: String
  var room: String
  var floor: String
  var village: String
  var houseNO: String
  var villageNO: String
  var soi: String
  var street: String
  var subDistrict: Int
  var district: Int
  var province: Int
  var postCode: String
  var isDelivery: Bool
  var isOnline: Bool
  var latitude: Double
  var longitude: Double
  var email: String
  var website: String
  var line: String
  var facebook: String
  var instagram: String
  var preOrderMinute: Int
  var priceSince: Int
  var priceTo: Int
  var brandID: Int
  var branchNO: String
  var phoneNumbers: [[String:Any]]
  var opendays: [String : Any]
  var foodImage: [[String : Any]]
  var menuImages: [[String : Any]]
  var viewImages: [[String : Any]]
  var foodCategory: [[String : Any]]
  var holidayWeeks: [[String : Any]]
  var holidayDays: [[String : Any]]
  var branchImageLogoID: String
  var branchImageLogoURL: String
  var branchImageBanerID: String
  var branchImageBanerURL: String
  var spoonwalkID: String
  
  init(branchID: Int, branchName: String, branchDetail: String, building: String, room: String, floor: String,
       village: String, houseNO: String, villageNO: String, soi: String, street: String, subDistrict: Int,
       district: Int, province: Int, postCode: String, isDelivery: Bool, isOnline: Bool,
       latitude: Double, longitude: Double, email: String, website: String, line: String, facebook: String,
       instagram: String, preOrderMinute: Int, priceSince: Int, priceTo: Int, brandID: Int,
       branchNO: String, phoneNumbers: [[String: Any]], openDays: [String: Any], foodImage: [[String: Any]],
       menuImages: [[String: Any]], viewImages: [[String: Any]], foodCategory: [[String: Any]],
       holidayWeeks: [[String: Any]], holidayDays: [[String: Any]], branchImageLogoID: String,
       branchImageLogoURL: String, branchImageBanerID: String, branchImageBanerURL: String, spoonwalkID: String) {
    
    self.branchID = branchID
    self.branchName = branchName
    self.branchDetail = branchDetail
    self.building = building
    self.room = room
    self.floor = floor
    self.village = village
    self.houseNO = houseNO
    self.villageNO = villageNO
    self.soi = soi
    self.street = street
    self.subDistrict = subDistrict
    self.district = district
    self.province = province
    self.postCode = postCode
    self.isDelivery = isDelivery
    self.isOnline = isOnline
    self.latitude = latitude
    self.longitude = longitude
    self.email = email
    self.website = website
    self.line = line
    self.facebook = facebook
    self.instagram = instagram
    self.preOrderMinute = preOrderMinute
    self.priceSince = priceSince
    self.priceTo = priceTo
    self.brandID = brandID
    self.branchNO = branchNO
    self.phoneNumbers = phoneNumbers
    self.opendays = openDays
    self.foodImage = foodImage
    self.holidayWeeks = holidayWeeks
    self.holidayDays = holidayDays
    self.branchImageLogoID = branchImageLogoID
    self.branchImageLogoURL = branchImageLogoURL
    self.branchImageBanerID = branchImageBanerID
    self.branchImageBanerURL = branchImageBanerURL
    self.menuImages = menuImages
    self.viewImages = viewImages
    self.foodCategory = foodCategory
    self.spoonwalkID = spoonwalkID
  }
  
}

extension UpdateBranchRequestModelType {
  func toDictionary() -> [String : Any] {
    let params = [
        "id": branchID,
        "branchName": branchName,
        "branchNo": branchNO,
        "branchDetail": branchDetail,
        "placeNumber": houseNO,
        "roomNumber": room,
        "floor": floor,
        "placeAlley" : soi,
        "building" : building,
        "moo": villageNO,
        "villageName": village,
        "placeRoad": street,
        "placeAreaId": subDistrict,
        "placeDistrictId": district,
        "placeProvinceId": province,
        "zipcode": postCode,
        "hasDelivery": isDelivery,
        "isOnlineOnly": isOnline,
        "placeLatitude": latitude,
        "placeLongitude": longitude,
        "email": email,
        "websiteUrl": website,
        "lineId": line,
        "facebookName": facebook,
        "instagramName": instagram,
        "preOrderMinute": preOrderMinute,
        "priceSince": priceSince,
        "priceTo": priceTo,
        "brandId": brandID,
        "telephones": phoneNumbers,
        "foodCategorys": foodCategory,
        "openDays": opendays,
        "holidayDates": holidayWeeks,
        "holidayDays": holidayDays,
        "foodImgs": foodImage,
        "menuImgs": menuImages,
        "viewImgs": viewImages,
        "branchImageProfileId": branchImageLogoID,
        "branchImageProfileUrl": branchImageLogoURL,
        "branchImageCoverId": branchImageBanerID,
        "branchImageCoverUrl": branchImageBanerURL,
        "spoonwalkId" : spoonwalkID
      ] as [String : Any]
    
    let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
    let decoded = String(data: jsonData, encoding: .utf8)!
    print("Update Params------> ", decoded)
    return params
  }
}
