//
//  CreateMultipleTableRequestModelType.swift
//  hive POS
//
//  Created by xAdmin on 27/6/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol CreateMultipleTableRequestModelType: BaseAPIServiceRequestType {
  var brandID: Int { get set }
  var branchID: Int { get set }
  var spoonWalkID: String { get set }
  var capacity: Int { get set }
  var amount: Int { get set }
}
