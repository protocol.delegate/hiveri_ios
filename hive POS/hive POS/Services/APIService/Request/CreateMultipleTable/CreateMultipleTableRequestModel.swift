//
//  CreateMultipleTableRequestModel.swift
//  hive POS
//
//  Created by xAdmin on 27/6/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class CreateMultipleTableRequestModel: CreateMultipleTableRequestModelType {
  var brandID: Int
  
  var branchID: Int
  
  var spoonWalkID: String
  
  var capacity: Int
  
  var amount: Int
  
  init(brandID: Int, branchID: Int, spoonWalkID: String, capacity: Int, amount: Int) {
    self.brandID = brandID
    self.branchID = branchID
    self.spoonWalkID = spoonWalkID
    self.capacity = capacity
    self.amount = amount
  }
  
  func toDictionary() -> [String : Any] {
    return ["brand_id" : brandID,
            "branch_id" : branchID,
            "restaurant_id" : spoonWalkID,
            "capacity" : capacity,
            "amount" : amount]
  }
}
