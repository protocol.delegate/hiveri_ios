//
//  GetMenuListRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 20/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class GetMenuListRequestModel: GetMenuListRequestModelType {
  
  var categoryID: String
  
  init(categoryID: String) {
    self.categoryID = categoryID
  }
}

extension GetMenuListRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["category_id" : categoryID]
  }
}
