//
//  GetMenuListRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 20/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol GetMenuListRequestModelType: BaseAPIServiceRequestType {
  var categoryID: String { get set } 
}
