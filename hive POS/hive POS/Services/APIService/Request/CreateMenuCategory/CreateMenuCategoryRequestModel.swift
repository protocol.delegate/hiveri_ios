//
//  CreateMenuCategoryRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 19/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class CreateMenuCategoryRequestModel: CreateMenuCategoryRequestModelType {
  
  var brandID: Int
  var branchID: Int
  var shopID: String
  var categoryName: String
  
  init(brandID: Int, branchID: Int, shopID: String, categoryName: String) {
    self.brandID = brandID
    self.branchID = branchID
    self.shopID = shopID
    self.categoryName = categoryName
  }
  
}
extension CreateMenuCategoryRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["name" : categoryName,
            "brand_id" : brandID,
            "branch_id" : branchID,
            "restaurant_id" : shopID]
  }
}
