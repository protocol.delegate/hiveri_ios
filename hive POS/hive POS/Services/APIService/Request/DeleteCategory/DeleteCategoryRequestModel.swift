//
//  DeleteCategoryRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 19/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class DeleteCategoryRequestModel: DeleteCategoryRequestModelType {
  var categoryID: String
  
  init(categoryID: String) {
    self.categoryID = categoryID
  }
}

extension DeleteCategoryRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["id" : categoryID]
  }
}
