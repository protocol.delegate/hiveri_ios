//
//  GetMenuCategoryRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 19/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class GetMenuCategoryRequestModel: GetMenuCategoryRequestModelType {

  var shopID: String
  
  init(shopID: String) {
    self.shopID = shopID
  }
}

extension GetMenuCategoryRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["restaurant_id" : shopID]
  }
}
