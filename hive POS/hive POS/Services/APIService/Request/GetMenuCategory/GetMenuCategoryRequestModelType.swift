//
//  GetMenuCategoryRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 19/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol GetMenuCategoryRequestModelType: BaseAPIServiceRequestType {
  var shopID: String { get set }
}
