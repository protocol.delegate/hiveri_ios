//
//  GetTableListRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 25/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol GetTableListRequestModelType: BaseAPIServiceRequestType {
  var restaurantID: String { get set }
  var brandID: Int { get set }
}
