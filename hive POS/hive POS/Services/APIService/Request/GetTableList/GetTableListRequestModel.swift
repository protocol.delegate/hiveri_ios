//
//  GetTableListRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 25/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class GetTableListRequestModel: GetTableListRequestModelType {
  
  var restaurantID: String
  var brandID: Int
  
  init(spoobWalkID: String, brandID: Int) {
    self.restaurantID = spoobWalkID
    self.brandID = brandID
  }
}
extension GetTableListRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["restaurant_id" : restaurantID,
            "brand_id" : brandID]
  }
}
