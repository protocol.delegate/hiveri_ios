//
//  UpdateUserRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol UpdateUserRequestModelType: BaseAPIServiceRequestType {
  
  var firstName: String { get set }
  var lastName: String { get set }
  var profileImagePath: String { get set }
  var profileImageID: String { get set }
  var countryCode: String { get set }
  var phoneNumber: String { get set }
  var prefix: String { get set }
  
}
