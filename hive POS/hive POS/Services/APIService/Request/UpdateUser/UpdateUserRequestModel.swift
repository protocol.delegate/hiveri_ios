//
//  UpdateUserRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class UpdateUserRequestModel: UpdateUserRequestModelType {
  
  var firstName: String
  var lastName: String
  var profileImagePath: String
  var profileImageID: String
  var countryCode: String
  var phoneNumber: String
  var prefix: String
  
  init(firstName: String, lastName: String, profileImagePath: String, profileImageID: String, countryCode: String, phoneNumber: String, prefix: String) {
    
    self.firstName = firstName
    self.lastName = lastName
    self.profileImagePath = profileImagePath
    self.profileImageID = profileImageID
    self.countryCode = countryCode
    self.phoneNumber = phoneNumber
    self.prefix = prefix
    
  }
}

extension UpdateUserRequestModelType {
  
  func toDictionary() -> [String : Any] {
    
    return ["firstname" : firstName,
            "lastname" : lastName,
            "profileImageUrl" : profileImagePath,
            "profileImageId" : profileImageID,
            "countryCode" : countryCode,
            "phoneNumber" : phoneNumber,
            "prefix" : prefix]
  }
  
}
