//
//  GetBrandRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 1/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class GetBrandRequestModel: GetBrandRequestModelType {
  
  var brandID: Int
  
  init(brandID: Int) {
    self.brandID = brandID
  }
}

extension GetBrandRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["id" : brandID]
  }
}
