//
//  GetBrandRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 1/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol GetBrandRequestModelType: BaseAPIServiceRequestType {
  var brandID: Int { get set }
}
