//
//  PaymentCashRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 16/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class PaymentCashRequestModel: PaymentCashRequestModelType {
  
  var orderID: Int
  var memberDiscountPercent: Int
  var memberDiscountValue: Double
  var billDiscountPercent: Int
  var billDiscountValue: Double
  var fine: Int
  var serviceChargePercent: Int
  var serviceChargeValue: Double
  var vatPercent: Int
  var vatValue: Double
  var totalValue: Double
  var cashAmount: Double
  var changeAmount: Double
  var payAmount: Double
  var nameBill: String
  
  init(orderID: Int, memberDiscountPercent: Int,
       memberDiscountValue: Double, billDiscountPercent: Int,
       billDiscountValue: Double, fine: Int,
       serviceChargePercent: Int, serviceChargeValue: Double,
       vatPercent: Int, vatValue: Double, totalValue: Double, cashAmount: Double,
       changeAmount: Double, payAmount: Double,
       nameBill: String) {
    self.orderID = orderID
    self.memberDiscountPercent = memberDiscountPercent
    self.memberDiscountValue = memberDiscountValue
    self.billDiscountPercent = billDiscountPercent
    self.billDiscountValue = billDiscountValue
    self.fine = fine
    self.serviceChargePercent = serviceChargePercent
    self.serviceChargeValue = serviceChargeValue
    self.vatPercent = vatPercent
    self.vatValue = vatValue
    self.totalValue = totalValue
    self.cashAmount = cashAmount
    self.changeAmount = changeAmount
    self.payAmount = payAmount
    self.nameBill = nameBill
  }
  
  func toDictionary() -> [String : Any] {
    return ["foodOrderId" : orderID,
            "memberDiscountPercent" : memberDiscountPercent,
            "memberDiscountValue" : memberDiscountValue,
            "billDiscountPercent" : billDiscountPercent,
            "billDiscountValue" : billDiscountValue,
            "fine" : fine,
            "serviceChargePercent" : serviceChargePercent,
            "serviceChargeValue" : serviceChargeValue,
            "vatPercent" : vatPercent,
            "vatValue" : vatValue,
            "totalValue" : totalValue,
            "cashAmount" : cashAmount,
            "changeAmount" : changeAmount,
            "payAmount" : payAmount,
            "nameBill" : nameBill]
  }
  
  
}
