//
//  PaymentCashRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 16/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol PaymentCashRequestModelType: BaseAPIServiceRequestType {
  var orderID: Int { get set }
  var memberDiscountPercent: Int { get set }
  var memberDiscountValue: Double { get set }
  var billDiscountPercent: Int { get set }
  var billDiscountValue: Double { get set }
  var fine: Int { get set }
  var serviceChargePercent: Int { get set }
  var serviceChargeValue: Double { get set }
  var vatPercent: Int { get set }
  var vatValue: Double { get set }
  var totalValue: Double { get set }
  var cashAmount: Double { get set }
  var changeAmount: Double { get set }
  var payAmount: Double { get set }
  var nameBill: String { get set }
}
