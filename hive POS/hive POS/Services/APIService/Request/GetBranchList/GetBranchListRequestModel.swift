//
//  GetBranchListRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class GetBranchListRequestModel: GetBranchListRequestModelType {
  
  var brandID: Int
  
  init(brandID: Int) {
    self.brandID = brandID
  }
}

extension GetBranchListRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["brand_id" : brandID]
  }
}
