
//
//  BindingCreditCardRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 15/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class BindingCreditCardRequestModel: BindingCreditCardRequestModelType {
  var platformID: String
  var customerID: String
  var cardID: String
  var cardName: String
  var callBackURL: String
  
  init(platformID: String, customerID: String, cardID: String, cardName: String, callBackURL: String) {
    self.platformID = platformID
    self.customerID = customerID
    self.cardID = cardID
    self.cardName = cardName
    self.callBackURL = callBackURL
  }
}
extension BindingCreditCardRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["platformId" : platformID,
            "customerId" : customerID,
            "cardId" : cardID,
            "cardNickname": cardName,
            "returnUrl" : callBackURL]
  }
}
