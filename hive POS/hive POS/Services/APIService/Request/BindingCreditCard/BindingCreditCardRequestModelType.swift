//
//  BindingCreditCardRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 15/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol BindingCreditCardRequestModelType: BaseAPIServiceRequestType {
  var platformID: String { get set }
  var customerID: String { get set }
  var cardID: String { get set }
  var cardName: String { get set }
  var callBackURL: String { get set }
}
