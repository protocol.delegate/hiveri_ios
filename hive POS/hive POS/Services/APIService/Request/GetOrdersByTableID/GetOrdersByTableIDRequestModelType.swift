//
//  GetOrdersByTableIDRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 1/7/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol GetOrdersByTableIDRequestModelType: BaseAPIServiceRequestType {
    var spoonWalkID: String { get set }
    var tableID: String { get set }
}
