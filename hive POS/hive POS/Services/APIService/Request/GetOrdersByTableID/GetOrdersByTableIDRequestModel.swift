//
//  GetOrdersByTableIDRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 1/7/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class GetOrdersByTableIDRequestModel: GetOrdersByTableIDRequestModelType {
  
  var spoonWalkID: String
  var tableID: String
  
  init(spoonWalkID: String, tableID: String) {
    self.spoonWalkID = spoonWalkID
    self.tableID = tableID
  }
}

extension GetOrdersByTableIDRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["restaurant_id" : spoonWalkID,
            "table_id" : tableID]
  }
}
