//
//  UpdateCategoryRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 19/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol UpdateCategoryRequestModelType: BaseAPIServiceRequestType {
  var categoryID: String { get set }
  var categoryName: String { get set }
}
