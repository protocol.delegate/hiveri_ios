//
//  UpdateCategoryRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 19/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class UpdateCategoryRequestModel: UpdateCategoryRequestModelType {

  var categoryID: String
  var categoryName: String
  
  init(categoryID: String, categoryName: String) {
    self.categoryID = categoryID
    self.categoryName = categoryName
  }
}

extension UpdateCategoryRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["id" : categoryID,
            "name" : categoryName]
  }
}
