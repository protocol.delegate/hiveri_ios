//
//  DeliveryOrdersRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 15/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol DeliveryOrdersRequestModelType: BaseAPIServiceRequestType {
  var token: String { get set }
}
