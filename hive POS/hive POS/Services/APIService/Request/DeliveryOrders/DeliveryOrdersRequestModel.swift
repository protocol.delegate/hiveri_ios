//
//  DeliveryOrdersRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 15/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class DeliveryOrdersRequestModel: DeliveryOrdersRequestModelType {
  
  var token: String
  
  init(token: String) {
    self.token = token
  }
}

extension DeliveryOrdersRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["bookingToken" : token]
  }
}
