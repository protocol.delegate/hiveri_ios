//
//  DeleteBranchRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 9/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class DeleteBranchRequestModel: DeleteBranchRequestModelType {
  
  var branchID: Int
  
  init(branchID: Int) {
    self.branchID = branchID
  }
}

extension DeleteBranchRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["id" : branchID]
  }
}
