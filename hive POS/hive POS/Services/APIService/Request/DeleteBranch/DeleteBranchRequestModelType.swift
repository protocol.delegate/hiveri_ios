//
//  DeleteBranchRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 9/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol DeleteBranchRequestModelType: BaseAPIServiceRequestType {
  var branchID: Int { get set }
}
