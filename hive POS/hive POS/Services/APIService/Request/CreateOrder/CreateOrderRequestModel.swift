//
//  CreateOrderRequestModel.swift
//  hive POS
//
//  Created by xAdmin on 23/6/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class CreateOrderRequestModel: CreateOrderRequestModelType {
  var restaurantID: String
  var tableID: String
  var type: String
  var order: [[String : Any]]
  var status: String
  
  init(restaurantID: String, type: String, order: [[String: Any]], status: String, tableID: String) {
    self.restaurantID = restaurantID
    self.type = type
    self.status = status
    self.order = order
    self.tableID = tableID
  }
  
  func toDictionary() -> [String : Any] {
    let params = ["restaurant_id" : restaurantID,
                  "type" : type,
                  "order" : order,
                  "status" : status] as [String : Any]
    let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
    let decoded = String(data: jsonData, encoding: .utf8)!
    print("JSON Params------> ", decoded)
    return params
  }
  
  
}
