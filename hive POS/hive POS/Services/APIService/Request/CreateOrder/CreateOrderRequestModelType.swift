//
//  CreateOrderRequestModelType.swift
//  hive POS
//
//  Created by xAdmin on 23/6/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol CreateOrderRequestModelType: BaseAPIServiceRequestType {
  var restaurantID: String { get set }
  var type: String { get set }
  var order: [[String:Any]] { get set }
  var status: String { get set }
}
