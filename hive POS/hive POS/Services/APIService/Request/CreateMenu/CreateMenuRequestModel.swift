//
//  CreateMenuRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 20/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class CreateMenuRequestModel: CreateMenuRequestModelType {
  var menuNO: String
  var brandID: Int
  var branchID: Int
  var categoryID: String
  var menuImagePath: String
  var nameTH: String
  var nameEN: String
  var detailTH: String
  var detailEN: String
  var price: Double
  var priceType: Int?
  var priceDetail: String
  var menuOption: [Int]
  var menuType: [Int]
  var timeToCook: Int
  var isPublish: Bool
  
  init(menuNO: String, brandID: Int, branchID: Int, categoryID: String, menuImagePath: String, menuNameTH: String, menuNameEN: String, detailTH: String, detailEN: String, price: Double, priceType: Int?, priceDetail: String, menuOption: [Int], menuType: [Int], timeToCook: Int, isPublish: Bool) {
    self.menuNO = menuNO
    self.brandID = brandID
    self.branchID = branchID
    self.categoryID = categoryID
    self.menuImagePath = menuImagePath
    self.nameTH = menuNameTH
    self.nameEN = menuNameEN
    self.detailTH = detailTH
    self.detailEN = detailEN
    self.price = price
    self.priceType = priceType
    self.priceDetail = priceDetail
    self.menuOption = menuOption
    self.menuType = menuType
    self.timeToCook = timeToCook
    self.isPublish = isPublish
  }
  
}

extension CreateMenuRequestModelType {
  func toDictionary() -> [String : Any] {
    var params = ["menu_id" : menuNO,
                  "brand_id" : brandID,
                  "branch_id" : branchID,
                  "category_id" : categoryID,
                  "image" : menuImagePath,
                  "name_th" : nameTH,
                  "name_en" : nameEN,
                  "desc_th" : detailTH,
                  "desc_en" : detailEN,
                  "price" : price,
                  "option" : menuOption,
                  "type" : menuType,
                  "time_to_cook" : timeToCook,
                  "isPublish" : isPublish] as [String : Any]
    
    if let priceType = self.priceType {
      params["price_method_type"] = priceType
    }
    
    if priceDetail != "" {
      params["price_method_detail"]  = priceDetail
    }
    return params
  }
}
