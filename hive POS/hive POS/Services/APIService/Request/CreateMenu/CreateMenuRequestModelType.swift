//
//  CreateMenuRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 20/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol CreateMenuRequestModelType: BaseAPIServiceRequestType {
  var menuNO: String { get set }
  var brandID: Int { get set }
  var branchID: Int { get set }
  var categoryID: String { get set }
  var menuImagePath: String { get set }
  var nameTH: String { get set }
  var nameEN: String { get set }
  var detailTH: String { get set }
  var detailEN: String { get set }
  var price: Double { get set }
  var priceType: Int? { get set }
  var priceDetail: String { get set }
  var menuOption: [Int] { get set }
  var menuType: [Int] { get set }
  var timeToCook: Int { get set }
  var isPublish: Bool { get set }
}
