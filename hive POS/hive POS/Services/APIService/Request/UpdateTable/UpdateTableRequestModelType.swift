//
//  UpdateTableRequestModelType.swift
//  hive POS
//
//  Created by xAdmin on 27/6/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol UpdateTableRequestModelType: BaseAPIServiceRequestType {
  var tableID: String { get set }
  var isEnable: Bool { get set }
}
