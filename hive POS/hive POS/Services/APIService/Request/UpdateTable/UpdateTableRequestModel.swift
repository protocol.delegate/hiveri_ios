//
//  UpdateTableRequestModel.swift
//  hive POS
//
//  Created by xAdmin on 27/6/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class UpdateTableRequestModel: UpdateTableRequestModelType {
  
  var tableID: String
  var isEnable: Bool
  
  init(tableID: String, isEnable: Bool) {
    self.tableID = tableID
    self.isEnable = isEnable
  }
}

extension UpdateTableRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["id" : tableID,
            "has_enable" : isEnable]
  }
}
