//
//  GetOrderByTableRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 25/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol GetOrderByTableRequestModelType: BaseAPIServiceRequestType {
  var tableID: Int { get set }
}
