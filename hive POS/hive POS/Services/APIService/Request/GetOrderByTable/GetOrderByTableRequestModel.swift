//
//  GetOrderByTableRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 25/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class GetOrderByTableRequestModel: GetOrderByTableRequestModelType {
  
  var tableID: Int
  
  init(tableID: Int) {
    self.tableID = tableID
  }
}

extension GetOrderByTableRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["tableId" : tableID]
  }
}
