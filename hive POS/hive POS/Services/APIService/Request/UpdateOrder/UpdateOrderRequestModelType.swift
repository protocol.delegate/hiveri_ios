//
//  UpdateOrderRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 1/6/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol UpdateOrderRequestModelType: BaseAPIServiceRequestType {
  var orderID: Int { get set }
  var personAmount: Int { get set }
  var orderItems: [[String: Any]] { get set }
}
