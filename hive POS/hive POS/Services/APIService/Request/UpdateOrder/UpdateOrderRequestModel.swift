//
//  UpdateOrderRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 1/6/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class UpdateOrderRequestModel: UpdateOrderRequestModelType {
  var orderID: Int
  var personAmount: Int
  var orderItems: [[String : Any]]
  
  init(orderID: Int, personAmount: Int, orderItems: [[String: Any]]) {
    self.orderID = orderID
    self.personAmount = personAmount
    self.orderItems = orderItems
  }
  
  func toDictionary() -> [String : Any] {
    
    let params = ["foodOrderId" : orderID,
                  "personAmount" : personAmount,
                  "foodOrderItems" : orderItems] as [String : Any]
    
    let jsonData = try! JSONSerialization.data(withJSONObject: params, options: [])
    let decoded = String(data: jsonData, encoding: .utf8)!
    print("Update Params------> ", decoded)
    return params
  }
}
