//
//  GetBranchRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 8/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol GetBranchRequestModelType: BaseAPIServiceRequestType {
  var branchID: Int { get set }
}
