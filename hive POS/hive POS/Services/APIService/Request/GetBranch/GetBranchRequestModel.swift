//
//  GetBranchRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 8/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class GetBranchRequestModel: GetBranchRequestModelType {
  var branchID: Int
  
  init(branchID: Int) {
    self.branchID = branchID
  }
}

extension GetBranchRequestModelType {
  func toDictionary() -> [String : Any] {
    return ["id":branchID]
  }
}
