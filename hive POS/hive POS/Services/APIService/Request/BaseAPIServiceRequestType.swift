//
//  BaseAPIServiceRequestType.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol BaseAPIServiceRequestType {
  
  func toDictionary() -> [String: Any]
  
}
