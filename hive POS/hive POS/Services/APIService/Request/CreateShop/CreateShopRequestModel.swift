//
//  CreateShopRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 18/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class CreateShopRequestModel: CreateShopRequestModelType {
  var brandID: Int
  var branchID: Int
  var logoImage: String
  var bannerImage: String
  var name: String
  var branch: String
  var description: String
  var priceSince: Int
  var priceTo: Int
  var phone: [String]
  var website: String
  var facebook: String
  var line: String
  var instagram: String
  var workTime: [[String : Any]]
  var closeTime: [[String : Any]]
  var orderBefore: Int
  var address: [String : Any]
  
  init(brandID: Int, branchID: Int, logoImage: String, bannerImage: String, name: String, branch: String, description: String, priceSince: Int, priceTo: Int, phone: [String], website: String, facebook: String, line: String, instagram: String, workTime: [[String: Any]], closeTime: [[String: Any]], orderBefore: Int, address:[String: Any]){
    self.brandID = brandID
    self.branchID = branchID
    self.logoImage = logoImage
    self.bannerImage = bannerImage
    self.name = name
    self.branch = branch
    self.description = description
    self.priceSince = priceSince
    self.priceTo = priceTo
    self.phone = phone
    self.website = website
    self.facebook = facebook
    self.line = line
    self.instagram = instagram
    self.workTime = workTime
    self.closeTime = closeTime
    self.orderBefore = orderBefore
    self.address = address
  }
  
  func toDictionary() -> [String : Any] {
    
    let params = ["brand_id" : brandID,
    "branch_id" : branchID,
    "logo" : logoImage,
    "cover" : bannerImage,
    "name" : name,
    "branch" : branch,
    "description" : description,
    "price_range_from" : priceSince,
    "price_range_to" : priceTo,
    "phone" : phone,
    "website" : website,
    "facebook" : facebook,
    "line" : line,
    "instagram" : instagram,
    "worktime" : workTime,
    "closetime" : closeTime,
    "order_before" : orderBefore,
    "tags" : [],
    "address" : address] as [String : Any]
    
    print("create shop params ------> ", params)
    return params
  }
}
