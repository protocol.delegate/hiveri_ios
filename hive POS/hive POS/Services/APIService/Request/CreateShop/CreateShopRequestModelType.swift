//
//  CreateShopRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 18/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol CreateShopRequestModelType: BaseAPIServiceRequestType {
  var brandID: Int { get set }
  var branchID: Int { get set }
  var logoImage: String { get set }
  var bannerImage: String { get set }
  var name: String { get set }
  var branch: String { get set }
  var description: String { get set }
  var priceSince: Int { get set }
  var priceTo: Int { get set }
  var phone:[String] { get set }
  var website: String { get set }
  var facebook: String { get set }
  var line: String { get set }
  var instagram: String { get set }
  var workTime: [[String: Any]] { get set }
  var closeTime: [[String: Any]] { get set }
  var orderBefore: Int { get set }
  var address: [String: Any] { get set }
}
