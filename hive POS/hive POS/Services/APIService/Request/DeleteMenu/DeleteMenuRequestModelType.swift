//
//  DeleteMenuRequestModelType.swift
//  hive POS
//
//  Created by Space-C4ts on 21/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

protocol DeleteMenuRequestModelType: BaseAPIServiceRequestType {
  var menuID: String { get set }
}
