//
//  DeleteMenuRequestModel.swift
//  hive POS
//
//  Created by Space-C4ts on 21/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

class DeleteMenuRequestModel: DeleteMenuRequestModelType {
  
  var menuID: String
  
  init(menuID: String) {
    self.menuID = menuID
  }
  
  func toDictionary() -> [String : Any] {
    print("deleteparams----> ", menuID)
    return ["id" : menuID]
  }
}
