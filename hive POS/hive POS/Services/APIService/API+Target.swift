//
//  API+Target.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import Moya
import Firebase
import SwiftyJSON

extension API: TargetType {
  var baseURL: URL {
    switch Constant.developmentState {
    case .development:
      switch self {
      case .deliveryOrders:
        return URL(string: "http://104.248.148.204/delivery")!
      case .bindingCreditCard:
        return URL(string: "http://104.248.148.204")!
      case .creditCardTransection:
        return URL(string: "http://104.248.148.204")!
      case .inqueryCreditCard:
        return URL(string: "http://104.248.148.204")!
      case .createShop:
        return URL(string: "https://devapi.spoonwalk.com")!
      case .createMenuCategory:
        return URL(string: "https://devapi.spoonwalk.com")!
      case .getMenuCategory:
        return URL(string: "https://devapi.spoonwalk.com")!
      case .updateCategory:
        return URL(string: "https://devapi.spoonwalk.com")!
      case .deleteCategory:
        return URL(string: "https://devapi.spoonwalk.com")!
      case .createMenu:
        return URL(string: "https://devapi.spoonwalk.com")!
      case .getMenuList:
        return URL(string: "https://devapi.spoonwalk.com")!
      case .editMenu:
        return URL(string: "https://devapi.spoonwalk.com")!
      case .deleteMenu:
        return URL(string: "https://devapi.spoonwalk.com")!
      case .createOrder(_):
        return URL(string: "https://devapi.spoonwalk.com")!
      case .getTableList(_):
        return URL(string: "https://devapi.spoonwalk.com")!
      case .createMultipleTable(_):
        return URL(string: "https://devapi.spoonwalk.com")!
      case .updateTable(_):
        return URL(string: "https://devapi.spoonwalk.com")!
      default:
        return URL(string: "https://sharp-doodad-226709.appspot.com")!
      }
    case .production:
      switch self {
      case .deliveryOrders:
        return URL(string: "http://104.248.148.204/delivery")!
      case .bindingCreditCard:
        return URL(string: "http://104.248.148.204")!
      case .creditCardTransection:
        return URL(string: "http://104.248.148.204")!
      case .inqueryCreditCard:
        return URL(string: "http://104.248.148.204")!
      case .createShop:
        return URL(string: "https://api.spoonwalk.com")!
      case .createMenuCategory:
        return URL(string: "https://api.spoonwalk.com")!
      case .getMenuCategory:
        return URL(string: "https://api.spoonwalk.com")!
      case .updateCategory:
        return URL(string: "https://api.spoonwalk.com")!
      case .deleteCategory:
        return URL(string: "https://api.spoonwalk.com")!
      case .createMenu:
        return URL(string: "https://api.spoonwalk.com")!
      case .getMenuList:
        return URL(string: "https://api.spoonwalk.com")!
      case .editMenu:
        return URL(string: "https://api.spoonwalk.com")!
      case .deleteMenu:
        return URL(string: "https://api.spoonwalk.com")!
      case .createOrder(_):
        return URL(string: "https://api.spoonwalk.com")!
      case .getTableList(_):
        return URL(string: "https://api.spoonwalk.com")!
      case .createMultipleTable(_):
        return URL(string: "https://api.spoonwalk.com")!
      case .updateTable(_):
        return URL(string: "https://api.spoonwalk.com")!
      case .getOrdersByTableID(_):
        return URL(string: "https://api.spoonwalk.com")!
      default:
        return URL(string: "https://sharp-doodad-226709.appspot.com")!
      }
    }
  }
  
  var path: String {
    switch self {
    case .updateUser(_):
      return "auth/v1/user"
    case .getBrandList(_):
      return "auth/v1/brandList"
    case .getBranchList(_):
      return "auth/v1/branchList"
    case .createBrand(_):
      return "auth/v1/brand"
    case .getBrand(_):
      return "auth/v1/brand"
    case .updateBrand(_):
      return "auth/v1/brand"
    case .getUser(_):
      return "auth/v1/user"
    case .deleteBrand(_):
      return "auth/v1/brand"
    case .createBranch(_):
      return "auth/v1/branchWithOpenDay"
    case .getBranch(_):
      return "auth/v1/branch"
    case .updateBranch(_):
      return "auth/v1/branch"
    case .deleteBranch(_):
      return "auth/v1/branch"
    case .deliveryOrders(_):
      return "v1/book-shipping"
    case .bindingCreditCard(_):
      return "payment/v1/creditcard/binding"
    case .creditCardTransection(_):
      return "payment/v1/creditcard/make-transaction"
    case .inqueryCreditCard(_):
      return "payment/v1/creditcard/inquire-cardtoken"
    case .paymentCash(_):
      return "auth/v1/cashPayment"
    case .createShop(_):
      return "restaurant/create_verify"
    case .createMenuCategory(_):
      return "category/add"
    case .getMenuCategory(_):
      return "category"
    case .updateCategory(_):
      return "category/edit"
    case .deleteCategory(_):
      return "category/delete"
    case .createMenu(_):
      return "menu/add"
    case .getMenuList(_):
      return "menu"
    case .editMenu(_):
      return "menu/edit"
    case .deleteMenu(_):
      return "menu/delete"
    case .createTable(_):
      return "auth/v1/table"
    case .getOrderByTable(_):
      return "auth/v1/tableToOrder"
    case .updateOrder(_):
      return "auth/v1/foodOrder"
    case .createOrder(_):
      return "order/add"
    case .getTableList(_):
      return "table"
    case .createMultipleTable(_):
      return "table/add/multi"
    case .updateTable(_):
      return "table/edit"
    case .getOrdersByTableID(_):
      return "order"
    }
  }
  
  var method: Moya.Method {
    switch self {
    case .updateUser(_):
      return .put
    case .getBrandList(_):
      return .get
    case .getBranchList(_):
      return .get
    case .createBrand(_):
      return .post
    case .getBrand(_):
      return .get
    case .updateBrand(_):
      return .put
    case .getUser(_):
      return .get
    case .deleteBrand(_):
      return .delete
    case .createBranch(_):
      return .post
    case .getBranch(_):
      return .get
    case .updateBranch(_):
      return .put
    case .deleteBranch(_):
      return .delete
    case .deliveryOrders(_):
      return .post
    case .bindingCreditCard(_):
      return .post
    case .creditCardTransection(_):
      return .post
    case .inqueryCreditCard(_):
      return .post
    case .paymentCash(_):
      return .post
    case .createShop(_):
      return .post
    case .createMenuCategory(_):
      return .post
    case .getMenuCategory(_):
      return .get
    case .updateCategory(_):
      return .patch
    case .deleteCategory(_):
      return .delete
    case .createMenu(_):
      return .post
    case .getMenuList(_):
      return .get
    case .editMenu(_):
      return .patch
    case .deleteMenu(_):
      return .delete
    case .createTable(_):
      return .post
    case .getOrderByTable(_):
      return .get
    case .updateOrder(_):
      return .put
    case .createOrder(_):
      return .post
    case .getTableList(_):
      return .get
    case .createMultipleTable(_):
      return .post
    case .updateTable(_):
      return .patch
    case .getOrdersByTableID(_):
      return .get
    }
  }
  
  var sampleData: Data {
    return Data()
  }
  
  var task: Task {
    switch self {
    case .updateUser(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .getBrandList(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: URLEncoding.queryString)
    case .getBranchList(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: URLEncoding.queryString)
    case .createBrand(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .getBrand(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: URLEncoding.queryString)
    case .updateBrand(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .getUser(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: URLEncoding.queryString)
    case .deleteBrand(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .createBranch(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .getBranch(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: URLEncoding.queryString)
    case .updateBranch(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .deleteBranch(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .deliveryOrders(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .bindingCreditCard(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .creditCardTransection(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .inqueryCreditCard(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .paymentCash(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .createShop(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .createMenuCategory(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .getMenuCategory(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: URLEncoding.queryString)
    case .updateCategory(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .deleteCategory(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .createMenu(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .getMenuList(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: URLEncoding.queryString)
    case .editMenu(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .deleteMenu(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .getTableList(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: URLEncoding.queryString)
    case .createTable(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .getOrderByTable(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: URLEncoding.queryString)
    case .updateOrder(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .createOrder(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .createMultipleTable(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .updateTable(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: JSONEncoding.default)
    case .getOrdersByTableID(requestModel: let requestModel):
      return .requestParameters(parameters: requestModel.toDictionary(),
                                encoding: URLEncoding.queryString)
    }
  }
  
  var headers: [String : String]? {
    let userToken = LocalDataService.getUserToken() ?? ""
    print("userToken-----> ", userToken)
    switch self {
    case .updateUser(_):
      return ["Authorization" : "bearer \(userToken)",
              "Secret" : Constant.headerSecret]
    case .getBrandList(_):
      return ["Content-Type": "application/json;charset=UTF-8",
              "Authorization" : "bearer \(userToken)",
              "Secret" : Constant.headerSecret]
    case .getBranchList(_):
      return ["Content-Type": "application/json;charset=UTF-8",
              "Authorization" : "bearer \(userToken)",
              "Secret" : Constant.headerSecret]
    case .createBrand(_):
      return ["Authorization" : "bearer \(userToken)",
              "Secret" : Constant.headerSecret]
    case .getBrand(_):
      return ["Authorization" : "bearer \(userToken)",
              "Content-Type": "application/json;charset=UTF-8",
              "Secret" : Constant.headerSecret]
    case .updateBrand(_):
      return ["Authorization" : "bearer \(userToken)",
              "Secret" : Constant.headerSecret]
    case .getUser(_):
      return ["Authorization" : "bearer \(userToken)",
              "Content-Type": "application/json;charset=UTF-8",
              "Secret" : Constant.headerSecret]
    case .deleteBrand(_):
      return ["Authorization" : "bearer \(userToken)",
              "Secret" : Constant.headerSecret]
    case .createBranch(_):
      return ["Authorization" : "bearer \(userToken)",
              "Secret" : Constant.headerSecret]
    case .getBranch(_):
      return ["Authorization" : "bearer \(userToken)",
              "Content-Type": "application/json;charset=UTF-8",
              "Secret" : Constant.headerSecret]
    case .updateBranch(_):
      return ["Authorization" : "bearer \(userToken)",
              "Secret" : Constant.headerSecret]
    case .deleteBranch(_):
      return ["Authorization" : "bearer \(userToken)",
              "Secret" : Constant.headerSecret]
    case .deliveryOrders(_):
      return ["Authorization" : "bearer \(userToken)",
              "Secret" : Constant.headerSecret]
    case .bindingCreditCard(_):
      return [:]
    case .creditCardTransection(_):
      return [:]
    case .inqueryCreditCard(_):
      return [:]
    case .paymentCash(_):
      return ["Authorization" : "bearer \(userToken)",
              "Secret" : Constant.headerSecret]
    case .createShop(_):
      return [:]
    case .createMenuCategory(_):
      return [:]
    case .getMenuCategory(_):
      return [:]
    case .updateCategory(_):
      return [:]
    case .deleteCategory(_):
      return [:]
    case .createMenu(_):
      return [:]
    case .getMenuList(_):
      return [:]
    case .editMenu(_):
      return [:]
    case .deleteMenu(_):
      return [:]
    case .getTableList(_):
      return [:]
    case .createTable(_):
      return ["Authorization" : "bearer \(userToken)",
              "Secret" : Constant.headerSecret]
    case .getOrderByTable(_):
      return ["Authorization" : "bearer \(userToken)",
              "Content-Type": "application/json;charset=UTF-8",
              "Secret" : Constant.headerSecret]
    case .updateOrder(_):
      return ["Authorization" : "bearer \(userToken)",
              "Secret" : Constant.headerSecret]
    case .createOrder(_):
      return [:]
    case .createMultipleTable(_):
      return [:]
    case .updateTable(_):
      return [:]
    case .getOrdersByTableID(_):
      return [:]
    }
  }
}
