//
//  DevelopmentStateType.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

enum DevelopmentStateType: String {
  case development = "Development"
  case production = "Production"
}
