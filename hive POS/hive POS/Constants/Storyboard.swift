//
//  Storyboard.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
enum Storyboard {
  case main
  case authentication
  case brandBranch
  case menu
  case order
  case delivery
  
  var identifier: String {
    switch self {
    case .main:
      return "Main"
    case .authentication:
      return "Authentication"
    case .brandBranch:
      return "BrandBranch"
    case .menu:
      return "Menu"
    case .delivery:
      return "Delivery"
    case .order:
      return "Order"
    }
  }
}
