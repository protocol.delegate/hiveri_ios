//
//  OrderType.swift
//  hive POS
//
//  Created by xAdmin on 23/6/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

enum OrderType: String {
  case takeAway = "takeaway"
  case dineIn = "dinein"
  case delivery = "delivery"
}
