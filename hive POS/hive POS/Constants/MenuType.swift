//
//  MenuType.swift
//  hive POS
//
//  Created by Space-C4ts on 20/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

enum MenuType: Int {
  case signature = 1
  case season = 2
  case takeTime = 3
}
