//
//  OrderStatusType.swift
//  hive POS
//
//  Created by xAdmin on 23/6/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

enum OrderStatusType: String {
  case confirmOrderPending = "CONFIRM_ORDER_PENDING"
  case findCarPending = "FIND_CAR_PENDING"
  case cookingPending = "COOKING_PENDING"
  case shippingPending = "SHIPPED_PENDING"
  case success = "SUCCESS"
  case issue = "ISSUE"
}
