//
//  Define.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

struct Constant {
  static let developmentState: DevelopmentStateType = .development
  static let headerSecret = "9187029340140912349724912749102979141097459"
}
