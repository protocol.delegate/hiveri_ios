//
//  MenuPriceType.swift
//  hive POS
//
//  Created by Space-C4ts on 20/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation

enum MenuPriceType: Int {
  case normal = 1
  case refill = 2
  case custom = 3
  case perKG = 4
  case buffe = 5
}
