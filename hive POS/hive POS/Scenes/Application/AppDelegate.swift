//
//  AppDelegate.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import RxSwift
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?
  private var appCoordinator: AppCoordinator!
  private let disposeBag = DisposeBag()
  
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
    
    // MARK:- Starscream
    
    WebSocketService.shared.connect()
    
    // MARK:- Notification
    
    registerRemoteNotification(application: application)
    
    // MARK:- Location service
    
    LocationService.shared.didRequestLocationService()
    
    // MARK:- Firebase
    
    FirebaseApp.configure()
    Messaging.messaging().delegate = self
    
    // MARK:- Defaut language
    
    LocalizationManager.share.setDefautLanguage(language: .th)
    
    // MARK:- scene coordinator
    
    window = UIWindow()
    
    appCoordinator = AppCoordinator(window: window!)
    appCoordinator.start()
      .subscribe()
      .disposed(by: disposeBag)
    
    // MARK:- IQKeyboard
  
    IQKeyboardManager.shared.enable = true
    
      
    return true
  }

}

