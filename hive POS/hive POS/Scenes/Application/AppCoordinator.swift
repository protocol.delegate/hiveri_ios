//
//  AppCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift
import Firebase

protocol AppInputsParamsType {
  
}

class AppCoordinator: BaseCoordinator<Void>, AppInputsParamsType {
  
  // MARK: Property
  
  // MARK: Public
  
  override func start() -> Observable<Void> {
    
    let window = self.window
    let authenCoordinator = AuthenticationCoordinator(window: window)
    let mainCoordinator = MainTabBarCoordinator(window: window)
    
    // MARK: Public
    
    Auth.auth().currentUser?.getIDToken(completion: { (token, error) in
      if let userToken = token {
        LocalDataService.setUserToken(token: userToken)
      }
    })
    
    if LocalDataService.getRememberMe() {
      if let user = Auth.auth().currentUser {
        if !user.isAnonymous {
          if user.isEmailVerified {
            return coordinate(to: mainCoordinator)
          } else {
            return coordinate(to: authenCoordinator)
          }
        } else {
          return coordinate(to: authenCoordinator)
        }
      } else {
        return coordinate(to: authenCoordinator)
      }
    } else {
      return coordinate(to: authenCoordinator)
    }
  }
  
  // MARK: Private
  
  // MARK: InputParams
  
  public var inputsParams: AppInputsParamsType { return self }
}
