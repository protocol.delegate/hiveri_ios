//
//  VerifyPhoneNumberCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol VerifyPhoneNumberInputsParamsType {
  var registerData: RegisterData? { get set }
}

class VerifyPhoneNumberCoordinator: BaseCoordinator<Void>, VerifyPhoneNumberInputsParamsType {
  
  // MARK: Property

  var registerData: RegisterData?
  
  // MARK: Public

  override func start() -> Observable<Void> {
    
    let window = self.window
    let viewModel = VerifyPhoneNumberViewModel(registerData: registerData)
    let viewController = VerifyPhoneNumberViewController.initFromStoryboard(name: Storyboard.authentication.identifier)
    viewController.viewModel = viewModel
    
    viewModel
    .coordinates
    .navigateToCondition
    .map{ ConditionCoordinator(window: window,
                               baseViewController: viewController,
                               transitionType: .push,
                               animated: true) }
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
    transition(to: viewController)
    return viewModel.coordinates.navigateBack
    .do(onNext: { [weak self] _ in self?.navigateBack()})
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: VerifyPhoneNumberInputsParamsType { return self }
}
