//
//  VerifyPhoneNumberViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import MMNumberKeyboard
import ZSwiftKit

class VerifyPhoneNumberViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var verifyTitleLabel: UILabel!
  @IBOutlet weak var backButton: UIButton!
  @IBOutlet weak var otp1Label: UILabel!
  @IBOutlet weak var otp1ImageView: UIImageView!
  
  @IBOutlet weak var otp2Label: UILabel!
  @IBOutlet weak var otp2ImageView: UIImageView!
  
  @IBOutlet weak var otp3Label: UILabel!
  @IBOutlet weak var otp3ImageView: UIImageView!
  
  @IBOutlet weak var otp4Label: UILabel!
  @IBOutlet weak var otp4ImageView: UIImageView!
  
  @IBOutlet weak var otp5Label: UILabel!
  @IBOutlet weak var otp5ImageView: UIImageView!
  
  @IBOutlet weak var otp6Label: UILabel!
  @IBOutlet weak var otp6ImageView: UIImageView!
  
  
  @IBOutlet weak var pad1Button: UIButton!
  @IBOutlet weak var pad2Button: UIButton!
  @IBOutlet weak var pad3Button: UIButton!
  @IBOutlet weak var pad4Button: UIButton!
  @IBOutlet weak var pad5Button: UIButton!
  @IBOutlet weak var pad6Button: UIButton!
  @IBOutlet weak var pad7Button: UIButton!
  @IBOutlet weak var pad8Button: UIButton!
  @IBOutlet weak var pad9Button: UIButton!
  @IBOutlet weak var pad0Button: UIButton!
  @IBOutlet weak var padDelButton: UIButton!
  
  @IBOutlet weak var phoneNumberTextField: UITextField!
  @IBOutlet weak var sendOTPButton: UIButton!
  @IBOutlet weak var countryCodeButton: UIButton!
  @IBOutlet weak var countryCodeTextField: UITextField!
  
  @IBOutlet weak var submitButton: UIButton!
  @IBOutlet weak var sendOTPAgainButton: UIButton!
  
  // MARK: Property 
  
  var viewModel: VerifyPhoneNumberViewModelType!
  let disposeBag = DisposeBag()
  var otpNumbers: [String] = []
  var countryCodes = ["+66", "+011"]
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
    setupL10N()
    bindViewModel()
    
  }
  
  func bindViewModel() {
    
    countryCodeButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        let controller = ArrayChoiceTableViewController(self.countryCodes) { [unowned self] (name) in
          self.countryCodeTextField.text = name
        }
        controller.preferredContentSize = CGSize(width: 100, height: 110)
        self.presentPopOverView(controller, sourceView: self.countryCodeTextField)
      })
      .disposed(by: disposeBag)
    
    sendOTPButton.rx.tap
      .filter{ [unowned self] in self.validatePhoneNumberField() }
      .map{ [unowned self] in self.countryCodeTextField.text! + self.phoneNumberTextField.text! }
      .bind(to: viewModel.inputs.sendOTPTrigger)
      .disposed(by: disposeBag)
    
    sendOTPAgainButton.rx.tap
      .filter{ [unowned self] in self.validatePhoneNumberField() }
      .map{ [unowned self] in self.countryCodeTextField.text! + self.phoneNumberTextField.text! }
      .bind(to: viewModel.inputs.sendOTPTrigger)
      .disposed(by: disposeBag)
    
    submitButton.rx.tap
      .filter{ [unowned self] in self.otpNumbers.count == 6 }
      .filter{ [unowned self] in self.validatePhoneNumberField() }
      .map{ [unowned self] in self.otpNumbers.map{$0}.joined() }
      .map{ [unowned self] in (countryCode: self.countryCodeTextField.text!,
                               phoneNumber: self.phoneNumberTextField.text!
                                .replacingOccurrences(of: "-",
                                                      with: ""),
                               verifyCode: $0) }
      .bind(to: viewModel.inputs.submitButtonTrigger)
      .disposed(by: disposeBag)
    
    pad0Button.rx.tap
      .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "0") })
      .disposed(by: disposeBag)
    
    pad1Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "1") })
    .disposed(by: disposeBag)
    
    pad2Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "2") })
    .disposed(by: disposeBag)
    
    pad3Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "3") })
    .disposed(by: disposeBag)
    
    pad4Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "4") })
    .disposed(by: disposeBag)
    
    pad5Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "5") })
    .disposed(by: disposeBag)
    
    pad6Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "6") })
    .disposed(by: disposeBag)
    
    pad7Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "7") })
    .disposed(by: disposeBag)
    
    pad8Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "8") })
    .disposed(by: disposeBag)
    
    pad9Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "9") })
    .disposed(by: disposeBag)
    
    padDelButton.rx.tap
      .subscribe(onNext: { [unowned self] in self.deleteOTP() })
      .disposed(by: disposeBag)
    
    backButton.rx.tap
    .bind(to: viewModel.inputs.navigaBackTrigger)
    .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displaySendOTPSuccess
      .subscribe(onNext: { [unowned self] _ in
        Helper.Alert(view: self,
                     title: "verifyphonenumber_otp_alert_title".localized(),
                     message: "verifyphonenumber_otp_alert_message".localized(),
                     confirmText: "submit".localized())
      })
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayOTPNotMatch
      .subscribe(onNext: { [unowned self] _ in
        Helper.Alert(view: self,
                     title: "verifyphonenumber_otp_notmatch_alert_title".localized(),
                     message: "verifyphonenumber_otp_notmatch_alert_message".localized(),
                     confirmText: "submit".localized())
      })
      .disposed(by: disposeBag)
    
    viewModel.outputs.displayPhoneSignSuccess
      .subscribe(onNext: { [unowned self] _ in
        self.viewModel.coordinates.navigateToCondition.onNext(())
      })
      .disposed(by: disposeBag)
    
    //MARK: Bind loading
    
    viewModel.outputs.loading
      .subscribe(onNext: { [unowned self] isLoading in
        LoadingActivityView.action(in: self.view, isShow: isLoading)})
      .disposed(by: disposeBag)
    
    // MARK: Bind Error
          
    viewModel.error
      .subscribe(onNext: { [unowned self] error in
          Helper.Alert(
              view: self,
              title: error.title,
              message: error.message,
              confirmText: "done".localized())})
      .disposed(by: disposeBag)
  }
  
  func setupUI() {
    
    sendOTPButton.layer.cornerRadius = sendOTPButton.frame.height / 2
    submitButton.layer.cornerRadius = submitButton.frame.height / 2
    
    countryCodeTextField.layer.cornerRadius = 10
    countryCodeTextField.layer.borderWidth = 1
    countryCodeTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    countryCodeTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: countryCodeTextField.frame.height))
    countryCodeTextField.leftViewMode = .always
    countryCodeTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: countryCodeTextField.frame.height))
    countryCodeTextField.rightViewMode = .always
    
    phoneNumberTextField.layer.cornerRadius = 10
    phoneNumberTextField.layer.borderWidth = 1
    phoneNumberTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    phoneNumberTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: phoneNumberTextField.frame.height))
    phoneNumberTextField.leftViewMode = .always
    
    let numberlicKeyboard = MMNumberKeyboard(frame: .zero)
    numberlicKeyboard.allowsDecimalPoint = false
    numberlicKeyboard.delegate = self
    phoneNumberTextField.inputView = numberlicKeyboard
    
  }
  
  func setupL10N() {
    verifyTitleLabel.text = "verifyphonenumber_title".localized()
    sendOTPButton.setTitle("verifyphonenumber_otp_button".localized(), for: .normal)
    sendOTPAgainButton.setTitle("verifyphonenumber_renew_otp".localized(), for: .normal)
    submitButton.setTitle("verifyphonenumber_submit".localized(), for: .normal)
  }
  
  func presentPopOverView(_ controller: UIViewController, sourceView: UIView) {
    let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
    presentationController.sourceView = sourceView
    presentationController.sourceRect = CGRect(x: 50  , y: 44, width: 0, height: 0)
    presentationController.permittedArrowDirections = [.up]
    self.present(controller, animated: true)
  }
  
  func insertOTP(number: String) {
    if otpNumbers.count > 5 { return }
    otpNumbers.append(number)
    setupOTP()
    print(otpNumbers)
  }
  
  func validatePhoneNumberField() -> Bool {
    
    let isPassValidatedCountryCode = validateTextField(text: countryCodeTextField.text!)
    let isPassValidatePhoneNumber = validateTextField(text: phoneNumberTextField.text!)
    
    setUIRequireCountryCode(isRequired: !isPassValidatedCountryCode)
    setUIRequirePhoneNumber(isRequired: !isPassValidatePhoneNumber)
    
    return isPassValidatedCountryCode && isPassValidatePhoneNumber
  }
  
  func setUIRequireCountryCode(isRequired: Bool) {
    countryCodeTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequirePhoneNumber(isRequired: Bool) {
    countryCodeTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func validateTextField(text: String) -> Bool {
    return text != ""
  }
  
  func deleteOTP() {
    print(otpNumbers)
    if otpNumbers.count == 0 { return }
    otpNumbers.remove(at: otpNumbers.count-1)
    setupOTP()
  }
  
  func setupOTP() {
    
    if otpNumbers.count == 1 {
      otp1Label.text = otpNumbers[0]
      otp1Label.isHidden = false
      otp1ImageView.isHidden = true
      
      otp2Label.isHidden = true
      otp2ImageView.isHidden = false
      
      otp3Label.isHidden = true
      otp3ImageView.isHidden = false
      
      otp4Label.isHidden = true
      otp4ImageView.isHidden = false
      
      otp5Label.isHidden = true
      otp5ImageView.isHidden = false
      
      otp6Label.isHidden = true
      otp6ImageView.isHidden = false
    } else if otpNumbers.count == 2 {
      otp1Label.text = otpNumbers[0]
      otp1Label.isHidden = false
      otp1ImageView.isHidden = true
      
      otp2Label.text = otpNumbers[1]
      otp2Label.isHidden = false
      otp2ImageView.isHidden = true
      
      otp3Label.isHidden = true
      otp3ImageView.isHidden = false
      
      otp4Label.isHidden = true
      otp4ImageView.isHidden = false
      
      otp5Label.isHidden = true
      otp5ImageView.isHidden = false
      
      otp6Label.isHidden = true
      otp6ImageView.isHidden = false
    } else if otpNumbers.count == 3 {
      otp1Label.text = otpNumbers[0]
      otp1Label.isHidden = false
      otp1ImageView.isHidden = true
      
      otp2Label.text = otpNumbers[1]
      otp2Label.isHidden = false
      otp2ImageView.isHidden = true
      
      otp3Label.text = otpNumbers[2]
      otp3Label.isHidden = false
      otp3ImageView.isHidden = true
      
      otp4Label.isHidden = true
      otp4ImageView.isHidden = false
      
      otp5Label.isHidden = true
      otp5ImageView.isHidden = false
      
      otp6Label.isHidden = true
      otp6ImageView.isHidden = false
    } else if otpNumbers.count == 4 {
      otp1Label.text = otpNumbers[0]
      otp1Label.isHidden = false
      otp1ImageView.isHidden = true
      
      otp2Label.text = otpNumbers[1]
      otp2Label.isHidden = false
      otp2ImageView.isHidden = true
      
      otp3Label.text = otpNumbers[2]
      otp3Label.isHidden = false
      otp3ImageView.isHidden = true
      
      otp4Label.text = otpNumbers[3]
      otp4Label.isHidden = false
      otp4ImageView.isHidden = true
      
      otp5Label.isHidden = true
      otp5ImageView.isHidden = false
      
      otp6Label.isHidden = true
      otp6ImageView.isHidden = false
    } else if otpNumbers.count == 5 {
      otp1Label.text = otpNumbers[0]
      otp1Label.isHidden = false
      otp1ImageView.isHidden = true
      
      otp2Label.text = otpNumbers[1]
      otp2Label.isHidden = false
      otp2ImageView.isHidden = true
      
      otp3Label.text = otpNumbers[2]
      otp3Label.isHidden = false
      otp3ImageView.isHidden = true
      
      otp4Label.text = otpNumbers[3]
      otp4Label.isHidden = false
      otp4ImageView.isHidden = true
      
      otp5Label.text = otpNumbers[4]
      otp5Label.isHidden = false
      otp5ImageView.isHidden = true
      
      otp6Label.isHidden = true
      otp6ImageView.isHidden = false
    } else if otpNumbers.count == 6 {
      otp1Label.text = otpNumbers[0]
      otp1Label.isHidden = false
      otp1ImageView.isHidden = true
      
      otp2Label.text = otpNumbers[1]
      otp2Label.isHidden = false
      otp2ImageView.isHidden = true
      
      otp3Label.text = otpNumbers[2]
      otp3Label.isHidden = false
      otp3ImageView.isHidden = true
      
      otp4Label.text = otpNumbers[3]
      otp4Label.isHidden = false
      otp4ImageView.isHidden = true
      
      otp5Label.text = otpNumbers[4]
      otp5Label.isHidden = false
      otp5ImageView.isHidden = true
      
      otp6Label.text = otpNumbers[5]
      otp6Label.isHidden = false
      otp6ImageView.isHidden = true
    } else if otpNumbers.count == 0 {
      otp1Label.isHidden = true
      otp1ImageView.isHidden = false
      
      otp2Label.isHidden = true
      otp2ImageView.isHidden = false
      
      otp3Label.isHidden = true
      otp3ImageView.isHidden = false
      
      otp4Label.isHidden = true
      otp4ImageView.isHidden = false
      
      otp5Label.isHidden = true
      otp5ImageView.isHidden = false
      
      otp6Label.isHidden = true
      otp6ImageView.isHidden = false
    }
  }
}

extension VerifyPhoneNumberViewController: MMNumberKeyboardDelegate {
  func numberKeyboard(_ numberKeyboard: MMNumberKeyboard, shouldInsertText text: String) -> Bool {
    if phoneNumberTextField.text?.count ?? 0 <= 8 {
      if phoneNumberTextField.text?.count == 8 {
        guard var str = phoneNumberTextField.text else { return true }
        let indexItem = str.index(str.startIndex, offsetBy: 2)
        str.insert("-", at: indexItem)
        let indexMidItem = str.index(str.startIndex, offsetBy: 6)
        str.insert("-", at: indexMidItem)
        phoneNumberTextField.text = str
        return true
      } else {
        return true
      }
    } else {
      return false
    }
  }
  
  func numberKeyboardShouldDeleteBackward(_ numberKeyboard: MMNumberKeyboard) -> Bool {
    guard var str = phoneNumberTextField.text else { return true }
    str = str.replacingOccurrences(of: "-", with: "")
    phoneNumberTextField.text = str
    return true
  }
}
