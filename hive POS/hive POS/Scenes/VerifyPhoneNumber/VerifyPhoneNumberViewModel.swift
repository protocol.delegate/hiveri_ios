//
//  VerifyPhoneNumberViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa
import Firebase

protocol VerifyPhoneNumberViewModelInputs {
  var navigaBackTrigger: PublishSubject<Void> { get }
  var sendOTPTrigger: PublishSubject<String> { get }
  var submitButtonTrigger: PublishSubject<(countryCode: String, phoneNumber: String, verifyCode: String)> { get }
}

protocol VerifyPhoneNumberViewModelOutputs {
  var loading: BehaviorSubject<Bool> { get }
  var displaySendOTPSuccess: PublishSubject<Void> { get }
  var displayOTPNotMatch: PublishSubject<Void> { get }
  var displayPhoneSignSuccess: PublishSubject<Void> { get }
}

protocol VerifyPhoneNumberViewModelCoordinates {
  var navigateBack: PublishSubject<Void> { get }
  var navigateToCondition: PublishSubject<Void> { get }
}

protocol VerifyPhoneNumberViewModelType: ErrorAlertableViewModelType {
  var inputs: VerifyPhoneNumberViewModelInputs { get }
  var outputs: VerifyPhoneNumberViewModelOutputs { get }
  var coordinates: VerifyPhoneNumberViewModelCoordinates { get }
}

class VerifyPhoneNumberViewModel: VerifyPhoneNumberViewModelType, VerifyPhoneNumberViewModelInputs, VerifyPhoneNumberViewModelOutputs, VerifyPhoneNumberViewModelCoordinates {
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  var verificationID: String?
  
  // MARK: Init
  
  public init(apiService: APIServiceType = APIService(),
              registerData: RegisterData?) {
    
    guard let registerData = registerData else { return }
    
    sendOTPTrigger
      .subscribe(onNext: { phoneNumber in
        Auth.auth().languageCode = LocalizationManager.share.getDefaultLanguage().rawValue
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { [unowned self] (response, error) in
          if let err = error as NSError? {
            if err.code == AuthErrorCode.tooManyRequests.rawValue {
              self.error.onNext((title: "verifyphonenumber_otp_over_title".localized(), message: "verifyphonenumber_otp_over_message".localized()))
            } else {
              self.error.onNext((title: "verifyphonenumber_otp_notmatch_alert_title".localized(), message: "verifyphonenumber_otp_notmatch_alert_message".localized()))
            }
            return
          }
          self.verificationID = response
          self.displaySendOTPSuccess.onNext(())
        }
      })
      .disposed(by:disposeBag)
    
    submitButtonTrigger
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
      .subscribe(onNext: { [unowned self] data in
        guard let ccCode = self.verificationID else {
          self.outputs.loading.onNext(false)
          self.displayOTPNotMatch.onNext(())
          return }
        let credential =
          PhoneAuthProvider
            .provider()
            .credential(withVerificationID: ccCode,
                        verificationCode: data.verifyCode)
        Auth.auth().signIn(with: credential) { (response, error) in
          self.outputs.loading.onNext(false)
          if error == nil {
            self.signOut()
            Auth.auth()
              .createUser(withEmail: registerData.email,
                          password: registerData.password) { (result, error) in
                            self.outputs.loading.onNext(false)
                            if error == nil {
                              guard let user = result?.user else { return }
                              user.sendEmailVerification { (error) in
                                self.outputs.loading.onNext(false)
                                if error == nil {
                                  let ref = Database.database().reference()
                                  ref.child("users").child(user.uid).setValue(
                                    ["firstName": registerData.firstName,
                                     "lastName": registerData.lastName,
                                     "prefix": registerData.prefix,
                                     "countryCode": data.countryCode,
                                     "phoneNumber": data.phoneNumber,
                                     "profileImageId": nil,
                                     "profileImageUrl": nil])
                                  self.displayPhoneSignSuccess.onNext(())
                                } else {
                                  self.error.onNext((title: "verifyphonenumber_otp_notmatch_title".localized(), message: "verifyphonenumber_otp_notmatch_message".localized()))
                                }
                              }
                            } else {
                              self.error.onNext((title: "verifyphonenumber_otp_notmatch_title".localized(), message: "verifyphonenumber_otp_notmatch_message".localized()))
                            }
            }
          } else {
            self.error.onNext((title: "verifyphonenumber_otp_notmatch_title".localized(), message: "verifyphonenumber_otp_notmatch_message".localized()))
          }
        }
      })
      .disposed(by:disposeBag)
    
    navigaBackTrigger
      .bind(to: navigateBack)
      .disposed(by: disposeBag)
    
  }
  
  // MARK: Private
  
  private func signOut() {
    do { try Auth.auth().signOut() } catch {}
  }
  
  // MARK: Input
  
  var navigaBackTrigger = PublishSubject<Void>()
  var sendOTPTrigger = PublishSubject<String>()
  var submitButtonTrigger = PublishSubject<(countryCode: String, phoneNumber: String, verifyCode: String)>()
  
  // MARK: Output
  
  var error = PublishSubject<(title: String, message: String)>()
  var loading = BehaviorSubject<Bool>(value: false)
  var displaySendOTPSuccess = PublishSubject<Void>()
  var displayOTPNotMatch = PublishSubject<Void>()
  var displayPhoneSignSuccess = PublishSubject<Void>()
  
  // MARK: Coordinates
  
  var navigateBack = PublishSubject<Void>()
  var navigateToCondition = PublishSubject<Void>()
  
  // MARK: Input&Output&Coordinates
  
  public var outputs: VerifyPhoneNumberViewModelOutputs { return self }
  public var inputs: VerifyPhoneNumberViewModelInputs { return self }
  public var coordinates: VerifyPhoneNumberViewModelCoordinates { return self }
}
