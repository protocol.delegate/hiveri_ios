//
//  OderManagement + DataSource.swift
//  hive POS
//
//  Created by Space-C4ts on 21/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import RxDataSources

struct DisplayOrderCategoryData {
  var categoryID: String
  var categoryName: String
}

struct SectionOfDisplayOrderCategoryData {
  var header: String
  var items: [Item]
}

extension SectionOfDisplayOrderCategoryData: SectionModelType {
  typealias Item = DisplayOrderCategoryData
  
  init(original: SectionOfDisplayOrderCategoryData, items: [Item]) {
    self = original
    self.items = items
  }
}

struct DisplayOrderMenuData {
  var menuID: String
  var menuName: String
  var menuPrice: Double
  var menuCount: Int
  var total: Double
}

struct SectionOfDisplayOrderMenuData {
  var header: String
  var items: [Item]
}

extension SectionOfDisplayOrderMenuData: SectionModelType {
  typealias Item = DisplayOrderMenuData
  
  init(original: SectionOfDisplayOrderMenuData, items: [Item]) {
    self = original
    self.items = items
  }
}
