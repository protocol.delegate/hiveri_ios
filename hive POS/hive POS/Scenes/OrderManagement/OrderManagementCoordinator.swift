//
//  OrderManagementCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 11/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol OrderManagementInputsParamsType {
    
}

class OrderManagementCoordinator: BaseCoordinator<Void>, OrderManagementInputsParamsType {
  
  // MARK: Property

  // MARK: Public

  override func start() -> Observable<Void> {
    let window = self.window
    let viewModel = OrderManagementViewModel()
    let viewController = OrderManagementViewController.initFromStoryboard(name: Storyboard.order.identifier)
    viewController.viewModel = viewModel
    
    transition(to: viewController)
    return .never()
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: OrderManagementInputsParamsType { return self }
}
