//
//  OrderManagementViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 11/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZSwiftKit
import RxDataSources

class OrderManagementViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var tableButton: UIButton!
  @IBOutlet weak var tableTextField: UITextField!
  @IBOutlet weak var customerButton: UIButton!
  @IBOutlet weak var menuView: UIView!
  @IBOutlet weak var listView: UIView!
  @IBOutlet weak var customerLabel: UILabel!
  @IBOutlet weak var customerTextFeld: UITextField!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var foodListLabel: UILabel!
  @IBOutlet weak var foodTableView: UITableView!
  @IBOutlet weak var priceTitleLabel: UILabel!
  @IBOutlet weak var priceUnitLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  
  @IBOutlet weak var billButton: UIButton!
  @IBOutlet weak var receiptButton: UIButton!
  @IBOutlet weak var sendOrderButton: UIButton!
  
  @IBOutlet weak var menuTabCollectionView: UICollectionView!
  @IBOutlet weak var menuCollectionView: UICollectionView!
  
  // MARK: Property
  
  var viewModel: OrderManagementViewModelType!
  var timer: Disposable?
  let disposeBag = DisposeBag()
  var selectCategory: String = ""
  var selectMenu: [DisplayOrderMenuData] = []
  var selectTableID: String = ""
  var tableData:[DisplayTableData] = []
  
  // MARK: View Life Cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupL10N()
    bindViewModel()
  }
  
  override func viewWillDisappear(_ animated: Bool) {
    super.viewWillDisappear(animated)
    timer?.dispose()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.setNavigationBarItem(titlePageName: "createorder_title".localized())
    timer?.dispose()
    timer = Observable<Int>.interval(0.1, scheduler: MainScheduler.instance)
      .subscribe(onNext: { [unowned self] _ in
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        self.timeLabel.text = dateString
      })
    
    if let branchData = BranchSignService.shared.getBranchData() {
      viewModel
        .inputs
        .getMenuCategoryListTrigger
        .onNext(branchData.spoonWalkID)
      if let brandData = LocalDataService.getSelectBrand() {
        viewModel.inputs.getTableListTrigger
          .onNext((spoonWalkID: branchData.spoonWalkID,
                   brandID: brandData.ID))
      }
      
    }
    
  }
  
  func bindViewModel() {
    
    customerButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        let customerData = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20"]
        let controller = ArrayChoiceTableViewController(customerData) { [unowned self] (customer) in
          self.customerTextFeld.text = customer
        }
        controller.tableView.separatorStyle = .none
        controller.preferredContentSize = CGSize(width:  100, height: 300)
        self.presentPopOverView(controller, sourceView: self.customerTextFeld)})
      .disposed(by: disposeBag)
    
    
    let dataSource = RxCollectionViewSectionedReloadDataSource<SectionOfDisplayOrderCategoryData>(
      configureCell: { dataSource, collectionView, indexPath, item in
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuCategoryCollectionViewCell.className, for: indexPath) as! MenuCategoryCollectionViewCell
        cell.configure(categoryData: DisplayMenuCategoryData(categoryID: item.categoryID,
                                                             categoryName: item.categoryName),
                       isSelect: self.selectCategory == item.categoryID)
        return cell})
    
    viewModel
      .outputs
      .displayCategoryData
      .bind(to: menuTabCollectionView.rx.items(dataSource: dataSource))
      .disposed(by: disposeBag)
    
    viewModel.outputs
      .displayCategory
      .subscribe(onNext: { [unowned self] data in
        if self.selectCategory == "" && data.count > 0 {
          self.selectCategory = data[0].categoryID
          if let branchData = BranchSignService.shared.getBranchData() {
            self.viewModel
              .inputs
              .getMenuCategoryListTrigger
              .onNext(branchData.spoonWalkID)
          }
        }
      })
      .disposed(by: disposeBag)
    
    menuTabCollectionView.rx
      .modelSelected(DisplayOrderCategoryData.self)
      .subscribe(onNext: { [unowned self] model in
        self.selectCategory = model.categoryID
        if let branchData = BranchSignService.shared.getBranchData() {
          self.viewModel.inputs.getMenuCategoryListTrigger.onNext(branchData.spoonWalkID)
        }
      })
      .disposed(by: disposeBag)
    
    viewModel.outputs
      .displayCategoryElement
      .subscribe(onNext: { [unowned self] model in
        if self.selectCategory == "" && model.count > 0 {
          self.selectCategory = model[0].categoryID
          self.viewModel.inputs.getMenuListTrigger.onNext(self.selectCategory)
        } else if self.selectCategory != "" {
          self.viewModel.inputs.getMenuListTrigger.onNext(self.selectCategory)
        }
      })
      .disposed(by: disposeBag)
    
    let menuDataSource = RxCollectionViewSectionedReloadDataSource<SectionOfDisplayMenuData>(
      configureCell: { [unowned self] dataSource, collectionView, indexPath, item in
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OrderCollectionViewCell.className, for: indexPath) as! OrderCollectionViewCell
        let filter = self.selectMenu.filter({ $0.menuID == item.menuID })
        cell.configure(name: item.nameTH, price: item.price, imagePath: item.imagePath, isSelect: filter.count > 0)
        return cell})
    
    viewModel.outputs.displayMenuList
      .bind(to: menuCollectionView.rx.items(dataSource: menuDataSource))
      .disposed(by: disposeBag)
    
    menuCollectionView.rx
      .modelSelected(DisplayMenuData.self)
      .subscribe(onNext: { [unowned self] model in
        let filter = self.selectMenu.filter({ $0.menuID == model.menuID })
        if filter.count > 0 { return }
        let selectMenuData = DisplayOrderMenuData(menuID: model.menuID,
                                                  menuName: model.nameTH,
                                                  menuPrice: model.price,
                                                  menuCount: 1,
                                                  total: model.price)
        self.selectMenu.append(selectMenuData)
        self.viewModel.inputs.getMenuListTrigger.onNext(self.selectCategory)
        self.viewModel.inputs.getSelectMenuTrigger.onNext(self.selectMenu)
        self.priceLabel.text = String(format: "%.2f", self.calculateTotalPrice())
        self.foodListLabel.text = "createorder_foodlist".localized() + "(\(self.selectMenu.count))"
      })
      .disposed(by: disposeBag)
    
    let orderDataSource = RxTableViewSectionedReloadDataSource<SectionOfDisplayOrderMenuData>(
    configureCell: { [unowned self] dataSource, tableView, indexPath, item in
      let cell = tableView.dequeueReusableCell(withIdentifier: OrderMenuTableViewCell.className) as! OrderMenuTableViewCell
      cell.configure(menuID: item.menuID,
                     name: item.menuName,
                     totalPrice: Double(item.menuCount) * item.menuPrice,
                     count: item.menuCount,
                     status: "createorder_status_receive".localized(),
                     viewController: self)
      cell.delegate = self
      return cell})
    
    viewModel
      .outputs
      .displaySelectMenu
      .bind(to: foodTableView.rx.items(dataSource: orderDataSource))
      .disposed(by: disposeBag)
    
    viewModel.outputs
      .displaySelectMenu
      .subscribe(onNext: { [unowned self] _ in
        if self.selectMenu.count > 0 {
          self.sendOrderButton.backgroundColor = UIColor(hex: 0x39D639)
        } else {
          self.sendOrderButton.backgroundColor = UIColor(hex: 0xD7DAE2)
          
        }
      })
      .disposed(by: disposeBag)
    
    //MARK: Create table flow
    
    viewModel.outputs.displayTableList
      .subscribe(onNext: { [unowned self] model in
        print("model---> ", model)
        if model.count == 0 {
          if let brandData = LocalDataService.getSelectBrand() {
            if let branchData = BranchSignService.shared.getBranchData() {
              self.viewModel.inputs.createTableTrigger.onNext((spoonWalkID: branchData.spoonWalkID,
                                                               brandID: brandData.ID,
                                                               branchID: branchData.ID,
                                                               capacity: 4,
                                                               amount: 20))
            }
          }
        } else {
          if self.selectTableID == "" {
            self.selectTableID = model[0].tableID
            self.tableTextField.text = model[0].tableName
          }
        }
//          if let branchData = BranchSignService.shared.getBranchData() {
//            for index in 1...20 {
//              print("createTable index ----> ", index)
//              self.viewModel
//                .inputs
//                .createTableTrigger
//                .onNext((branchID: branchData.ID,
//                         tableName: "createorder_table".localized() + " \(index)"))
//            }
//          }
//        } else {
//          if self.selectTableID == nil {
//            self.selectTableID = model[0].tableID
//            self.tableTextField.text = model[0].tableName
//          }
          self.tableData = model
//        }
      })
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayCreateTableSuccess
      .subscribe(onNext: { [unowned self] _ in
        if let branchData = BranchSignService.shared.getBranchData() {
          if let brandData = LocalDataService.getSelectBrand() {
            self.viewModel.inputs.getTableListTrigger.onNext((spoonWalkID: branchData.spoonWalkID,
                                                              brandID: brandData.ID))
          }
        }
      })
      .disposed(by: disposeBag)
    
    tableButton.rx.tap
      .subscribe(onNext: { _ in
        if self.tableData.count == 0 { return }
        let controller = ArrayChoiceOrderTableTableViewController<Any>(self.tableData)
        { [unowned self] (table) in
          self.selectTableID = table.tableID
          self.tableTextField.text = table.tableName
        }
        controller.preferredContentSize = CGSize(width:  200, height: 200)
        self.presentPopOverView(controller, sourceView: self.tableTextField)
      })
      .disposed(by: disposeBag)
    
    sendOrderButton.rx.tap.filter{ [unowned self] _ in self.selectMenu.count > 0 }
      .subscribe(onNext: { [unowned self] _ in
        guard let branchData = BranchSignService.shared.getBranchData() else { return }
        self.viewModel.inputs
          .createOrderTrigger.onNext((restaurantID: branchData.spoonWalkID,
                                      tableID: self.selectTableID,
                                      status: OrderStatusType.cookingPending.rawValue,
                                      orderType: OrderType.dineIn.rawValue,
                                      menuData: self.selectMenu,
                                      personAmount: Int(self.customerTextFeld.text ?? "1") ?? 1))
      })
      .disposed(by: disposeBag)
    
    billButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.tabBarController?.selectedIndex = 4
      })
      .disposed(by: disposeBag)
    
    viewModel.outputs.displayCreateOrderSuccess
      .subscribe(onNext: { [unowned self] _ in
        self.selectMenu = []
        if let branchData = BranchSignService.shared.getBranchData() {
          if let brandData = LocalDataService.getSelectBrand() {
            self.selectTableID = ""
            self.tableData = []
            self.tableTextField.text = ""
            self.viewModel.inputs.getTableListTrigger.onNext((spoonWalkID: branchData.spoonWalkID,
                                                              brandID: brandData.ID))
            self.selectMenu = []
            self.viewModel.inputs.getSelectMenuTrigger.onNext(self.selectMenu)
            self.customerTextFeld.text = "1"
            self.viewModel.inputs.getMenuCategoryListTrigger.onNext(branchData.spoonWalkID)
          }
        }
      })
      .disposed(by: disposeBag)
    
    //MARK: Bind loading
    
    viewModel.outputs.loading
      .subscribe(onNext: { [unowned self] isLoading in
        guard let tabBarController = self.tabBarController else { return }
        LoadingActivityView.action(in: tabBarController.view, isShow: isLoading)})
      .disposed(by: disposeBag)
    
    // MARK: Bind Error
    
    viewModel.error
      .subscribe(onNext: { [unowned self] error in
        Helper.Alert(
          view: self,
          title: error.title,
          message: error.message,
          confirmText: "done".localized())})
      .disposed(by: disposeBag)
  }
  
  func setupUI() {
    listView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
    menuView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
    billButton.layer.cornerRadius = billButton.frame.height / 2
    receiptButton.layer.cornerRadius = receiptButton.frame.height / 2
    sendOrderButton.layer.cornerRadius = sendOrderButton.frame.height / 2
    customerTextFeld.layer.cornerRadius = 10
    customerTextFeld.layer.borderWidth = 1
    customerTextFeld.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    customerTextFeld.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: customerTextFeld.frame.height))
    customerTextFeld.leftViewMode = .always
    customerTextFeld.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: customerTextFeld.frame.height))
    customerTextFeld.rightViewMode = .always
    
    tableTextField.layer.cornerRadius = 10
    tableTextField.layer.borderWidth = 1
    tableTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    tableTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: tableTextField.frame.height))
    tableTextField.leftViewMode = .always
    tableTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: tableTextField.frame.height))
    tableTextField.rightViewMode = .always
    
    if let collectionViewLayout = menuTabCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      collectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      collectionViewLayout.scrollDirection = .horizontal
    }
    self.foodTableView.register(UINib(nibName: OrderMenuTableViewCell.className, bundle: nil), forCellReuseIdentifier: OrderMenuTableViewCell.className)
  }
  
  func setupL10N() {
    
    customerLabel.text = "createorder_customer".localized()
    foodListLabel.text = "createorder_foodlist".localized() + "(0)"
    billButton.setTitle("createorder_bill".localized(), for: .normal)
    receiptButton.setTitle("createorder_receipt".localized(), for: .normal)
    sendOrderButton.setTitle("createorder_sendorder".localized(), for: .normal)
    priceTitleLabel.text = "createorder_pricetotal".localized()
    priceLabel.text = "0.00"
    timeLabel.text = "00:00"
    priceUnitLabel.text = "createorder_pricetotal_unit".localized()
    customerTextFeld.text = "1"
    
    menuCollectionView.register(UINib(nibName: OrderCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: OrderCollectionViewCell.className)
  }
  
  func presentPopOverView(_ controller: UIViewController, sourceView: UIView) {
    let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
    presentationController.sourceView = sourceView
    presentationController.sourceRect = CGRect(x: 50  , y: 44, width: 0, height: 0)
    presentationController.permittedArrowDirections = [.up]
    self.present(controller, animated: true)
  }
  
  func calculateTotalPrice() -> Double {
    let prices = selectMenu.map{ $0.total }
    var totalPrice: Double = 0
    for price in prices {
      totalPrice += price
    }
    return totalPrice
  }
}

extension OrderManagementViewController: OrderMenuTableViewCellDelegate {
  func didClickOnPlus(menuID: String, count: Int) {
    if selectMenu.count > 0 {
      for (index, menu) in self.selectMenu.enumerated() {
        if menu.menuID == menuID {
          let modifySource = DisplayOrderMenuData(menuID: menu.menuID,
                                                  menuName: menu.menuName,
                                                  menuPrice: menu.menuPrice,
                                                  menuCount: count,
                                                  total: menu.menuPrice * Double(count))
          self.selectMenu.remove(at: index)
          self.selectMenu.insert(modifySource, at: index)
          self.viewModel.inputs.getSelectMenuTrigger.onNext(self.selectMenu)
          self.priceLabel.text = String(format: "%.2f", calculateTotalPrice())
        }
      }
    }
  }
  
  func didClickOnMinus(menuID: String, count: Int) {
    if selectMenu.count > 0 {
      for (index, menu) in self.selectMenu.enumerated() {
        if menu.menuID == menuID {
          let modifySource = DisplayOrderMenuData(menuID: menu.menuID,
                                                  menuName: menu.menuName,
                                                  menuPrice: menu.menuPrice,
                                                  menuCount: count,
                                                  total: menu.menuPrice * Double(count))
          self.selectMenu.remove(at: index)
          self.selectMenu.insert(modifySource, at: index)
          self.viewModel.inputs.getSelectMenuTrigger.onNext(self.selectMenu)
          self.priceLabel.text = String(format: "%.2f", calculateTotalPrice())
        }
      }
    }
  }
  
  func didClickOnCancel(menuID: String) {
    if selectMenu.count > 0 {
      for (index, menu) in self.selectMenu.enumerated() {
        if menu.menuID == menuID {
          self.selectMenu.remove(at: index)
          self.viewModel.inputs.getSelectMenuTrigger.onNext(self.selectMenu)
          self.priceLabel.text = String(format: "%.2f", calculateTotalPrice())
          self.menuCollectionView.reloadData()
          self.foodListLabel.text = "createorder_foodlist".localized() + "(\(self.selectMenu.count))"
        }
      }
    }
  }
}
