//
//  OrderMenuTableViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 25/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

protocol OrderMenuTableViewCellDelegate: class {
  func didClickOnPlus(menuID: String, count: Int)
  func didClickOnMinus(menuID: String, count: Int)
  func didClickOnCancel(menuID: String)
}

class OrderMenuTableViewCell: UITableViewCell {
  
  @IBOutlet weak var cellBackgroundView: UIView!
  @IBOutlet weak var menuName: UILabel!
  @IBOutlet weak var plusButton: UIButton!
  @IBOutlet weak var minusButton: UIButton!
  @IBOutlet weak var menuCount: UILabel!
  @IBOutlet weak var menuPrice: UILabel!
  @IBOutlet weak var menuButton: UIButton!
  @IBOutlet weak var status: UILabel!
  @IBOutlet weak var statusView: UIView!
  
  var count = 1
  var menuID: String?
  var viewController: UIViewController?
  var delegate: OrderMenuTableViewCellDelegate?
  
  override func awakeFromNib() {
    super.awakeFromNib()
    self.statusView.layer.cornerRadius = 5
    self.cellBackgroundView.layer.cornerRadius = 10
    
    
  }
  
  override func setSelected(_ selected: Bool, animated: Bool) {
    super.setSelected(selected, animated: animated)
    
    // Configure the view for the selected state
  }
  
  func configure(menuID: String, name: String, totalPrice: Double, count: Int, status: String, viewController: UIViewController) {
    self.menuName.text = name
    self.menuPrice.text = String(format: "%.2f", totalPrice)
    self.menuCount.text = "\(count)"
    self.status.text = status
    self.cellBackgroundView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
    self.count = count
    self.menuID = menuID
    self.viewController = viewController
  }
  
  
  @IBAction func didClickOnMinus(_ sender: Any) {
    guard let menuID = self.menuID else { return }
    if count == 1 {
      delegate?.didClickOnCancel(menuID: menuID)
      return
    }
    self.count -= 1
    delegate?.didClickOnMinus(menuID: menuID, count: count)
  }
  
  @IBAction func didClickOnPlus(_ sender: Any) {
    guard let menuID = self.menuID else { return }
    if count > 99 { return }
    self.count += 1
    delegate?.didClickOnPlus(menuID: menuID, count: count)
  }
  
  @IBAction func didClickOnMenu(_ sender: Any) {
    self.uiCallPopup(UIButton(type: .custom), sender)
  }
  
  private func uiCallPopup(_ button: UIButton, _ sender: Any ) {
    let popView = PopOverMenuViewController(nibName: PopOverMenuViewController.className, bundle: nil)
    popView.delegate = self
    popView.preferredContentSize = CGSize(width: 220, height: 44)
    uiShowPopup(popView, sourceView: menuButton)
    
  }
  
  private func uiShowPopup(_ controller: UIViewController, sourceView: UIButton) {
    let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
    presentationController.sourceView = sourceView
    presentationController.sourceRect = sourceView.bounds
    presentationController.permittedArrowDirections = [.left]
    self.viewController?.present(controller, animated: true)
  }
}

extension OrderMenuTableViewCell: PopOverMenuViewControllerDelegate {
  func didCickOnCancel() {
    guard let menuID = self.menuID else { return }
    delegate?.didClickOnCancel(menuID: menuID)
  }
}
