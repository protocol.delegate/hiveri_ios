//
//  OrderCollectionViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 22/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

class OrderCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var menuBackgroundView: UIView!
  @IBOutlet weak var menuImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var menuSelectButton: UIButton!
  
  
  func configure(name: String, price: Double, imagePath: String, isSelect: Bool) {
    nameLabel.text = name
    priceLabel.text = String(format: "%.2f", price) + " ฿"
    menuImageView.kf.setImage(with: URL(string: imagePath), placeholder: UIImage(named: "ic_default_image"))
    menuBackgroundView.layer.cornerRadius = 10
    menuSelectButton.isSelected = isSelect
  }
}
