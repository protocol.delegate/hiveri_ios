//
//  PopOverMenuViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 25/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

protocol PopOverMenuViewControllerDelegate: class {
  func didCickOnCancel()
}

class PopOverMenuViewController: UIViewController {

  @IBOutlet weak var cancelButton: UIButton!
  
  var menuID: String?
  var delegate: PopOverMenuViewControllerDelegate?
  
  override func viewDidLoad() {
        super.viewDidLoad()
    cancelButton.setTitle("createorder_cancel_order".localized(), for: .normal)
    
  }
  @IBAction func didClickOnCancel(_ sender: Any) {
    delegate?.didCickOnCancel()
    self.dismiss(animated: true, completion: nil)
  }
  
}
