//
//  OrderManagementViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 11/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa

struct DisplayTableData {
  var tableID: String
  var tableName: String
  var isEnable: Bool
}

protocol OrderManagementViewModelInputs {
  var getMenuCategoryListTrigger: PublishSubject<String> { get }
  var getMenuListTrigger: PublishSubject<String> { get }
  var getSelectMenuTrigger: PublishSubject<[DisplayOrderMenuData]> { get }
  var getTableListTrigger: PublishSubject<(spoonWalkID: String, brandID: Int)> { get }
  var createTableTrigger: PublishSubject<(spoonWalkID: String, brandID: Int, branchID: Int, capacity: Int, amount: Int)> { get }
  var createOrderTrigger: PublishSubject<(restaurantID: String, tableID: String, orderType: String, status: String, menuData: [DisplayOrderMenuData], personAmount: Int)> { get }
}

protocol OrderManagementViewModelOutputs {
  var loading: BehaviorSubject<Bool> { get }
  var displayCategoryData: PublishSubject<[SectionOfDisplayOrderCategoryData]> { get }
  var displayCategoryElement: PublishSubject<[MenuCategoryModel]> { get }
  var displayMenuList: PublishSubject<[SectionOfDisplayMenuData]> { get }
  var displaySelectMenu: PublishSubject<[SectionOfDisplayOrderMenuData]> { get }
  var displayTableList: PublishSubject<[DisplayTableData]> { get }
  var displayCreateTableSuccess: PublishSubject<Void> { get }
  var displayUpdateOrderSuccess: PublishSubject<Void> { get }
  var displayCreateOrderSuccess: PublishSubject<Void> { get }
  var displayCategory: PublishSubject<[DisplayMenuCategoryData]> { get }
}

protocol OrderManagementViewModelCoordinates {
  
}

protocol OrderManagementViewModelType: ErrorAlertableViewModelType {
  var inputs: OrderManagementViewModelInputs { get }
  var outputs: OrderManagementViewModelOutputs { get }
  var coordinates: OrderManagementViewModelCoordinates { get }
}

class OrderManagementViewModel: OrderManagementViewModelType, OrderManagementViewModelInputs, OrderManagementViewModelOutputs, OrderManagementViewModelCoordinates {
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  
  // MARK: Init
  
  public init(apiService: APIServiceType = APIService()) {
    
    let getMenuCategoryListTriggerSignal =
      getMenuCategoryListTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ GetMenuCategoryRequestModel(shopID: $0) }
        .flatMapLatest{ apiService.rx_getMenuCategory(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    getMenuCategoryListTriggerSignal
      .elements()
      .map{ model -> [SectionOfDisplayOrderCategoryData] in
        let categoryData = model.map{DisplayOrderCategoryData(categoryID: $0.categoryID,
                                                              categoryName: $0.categoryName)}
        return [SectionOfDisplayOrderCategoryData(header: "", items: categoryData)]}
      .bind(to: displayCategoryData)
      .disposed(by: disposeBag)
    
    getMenuCategoryListTriggerSignal
      .elements()
      .map{ $0.map{ DisplayMenuCategoryData(categoryID: $0.categoryID,
                                            categoryName: $0.categoryName) } }
      .bind(to: displayCategory)
      .disposed(by: disposeBag)
    
    getMenuCategoryListTriggerSignal
      .elements()
      .bind(to: displayCategoryElement)
      .disposed(by: disposeBag)
    
    let getMenuListTriggerSignal = getMenuListTrigger
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
      .map{ GetMenuListRequestModel(categoryID: $0) }
      .flatMapLatest{ apiService.rx_getMenuList(requestModel: $0).materialize() }
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
      .share(replay: 1)
    
    getMenuListTriggerSignal
      .elements()
      .map{ model -> [SectionOfDisplayMenuData] in
        let filterMenuData = model.filter({ $0.isPublish == true })
        let menuData = filterMenuData.map{ DisplayMenuData(menuID: $0.menuID,
                                                           brandID: $0.brandID,
                                                           branchID: $0.branchID,
                                                           menuNumber: $0.menuNO,
                                                           categoryID: $0.categoryID,
                                                           imagePath: $0.imagePath,
                                                           nameTH: $0.nameTH,
                                                           nameEN: $0.nameEN,
                                                           detailTH: $0.detailTH,
                                                           detailEN: $0.detailEN,
                                                           price: $0.price,
                                                           priceType: $0.priceType,
                                                           priceDetail: "\($0.priceDetail)",
          menuOption: $0.menuOption,
          menuType: $0.menuType,
          timeToCook: $0.timeToCook,
          isPublish: $0.isPublish) }
        return [SectionOfDisplayMenuData(header: "", items: menuData)]}
      .bind(to: displayMenuList)
      .disposed(by: disposeBag)
    
    getSelectMenuTrigger
      .map{ [SectionOfDisplayOrderMenuData(header: "", items: $0)] }
      .bind(to: displaySelectMenu)
      .disposed(by: disposeBag)
    
    let getTableListTiggerSignal =
      getTableListTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ GetTableListRequestModel(spoobWalkID: $0.spoonWalkID, brandID: $0.brandID)}
        .flatMapLatest{ apiService.rx_getTableList(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    getTableListTiggerSignal
      .elements()
      .map{ $0.map { DisplayTableData(tableID: $0.tableID,
                                      tableName: $0.tableName,
                                      isEnable: $0.isEnable)}}
//      .map{
//        let sort = $0.sorted(by: { Int($0.tableName.split(separator: " ")[1]) ?? 0 < Int($1.tableName.split(separator: " ")[1]) ?? 0 })
//        return sort}
      .map { $0.filter{ $0.isEnable == true }}
      .bind(to: displayTableList)
      .disposed(by: disposeBag)
    
    let createTableTriggerSignal =
      createTableTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map { CreateMultipleTableRequestModel(brandID: $0.brandID,
                                               branchID: $0.branchID,
                                               spoonWalkID: $0.spoonWalkID,
                                               capacity: $0.capacity,
                                               amount: $0.amount) }
        .flatMapLatest{ apiService.rx_createMultipleTable(requestModel: $0)
        .materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    createTableTriggerSignal
      .elements()
      .bind(to: displayCreateTableSuccess)
      .disposed(by: disposeBag)
    
    let createOrderTriggerSignal =
      createOrderTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{
          CreateOrderRequestModel(restaurantID: $0.restaurantID,
                                  type: $0.orderType,
                                  order: $0.menuData.map{ ["id": $0.menuID,
                                                           "name_th" : $0.menuName,
                                                           "price" : $0.menuPrice,
                                                           "amount" : $0.menuCount ] },
                                  status: $0.status,
                                  tableID: $0.tableID) }
        .flatMapLatest{apiService.rx_createOrder(requestModel: $0)
          .materialize()}
        .share(replay: 1)
    
    let updateTableTriggerSignal = createOrderTrigger
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
      .map{ UpdateTableRequestModel(tableID: $0.tableID,
                                    isEnable: false) }
      .flatMapLatest{ apiService.rx_updateTable(requestModel: $0).materialize() }
      .share(replay: 1)
    
    Observable.zip(createOrderTriggerSignal.elements(),
                   updateTableTriggerSignal.elements())
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
      .map{ _ in () }.bind(to: displayCreateOrderSuccess)
      .disposed(by: disposeBag)
    
    // Error ---------------------------------------------------------------
    
    let errorSignal = Observable
      .merge(getMenuCategoryListTriggerSignal.errors(),
             getMenuListTriggerSignal.errors(),
             getTableListTiggerSignal.errors(),
             createOrderTriggerSignal.errors(),
             updateTableTriggerSignal.errors())
       .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
      .share(replay: 1)
    
    errorSignal
      .map(errorToAlertable)
      .bind(to: error)
      .disposed(by: disposeBag)
    
  }
  
  // MARK: Private
  
  // MARK: Input
  
  var getMenuCategoryListTrigger = PublishSubject<String>()
  var getMenuListTrigger = PublishSubject<String>()
  var getSelectMenuTrigger = PublishSubject<[DisplayOrderMenuData]>()
  var getTableListTrigger = PublishSubject<(spoonWalkID: String, brandID: Int)>()
  var createTableTrigger = PublishSubject<(spoonWalkID: String, brandID: Int, branchID: Int, capacity: Int, amount: Int)>()
  var createOrderTrigger = PublishSubject<(restaurantID: String, tableID: String, orderType: String, status: String, menuData: [DisplayOrderMenuData], personAmount: Int)>()
  var updateTable = PublishSubject<Void>()
  
  // MARK: Output
  
  var displayCategoryData = PublishSubject<[SectionOfDisplayOrderCategoryData]>()
  var displayCategoryElement = PublishSubject<[MenuCategoryModel]>()
  var displayMenuList = PublishSubject<[SectionOfDisplayMenuData]>()
  var displaySelectMenu = PublishSubject<[SectionOfDisplayOrderMenuData]>()
  var displayTableList = PublishSubject<[DisplayTableData]>()
  var displayCreateTableSuccess = PublishSubject<Void>()
  var error = PublishSubject<(title: String, message: String)>()
  var loading = BehaviorSubject<Bool>(value: false)
  var displayUpdateOrderSuccess = PublishSubject<Void>()
  var displayCreateOrderSuccess = PublishSubject<Void>()
  var displayCategory = PublishSubject<[DisplayMenuCategoryData]>()
  
  // MARK: Coordinates
  
  // MARK: Input&Output&Coordinates
  
  public var outputs: OrderManagementViewModelOutputs { return self }
  public var inputs: OrderManagementViewModelInputs { return self }
  public var coordinates: OrderManagementViewModelCoordinates { return self }
}
