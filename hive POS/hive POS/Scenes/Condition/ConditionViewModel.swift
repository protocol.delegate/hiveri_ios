//
//  ConditionViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 23/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa

protocol ConditionViewModelInputs {
  var submitButtonTrigger: PublishSubject<Void> { get }
}

protocol ConditionViewModelOutputs {
  var isFromModal: BehaviorRelay<Bool> { get }
}

protocol ConditionViewModelCoordinates {
  var navigateBack: PublishSubject<Void> { get }
  var navigateToMain: PublishSubject<Void> { get }
}

protocol ConditionViewModelType {
  var inputs: ConditionViewModelInputs { get }
  var outputs: ConditionViewModelOutputs { get }
  var coordinates: ConditionViewModelCoordinates { get }
}

class ConditionViewModel: ConditionViewModelType, ConditionViewModelInputs, ConditionViewModelOutputs, ConditionViewModelCoordinates {
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  
  // MARK: Init
    
  public init(apiService: APIServiceType = APIService(), isModal: Bool) {
    isFromModal.accept(isModal)
    
    submitButtonTrigger
      .bind(to: navigateToMain)
      .disposed(by: disposeBag)
  }
  
  // MARK: Private
  
  // MARK: Input
  
  var submitButtonTrigger = PublishSubject<Void>()
    
  // MARK: Output
  
  var isFromModal = BehaviorRelay<Bool>(value: false)

  // MARK: Coordinates
  
  var navigateBack = PublishSubject<Void>()
  var navigateToMain = PublishSubject<Void>()
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: ConditionViewModelOutputs { return self }
  public var inputs: ConditionViewModelInputs { return self }
  public var coordinates: ConditionViewModelCoordinates { return self }
}
