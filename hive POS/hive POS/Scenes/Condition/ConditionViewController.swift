//
//  ConditionViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 23/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZSwiftKit

class ConditionViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var backButton: UIButton!
  @IBOutlet weak var conditionTitleLabel: UILabel!
  @IBOutlet weak var conditionDescriptionLabel: UILabel!
  @IBOutlet weak var conditionBackButton: UIButton!
  @IBOutlet weak var conditionNextButton: UIButton!
  @IBOutlet weak var conditionAcceptButton: UIButton!
  @IBOutlet weak var submitButton: UIButton!
  @IBOutlet weak var conditionTextView: UITextView!
  @IBOutlet weak var conditionView: UIView!
  
  // MARK: Property
  
  var viewModel: ConditionViewModelType!
  let disposeBag = DisposeBag()
  var acceptCondition = false
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
    setupL10N()
    bindViewModel()
    
  }
  
  func bindViewModel() {
    
    backButton.rx.tap
      .bind(to: viewModel.coordinates.navigateBack)
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .isFromModal
      .subscribe(onNext: { [unowned self] isFromModel in
        if isFromModel {
          self.backButton.setImage(UIImage(named: "ic_dismiss"), for: .normal)
        } else {
          self.backButton.setImage(UIImage(named: "ic_authen_back"), for: .normal)
        }
      })
      .disposed(by: disposeBag)
    
    conditionAcceptButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.conditionAcceptButton.isSelected = !self.conditionAcceptButton.isSelected
        self.acceptCondition = self.conditionAcceptButton.isSelected })
      .disposed(by: disposeBag)
    
    submitButton.rx.tap
      .filter{ [unowned self] in self.acceptCondition }
      .subscribe(onNext: { [unowned self] _ in
        
        Helper.Alert(view: self,
                     title: "condition_alert_title".localized(),
                     message: "condition_alert_message".localized(),
                     confirmText: "ok".localized()) { [unowned self] (_) in
                      self.dismiss(animated: true, completion: nil)
        }
      })
      .disposed(by: disposeBag)
    
  }
  
  func setupUI() {
    conditionView.layer.cornerRadius = 10
    submitButton.layer.cornerRadius = submitButton.frame.height / 2
    conditionView.layer.borderWidth = 1
    conditionView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    conditionTextView.font = UIFont(name: "Kanit-Regular", size: 15)
  }
  
  func setupL10N() {
    conditionTitleLabel.text = "condition_title".localized()
    conditionDescriptionLabel.text = "condition_description".localized()
    conditionBackButton.setTitle("condition_back".localized(), for: .normal)
    conditionNextButton.setTitle("condition_next".localized(), for: .normal)
    conditionAcceptButton.setTitle("condition_accept".localized(), for: .normal)
    submitButton.setTitle("condition_submit".localized(), for: .normal)
  }
  
}
