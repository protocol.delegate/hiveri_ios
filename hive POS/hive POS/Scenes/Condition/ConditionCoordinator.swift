//
//  ConditionCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 23/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol ConditionInputsParamsType {
    
}

class ConditionCoordinator: BaseCoordinator<Void>, ConditionInputsParamsType {
  
  // MARK: Property

  // MARK: Public

  override func start() -> Observable<Void> { 
    let window = self.window
    let viewModel = ConditionViewModel(isModal: self.transitionType == CoordinatorTransitionType.modal)
    let viewController = ConditionViewController.initFromStoryboard(name: Storyboard.authentication.identifier)
    viewController.viewModel = viewModel
    
    viewModel
    .coordinates
    .navigateToMain
    .map{ MainCoordinator(window: window,
                               baseViewController: viewController,
                               transitionType: .rootWindow,
                               animated: true) }
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
    transition(to: viewController)
    return viewModel.coordinates.navigateBack
    .do(onNext: { [weak self] _ in self?.navigateBack()})
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: ConditionInputsParamsType { return self }
}
