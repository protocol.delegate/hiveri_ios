//
//  BrandBranchManagement+Datasource.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import RxDataSources

struct DisplayBrandData {
  var brandID: Int
  var brandName: String
  var brandImagePath: String
}

enum DisplayBrandDataType {
  case brandData(items: DisplayBrandData)
  case createBrand
}

enum SectionOfDisplayBrandDataType {
  case Section(title: String, items: [DisplayBrandDataType])
}

extension SectionOfDisplayBrandDataType: SectionModelType {
  typealias Item = DisplayBrandDataType
  
  var items: [DisplayBrandDataType] {
    switch self {
    case .Section(_, items: let items):
      return items.map { $0 }
    }
  }
  
  init(original: SectionOfDisplayBrandDataType, items: [Item]) {
    switch original {
    case .Section(title: let title, _):
      self = .Section(title: title, items: items)
    }
  }
}

extension SectionOfDisplayBrandDataType {
  var title: String {
    switch self {
    case .Section(title: let title, _):
      return title
    }
  }
}

struct DisplayBranchData {
  var title: String
  var ID: Int
  var imagePath: String
}

struct SectionOfDisplayBranchData {
  var header: String
  var items: [Item]
}

extension SectionOfDisplayBranchData: SectionModelType {
  typealias Item = DisplayBranchData
  
  init(original: SectionOfDisplayBranchData, items: [Item]) {
    self = original
    self.items = items
  }
}
