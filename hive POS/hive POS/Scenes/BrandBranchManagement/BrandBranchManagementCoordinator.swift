//
//  BrandBranchManagementCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 24/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol BrandBranchManagementInputsParamsType {
    
}

class BrandBranchManagementCoordinator: BaseCoordinator<Void>, BrandBranchManagementInputsParamsType {
  
  // MARK: Property

  // MARK: Public

  override func start() -> Observable<Void> { 
    let window = self.window
    let viewModel = BrandBranchManagementViewModel()
    let viewController = BrandBranchManagementViewController.initFromStoryboard(name: Storyboard.brandBranch.identifier)
    viewController.viewModel = viewModel
    
    viewModel
    .coordinates
    .navigateToCreateBrand
    .map{ BrandCoordinator(window: window,
                                 baseViewController: viewController,
                                 transitionType: .push,
                                 animated: true) }
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
    viewModel
    .coordinates
    .navigateToBrandDetail
    .map{ brandID -> BrandCoordinator in
      let coordinator = BrandCoordinator(window: window,
                                               baseViewController: viewController,
                                               transitionType: .push,
                                               animated: true)
      coordinator.brandID = brandID
      return coordinator
    }
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
    viewModel
    .coordinates
    .navigateToCreateBranch
    .map{ brandID -> BranchCoordinator in
      let coordinator = BranchCoordinator(window: window,
                                          baseViewController: viewController,
                                          transitionType: .push,
                                          animated: true)
      coordinator.brandID = brandID
      return coordinator
    }
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
    viewModel
    .coordinates
    .navigateToBranchDetail
    .map{ data -> BranchCoordinator in
      let coordinator = BranchCoordinator(window: window,
                                          baseViewController: viewController,
                                          transitionType: .push,
                                          animated: true)
      coordinator.brandID = data.brandID
      coordinator.branchID = data.branchID
      return coordinator
    }
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
    transition(to: viewController)
    return .never()
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: BrandBranchManagementInputsParamsType { return self }
}
