//
//  BrandBranchManagementViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 24/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa

protocol BrandBranchManagementViewModelInputs {
  var getBrandListTrigger: PublishSubject<Void> { get }
  var createBrandTrigger: PublishSubject<Void> { get }
  var brandDetailTrigger: PublishSubject<Int?> { get }
  var createBranchTrigger: PublishSubject<Int> { get }
  var getBranchListTrigger: PublishSubject<Int> { get }
  var branchSignTrigger: PublishSubject<Int> { get }
  var branchDetailTrigger: PublishSubject<(brandID: Int, branchID: Int)> { get }
}

protocol BrandBranchManagementViewModelOutputs {
  var displayBrandData: PublishSubject<[SectionOfDisplayBrandDataType]> { get }
  var loading: BehaviorSubject<Bool> { get }
  var displayBrands: PublishSubject<[BrandModel]> { get }
  var displayBranchsList: PublishSubject<[SectionOfDisplayBranchData]> { get }
  var displayBranchSignSuccess: PublishSubject<BranchModel> { get }
}

protocol BrandBranchManagementViewModelCoordinates {
  var navigateToCreateBrand: PublishSubject<Void> { get }
  var navigateToSignIn: PublishSubject<Void> { get }
  var navigateToBrandDetail: PublishSubject<Int?> { get }
  var navigateToCreateBranch: PublishSubject<Int> { get }
  var navigateToBranchDetail: PublishSubject<(brandID: Int, branchID: Int)> { get }
}

protocol BrandBranchManagementViewModelType: ErrorAlertableViewModelType {
  var inputs: BrandBranchManagementViewModelInputs { get }
  var outputs: BrandBranchManagementViewModelOutputs { get }
  var coordinates: BrandBranchManagementViewModelCoordinates { get }
}

class BrandBranchManagementViewModel: BrandBranchManagementViewModelType, BrandBranchManagementViewModelInputs, BrandBranchManagementViewModelOutputs, BrandBranchManagementViewModelCoordinates {
  
  
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  
  // MARK: Init
    
  public init(apiService: APIService = APIService()) {
    
    let getUserTriggerSignal =
      Observable
        .just{GetUserRequestModel()}
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{$0}
        .flatMapLatest{apiService.rx_getUser(requestModel: $0()).materialize()}
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    getUserTriggerSignal
      .elements()
      .subscribe(onNext: { LocalDataService.setUser($0) })
      .disposed(by: disposeBag)
    
    let getBrandListTriggerSignal =
      getBrandListTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ GetBrandListRequestModel() }
        .flatMapLatest{ apiService.rx_getBrandList(requestModel: $0).retry(2)
        .materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    getBrandListTriggerSignal
      .elements()
      .map{ model -> [SectionOfDisplayBrandDataType] in
        var sectionData: [SectionOfDisplayBrandDataType] = []
        let brandData = model.map{ DisplayBrandDataType
          .brandData(items: DisplayBrandData(brandID: $0.ID,
                                             brandName: $0.brandName,
                                             brandImagePath: $0.brandImageUrl)) }
        
        sectionData.append(SectionOfDisplayBrandDataType.Section(title: "", items: brandData))
        sectionData.append(SectionOfDisplayBrandDataType.Section(title: "", items: [.createBrand]))
        
        return sectionData
      }
      .bind(to: displayBrandData)
      .disposed(by: disposeBag)
    
    getBrandListTriggerSignal
      .elements()
//      .map{ $0.sorted(by: {$0.ID < $1.ID}) }
      .bind(to: displayBrands)
      .disposed(by: disposeBag)
    
    let getBranchListTriggerSignal =
      getBranchListTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ GetBranchListRequestModel(brandID: $0) }
        .flatMapLatest{apiService.rx_getBranchList(requestModel: $0).retry(2).materialize()}
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    getBranchListTriggerSignal
      .elements()
      .map{ model -> [SectionOfDisplayBranchData] in
        let branchData = model.map{ DisplayBranchData(title: $0.branchName,
                                                      ID: $0.ID,
                                                      imagePath: $0.branchImageProfileUrl) }
        return [SectionOfDisplayBranchData(header: "", items: branchData)]}
      .bind(to: outputs.displayBranchsList)
      .disposed(by: disposeBag)
    
    let branchSignTriggerSignal =
      branchSignTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ GetBranchRequestModel(branchID: $0) }
        .flatMapLatest{ apiService.rx_getBranch(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    branchSignTriggerSignal.elements()
    .bind(to: displayBranchSignSuccess)
    .disposed(by: disposeBag)
    
    createBrandTrigger
      .bind(to: navigateToCreateBrand)
      .disposed(by: disposeBag)
    
    brandDetailTrigger
      .bind(to: navigateToBrandDetail)
      .disposed(by: disposeBag)
    
    createBranchTrigger
      .bind(to: navigateToCreateBranch)
      .disposed(by: disposeBag)
    
    branchDetailTrigger
      .bind(to: navigateToBranchDetail)
      .disposed(by: disposeBag)
    
    // Error ---------------------------------------------------------------
    
    let errorSignal = Observable
      .merge(getUserTriggerSignal.errors(),
             getBrandListTriggerSignal.errors(),
             getBranchListTriggerSignal.errors(),
             branchSignTriggerSignal.errors())
      .share(replay: 1)
    
    errorSignal
      .map(errorToAlertable)
      .bind(to: error)
      .disposed(by: disposeBag)
    
  }
  
  // MARK: Private
  
  // MARK: Input
  
  var getBrandListTrigger = PublishSubject<Void>()
  var getBranchListTrigger = PublishSubject<Int>()
  var createBrandTrigger = PublishSubject<Void>()
  var brandDetailTrigger = PublishSubject<Int?>()
  var branchSignTrigger = PublishSubject<Int>()
    
  // MARK: Output
  
  var error = PublishSubject<(title: String, message: String)>()
  var loading = BehaviorSubject<Bool>(value: false)
  var displayBrandData = PublishSubject<[SectionOfDisplayBrandDataType]>()
  var displayBrands = PublishSubject<[BrandModel]>()
  var createBranchTrigger = PublishSubject<Int>()
  var displayBranchsList = PublishSubject<[SectionOfDisplayBranchData]>()
  var displayBranchSignSuccess = PublishSubject<BranchModel>()
  var branchDetailTrigger = PublishSubject<(brandID: Int, branchID: Int)>()

  // MARK: Coordinates
  
  var navigateToSignIn = PublishSubject<Void>()
  var navigateToCreateBrand = PublishSubject<Void>()
  var navigateToBrandDetail = PublishSubject<Int?>()
  var navigateToCreateBranch = PublishSubject<Int>()
  var navigateToBranchDetail = PublishSubject<(brandID: Int, branchID: Int)>()
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: BrandBranchManagementViewModelOutputs { return self }
  public var inputs: BrandBranchManagementViewModelInputs { return self }
  public var coordinates: BrandBranchManagementViewModelCoordinates { return self }
}
