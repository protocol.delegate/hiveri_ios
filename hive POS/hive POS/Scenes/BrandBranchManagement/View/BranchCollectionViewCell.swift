//
//  BranchCollectionViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 8/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

protocol BranchCollectionViewCellDelegate: class {
  func didClickOnBranchDetail(branchID: Int)
  func didClickOnBranchSign(branchID: Int)
}

class BranchCollectionViewCell: UICollectionViewCell {
    
  @IBOutlet weak var branchSignVew: UIView!
  @IBOutlet weak var branchDetailView: UIView!
  @IBOutlet weak var branchShadowView: UIView!
  @IBOutlet weak var branchBackgroundView: UIView!
  @IBOutlet weak var logoImageView: UIImageView!
  @IBOutlet weak var branchNameLabel: UILabel!
  @IBOutlet weak var branchDetailButton: UIButton!
  @IBOutlet weak var branchSignButton: UIButton!
  
  weak var delegate: BranchCollectionViewCellDelegate?
  var branchID: Int = -1
  
  func configure(branchName: String, branchID: Int, branchLogoImage: String) {
    print("branch logo image---> ", branchLogoImage)
    let url = URL(string: branchLogoImage)
    logoImageView.kf.setImage(with: url, placeholder: UIImage(named: "ic_default_image"))
    branchDetailView.layer.cornerRadius = branchDetailView.frame.height / 2
    branchSignVew.layer.cornerRadius = branchSignVew.frame.height / 2
    branchBackgroundView.layer.cornerRadius = 12
    branchShadowView.layer.cornerRadius = 12
    branchShadowView.layer.applySketchShadow(color: .black, alpha: 0.15, x: 0, y: 3, blur: 16, spread: 0)
    branchNameLabel.text = branchName
    self.branchID = branchID
    branchDetailButton.setTitle("brandbranch_branch_detail".localized(), for: .normal)
    branchSignButton.setTitle("brandbranch_branch_sign".localized(), for: .normal)
  }
  
  @IBAction func didClickOnDetail(_ sender: Any) {
    delegate?.didClickOnBranchDetail(branchID: self.branchID)
  }
  @IBAction func didClickOnSign(_ sender: Any) {
    delegate?.didClickOnBranchSign(branchID: self.branchID)
  }
}
