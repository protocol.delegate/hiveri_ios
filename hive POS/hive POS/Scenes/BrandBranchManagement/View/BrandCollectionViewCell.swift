//
//  BrandCollectionViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class BrandCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Instances
    
  var brandToShow: Bool?
    var brandId: Int?
    var brandIsSelect: Bool?
    var brandImg: String?
    var checkHidden: Bool = false
    var onSelectBrand : (() -> Void)? = nil

    // MARK: - @IBOutlet
  
    @IBOutlet weak var brandLabel: UILabel!
    @IBOutlet weak var handleBandUIView: UIView!
    @IBOutlet weak var brandUIImage: UIImageView!
    @IBOutlet weak var cellBackgroundView: UIView!
    
    //MARK: - IBAction
    
    
    // MARK: - ui
    
    func configure(brand: DisplayBrandData, isSelect: Bool) {
        self.brandLabel.text = brand.brandName
        self.brandId = brand.brandID
        self.brandIsSelect = false
        if (brand.brandImagePath == "") {
            self.brandUIImage.image = UIImage(named: "ic_default_brandbranch")
        } else {
            let url = URL(string: brand.brandImagePath)
            self.brandUIImage.kf.setImage(with: url)
            self.brandUIImage.layer.cornerRadius = brandUIImage.frame.width / 2
        }
      
        handleBandUIView.isHidden = brandIsSelect!
        if brandIsSelect == isSelect {
            uiShowoffBrand()
        } else {
            uiShowOnBrand()
        }
      
      setupUI()
        
    }
    
    func uiShowOnBrand() {
        handleBandUIView.isHidden = false
        brandLabel.alpha = 1
        brandUIImage.alpha = 1
    }
    
    func uiShowoffBrand() {
        handleBandUIView.isHidden = true
        brandLabel.alpha = 0.3
        brandUIImage.alpha = 0.3
    }
    
    func checkDataRestaurantResponse(){
        print("Data not found")
    }
  
  func setupUI() {
    cellBackgroundView.layer.cornerRadius = 10
    cellBackgroundView.layer.applySketchShadow(color: .black, alpha: 0.5, x: 0, y: 2, blur: 6, spread: 0)
  }
    
}
