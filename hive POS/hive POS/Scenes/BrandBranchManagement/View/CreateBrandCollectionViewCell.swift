//
//  CreateBrandCollectionViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 27/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import UIKit

protocol CreateBrandCollectionViewCellDelegate: class {
  func didClickOnCreateBrand()
}

class CreateBrandCollectionViewCell: UICollectionViewCell {
  
  @IBOutlet weak var createBrandBackgroundView: UIView!
  @IBOutlet weak var createBrandTitleLabel: UILabel!
  
  weak var delegate: CreateBrandCollectionViewCellDelegate?
  
  @IBAction func didClickOnCreateBrand(_ sender: Any) {
    delegate?.didClickOnCreateBrand()
  }
  
  func configure() {
    setupUI()
    setupL10N()
  }
  
  func setupUI() {
    createBrandBackgroundView.layer.cornerRadius = 10
  }
  
  func setupL10N() {
    createBrandTitleLabel.text = "brandbranch_create_brand".localized()
  }
}
