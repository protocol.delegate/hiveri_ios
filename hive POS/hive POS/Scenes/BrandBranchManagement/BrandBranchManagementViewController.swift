//
//  BrandBranchManagementViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 24/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZSwiftKit
import RxDataSources

class BrandBranchManagementViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var branchCollectionView: UICollectionView!
  @IBOutlet weak var brandCollectionView: UICollectionView!
  @IBOutlet weak var branchView: UIView!
  @IBOutlet weak var menuView: UIView!
  @IBOutlet weak var allBranchTitleLabel: UILabel!
  @IBOutlet weak var allBranchLabel: UILabel!
  @IBOutlet weak var brandDetailButton: UIButton!
  @IBOutlet weak var createBranchButton: UIButton!
  
  // MARK: Property 
  
  var viewModel: BrandBranchManagementViewModelType!
  let disposeBag = DisposeBag()
  var extraview : UIView = UIView()
  
  var selectBrandIndex: IndexPath = IndexPath(row: 0, section: 0)
  var brandDatas: [BrandModel] = []
  var isReload = false
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
    setupL10N()
    bindViewModel()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    viewModel.inputs.getBrandListTrigger.onNext(())
  }
  
  func bindViewModel() {
    
    let brandDataSource = RxCollectionViewSectionedReloadDataSource<SectionOfDisplayBrandDataType>(
    configureCell: { dataSource, collectionView, indexPath, item in
      switch dataSource[indexPath] {
      case .brandData(let data):
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BrandCollectionViewCell.className, for: indexPath) as! BrandCollectionViewCell
        
        cell.configure(brand: data, isSelect: self.selectBrandIndex == indexPath)
        return cell
      case .createBrand:
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CreateBrandCollectionViewCell.className, for: indexPath) as! CreateBrandCollectionViewCell
        cell.delegate = self
        cell.configure()
        return cell
      }
    })
    
    let branchDataSource = RxCollectionViewSectionedReloadDataSource<SectionOfDisplayBranchData>(
    configureCell: { dataSource, collectionView, indexPath, item in
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BranchCollectionViewCell.className, for:      indexPath) as! BranchCollectionViewCell
      cell.configure(branchName: item.title, branchID: item.ID, branchLogoImage: item.imagePath)
      cell.delegate = self
      return cell })
    
    viewModel.outputs
      .displayBranchsList
      .bind(to: branchCollectionView.rx.items(dataSource: branchDataSource))
      .disposed(by: disposeBag)
    
    viewModel.outputs
      .displayBranchsList
      .subscribe(onNext: { list in
        if list.count == 0 {
          BranchSignService.shared.deleteBranchData()
          self.setNavigationBarItem(titlePageName: "brandbranch_title".localized())
        }
        self.allBranchLabel.text = "\(list[0].items.count) " + "brandbranch_unit".localized()
      })
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayBrandData
      .bind(to: brandCollectionView.rx.items(dataSource: brandDataSource))
      .disposed(by: disposeBag)
    
    brandCollectionView.rx
      .itemSelected
      .subscribe(onNext: { [unowned self] indexPath in
        if indexPath.section == 1 { return }
        BranchSignService.shared.deleteBranchData()
        self.selectBrandIndex = indexPath
        self.brandCollectionView.reloadData()
        LocalDataService.setSelectBrand(self.brandDatas[indexPath.row])
        self.viewModel.inputs.getBranchListTrigger.onNext((self.brandDatas[indexPath.row].ID))
        self.setNavigationBarItem(titlePageName: "brandbranch_title".localized())
      })
      .disposed(by:disposeBag)
    
    viewModel.outputs
      .displayBranchSignSuccess
      .subscribe(onNext: { model in
        BranchSignService.shared.setBranch(branchData: model)
        self.setNavigationBarItem(titlePageName: "brandbranch_title".localized())
        self.tabBarController?.selectedIndex = 3
      })
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayBrands
      .subscribe(onNext: { [unowned self] models in
        if models.count == 0 {
          LocalDataService.deleteSelectBrand()
          self.viewModel.inputs.createBrandTrigger.onNext(())
          BranchSignService.shared.deleteBranchData()
          self.setNavigationBarItem(titlePageName: "brandbranch_title".localized())
          return
        }
        
        self.brandDatas = models
        if let brand = LocalDataService.getSelectBrand() {
          let isInSet = models.filter{ $0.ID == brand.ID }
          if isInSet.count > 0 {
            let indice = models.indices.filter{ models[$0].ID == brand.ID }.first!
            self.selectBrandIndex = IndexPath(row: indice, section: 0)
            self.brandCollectionView.reloadData()
            self.setNavigationBarItem(titlePageName: "brandbranch_title".localized())
            self.viewModel.inputs.getBranchListTrigger.onNext(brand.ID)
          } else {
            LocalDataService.setSelectBrand(models[0])
            self.selectBrandIndex = IndexPath(row: 0, section: 0)
            self.brandCollectionView.reloadData()
            self.setNavigationBarItem(titlePageName: "brandbranch_title".localized())
            self.viewModel.inputs.getBranchListTrigger.onNext(models[0].ID)
          }
        } else {
          LocalDataService.setSelectBrand(models[0])
          self.selectBrandIndex = IndexPath(row: 0, section: 0)
          self.brandCollectionView.reloadData()
          self.viewModel.inputs.getBranchListTrigger.onNext(models[0].ID)
          self.setNavigationBarItem(titlePageName: "brandbranch_title".localized())
        }
      })
      .disposed(by: disposeBag)
    
    brandDetailButton.rx.tap
      .map{ LocalDataService.getSelectBrand()?.ID }
      .bind(to: viewModel.inputs.brandDetailTrigger)
      .disposed(by: disposeBag)
    
    createBranchButton.rx.tap
//      .map{ [unowned self] in self.brandDatas[self.selectBrandIndex.row].ID }
      .map{LocalDataService.getSelectBrand()?.ID}
      .filterNil()
      .bind(to: viewModel.inputs.createBranchTrigger)
      .disposed(by: disposeBag)
    
    //MARK: Bind loading
       
    viewModel.outputs.loading
      .subscribe(onNext: { [unowned self] isLoading in
        guard let tabBarController = self.tabBarController else { return }
        LoadingActivityView.action(in: tabBarController.view, isShow: isLoading)})
      .disposed(by: disposeBag)
    
    // MARK: Bind Error
          
    viewModel.error
      .subscribe(onNext: { [unowned self] error in
          Helper.Alert(
            view: self,
              title: error.title,
              message: error.message,
              confirmText: "done".localized())})
      .disposed(by: disposeBag)
  }
  
  func setupUI() {
    self.setNavigationBarItem(titlePageName: "brandbranch_title".localized())
    if let collectionViewLayout = brandCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      collectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      collectionViewLayout.scrollDirection = .horizontal
    }
    
    branchCollectionView.register(UINib(nibName: BranchCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: BranchCollectionViewCell.className)
//    if let branchCollectionViewLayout = branchCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
//      branchCollectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
//      branchCollectionViewLayout.scrollDirection = .vertical
//    }
    
    branchView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
    menuView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: -2, blur: 6, spread: 0)
  }
  
  func setupL10N() {
    allBranchTitleLabel.text = "brandbranch_all_title".localized()
    allBranchLabel.text = "0 " + "brandbranch_unit".localized()
    brandDetailButton.setTitle("brandbranch_detail".localized(), for: .normal)
  }
}

extension BrandBranchManagementViewController: CreateBrandCollectionViewCellDelegate {

  func didClickOnCreateBrand() {
    viewModel.inputs.createBrandTrigger.onNext(())
  }

}

extension BrandBranchManagementViewController: BranchCollectionViewCellDelegate {
  func didClickOnBranchDetail(branchID: Int) {
    guard let brandID = LocalDataService.getSelectBrand()?.ID else { return }
    viewModel.inputs.branchDetailTrigger.onNext((brandID: brandID, branchID: branchID))
  }
  
  func didClickOnBranchSign(branchID: Int) {
    viewModel.inputs.branchSignTrigger.onNext(branchID)
  }
}
