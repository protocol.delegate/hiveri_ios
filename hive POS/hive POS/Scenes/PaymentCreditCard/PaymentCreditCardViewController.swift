//
//  PaymentCreditCardViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 15/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import WebKit
import ZSwiftKit

protocol PaymentCreditCardViewControllerDelegate: class {
  func didCardBindingSuccess(refCode: String)
}

class PaymentCreditCardViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var webView: WKWebView!
  @IBOutlet weak var dissmissButton: UIButton!
  
  // MARK: Property
  
  var viewModel: PaymentCreditCardViewModelType!
  var delegate: PaymentCreditCardViewControllerDelegate?
  let disposeBag = DisposeBag()
  var cardToken: String?
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    bindViewModel()
  }
  
  func bindViewModel() {
    
    dissmissButton.rx.tap
      .bind(to: viewModel.coordinates.dissmissView)
      .disposed(by: disposeBag)
    
    viewModel.outputs.deepLink
      .subscribe(onNext: { path in
        self.getWebKit(path: path)
      })
      .disposed(by: disposeBag)
    
  }
  
  func getWebKit(path: String) {
    guard let url = URL(string: path) else { return }
    webView.navigationDelegate = self
    webView.uiDelegate = self
    webView.load(URLRequest(url: url))
  }
}

extension PaymentCreditCardViewController: WKNavigationDelegate {
//  @available(iOS 13.0, *)
//  func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, preferences: WKWebpagePreferences, decisionHandler: @escaping (WKNavigationActionPolicy, WKWebpagePreferences) -> Void) {
//
//  }
//
//  func webView(_ webView: WKWebView, decidePolicyFor navigationResponse: WKNavigationResponse, decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void) {
//
//  }
  
  func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
 
    let urlString = (navigationAction.request.url?.absoluteString ?? "") as String
    LoadingActivityView.action(in: self.view, isShow: true)
    if urlString.contains("return") {
      cardToken = getQueryStringParameter(url: urlString, param: "txn_id")
    }
    if urlString.contains("paymentSuccess") {
      LoadingActivityView.action(in: self.view, isShow: false)
      if let token = cardToken {
        delegate?.didCardBindingSuccess(refCode: token)
      }
      viewModel.coordinates.dissmissView.onNext(())
    } else {
      LoadingActivityView.action(in: self.view, isShow: false)
    }
    
    decisionHandler(.allow)
  }
  
  func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
    LoadingActivityView.action(in: self.view, isShow: false)
  }
  
  func getQueryStringParameter(url: String, param: String) -> String? {
    guard let url = URLComponents(string: url) else { return nil }
    return url.queryItems?.first(where: { $0.name == param })?.value
  }
  
}

extension PaymentCreditCardViewController: WKUIDelegate {
  
  func webView(_ webView: WKWebView, runJavaScriptAlertPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping () -> Void) {
    Helper.Alert(view: self, title: message, message: message, confirmText: "ok".localized()) { (_) in
      completionHandler()
    }
  }
  
  func webView(_ webView: WKWebView, runJavaScriptConfirmPanelWithMessage message: String, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (Bool) -> Void) {
    Helper.AlertConfirm(view: self, title: message, message: message, confirmText: "ok".localized(),
                        cancelText: "cancel".localized(), isInverseButtonPos: true,
                        confirmCompletion: { (_) in
                          completionHandler(true)
    }) { (_) in
      completionHandler(false)
    }
  }
  
  func webView(_ webView: WKWebView, runJavaScriptTextInputPanelWithPrompt prompt: String, defaultText: String?, initiatedByFrame frame: WKFrameInfo, completionHandler: @escaping (String?) -> Void) {
    
  }
}
