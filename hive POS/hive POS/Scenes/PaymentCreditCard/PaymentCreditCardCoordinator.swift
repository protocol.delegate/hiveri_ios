//
//  PaymentCreditCardCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 15/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol PaymentCreditCardInputsParamsType {
  var deepLink: String { get set }
}

class PaymentCreditCardCoordinator: BaseCoordinator<Void>, PaymentCreditCardInputsParamsType {
  
  var deepLink: String = ""
  
  // MARK: Property
  
  let viewController = PaymentCreditCardViewController.initFromStoryboard(name: Storyboard.order.identifier)

  // MARK: Public

  override func start() -> Observable<Void> { 
    let window = self.window
    let viewModel = PaymentCreditCardViewModel(deepLink: deepLink)
    viewController.viewModel = viewModel
    
    
    transition(to: viewController)
    
    return viewModel.coordinates.dissmissView
    .do(onNext: { [weak self] _ in self?.navigateBack()})
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: PaymentCreditCardInputsParamsType { return self }
}
