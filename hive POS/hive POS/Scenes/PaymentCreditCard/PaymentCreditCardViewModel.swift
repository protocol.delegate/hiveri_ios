//
//  PaymentCreditCardViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 15/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa

protocol PaymentCreditCardViewModelInputs {
    
}

protocol PaymentCreditCardViewModelOutputs {
  var deepLink: BehaviorRelay<String> { get }
}

protocol PaymentCreditCardViewModelCoordinates {
  var dissmissView: PublishSubject<Void> { get }
}

protocol PaymentCreditCardViewModelType {
  var inputs: PaymentCreditCardViewModelInputs { get }
  var outputs: PaymentCreditCardViewModelOutputs { get }
  var coordinates: PaymentCreditCardViewModelCoordinates { get }
}

class PaymentCreditCardViewModel: PaymentCreditCardViewModelType, PaymentCreditCardViewModelInputs, PaymentCreditCardViewModelOutputs, PaymentCreditCardViewModelCoordinates {
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  
  // MARK: Init
    
  public init(deepLink: String?) {
    Observable.of(deepLink)
      .filterNil()
      .subscribe(onNext: { [unowned self] link in
        self.deepLink.accept(link)})
      .disposed(by: disposeBag)
  }
  
  // MARK: Private
  
  // MARK: Input
    
  // MARK: Output
  var deepLink = BehaviorRelay<String>(value: "")

  // MARK: Coordinates
  
  var dissmissView = PublishSubject<Void>()
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: PaymentCreditCardViewModelOutputs { return self }
  public var inputs: PaymentCreditCardViewModelInputs { return self }
  public var coordinates: PaymentCreditCardViewModelCoordinates { return self }
}
