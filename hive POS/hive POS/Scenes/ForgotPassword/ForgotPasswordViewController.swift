//
//  ForgotPasswordViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZSwiftKit

class ForgotPasswordViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var errorLabel: UILabel!
  @IBOutlet weak var forgotPasswordDescriptionLabel: UILabel!
  @IBOutlet weak var forgotPasswordEmailTitleLabel: UILabel!
  @IBOutlet weak var forgotPasswordEmailbackTextField: UITextField!
  @IBOutlet weak var forgotPasswordEmailTextField: UITextField!
  @IBOutlet weak var forgotPasswordSubmitButton: UIButton!
  @IBOutlet weak var forgotPasswordSubmitView: UIView!
  @IBOutlet weak var forgotPasswordSubmitLabel: UILabel!
  @IBOutlet weak var forgotPasswordBackButton: UIButton!
  @IBOutlet weak var forgotPasswordView: UIView!
  
  // MARK: Property 
  
  var viewModel: ForgotPasswordViewModelType!
  let disposeBag: DisposeBag = DisposeBag()
  
  // MARK: View Life Cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupL10N()
    bindViewModel()
  }
  
  func bindViewModel() {
    
    forgotPasswordSubmitButton.rx.tap
      .filter{ [unowned self] in self.validateForgotPasswordTextField() }
      .filter{ [unowned self] in self.isValidEmailFormat(email: self.forgotPasswordEmailTextField.text!) }
      .map{ [unowned self] in self.forgotPasswordEmailTextField.text! }
      .bind(to: viewModel.inputs.forgotPasswordTrigger)
      .disposed(by: disposeBag)
    
    forgotPasswordBackButton.rx.tap
      .bind(to: viewModel.coordinates.dissmissView)
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayForgotPasswordSuccess
      .subscribe(onNext: { [unowned self] _ in
        Helper.Alert(view: self, title: "forgotpassword_title".localized(),
                     message: "forgotpassword_alert_description".localized(),
                     confirmText: "ok".localized()) { [unowned self] (_) in
                      self.viewModel.coordinates.dissmissView.onNext(())
//                      let mailURL = URL(string: "message://")!
//                      if UIApplication.shared.canOpenURL(mailURL) {
//                        UIApplication.shared.open(mailURL, options: [:], completionHandler: nil)
//                      }
        }})
      .disposed(by: disposeBag)
    
    viewModel.outputs
      .displayValidEmail
      .subscribe(onNext: { [unowned self] _ in
        self.errorLabel.text = "forgotpassword_email_valid".localized()
        self.setupUIRequireForgotPassword(isRequired: true)
        self.errorLabel.isHidden = false
      })
      .disposed(by: disposeBag)
    
    //MARK: Bind loading
    
    viewModel.outputs.loading
      .subscribe(onNext: { [unowned self] isLoading in
        LoadingActivityView.action(in: self.view, isShow: isLoading)})
      .disposed(by: disposeBag)
    
    // MARK: Bind Error
    
    viewModel.error
      .subscribe(onNext: { [unowned self] error in
        Helper.Alert(
          view: self,
          title: error.title,
          message: error.message,
          confirmText: "done".localized())})
      .disposed(by: disposeBag)
    
  }
  
  func setupUI() {
    
    forgotPasswordEmailbackTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    forgotPasswordEmailbackTextField.layer.cornerRadius = 10
    forgotPasswordSubmitView.layer.cornerRadius = forgotPasswordSubmitView.frame.height / 2
    forgotPasswordEmailbackTextField.layer.borderWidth = 1
  }
  
  func setupL10N() {
    
    forgotPasswordDescriptionLabel.text = "forgotpassword_description".localized()
    forgotPasswordEmailTitleLabel.text = "forgotpassword_email".localized()
    forgotPasswordEmailTextField.placeholder = "forgotpassword_email".localized()
    forgotPasswordSubmitLabel.text = "forgotpassword_submit".localized()
    
  }
  
  func setupUIRequireForgotPassword(isRequired: Bool) {
    forgotPasswordEmailbackTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func validateTextField(text: String) -> Bool {
    return text != ""
  }
  
  func validateForgotPasswordTextField() -> Bool {
    let isPassValidateEmail = validateTextField(text: forgotPasswordEmailTextField.text!)
    setupUIRequireForgotPassword(isRequired: !isPassValidateEmail)
    return isPassValidateEmail
  }
  
  func isValidEmailFormat(email:String) -> Bool {
    
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let validFormat = emailTest.evaluate(with: email)
//    let appName = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String ?? ""
    if !validFormat {
//      Helper.Alert(view: self, title: appName, message: "authentication_valid_email".localized(), confirmText: "submit".localized())
      errorLabel.text = "authentication_valid_email".localized()
      errorLabel.isHidden = false
      setupUIRequireForgotPassword(isRequired: true)
    } else {
      errorLabel.isHidden = true
      setupUIRequireForgotPassword(isRequired: false)
    }
    return validFormat
  }
}
