//
//  ForgotPasswordCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol ForgotPasswordInputsParamsType {
    
}

class ForgotPasswordCoordinator: BaseCoordinator<Void>, ForgotPasswordInputsParamsType {
  
  // MARK: Property

  // MARK: Public

  override func start() -> Observable<Void> { 
    let window = self.window
    let viewModel = ForgotPasswordViewModel()
    let viewController = ForgotPasswordViewController.initFromStoryboard(name: Storyboard.authentication.identifier)
    viewController.viewModel = viewModel
    
    transition(to: viewController)
    return viewModel.coordinates.dissmissView
      .do(onNext: { [weak self] _ in self?.navigateBack()})
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: ForgotPasswordInputsParamsType { return self }
}
