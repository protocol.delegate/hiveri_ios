//
//  ForgotPasswordViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa
import Firebase

protocol ForgotPasswordViewModelInputs {
  var forgotPasswordTrigger: PublishSubject<String> { get }
}

protocol ForgotPasswordViewModelOutputs {
  var displayForgotPasswordSuccess: PublishSubject<Void> { get }
  var displayValidEmail: PublishSubject<Void> { get }
  var loading: BehaviorSubject<Bool> { get }
}

protocol ForgotPasswordViewModelCoordinates {
  var dissmissView: PublishSubject<Void> { get }
}

protocol ForgotPasswordViewModelType: ErrorAlertableViewModelType {
  var inputs: ForgotPasswordViewModelInputs { get }
  var outputs: ForgotPasswordViewModelOutputs { get }
  var coordinates: ForgotPasswordViewModelCoordinates { get }
}

class ForgotPasswordViewModel: ForgotPasswordViewModelType, ForgotPasswordViewModelInputs, ForgotPasswordViewModelOutputs, ForgotPasswordViewModelCoordinates {
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  
  // MARK: Init
    
  public init(apiService: APIServiceType = APIService()) {
    
    forgotPasswordTrigger
    .subscribe(onNext: { [unowned self] email in
      Auth.auth().sendPasswordReset(withEmail: email,
                                    completion: { [unowned self] (error) in
        if error == nil{
          self.displayForgotPasswordSuccess.onNext(())
        } else {
          switch error {
          case .some(let error as NSError) where error.code == AuthErrorCode.userNotFound.rawValue:
            self.displayValidEmail.onNext(())
          default:
            self.error.onNext((title: "Error", message: error?.localizedDescription ?? ""))
          }
        }
      })})
    .disposed(by: disposeBag)
    
  }
  
  // MARK: Private
  
  // MARK: Input
  
  var forgotPasswordTrigger = PublishSubject<String>()
    
  // MARK: Output
  
  var error = PublishSubject<(title: String, message: String)>()
  var loading = BehaviorSubject<Bool>(value: false)
  var displayForgotPasswordSuccess = PublishSubject<Void>()
  var displayValidEmail = PublishSubject<Void>()

  // MARK: Coordinates
  
  var dissmissView = PublishSubject<Void>()
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: ForgotPasswordViewModelOutputs { return self }
  public var inputs: ForgotPasswordViewModelInputs { return self }
  public var coordinates: ForgotPasswordViewModelCoordinates { return self }
}
