//
//  InAppVerifyPhoneNumberCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 2/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol InAppVerifyPhoneNumberInputsParamsType {
    
}

class InAppVerifyPhoneNumberCoordinator: BaseCoordinator<Void>, InAppVerifyPhoneNumberInputsParamsType {
  
  // MARK: Property

  let viewController = InAppVerifyPhoneNumberViewController.initFromStoryboard(name: Storyboard.brandBranch.identifier)

  // MARK: Public

  override func start() -> Observable<Void> {
    let window = self.window
    let viewModel = InAppVerifyPhoneNumberViewModel()
    viewController.viewModel = viewModel
    
    
    transition(to: viewController)
    return viewModel.coordinates.navigateBack
      .do(onNext: { [weak self] _ in self?.navigateBack()})
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: InAppVerifyPhoneNumberInputsParamsType { return self }
}
