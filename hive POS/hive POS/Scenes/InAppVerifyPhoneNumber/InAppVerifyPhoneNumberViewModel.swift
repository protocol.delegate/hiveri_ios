//
//  InAppVerifyPhoneNumberViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 2/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa
import FirebaseAuth

protocol InAppVerifyPhoneNumberViewModelInputs {
  var sendOTPTrigger: PublishSubject<Void> { get }
  var submitButtonTrigger: PublishSubject<String> { get }
}

protocol InAppVerifyPhoneNumberViewModelOutputs {
  var displayPhoneNumber: BehaviorRelay<String> { get }
  var displayVerifySuccess: PublishSubject<Void> { get }
  var loading: BehaviorSubject<Bool> { get }
}

protocol InAppVerifyPhoneNumberViewModelCoordinates {
  var navigateBack: PublishSubject<Void> { get }
}

protocol InAppVerifyPhoneNumberViewModelType: ErrorAlertableViewModelType {
  var inputs: InAppVerifyPhoneNumberViewModelInputs { get }
  var outputs: InAppVerifyPhoneNumberViewModelOutputs { get }
  var coordinates: InAppVerifyPhoneNumberViewModelCoordinates { get }
}

class InAppVerifyPhoneNumberViewModel: InAppVerifyPhoneNumberViewModelType, InAppVerifyPhoneNumberViewModelInputs, InAppVerifyPhoneNumberViewModelOutputs, InAppVerifyPhoneNumberViewModelCoordinates {
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  var verifyID = ""
  
  // MARK: Init
    
  public init() {
    guard let userModel = LocalDataService.getUser() else { return }
    
    displayPhoneNumber
      .accept(userModel.countryCode + " " + "xx-xxx-" + userModel.phoneNumber.suffix(4))
    
    sendOTPTrigger
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
      .map{ userModel.countryCode+userModel.phoneNumber }
      .subscribe(onNext: { [unowned self] phoneNumber in
        Auth.auth().languageCode = "th";
        Auth.auth().currentUser?.getIDToken(completion: { (result, error) in
          if let _ = error {
            self.error.onNext((title: "Error", message: error?.localizedDescription ?? ""))
          } else {
            LocalDataService.setUserToken(token: result ?? "")
            PhoneAuthProvider.provider()
              .verifyPhoneNumber(phoneNumber, uiDelegate: nil) { [unowned self] (verificationID, error) in
                self.loading.onNext(false)
                if let _ = error {
                  self.error.onNext((title: "Error", message: error?.localizedDescription ?? ""))
                  return
                }
                if let verifyID = verificationID {
                  self.verifyID = verifyID
                } else {
                  self.error.onNext((title: "Error", message: "Can not get verificationID"))
                }
            }
          }
        })
        
      })
      .disposed(by: disposeBag)
    
    submitButtonTrigger
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
      .subscribe(onNext: { [unowned self] otpCode in
        guard let prevUser = Auth.auth().currentUser else {
          self.error.onNext((title: "Error", message: "Can not get current user"))
          self.outputs.loading.onNext(false)
          return }
        let credential =
          PhoneAuthProvider.provider().credential(withVerificationID: self.verifyID,
                                                  verificationCode: otpCode)
        
        
        
        
//        prevUser.link(with: credential) { (result, error) in
//          self.outputs.loading.onNext(false)
//          if let error = error {
//            self.error.onNext((title: "Error", message: error.localizedDescription))
//          }
//        }
        
//        Auth.auth().signIn(with: credential) { (response, error) in
//          if let error = error {
//            self.error.onNext((title: "Error", message: error.localizedDescription))
//            return
//          }
//
//        }
//
//          if let result = response {
////            guard let cred = result.credential else {
////              self.error.onNext((title: "Error", message: "Can not get credential"))
////              self.outputs.loading.onNext(false)
////              return
////            }
//
//            prevUser.link(with: credential, completion: { (result, error) in
//              if let error = error {
//                self.error.onNext((title: "Error", message: error.localizedDescription))
//              }
//              if let _ = result {
//                self.displayVerifySuccess.onNext(())
//              } else {
//                self.error.onNext((title: "inappphonenumberverify_otp_wrong_title".localized(),
//                                   message: "inappphonenumberverify_otp_wrong_message".localized()))
//              }
//            })
//          } else {
//            self.error.onNext((title: "inappphonenumberverify_otp_wrong_title".localized(),
//                              message: "inappphonenumberverify_otp_wrong_message".localized()))
//          }
//
//        }
        
        
        
//        prevUser.reauthenticate(with: credential) { (result, error) in
//          if let error = error {
//            self.error.onNext((title: "Error", message: error.localizedDescription))
//          }
//        }
        
        Auth.auth().signIn(with: credential) { (result, error) in
          self.loading.onNext(false)
          if let _ = error {
            self.error.onNext((title: "inappphonenumberverify_otp_wrong_title".localized(),
                               message: "inappphonenumberverify_otp_wrong_message".localized()))
            return
          }

          self.loading.onNext(true)
          if let currentUser = result?.user {
            currentUser.delete { (error) in
              self.loading.onNext(false)
              if let error = error {
                self.error.onNext((title: "Error", message: error.localizedDescription))
              } else {
                let auth = LocalDataService.getAuthen()
                self.loading.onNext(true)
                Auth.auth().signIn(withEmail: auth?["email"] as? String ?? "", password: auth?["password"] as? String ?? "") { (result, error) in
                  self.loading.onNext(false)
                  if let error = error {
                    self.error.onNext((title: "Error", message: error.localizedDescription))
                  } else {
                    self.displayVerifySuccess.onNext(())
                  }
                }
              }
            }
          } else {
            self.error.onNext((title: "inappphonenumberverify_otp_wrong_title".localized(),
                               message: "inappphonenumberverify_otp_wrong_message".localized()))
          }
        }
        
      })
      .disposed(by: disposeBag)
    
  }
  
  // MARK: Private
  
  // MARK: Input
  
  var sendOTPTrigger = PublishSubject<Void>()
  var submitButtonTrigger = PublishSubject<String>()
    
  // MARK: Output
  
  var displayPhoneNumber = BehaviorRelay<String>(value: "")
  var loading = BehaviorSubject<Bool>(value: false)
  var error = PublishSubject<(title: String, message: String)>()
  var displayVerifySuccess = PublishSubject<Void>()

  // MARK: Coordinates
  
  var navigateBack = PublishSubject<Void>()
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: InAppVerifyPhoneNumberViewModelOutputs { return self }
  public var inputs: InAppVerifyPhoneNumberViewModelInputs { return self }
  public var coordinates: InAppVerifyPhoneNumberViewModelCoordinates { return self }
}
