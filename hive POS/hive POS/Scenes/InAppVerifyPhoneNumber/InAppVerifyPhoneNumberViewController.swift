//
//  InAppVerifyPhoneNumberViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 2/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZSwiftKit

protocol InAppVerifyPhoneNumberViewControllerDelegate: class {
  func didVerifySuccess()
}

class InAppVerifyPhoneNumberViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var verifyTitleLabel: UILabel!
  @IBOutlet weak var backButton: UIButton!
  
  @IBOutlet weak var otp1Label: UILabel!
  @IBOutlet weak var otp1ImageView: UIImageView!
  
  @IBOutlet weak var otp2Label: UILabel!
  @IBOutlet weak var otp2ImageView: UIImageView!
  
  @IBOutlet weak var otp3Label: UILabel!
  @IBOutlet weak var otp3ImageView: UIImageView!
  
  @IBOutlet weak var otp4Label: UILabel!
  @IBOutlet weak var otp4ImageView: UIImageView!
  
  @IBOutlet weak var otp5Label: UILabel!
  @IBOutlet weak var otp5ImageView: UIImageView!
  
  @IBOutlet weak var otp6Label: UILabel!
  @IBOutlet weak var otp6ImageView: UIImageView!
  
  
  @IBOutlet weak var pad1Button: UIButton!
  @IBOutlet weak var pad2Button: UIButton!
  @IBOutlet weak var pad3Button: UIButton!
  @IBOutlet weak var pad4Button: UIButton!
  @IBOutlet weak var pad5Button: UIButton!
  @IBOutlet weak var pad6Button: UIButton!
  @IBOutlet weak var pad7Button: UIButton!
  @IBOutlet weak var pad8Button: UIButton!
  @IBOutlet weak var pad9Button: UIButton!
  @IBOutlet weak var pad0Button: UIButton!
  @IBOutlet weak var padDelButton: UIButton!
  
  @IBOutlet weak var submitButton: UIButton!
  @IBOutlet weak var sendOTPAgainButton: UIButton!
  
  @IBOutlet weak var phoneNumberLabel: UILabel!
  
  // MARK: Property
  
  var viewModel: InAppVerifyPhoneNumberViewModelType!
  let disposeBag = DisposeBag()
  var otpNumbers: [String] = []
  weak var delegate: InAppVerifyPhoneNumberViewControllerDelegate?
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
    setupL10N()
    bindViewModel()
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    viewModel.inputs.sendOTPTrigger.onNext(())
  }
  
  func bindViewModel() {
    
    backButton.rx.tap
      .bind(to: viewModel.coordinates.navigateBack)
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayPhoneNumber
      .subscribe(onNext: { [unowned self] phoneNumber in self.phoneNumberLabel.text = phoneNumber })
      .disposed(by: disposeBag)
    
    pad0Button.rx.tap
      .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "0") })
      .disposed(by: disposeBag)
    
    pad1Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "1") })
    .disposed(by: disposeBag)
    
    pad2Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "2") })
    .disposed(by: disposeBag)
    
    pad3Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "3") })
    .disposed(by: disposeBag)
    
    pad4Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "4") })
    .disposed(by: disposeBag)
    
    pad5Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "5") })
    .disposed(by: disposeBag)
    
    pad6Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "6") })
    .disposed(by: disposeBag)
    
    pad7Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "7") })
    .disposed(by: disposeBag)
    
    pad8Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "8") })
    .disposed(by: disposeBag)
    
    pad9Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in self.insertOTP(number: "9") })
    .disposed(by: disposeBag)
    
    padDelButton.rx.tap
      .subscribe(onNext: { [unowned self] in self.deleteOTP() })
      .disposed(by: disposeBag)
    
    sendOTPAgainButton.rx.tap
      .bind(to: viewModel.inputs.sendOTPTrigger)
      .disposed(by: disposeBag)
    
    submitButton.rx.tap
    .filter{ [unowned self] in self.otpNumbers.count == 6 }
    .map{ [unowned self] in self.otpNumbers.map{$0}.joined() }
    .bind(to: viewModel.inputs.submitButtonTrigger)
    .disposed(by: disposeBag)
    
    viewModel.outputs
      .displayVerifySuccess
      .do(onNext: { [unowned self] _ in self.delegate?.didVerifySuccess() })
      .bind(to: viewModel.coordinates.navigateBack)
      .disposed(by: disposeBag)
    
    //MARK: Bind loading
    
    viewModel.outputs.loading
      .subscribe(onNext: { [unowned self] isLoading in
        LoadingActivityView.action(in: self.view, isShow: isLoading)})
      .disposed(by: disposeBag)
    
    // MARK: Bind Error
          
    viewModel.error
      .subscribe(onNext: { [unowned self] error in
          Helper.Alert(
              view: self,
              title: error.title,
              message: error.message,
              confirmText: "done".localized())})
      .disposed(by: disposeBag)
    
  }
  
  func setupUI() {
    
    submitButton.layer.cornerRadius = submitButton.frame.height / 2
    
  }
  
  func setupL10N() {
    verifyTitleLabel.text = "inappphonenumberverify_title".localized()
    sendOTPAgainButton.setTitle("inappphonenumberverify_renew_otp".localized(), for: .normal)
    submitButton.setTitle("ok".localized(), for: .normal)
  }
  
  func insertOTP(number: String) {
    if otpNumbers.count > 5 { return }
    otpNumbers.append(number)
    setupOTP()
    print(otpNumbers)
  }
  
  func deleteOTP() {
    print(otpNumbers)
    if otpNumbers.count == 0 { return }
    otpNumbers.remove(at: otpNumbers.count-1)
    setupOTP()
  }
  
  func setupOTP() {
    
    if otpNumbers.count == 1 {
      otp1Label.text = otpNumbers[0]
      otp1Label.isHidden = false
      otp1ImageView.isHidden = true
      
      otp2Label.isHidden = true
      otp2ImageView.isHidden = false
      
      otp3Label.isHidden = true
      otp3ImageView.isHidden = false
      
      otp4Label.isHidden = true
      otp4ImageView.isHidden = false
      
      otp5Label.isHidden = true
      otp5ImageView.isHidden = false
      
      otp6Label.isHidden = true
      otp6ImageView.isHidden = false
    } else if otpNumbers.count == 2 {
      otp1Label.text = otpNumbers[0]
      otp1Label.isHidden = false
      otp1ImageView.isHidden = true
      
      otp2Label.text = otpNumbers[1]
      otp2Label.isHidden = false
      otp2ImageView.isHidden = true
      
      otp3Label.isHidden = true
      otp3ImageView.isHidden = false
      
      otp4Label.isHidden = true
      otp4ImageView.isHidden = false
      
      otp5Label.isHidden = true
      otp5ImageView.isHidden = false
      
      otp6Label.isHidden = true
      otp6ImageView.isHidden = false
    } else if otpNumbers.count == 3 {
      otp1Label.text = otpNumbers[0]
      otp1Label.isHidden = false
      otp1ImageView.isHidden = true
      
      otp2Label.text = otpNumbers[1]
      otp2Label.isHidden = false
      otp2ImageView.isHidden = true
      
      otp3Label.text = otpNumbers[2]
      otp3Label.isHidden = false
      otp3ImageView.isHidden = true
      
      otp4Label.isHidden = true
      otp4ImageView.isHidden = false
      
      otp5Label.isHidden = true
      otp5ImageView.isHidden = false
      
      otp6Label.isHidden = true
      otp6ImageView.isHidden = false
    } else if otpNumbers.count == 4 {
      otp1Label.text = otpNumbers[0]
      otp1Label.isHidden = false
      otp1ImageView.isHidden = true
      
      otp2Label.text = otpNumbers[1]
      otp2Label.isHidden = false
      otp2ImageView.isHidden = true
      
      otp3Label.text = otpNumbers[2]
      otp3Label.isHidden = false
      otp3ImageView.isHidden = true
      
      otp4Label.text = otpNumbers[3]
      otp4Label.isHidden = false
      otp4ImageView.isHidden = true
      
      otp5Label.isHidden = true
      otp5ImageView.isHidden = false
      
      otp6Label.isHidden = true
      otp6ImageView.isHidden = false
    } else if otpNumbers.count == 5 {
      otp1Label.text = otpNumbers[0]
      otp1Label.isHidden = false
      otp1ImageView.isHidden = true
      
      otp2Label.text = otpNumbers[1]
      otp2Label.isHidden = false
      otp2ImageView.isHidden = true
      
      otp3Label.text = otpNumbers[2]
      otp3Label.isHidden = false
      otp3ImageView.isHidden = true
      
      otp4Label.text = otpNumbers[3]
      otp4Label.isHidden = false
      otp4ImageView.isHidden = true
      
      otp5Label.text = otpNumbers[4]
      otp5Label.isHidden = false
      otp5ImageView.isHidden = true
      
      otp6Label.isHidden = true
      otp6ImageView.isHidden = false
    } else if otpNumbers.count == 6 {
      otp1Label.text = otpNumbers[0]
      otp1Label.isHidden = false
      otp1ImageView.isHidden = true
      
      otp2Label.text = otpNumbers[1]
      otp2Label.isHidden = false
      otp2ImageView.isHidden = true
      
      otp3Label.text = otpNumbers[2]
      otp3Label.isHidden = false
      otp3ImageView.isHidden = true
      
      otp4Label.text = otpNumbers[3]
      otp4Label.isHidden = false
      otp4ImageView.isHidden = true
      
      otp5Label.text = otpNumbers[4]
      otp5Label.isHidden = false
      otp5ImageView.isHidden = true
      
      otp6Label.text = otpNumbers[5]
      otp6Label.isHidden = false
      otp6ImageView.isHidden = true
    } else if otpNumbers.count == 0 {
      otp1Label.isHidden = true
      otp1ImageView.isHidden = false
      
      otp2Label.isHidden = true
      otp2ImageView.isHidden = false
      
      otp3Label.isHidden = true
      otp3ImageView.isHidden = false
      
      otp4Label.isHidden = true
      otp4ImageView.isHidden = false
      
      otp5Label.isHidden = true
      otp5ImageView.isHidden = false
      
      otp6Label.isHidden = true
      otp6ImageView.isHidden = false
    }
  }
}
