//
//  Menu+DataSource.swift
//  hive POS
//
//  Created by Space-C4ts on 11/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import RxDataSources

struct DisplayTabMenuData {
  var title: String
  var ID: Int
}

struct SectionOfDisplayTabMenuData {
  var header: String
  var items: [Item]
}

extension SectionOfDisplayTabMenuData: SectionModelType {
  typealias Item = DisplayTabMenuData
  
  init(original: SectionOfDisplayTabMenuData, items: [Item]) {
    self = original
    self.items = items
  }
}
