//
//  MenuCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 10/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol MenuInputsParamsType {
  var categoryID: String { get set }
  var menuData: DisplayMenuData? { get set }
}

class MenuCoordinator: BaseCoordinator<Void>, MenuInputsParamsType {
  
  // MARK: Property
  
  var categoryID: String = ""
  var menuData: DisplayMenuData?

  // MARK: Public

  override func start() -> Observable<Void> {
    let window = self.window
    let viewModel = MenuViewModel(categoryID: categoryID, menuData: menuData)
    let viewController = MenuViewController.initFromStoryboard(name: Storyboard.menu.identifier)
    viewController.viewModel = viewModel
    viewController.hidesBottomBarWhenPushed = true
    
    transition(to: viewController)
    return viewModel.coordinates.navigateBack
    .do(onNext: { [weak self] _ in self?.navigateBack()})
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: MenuInputsParamsType { return self }
}
