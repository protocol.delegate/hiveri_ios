//
//  MenuViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 10/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources
import MMNumberKeyboard
import ZSwiftKit
import Firebase

class MenuViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var submitButtonTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var submitButtonHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var editLogoView: UIView!
  @IBOutlet var dropDownButtons: [UIButton]!
  @IBOutlet var starLabels: [UILabel]!
  @IBOutlet weak var editButton: UIButton!
  @IBOutlet weak var printerTextView: UITextView!
  @IBOutlet weak var deleteButton: UIButton!
  @IBOutlet weak var menuCollectonView: UICollectionView!
  @IBOutlet weak var detailView: UIView!
  @IBOutlet weak var detailButtom: UIButton!
  @IBOutlet weak var tabMenuCollectionView: UICollectionView!
  @IBOutlet weak var statusSwitch: UISwitch!
  @IBOutlet weak var statusLabel: UILabel!
  @IBOutlet weak var statusView: UIView!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var priceTextField: UITextField!
  @IBOutlet weak var menuImageLabel: UILabel!
  @IBOutlet weak var menuImageButton: UIButton!
  @IBOutlet weak var menuImageView: UIImageView!
  
  @IBOutlet weak var menuNumberLabel: UILabel!
  @IBOutlet weak var menuNumberTextField: UITextField!
  
  @IBOutlet weak var brandTitle: UILabel!
  @IBOutlet weak var brandButton: UIButton!
  @IBOutlet weak var brandTextField: UITextField!
  
  @IBOutlet weak var branchTextField: UITextField!
  @IBOutlet weak var branchButton: UIButton!
  @IBOutlet weak var branchLabel: UILabel!
  
  @IBOutlet weak var categoryButton: UIButton!
  @IBOutlet weak var categoryLabel: UILabel!
  @IBOutlet weak var categoryTextField: UITextField!
  
  @IBOutlet weak var menuNameTHLabel: UILabel!
  @IBOutlet weak var menuNameTHTextField: UITextField!
  
  @IBOutlet weak var menuNameENLabel: UILabel!
  @IBOutlet weak var menuNameENTextField: UITextField!
  
  @IBOutlet weak var menuDetailTHLabel: UILabel!
  @IBOutlet weak var menuDetailTHTextView: UITextView!
  @IBOutlet weak var menuDetailTHView: UIView!
  
  @IBOutlet weak var menuDetailENLabel: UILabel!
  @IBOutlet weak var menuDetailENTextView: UITextView!
  @IBOutlet weak var menuDetailENView: UIView!
  
  @IBOutlet weak var priceGenreView: UIView!
  @IBOutlet weak var priceGenreLabel: UILabel!
  @IBOutlet weak var priceNormalButton: UIButton!
  @IBOutlet weak var priceRefillButton: UIButton!
  @IBOutlet weak var priceCustomButton: UIButton!
  @IBOutlet weak var pricePerKGButton: UIButton!
  @IBOutlet weak var priceBuffeButton: UIButton!
  @IBOutlet weak var priceBuffeTextField: UITextField!
  @IBOutlet weak var priceBuffeLabel: UILabel!
  
  
  @IBOutlet weak var spacialView: UIView!
  @IBOutlet weak var spacialLabel: UILabel!
  @IBOutlet weak var spacialInStoreButton: UIButton!
  @IBOutlet weak var spacialTakeAwayButton: UIButton!
  @IBOutlet weak var spacialDeliveryButton: UIButton!
  
  @IBOutlet weak var printerView: UIView!
  @IBOutlet weak var printerLabel: UILabel!
  
  @IBOutlet weak var menuTypeLabel: UILabel!
  @IBOutlet weak var menuTypeSignatureButton: UIButton!
  @IBOutlet weak var menuTypeSeasonButton: UIButton!
  @IBOutlet weak var menuTypeTakeTime: UIButton!
  @IBOutlet weak var menuTypeView: UIView!
  
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var timeTextField: UITextField!
  @IBOutlet weak var timeUnit: UILabel!
  
  @IBOutlet weak var submitButton: UIButton!
  
  @IBOutlet weak var editImageView: UIView!
  @IBOutlet weak var editImageLabel: UILabel!
  
  // MARK: Property
  
  var viewModel: MenuViewModelType!
  let disposeBag = DisposeBag()
  var categoryDatas: [MenuCategoryModel]?
  var selectCategoryID: String?
  var categoryID: String = ""
  var menuImageID: String?
  var menuImagePath: String?
  var menuPriceType: MenuPriceType?
  var menuOptionType: [MenuOptionType] = []
  var menuType: [MenuType] = []
  var isFromEdit: Bool = false
  var menuData: DisplayMenuData?
  lazy var storage = Storage.storage(url:"gs://hive-world.appspot.com")
  var textFieldOffSet: CGFloat = 16
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
    setupL10N()
    bindViewModel()
    setupBackButton()
  }
  
  func bindViewModel() {
    
    viewModel.outputs.categoryID
      .subscribe(onNext: { [unowned self] ID in self.categoryID = ID })
      .disposed(by: disposeBag)
    
    let dataSource = RxCollectionViewSectionedReloadDataSource<SectionOfDisplayTabMenuData>(
    configureCell: { dataSource, collectionView, indexPath, item in
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: TabMenuCollectionViewCell.className, for: indexPath) as! TabMenuCollectionViewCell
      cell.configure(title: item.title, isSelect: true)
      return cell})
    
    viewModel.outputs.displayTabMenu
      .bind(to: tabMenuCollectionView.rx.items(dataSource: dataSource))
      .disposed(by: disposeBag)
    
    viewModel.inputs
      .getTabMenuTrigger
      .onNext(())
    
    viewModel.outputs
      .displayMenuCategory
      .subscribe(onNext: { [unowned self] model in
        self.categoryDatas = model
        if model.count > 0 {
          if self.categoryID != "" {
            if let filter = model.filter({$0.categoryID == self.categoryID}).first {
              self.selectCategoryID = filter.categoryID
              self.categoryTextField.text = filter.categoryName
            } else {
              self.selectCategoryID = model[0].categoryID
              self.categoryTextField.text = model[0].categoryName
            }
          } else {
            self.selectCategoryID = model[0].categoryID
            self.categoryTextField.text = model[0].categoryName
          }
        }
      })
      .disposed(by: disposeBag)
    
    viewModel
      .inputs
      .getMenuCategoryTrigger
      .onNext(())
    
    menuImageButton.rx.tap
      .subscribe(onNext: { _ in
        let imagePicker = UIImagePickerController()
        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        self.present(imagePicker, animated: true, completion: nil)
      })
      .disposed(by: disposeBag)
    
    categoryButton.rx.tap
      .map{ [unowned self] in self.categoryDatas}
      .filterNil()
      .subscribe(onNext: { model in
        let controller = ArrayChoiceMenuCategoryTableViewController<Any>(model) { [unowned self] (selectModel) in
          self.categoryTextField.text = selectModel.categoryName
          self.selectCategoryID = selectModel.categoryID
        }
        controller.tableView.separatorStyle = .none
        controller.preferredContentSize = CGSize(width: 200, height: 44 * model.count)
        self.presentPopOverView(controller, sourceView: self.categoryTextField)
      })
      .disposed(by: disposeBag)
    
    menuDetailTHTextView.rx
      .didBeginEditing.subscribe(onNext: { [unowned self] _ in
        if self.menuDetailTHTextView.textColor == UIColor(hex: 0x8E8E93).withAlphaComponent(0.5) {
          self.menuDetailTHTextView.text = nil
          self.menuDetailTHTextView.textColor = .black}})
      .disposed(by: disposeBag)

    menuDetailTHTextView.rx
      .didEndEditing
      .subscribe(onNext:{ _ in
        if self.menuDetailTHTextView.text.isEmpty {
          self.menuDetailTHTextView.text = "menu_detailth_placeholder".localized()
          self.menuDetailTHTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)}})
      .disposed(by: disposeBag)
    
    menuDetailENTextView.rx
      .didBeginEditing.subscribe(onNext: { [unowned self] _ in
        if self.menuDetailENTextView.textColor == UIColor(hex: 0x8E8E93).withAlphaComponent(0.5) {
          self.menuDetailENTextView.text = nil
          self.menuDetailENTextView.textColor = .black}})
      .disposed(by: disposeBag)

    menuDetailENTextView.rx
      .didEndEditing
      .subscribe(onNext:{ _ in
        if self.menuDetailENTextView.text.isEmpty {
          self.menuDetailENTextView.text = "menu_detailen_placeholder".localized()
          self.menuDetailENTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)}})
      .disposed(by: disposeBag)
    
    priceNormalButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.priceNormalButton.isSelected = true
        if self.priceNormalButton.isSelected {
          self.menuPriceType = .normal
          self.priceRefillButton.isSelected = false
          self.priceCustomButton.isSelected = false
          self.pricePerKGButton.isSelected = false
          self.priceBuffeButton.isSelected = false
          self.priceBuffeTextField.text = nil
          self.priceBuffeTextField.isUserInteractionEnabled = false
        }
      })
      .disposed(by: disposeBag)
    
    priceRefillButton.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      self.priceRefillButton.isSelected = true
      if self.priceRefillButton.isSelected {
        self.menuPriceType = .refill
        self.priceNormalButton.isSelected = false
        self.priceCustomButton.isSelected = false
        self.pricePerKGButton.isSelected = false
        self.priceBuffeButton.isSelected = false
        self.priceBuffeTextField.text = nil
        self.priceBuffeTextField.isUserInteractionEnabled = false
      }
    })
    .disposed(by: disposeBag)
    
    priceCustomButton.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      self.priceCustomButton.isSelected = true
      if self.priceCustomButton.isSelected {
        self.menuPriceType = .custom
        self.priceNormalButton.isSelected = false
        self.priceRefillButton.isSelected = false
        self.pricePerKGButton.isSelected = false
        self.priceBuffeButton.isSelected = false
        self.priceBuffeTextField.text = nil
        self.priceBuffeTextField.isUserInteractionEnabled = false
      }
    })
    .disposed(by: disposeBag)
    
    pricePerKGButton.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      self.pricePerKGButton.isSelected = true
      if self.pricePerKGButton.isSelected {
        self.menuPriceType = .perKG
        self.priceNormalButton.isSelected = false
        self.priceRefillButton.isSelected = false
        self.priceCustomButton.isSelected = false
        self.priceBuffeButton.isSelected = false
        self.priceBuffeTextField.text = nil
        self.priceBuffeTextField.isUserInteractionEnabled = false
      }
    })
    .disposed(by: disposeBag)
    
    priceBuffeButton.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      self.priceBuffeButton.isSelected = true
      if self.priceBuffeButton.isSelected {
        self.menuPriceType = .buffe
        self.priceNormalButton.isSelected = false
        self.priceRefillButton.isSelected = false
        self.priceCustomButton.isSelected = false
        self.pricePerKGButton.isSelected = false
        self.priceBuffeTextField.text = nil
        self.priceBuffeTextField.isUserInteractionEnabled = true
//        self.priceBuffeTextField.becomeFirstResponder()
      }
    })
    .disposed(by: disposeBag)
    
    spacialInStoreButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.spacialInStoreButton.isSelected = !self.spacialInStoreButton.isSelected
        if self.spacialInStoreButton.isSelected {
          self.menuOptionType.append(.inhouse)
        } else {
          for (index, option) in self.menuOptionType.enumerated() {
            if option == .inhouse {
              self.menuOptionType.remove(at: index)
            }
          }
        }
      })
      .disposed(by: disposeBag)
    
    spacialTakeAwayButton.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      self.spacialTakeAwayButton.isSelected = !self.spacialTakeAwayButton.isSelected
      if self.spacialTakeAwayButton.isSelected {
        self.menuOptionType.append(.takeAway)
      } else {
        for (index, option) in self.menuOptionType.enumerated() {
          if option == .takeAway {
            self.menuOptionType.remove(at: index)
          }
        }
      }
    })
    .disposed(by: disposeBag)
    
    spacialDeliveryButton.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      self.spacialDeliveryButton.isSelected = !self.spacialDeliveryButton.isSelected
      if self.spacialDeliveryButton.isSelected {
        self.menuOptionType.append(.delivery)
      } else {
        for (index, option) in self.menuOptionType.enumerated() {
          if option == .delivery {
            self.menuOptionType.remove(at: index)
          }
        }
      }
    })
    .disposed(by: disposeBag)
    
    menuTypeSignatureButton.rx.tap
      .subscribe(onNext: { _ in
        self.menuTypeSignatureButton.isSelected = !self.menuTypeSignatureButton.isSelected
        if self.menuTypeSignatureButton.isSelected {
          self.menuType.append(.signature)
        } else {
          for (index, type) in self.menuType.enumerated() {
            if type == .signature {
              self.menuType.remove(at: index)
            }
          }
        }
      })
      .disposed(by: disposeBag)
    
    menuTypeSeasonButton.rx.tap
    .subscribe(onNext: { _ in
      self.menuTypeSeasonButton.isSelected = !self.menuTypeSeasonButton.isSelected
      if self.menuTypeSeasonButton.isSelected {
        self.menuType.append(.season)
      } else {
        for (index, type) in self.menuType.enumerated() {
          if type == .season {
            self.menuType.remove(at: index)
          }
        }
      }
    })
    .disposed(by: disposeBag)
    
    menuTypeTakeTime.rx.tap
    .subscribe(onNext: { _ in
      self.menuTypeTakeTime.isSelected = !self.menuTypeTakeTime.isSelected
      if self.menuTypeTakeTime.isSelected {
        self.menuType.append(.takeTime)
      } else {
        for (index, type) in self.menuType.enumerated() {
          if type == .takeTime {
            self.menuType.remove(at: index)
          }
        }
      }
    })
    .disposed(by: disposeBag)
    
    submitButton.rx.tap
      .filter{ [unowned self] _ in self.validateField()}
      .subscribe(onNext: { [unowned self] _ in
        if self.isFromEdit {
          Helper.AlertConfirm(view: self,
                              title: "menu_edit_confirm_title".localized(),
                              message: "menu_edit_confirm_message".localized(),
                              confirmText: "ok".localized(),
                              cancelText: "cancel".localized(),
                              isInverseButtonPos: true,
                              confirmCompletion: { (_) in
            guard let data = self.menuData else { return }
            var detailEN = ""
            if self.menuDetailENTextView.text != "menu_detailen_placeholder".localized() {
              detailEN = self.menuDetailENTextView.text
            }
            var detailTH = ""
            if self.menuDetailTHTextView.text != "menu_detailth_placeholder".localized() {
              detailTH = self.menuDetailTHTextView.text
            }
            
            guard let brandData = LocalDataService.getSelectBrand() else { return }
            guard let branchData = BranchSignService.shared.getBranchData() else { return }
            let menuOption = self.menuOptionType.map{ $0.rawValue }
            let menuType = self.menuType.map{ $0.rawValue }
            let menuData = MenuData(menuNO: self.menuNumberTextField.text ?? "",
                                    brandID: brandData.ID,
                                    branchID: branchData.ID,
                                    categoryID: self.selectCategoryID ?? self.categoryID,
                                    menuImage: self.menuImagePath ?? "",
                                    nameTH: self.menuNameTHTextField.text ?? "",
                                    nameEN: self.menuNameENTextField.text ?? "",
                                    detailTH: detailTH,
                                    detailEN: detailEN,
                                    price: Double(self.priceTextField.text ?? "") ?? 0,
                                    priceType: self.menuPriceType?.rawValue,
                                    priceDetail: self.priceBuffeTextField.text ?? "",
                                    menuOption: menuOption,
                                    menuType: menuType,
                                    timeToCook: Int(self.timeTextField.text ?? "0") ?? 0,
                                    isPublish: self.statusSwitch.isOn)
            self.viewModel.inputs.editMenuTrigger.onNext((menuData: menuData, menuID: data.menuID))
          }, cancelCompletion: nil)
          
        } else {
          var detailEN = ""
          if self.menuDetailENTextView.text != "menu_detailen_placeholder".localized() {
            detailEN = self.menuDetailENTextView.text
          }
          var detailTH = ""
          if self.menuDetailTHTextView.text != "menu_detailth_placeholder".localized() {
            detailTH = self.menuDetailTHTextView.text
          }
          
          guard let brandData = LocalDataService.getSelectBrand() else { return }
          guard let branchData = BranchSignService.shared.getBranchData() else { return }
          let menuOption = self.menuOptionType.map{ $0.rawValue }
          let menuType = self.menuType.map{ $0.rawValue }
          let menuData = MenuData(menuNO: self.menuNumberTextField.text ?? "",
                                  brandID: brandData.ID,
                                  branchID: branchData.ID,
                                  categoryID: self.selectCategoryID ?? self.categoryID,
                                  menuImage: self.menuImagePath ?? "",
                                  nameTH: self.menuNameTHTextField.text ?? "",
                                  nameEN: self.menuNameENTextField.text ?? "",
                                  detailTH: detailTH,
                                  detailEN: detailEN,
                                  price: Double(self.priceTextField.text ?? "") ?? 0,
                                  priceType: self.menuPriceType?.rawValue,
                                  priceDetail: self.priceBuffeTextField.text ?? "",
                                  menuOption: menuOption,
                                  menuType: menuType,
                                  timeToCook: Int(self.timeTextField.text ?? "0") ?? 0,
                                  isPublish: self.statusSwitch.isOn)
          self.viewModel.inputs.createMenuTrigger.onNext(menuData)
        }
      })
      .disposed(by: disposeBag)
    
    viewModel.outputs
      .displayCreateMenuSuccess
      .subscribe(onNext: { _ in
        Helper.Alert(view: self, title: "menu_create_success_title".localized(), message: "menu_create_success_message".localized(),
                     confirmText: "ok".localized()) { [unowned self] (_) in
                      self.viewModel.coordinates.navigateBack.onNext(())
        }})
      .disposed(by: disposeBag)
    
    //MARK: Edit
    
    viewModel
      .outputs
      .menuData
      .filterNil()
      .subscribe(onNext: { [unowned self] model in
        self.submitButton.isHidden = true
        self.menuData = model
        self.isFromEdit = true
        self.disableField()
        self.editButton.isHidden = false
        self.deleteButton.isHidden = true
        self.statusSwitch.isOn = model.isPublish
        self.priceTextField.text = String(format: "%.2f", model.price)
        self.selectCategoryID = model.categoryID
        self.menuImagePath = model.imagePath
        self.menuImageView.kf.setImage(with: URL(string: model.imagePath),
                                       placeholder: UIImage(named: "ic_default_image"))
        self.menuNumberTextField.text = model.menuNumber
        self.menuNameTHTextField.text = model.nameTH
        self.menuNameENTextField.text = model.nameEN
        if model.detailTH != "" {
          self.menuDetailTHTextView.text = model.detailTH
          self.menuDetailTHTextView.textColor = .black
        }
        
        if model.detailEN != "" {
          self.menuDetailENTextView.text = model.detailEN
          self.menuDetailENTextView.textColor = .black
        }
        
        self.menuPriceType = MenuPriceType(rawValue: model.priceType)
        switch self.menuPriceType {
        case .normal:
          self.priceNormalButton.isSelected = true
        case .refill:
          self.priceRefillButton.isSelected = true
        case .custom:
          self.priceCustomButton.isSelected = true
        case .perKG:
          self.pricePerKGButton.isSelected = true
        case .buffe:
          self.priceBuffeButton.isSelected = true
          self.priceBuffeTextField.text = model.priceDetail
        default:
          break
        }
        
        self.menuOptionType = model.menuOption.map{(MenuOptionType(rawValue: $0) ?? .inhouse)}
        for option in self.menuOptionType {
        switch option {
          case .inhouse:
            self.spacialInStoreButton.isSelected = true
          case .delivery:
            self.spacialDeliveryButton.isSelected = true
          case .takeAway:
            self.spacialTakeAwayButton.isSelected = true
          }
        }
        
        
        self.menuType = model.menuType.map{MenuType(rawValue: $0) ?? .signature}
        for type in self.menuType {
          switch type {
          case.signature:
            self.menuTypeSignatureButton.isSelected = true
          case .season:
            self.menuTypeSeasonButton.isSelected = true
          case .takeTime:
            self.menuTypeTakeTime.isSelected = true
          }
        }
        self.timeTextField.text = "\(model.timeToCook)"
        self.setNavigationBarItem(titlePageName: "menu_edit_title".localized(), isBackButton: true)
        self.setupBackButton()
        
      })
      .disposed(by: disposeBag)
    
    editButton.rx.tap
      .subscribe(onNext: { [unowned self] in
        self.enableField()
        self.editButton.isHidden = true
        self.submitButton.isHidden = false
        self.deleteButton.isUserInteractionEnabled = true
//        self.menuNumberTextField.becomeFirstResponder()
      })
      .disposed(by: disposeBag)
    
    viewModel.outputs.displayEditMenuSuccess
      .subscribe(onNext: { [unowned self] _ in
        Helper.Alert(view: self, title: "menu_edit_success_title".localized(),
                     message: "menu_edit_success_message".localized(),
                     confirmText: "ok".localized()) { (_) in
          self.viewModel.coordinates.navigateBack.onNext(())
        }
      })
      .disposed(by: disposeBag)
    
    //MARK: Delete
    
    deleteButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        guard let menuData = self.menuData else { return }
        Helper.AlertConfirm(view: self,
                            title: "menu_delete_title".localized(),
                            message: "menu_delete_message".localized(),
                            confirmText: "ok".localized(),
                            cancelText: "cancel".localized(),
                            isInverseButtonPos: true,
                            confirmCompletion: { (_) in
                              self.viewModel.inputs.deleteMenuTrigger.onNext(menuData.menuID)
        }, cancelCompletion: nil)
      })
      .disposed(by: disposeBag)
    
    viewModel.outputs
      .displayDeleteMenuSuccess
      .subscribe(onNext: { [unowned self] _ in
        Helper.Alert(view: self, title: "menu_delete_success_title".localized(),
                     message: "menu_delete_success_message".localized(),
                     confirmText: "ok".localized()) { [unowned self] (_) in
                      self.viewModel.coordinates.navigateBack.onNext(())
        }
      })
      .disposed(by: disposeBag)
    
    //MARK: Bind loading
       
    viewModel.outputs.loading
      .subscribe(onNext: { [unowned self] isLoading in
        guard let tabBarController = self.tabBarController else { return }
        LoadingActivityView.action(in: tabBarController.view, isShow: isLoading)})
      .disposed(by: disposeBag)
    
    // MARK: Bind Error
          
    viewModel.error
      .subscribe(onNext: { [unowned self] error in
          Helper.Alert(
            view: self,
              title: error.title,
              message: error.message,
              confirmText: "done".localized())})
      .disposed(by: disposeBag)
    
  }
  
  func setupUI() {
    
    if let collectionViewLayout = tabMenuCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      collectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      collectionViewLayout.scrollDirection = .horizontal
    }
    
    detailView.layer.applySketchShadow(color: .black,
    alpha: 0.16,
    x: 0,
    y: 3,
    blur: 6,
    spread: 0)
    
    let numberlicKeyboard = MMNumberKeyboard(frame: .zero)
    numberlicKeyboard.allowsDecimalPoint = true
    numberlicKeyboard.delegate = self
    priceTextField.delegate = self
    priceTextField.inputView = numberlicKeyboard
    
    let buffeNumberlicKeyboard = MMNumberKeyboard(frame: .zero)
    buffeNumberlicKeyboard.allowsDecimalPoint = false
    priceBuffeTextField.inputView = buffeNumberlicKeyboard
    
    let minuteNumberlicKeyboard = MMNumberKeyboard(frame: .zero)
    minuteNumberlicKeyboard.allowsDecimalPoint = false
    timeTextField.inputView = minuteNumberlicKeyboard
    
    priceTextField.layer.cornerRadius = 10
    priceTextField.layer.borderWidth = 1
    priceTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    priceTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: priceTextField.frame.height))
    priceTextField.leftViewMode = .always
    priceTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: priceTextField.frame.height))
    priceTextField.rightViewMode = .always
    
    brandTextField.layer.cornerRadius = 10
    brandTextField.layer.borderWidth = 1
    brandTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    brandTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: brandTextField.frame.height))
    brandTextField.leftViewMode = .always
    brandTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: brandTextField.frame.height))
    brandTextField.rightViewMode = .always
    
    branchTextField.layer.cornerRadius = 10
    branchTextField.layer.borderWidth = 1
    branchTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    branchTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: branchTextField.frame.height))
    branchTextField.leftViewMode = .always
    branchTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: branchTextField.frame.height))
    branchTextField.rightViewMode = .always
    
    menuNumberTextField.layer.cornerRadius = 10
    menuNumberTextField.layer.borderWidth = 1
    menuNumberTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    menuNumberTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: branchTextField.frame.height))
    menuNumberTextField.leftViewMode = .always
    menuNumberTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: branchTextField.frame.height))
    menuNumberTextField.rightViewMode = .always
    
    categoryTextField.layer.cornerRadius = 10
    categoryTextField.layer.borderWidth = 1
    categoryTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    categoryTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: categoryTextField.frame.height))
    categoryTextField.leftViewMode = .always
    categoryTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: categoryTextField.frame.height))
    categoryTextField.rightViewMode = .always
    
    menuNameTHTextField.layer.cornerRadius = 10
    menuNameTHTextField.layer.borderWidth = 1
    menuNameTHTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    menuNameTHTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: menuNameTHTextField.frame.height))
    menuNameTHTextField.leftViewMode = .always
    menuNameTHTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: menuNameTHTextField.frame.height))
    menuNameTHTextField.rightViewMode = .always
    
    menuNameENTextField.layer.cornerRadius = 10
    menuNameENTextField.layer.borderWidth = 1
    menuNameENTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    menuNameENTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: menuNameTHTextField.frame.height))
    menuNameENTextField.leftViewMode = .always
    menuNameENTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: menuNameTHTextField.frame.height))
    menuNameENTextField.rightViewMode = .always
    
    menuDetailTHView.layer.cornerRadius = 10
    menuDetailTHView.layer.borderWidth = 1
    menuDetailTHView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    
    menuDetailENView.layer.cornerRadius = 10
    menuDetailENView.layer.borderWidth = 1
    menuDetailENView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    
    priceGenreView.layer.cornerRadius = 10
    priceGenreView.layer.borderWidth = 1
    priceGenreView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    
    spacialView.layer.cornerRadius = 10
    spacialView.layer.borderWidth = 1
    spacialView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    
    printerView.layer.cornerRadius = 10
    printerView.layer.borderWidth = 1
    printerView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    
    menuTypeView.layer.cornerRadius = 10
    menuTypeView.layer.borderWidth = 1
    menuTypeView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    
    priceBuffeTextField.layer.cornerRadius = 10
    priceBuffeTextField.layer.borderWidth = 1
    priceBuffeTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    priceBuffeTextField.textAlignment = .right
    priceBuffeTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: priceBuffeTextField.frame.height))
    priceBuffeTextField.leftViewMode = .always
    priceBuffeTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: priceBuffeTextField.frame.height))
    priceBuffeTextField.rightViewMode = .always
    
    timeTextField.layer.cornerRadius = 10
    timeTextField.layer.borderWidth = 1
    timeTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    timeTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: timeTextField.frame.height))
    timeTextField.leftViewMode = .always
    timeTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: timeTextField.frame.height))
    timeTextField.rightViewMode = .always
    timeTextField.textAlignment = .right
    
    submitButton.layer.cornerRadius = submitButton.frame.height / 2
    
    menuDetailTHTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)
    menuDetailENTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)
    
    statusSwitch.onTintColor = UIColor(hex: 0x4DD43B)
    statusSwitch.tintColor = UIColor(hex: 0x8E8E93)
    statusSwitch.layer.cornerRadius = statusSwitch.frame.height / 2
    statusSwitch.backgroundColor = UIColor(hex: 0x8E8E93)
    
    let underline: [NSAttributedString.Key: Any] = [
      .underlineStyle: NSUnderlineStyle.single.rawValue,
      .font : UIFont(name: "Kanit-SemiBold", size: 15)!,
      .foregroundColor : UIColor(hex: 0xFF6722)]
    let attributeString = NSMutableAttributedString(string: "menu_edit_title".localized(),
                                                    attributes: underline)
    editButton.setAttributedTitle(attributeString, for: .normal)
    statusSwitch.layer.applySketchShadow(color: .black, alpha: 0.5, x: 0, y: 3, blur: 6, spread: 0)

  }
  
  func setupL10N() {
    setNavigationBarItem(titlePageName: "menu_create_title".localized(), isBackButton: true)
    detailButtom.setTitle("menu_detail".localized(), for: .normal)
    statusLabel.text = "menu_status_title".localized()
    priceLabel.text = "menu_price".localized()
    priceTextField.attributedPlaceholder = NSAttributedString(string: "menu_price_placeholder".localized(),
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor(hex: 0x4DD43B)])
    menuImageLabel.text = "menu_image".localized()
    brandTitle.text = "menu_brand".localized()
    brandTextField.placeholder = "menu_brand_placeholder".localized()
    branchLabel.text = "menu_branch".localized()
    branchTextField.placeholder = "menu_branch_placeholder".localized()
    menuNumberLabel.text = "menu_number".localized()
    menuNumberTextField.placeholder = "000001"
    categoryLabel.text = "menu_category".localized()
    categoryTextField.placeholder = "menu_category_placeholder".localized()
    menuNameTHLabel.text = "menu_nameth".localized()
    menuNameTHTextField.placeholder = "menu_nameth_placeholder".localized()
    menuNameENLabel.text = "menu_nameen".localized()
    menuNameENTextField.placeholder = "menu_nameen_placeholder".localized()
    menuDetailTHLabel.text = "menu_detailth".localized()
    menuDetailTHTextView.text = "menu_detailth_placeholder".localized()
    menuDetailENLabel.text = "menu_detailen".localized()
    menuDetailENTextView.text = "menu_detailen_placeholder".localized()
    priceGenreLabel.text = "menu_price_rate".localized()
    priceNormalButton.setTitle("menu_price_normal".localized(), for: .normal)
    priceRefillButton.setTitle("menu_price_refill".localized(), for: .normal)
    priceCustomButton.setTitle("menu_price_custom".localized(), for: .normal)
    priceBuffeButton.setTitle("menu_price_buffe".localized(), for: .normal)
    priceBuffeLabel.text = "menu_price_buffe_unit".localized()
    pricePerKGButton.setTitle("menu_price_perkg".localized(), for: .normal)
    priceBuffeTextField.placeholder = "00"
    spacialLabel.text = "menu_spacial".localized()
    spacialInStoreButton.setTitle("menu_spacial_instore".localized(), for: .normal)
    spacialTakeAwayButton.setTitle("menu_spacial_takeaway".localized(), for: .normal)
    spacialDeliveryButton.setTitle("menu_spacial_delivery".localized(), for: .normal)
    printerLabel.text = "menu_printer".localized()
    menuTypeLabel.text = "menu_type".localized()
    menuTypeSignatureButton.setTitle("menu_type_signature".localized(), for: .normal)
    menuTypeSeasonButton.setTitle("menu_type_season".localized(), for: .normal)
    menuTypeTakeTime.setTitle("menu_type_takehour".localized(), for: .normal)
    timeLabel.text = "menu_time".localized()
    timeUnit.text = "menu_time_unit".localized()
    timeTextField.placeholder = "00"
    editImageLabel.text = "menu_image_edit".localized()
    submitButton.setTitle("menu_submit".localized(), for: .normal)
    let deleteUnderline: [NSAttributedString.Key: Any] = [
      .underlineStyle: NSUnderlineStyle.single.rawValue,
      .font : UIFont(name: "Kanit-SemiBold", size: 15)!,
      .foregroundColor : UIColor(hex: 0xD7DAE2)]
    let deleteAttributeString = NSMutableAttributedString(string: "menu_delete".localized(),
                                                          attributes: deleteUnderline)
    deleteButton.setAttributedTitle(deleteAttributeString, for: .normal)
    deleteButton.isHidden = false
    deleteButton.isUserInteractionEnabled = false
    guard let brandData = LocalDataService.getSelectBrand() else { return }
    guard let branchData = BranchSignService.shared.getBranchData() else { return }
    brandTextField.text = brandData.brandName
    branchTextField.text = branchData.branchName
    printerTextView.text = "menu_printer_placeholder".localized()
    printerTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)
  }
  
  func setupBackButton() {
    let buttonItems = navigationItem.leftBarButtonItems
    let backButton = buttonItems?.filter{ $0.tag == 33 }.last
    
    backButton?.rx.tap.subscribe(onNext: { [unowned self] _ in
      self.viewModel.coordinates.navigateBack.onNext(())})
    .disposed(by: disposeBag)
  }
  
  func presentPopOverView(_ controller: UIViewController, sourceView: UIView) {
    let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
    presentationController.sourceView = sourceView
    presentationController.sourceRect = CGRect(x: 50  , y: 44, width: 0, height: 0)
    presentationController.permittedArrowDirections = [.up]
    self.present(controller, animated: true)
  }
  
  func validateField() -> Bool {
    let isPassValidatedPrice = validateTextField(text: priceTextField.text!)
    let isPassValidatedNameTH = validateTextField(text: menuNameTHTextField.text!)
    let isPassValidatedMenuImage = validateMenuImage()
    let isPassValidatedBuffee = validateMenuBuffe()
    
    setUIRequirePrice(isRequired: !isPassValidatedPrice)
    setUIRequireNameTH(isRequired: !isPassValidatedNameTH)
    setUIRequireMenuImage(isRequired: !isPassValidatedMenuImage)
    setUIRequireBuffe(isRequired: !isPassValidatedBuffee)
    
    return isPassValidatedPrice && isPassValidatedNameTH && isPassValidatedMenuImage && isPassValidatedBuffee
  }
  
  func validateTextField(text: String) -> Bool {
    return text.trimmingCharacters(in: .whitespacesAndNewlines) != ""
  }
  
  func validateMenuImage() -> Bool {
    return menuImagePath != nil
  }
  
  func validateMenuBuffe() -> Bool {
    if self.menuPriceType == .buffe {
      return validateTextField(text: self.priceBuffeTextField.text ?? "")
    } else {
      return true
    }
  }
  
  func setUIRequireBuffe(isRequired: Bool) {
    priceBuffeTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequirePrice(isRequired: Bool) {
    priceTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireNameTH(isRequired: Bool) {
    menuNameTHTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireMenuImage(isRequired: Bool) {
    menuImageView.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
    menuImageView.layer.borderWidth = isRequired ? 1 : 0
  }
  
  func disableField() {
    statusSwitch.isUserInteractionEnabled = false
    priceTextField.layer.borderColor = UIColor.clear.cgColor
    brandButton.isUserInteractionEnabled = false
    branchButton.isUserInteractionEnabled = false
    menuNumberTextField.isUserInteractionEnabled = false
    categoryButton.isUserInteractionEnabled = false
    menuNameTHTextField.isUserInteractionEnabled = false
    menuNameENTextField.isUserInteractionEnabled = false
    menuDetailTHTextView.isUserInteractionEnabled = false
    menuDetailENTextView.isUserInteractionEnabled = false
    timeTextField.isUserInteractionEnabled = false
    priceGenreView.isUserInteractionEnabled = false
    spacialView.isUserInteractionEnabled = false
    menuTypeView.isUserInteractionEnabled = false
    
    priceTextField.isUserInteractionEnabled = false
    brandTextField.layer.borderColor = UIColor.clear.cgColor
    branchTextField.layer.borderColor = UIColor.clear.cgColor
    menuNumberTextField.layer.borderColor = UIColor.clear.cgColor
    categoryTextField.layer.borderColor = UIColor.clear.cgColor
    menuNameTHTextField.layer.borderColor = UIColor.clear.cgColor
    menuNameENTextField.layer.borderColor = UIColor.clear.cgColor
    menuDetailTHView.layer.borderColor = UIColor.clear.cgColor
    menuDetailENView.layer.borderColor = UIColor.clear.cgColor
    timeTextField.layer.borderColor = UIColor.clear.cgColor
    priceGenreView.layer.borderColor = UIColor.clear.cgColor
    spacialView.layer.borderColor = UIColor.clear.cgColor
    menuTypeView.layer.borderColor = UIColor.clear.cgColor
    printerView.layer.borderColor = UIColor.clear.cgColor
    
    starLabels.forEach{ $0.isHidden = true }
    dropDownButtons.forEach{ $0.isHidden = true }
    editImageView.isHidden = true
    
    removePlaceholder()
    self.submitButtonTopConstraint.constant = 0
    self.submitButtonHeightConstraint.constant = 0
    
  }
  
  func removePlaceholder() {
    priceTextField.placeholder = nil
    brandTextField.placeholder = nil
    branchTextField.placeholder = nil
    menuNumberTextField.placeholder = nil
    categoryTextField.placeholder = nil
    menuNameTHTextField.placeholder = nil
    menuNameENTextField.placeholder = nil
    if menuDetailENTextView.text == "menu_detailen_placeholder".localized() {
      menuDetailENTextView.text = nil
    }
    
    if menuDetailTHTextView.text == "menu_detailth_placeholder".localized() {
      menuDetailTHTextView.text = nil
    }
    
    priceBuffeTextField.placeholder = nil
    printerTextView.text = nil
    timeTextField.placeholder = nil
  }
  
  func addPlaceholder() {
    detailButtom.setTitle("menu_detail".localized(), for: .normal)
    statusLabel.text = "menu_status_title".localized()
    priceLabel.text = "menu_price".localized()
    priceTextField.attributedPlaceholder = NSAttributedString(string: "menu_price_placeholder".localized(),
                                                              attributes: [NSAttributedString.Key.foregroundColor: UIColor(hex: 0x4DD43B)])
    menuImageLabel.text = "menu_image".localized()
    brandTitle.text = "menu_brand".localized()
    brandTextField.placeholder = "menu_brand_placeholder".localized()
    branchLabel.text = "menu_branch".localized()
    branchTextField.placeholder = "menu_branch_placeholder".localized()
    menuNumberLabel.text = "menu_number".localized()
    menuNumberTextField.placeholder = "000001"
    categoryLabel.text = "menu_category".localized()
    categoryTextField.placeholder = "menu_category_placeholder".localized()
    menuNameTHLabel.text = "menu_nameth".localized()
    menuNameTHTextField.placeholder = "menu_nameth_placeholder".localized()
    menuNameENLabel.text = "menu_nameen".localized()
    menuNameENTextField.placeholder = "menu_nameen_placeholder".localized()
    menuDetailTHLabel.text = "menu_detailth".localized()
    if menuDetailTHTextView.text == "" {
      menuDetailTHTextView.text = "menu_detailth_placeholder".localized()
      menuDetailTHTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)
    }
    
    if menuDetailENTextView.text == "" {
      menuDetailENTextView.text = "menu_detailen_placeholder".localized()
      menuDetailENTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)
    }
    menuDetailENLabel.text = "menu_detailen".localized()
    priceGenreLabel.text = "menu_price_rate".localized()
    priceNormalButton.setTitle("menu_price_normal".localized(), for: .normal)
    priceRefillButton.setTitle("menu_price_refill".localized(), for: .normal)
    priceCustomButton.setTitle("menu_price_custom".localized(), for: .normal)
    priceBuffeButton.setTitle("menu_price_buffe".localized(), for: .normal)
    priceBuffeLabel.text = "menu_price_buffe_unit".localized()
    pricePerKGButton.setTitle("menu_price_perkg".localized(), for: .normal)
    priceBuffeTextField.placeholder = "00"
    spacialLabel.text = "menu_spacial".localized()
    spacialInStoreButton.setTitle("menu_spacial_instore".localized(), for: .normal)
    spacialTakeAwayButton.setTitle("menu_spacial_takeaway".localized(), for: .normal)
    spacialDeliveryButton.setTitle("menu_spacial_delivery".localized(), for: .normal)
    printerLabel.text = "menu_printer".localized()
    menuTypeLabel.text = "menu_type".localized()
    menuTypeSignatureButton.setTitle("menu_type_signature".localized(), for: .normal)
    menuTypeSeasonButton.setTitle("menu_type_season".localized(), for: .normal)
    menuTypeTakeTime.setTitle("menu_type_takehour".localized(), for: .normal)
    timeLabel.text = "menu_time".localized()
    timeUnit.text = "menu_time_unit".localized()
    timeTextField.placeholder = "00"
    editImageLabel.text = "menu_image_edit".localized()
    submitButton.setTitle("menu_submit".localized(), for: .normal)
    guard let brandData = LocalDataService.getSelectBrand() else { return }
    guard let branchData = BranchSignService.shared.getBranchData() else { return }
    brandTextField.text = brandData.brandName
    branchTextField.text = branchData.branchName
    printerTextView.text = "menu_printer_placeholder".localized()
    printerTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)
  }
  
  func enableField() {
    statusSwitch.isUserInteractionEnabled = true
    priceTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    brandButton.isUserInteractionEnabled = true
    branchButton.isUserInteractionEnabled = true
    menuNumberTextField.isUserInteractionEnabled = true
    categoryButton.isUserInteractionEnabled = true
    menuNameTHTextField.isUserInteractionEnabled = true
    menuNameENTextField.isUserInteractionEnabled = true
    menuDetailTHTextView.isUserInteractionEnabled = true
    menuDetailENTextView.isUserInteractionEnabled = true
    timeTextField.isUserInteractionEnabled = true
    priceGenreView.isUserInteractionEnabled = true
    spacialView.isUserInteractionEnabled = true
    menuTypeView.isUserInteractionEnabled = true
    
    priceTextField.isUserInteractionEnabled = true
    brandTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    branchTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    menuNumberTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    categoryTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    menuNameTHTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    menuNameENTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    menuDetailTHView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    menuDetailENView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    timeTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    priceGenreView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    spacialView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    menuTypeView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    printerView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    
    let deleteUnderline: [NSAttributedString.Key: Any] = [
      .underlineStyle: NSUnderlineStyle.single.rawValue,
      .font : UIFont(name: "Kanit-SemiBold", size: 15)!,
      .foregroundColor : UIColor(hex: 0xF2503E)]
    let deleteAttributeString = NSMutableAttributedString(string: "menu_delete".localized(),
                                                          attributes: deleteUnderline)
    self.deleteButton.setAttributedTitle(deleteAttributeString, for: .normal)
    self.deleteButton.isSelected = true
    self.deleteButton.isHidden = false
    starLabels.forEach{ $0.isHidden = false }
    dropDownButtons.forEach{ $0.isHidden = false }
    menuImageView.kf.setImage(with: URL(string: menuImagePath ?? ""),
                              placeholder: UIImage(named: "ic_branch_default_logo"))
    addPlaceholder()
    editImageView.isHidden = false
    self.submitButtonHeightConstraint.constant = 44
    self.submitButtonTopConstraint.constant = 40
  }

}

extension MenuViewController: MMNumberKeyboardDelegate, UITextFieldDelegate {
  func numberKeyboardShouldReturn(_ numberKeyboard: MMNumberKeyboard) -> Bool {
    return true
  }
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    if textField == priceTextField {
      if priceTextField.text != "" {
        guard let doubleValue = Double(priceTextField.text ?? "0") else { return }
        priceTextField.text = String(format: "%.2f", doubleValue)
      }
    }
  }
}

extension MenuViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    dismiss(animated: true, completion: nil)
    guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
    guard let resize = image.resize(newWidth: 400) else { return }
    self.view.layoutIfNeeded()
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [unowned self] in
      self.uploadMenuImage(image: resize)
    }
  }
  
  func uploadMenuImage(image: UIImage) {
    let storageRef = self.storage.reference(withPath: "inventory/\(NSUUID().uuidString).png")
    if let imageData = image.pngData() {
      viewModel.outputs.loading.onNext(true)
      storageRef.putData(imageData, metadata: nil) { [unowned self] (response, error) in
        self.viewModel.outputs.loading.onNext(false)
        if let error = error {
          Helper.Alert(view: self, title: "Error",
                       message: error.localizedDescription,
                       confirmText: "ok".localized())
          return
        }
        self.viewModel.outputs.loading.onNext(true)
        storageRef.downloadURL { [unowned self] (url, error) in
          self.viewModel.outputs.loading.onNext(false)
          if let error = error {
            Helper.Alert(view: self, title: "Error",
                         message: error.localizedDescription,
                         confirmText: "ok".localized())
            return
          }
          
          guard let url = url else { return }
          self.menuImageID = "\(storageRef)"
          self.menuImagePath = "\(url)"
          self.menuImageView.kf.setImage(with: url, placeholder: UIImage(named: "ic_default_image"))
          self.editImageView.isHidden = false
        }
      }
    }
  }
}
