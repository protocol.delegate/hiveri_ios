//
//  MenuViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 10/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa

struct MenuData {
  var menuNO: String
  var brandID: Int
  var branchID: Int
  var categoryID: String
  var menuImage: String
  var nameTH: String
  var nameEN: String
  var detailTH: String
  var detailEN: String
  var price: Double
  var priceType: Int?
  var priceDetail: String
  var menuOption: [Int]
  var menuType: [Int]
  var timeToCook: Int
  var isPublish: Bool
}

protocol MenuViewModelInputs {
  var getTabMenuTrigger: PublishSubject<Void> { get }
  var getMenuCategoryTrigger: PublishSubject<Void> { get }
  var createMenuTrigger: PublishSubject<MenuData> { get }
  var editMenuTrigger: PublishSubject<(menuData: MenuData, menuID: String)> { get }
  var deleteMenuTrigger: PublishSubject<String> { get }
}

protocol MenuViewModelOutputs {
  var displayTabMenu: PublishSubject<[SectionOfDisplayTabMenuData]>{ get }
  var displayMenuCategory: PublishSubject<[MenuCategoryModel]> { get }
  var displayCreateMenuSuccess: PublishSubject<Void> { get }
  var categoryID: BehaviorRelay<String> { get }
  var menuData: BehaviorRelay<DisplayMenuData?> { get }
  var loading: BehaviorSubject<Bool> { get }
  var displayEditMenuSuccess: PublishSubject<Void> { get }
  var displayDeleteMenuSuccess: PublishSubject<Void> { get }
}

protocol MenuViewModelCoordinates {
  var navigateBack: PublishSubject<Void> { get }
}

protocol MenuViewModelType: ErrorAlertableViewModelType {
  var inputs: MenuViewModelInputs { get }
  var outputs: MenuViewModelOutputs { get }
  var coordinates: MenuViewModelCoordinates { get }
}

class MenuViewModel: MenuViewModelType, MenuViewModelInputs, MenuViewModelOutputs, MenuViewModelCoordinates {
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  
  // MARK: Init
  
  public init(apiService: APIServiceType = APIService(), categoryID: String, menuData: DisplayMenuData?) {
    guard let branchData = BranchSignService.shared.getBranchData() else { return }
    
    let tabMenuData = [DisplayTabMenuData(title: "menu_information".localized(), ID: 0)]
    outputs.categoryID.accept(categoryID)
    outputs.menuData.accept(menuData)
    
    getTabMenuTrigger
      .map{ [SectionOfDisplayTabMenuData(header: "", items: tabMenuData)] }
      .bind(to: displayTabMenu)
      .disposed(by: disposeBag)
    
    let getMenuCategoryTriggerSignal =
      getMenuCategoryTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ GetMenuCategoryRequestModel(shopID: branchData.spoonWalkID) }
        .flatMapLatest{ apiService.rx_getMenuCategory(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    getMenuCategoryTriggerSignal.elements()
      .bind(to: displayMenuCategory)
      .disposed(by: disposeBag)
    
    let createMenuTriggerSignal =
      createMenuTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ CreateMenuRequestModel(menuNO: $0.menuNO,
                                     brandID: $0.brandID,
                                     branchID: $0.branchID,
                                     categoryID: $0.categoryID,
                                     menuImagePath: $0.menuImage,
                                     menuNameTH: $0.nameTH,
                                     menuNameEN: $0.nameEN,
                                     detailTH: $0.detailTH,
                                     detailEN: $0.detailEN,
                                     price: $0.price,
                                     priceType: $0.priceType,
                                     priceDetail: $0.priceDetail,
                                     menuOption: $0.menuOption,
                                     menuType: $0.menuType,
                                     timeToCook: $0.timeToCook,
                                     isPublish: $0.isPublish) }
        .flatMapLatest{ apiService.rx_createMenu(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    createMenuTriggerSignal
      .elements()
      .bind(to: displayCreateMenuSuccess)
      .disposed(by: disposeBag)
    
    // Edit
    
    let editMenuTriggerSignal = editMenuTrigger
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
      .map{ EditMenuRequestModel(menuID: $0.menuID,
                                 menuNO: $0.menuData.menuNO,
                                 brandID: $0.menuData.brandID,
                                 branchID: $0.menuData.branchID,
                                 categoryID: $0.menuData.categoryID,
                                 menuImagePath: $0.menuData.menuImage,
                                 menuNameTH: $0.menuData.nameTH,
                                 menuNameEN: $0.menuData.nameEN,
                                 detailTH: $0.menuData.detailTH,
                                 detailEN: $0.menuData.detailEN,
                                 price: $0.menuData.price,
                                 priceType: $0.menuData.priceType,
                                 priceDetail: $0.menuData.priceDetail,
                                 menuOption: $0.menuData.menuOption,
                                 menuType: $0.menuData.menuType,
                                 timeToCook: $0.menuData.timeToCook,
                                 isPublish: $0.menuData.isPublish) }
      .flatMapLatest{ apiService.rx_editMenu(requestModel: $0).materialize() }
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
      .share(replay: 1)
    
    editMenuTriggerSignal
      .elements()
      .bind(to: displayEditMenuSuccess)
      .disposed(by: disposeBag)
    
    // Delete
    
    let deleteMenuTriggerSignal =
      deleteMenuTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{DeleteMenuRequestModel(menuID: $0)}
        .flatMapLatest{ apiService.rx_deleteMenu(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    deleteMenuTriggerSignal.elements()
      .bind(to: displayDeleteMenuSuccess)
      .disposed(by: disposeBag)
    
    // Error ---------------------------------------------------------------
    
    let errorSignal = Observable
      .merge(getMenuCategoryTriggerSignal.errors(),
             createMenuTriggerSignal.errors(),
             editMenuTriggerSignal.errors(),
             deleteMenuTriggerSignal.errors())
      .share(replay: 1)
    
    errorSignal
      .map(errorToAlertable)
      .bind(to: error)
      .disposed(by: disposeBag)
    
  }
  
  // MARK: Private
  
  // MARK: Input
  
  var getTabMenuTrigger = PublishSubject<Void>()
  var getMenuCategoryTrigger = PublishSubject<Void>()
  var createMenuTrigger = PublishSubject<MenuData>()
  var editMenuTrigger = PublishSubject<(menuData: MenuData, menuID: String)>()
  var deleteMenuTrigger = PublishSubject<String>()
  
  // MARK: Output
  
  var displayTabMenu = PublishSubject<[SectionOfDisplayTabMenuData]>()
  var displayMenuCategory = PublishSubject<[MenuCategoryModel]>()
  var error = PublishSubject<(title: String, message: String)>()
  var loading = BehaviorSubject<Bool>(value: false)
  var categoryID = BehaviorRelay<String>(value: "")
  var displayCreateMenuSuccess = PublishSubject<Void>()
  var menuData = BehaviorRelay<DisplayMenuData?>(value: nil)
  var displayEditMenuSuccess = PublishSubject<Void>()
  var displayDeleteMenuSuccess = PublishSubject<Void>()
  
  // MARK: Coordinates
  
  var navigateBack = PublishSubject<Void>()
  
  // MARK: Input&Output&Coordinates
  
  public var outputs: MenuViewModelOutputs { return self }
  public var inputs: MenuViewModelInputs { return self }
  public var coordinates: MenuViewModelCoordinates { return self }
}
