//
//  BrandViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 28/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZFTokenField
import MMNumberKeyboard
import MapKit
import IQKeyboardManagerSwift
import ZSwiftKit
import Firebase
import Kingfisher

class BrandViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  @IBOutlet weak var submitHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var submitTopContraint: NSLayoutConstraint!
  @IBOutlet weak var provinceButton: UIButton!
  @IBOutlet weak var subDistricButton: UIButton!
  @IBOutlet weak var districtButton: UIButton!
  
  @IBOutlet weak var brandBackgroundView: UIView!
  
  @IBOutlet weak var brandPhoneTitleLabel: UILabel!
  @IBOutlet var starLabels: [UILabel]!
  @IBOutlet var dropDownImageViews: [UIImageView]!
  @IBOutlet weak var brandPhoneTokenField: ZFTokenField!
  @IBOutlet weak var brandPhoneView: UIView!
  
  @IBOutlet weak var brandPhoneScrollView: UIScrollView!
  @IBOutlet weak var brandDetailButton: UIButton!
  @IBOutlet weak var logoImageLabel: UILabel!
  @IBOutlet weak var logoImageButton: UIButton!
  @IBOutlet weak var logoImageView: UIImageView!
  
  @IBOutlet weak var brandNameLabel: UILabel!
  @IBOutlet weak var brandNameTextField: UITextField!
  
  @IBOutlet weak var brandEmailLabel: UILabel!
  @IBOutlet weak var brandEmailTextfield: UITextField!
  
  @IBOutlet weak var brandLineLabel: UILabel!
  @IBOutlet weak var brandLineTextfield: UITextField!
  
  @IBOutlet weak var websiteLabel: UILabel!
  @IBOutlet weak var websiteTextfield: UITextField!
  
  @IBOutlet weak var facebookLabel: UILabel!
  @IBOutlet weak var facebookTextfield: UITextField!
  
  @IBOutlet weak var instagramLabel: UILabel!
  @IBOutlet weak var instagramTextfield: UITextField!
  
  @IBOutlet weak var brandDetailLabel: UILabel!
  @IBOutlet weak var brandDetailView: UIView!
  @IBOutlet weak var brandDetailTextView: UITextView!
  
  @IBOutlet weak var businessLabel: UILabel!
  
  @IBOutlet weak var addressLabel: UILabel!
  
  @IBOutlet weak var prefixTextField: UITextField!
  @IBOutlet weak var prefixButton: UIButton!
  @IBOutlet weak var prefixLabel: UILabel!
  @IBOutlet weak var prefixView: UIView!
  
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var nameTextField: UITextField!
  
  @IBOutlet weak var lastNameLabel: UILabel!
  @IBOutlet weak var lastNameTextField: UITextField!
  
  @IBOutlet weak var emailLabel: UILabel!
  @IBOutlet weak var emailTextField: UITextField!
  
  @IBOutlet weak var phoneLabel: UILabel!
  
  @IBOutlet weak var mapLabel: UILabel!
  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var mapButton: UIButton!
  
  @IBOutlet weak var buildingLabel: UILabel!
  @IBOutlet weak var buildingTextfield: UITextField!
  
  @IBOutlet weak var roomNoLabel: UILabel!
  @IBOutlet weak var roomTextField: UITextField!
  
  @IBOutlet weak var floorLabel: UILabel!
  @IBOutlet weak var floorTextField: UITextField!
  
  @IBOutlet weak var villageLabel: UILabel!
  @IBOutlet weak var villageTextField: UITextField!
  
  @IBOutlet weak var houseNoLabel: UILabel!
  @IBOutlet weak var houseNoTextField: UITextField!
  
  @IBOutlet weak var villageNoLabel: UILabel!
  @IBOutlet weak var villageNoTextField: UITextField!
  
  @IBOutlet weak var soiLabel: UILabel!
  @IBOutlet weak var soiTextField: UITextField!
  
  @IBOutlet weak var streetLabel: UILabel!
  @IBOutlet weak var streetTextField: UITextField!
  
  @IBOutlet weak var subDistrictLabel: UILabel!
  @IBOutlet weak var subDistrictTextField: UITextField!
  
  @IBOutlet weak var districtLabel: UILabel!
  @IBOutlet weak var districtTextField: UITextField!
  
  @IBOutlet weak var provinceLabel: UILabel!
  @IBOutlet weak var provinceTextField: UITextField!
  
  @IBOutlet weak var postCodeLabel: UILabel!
  @IBOutlet weak var postCodeTextField: UITextField!
  
  @IBOutlet weak var submitButton: UIButton!
  
  
  @IBOutlet weak var phoneScrollView: UIScrollView!
  @IBOutlet weak var phoneView: UIView!
  @IBOutlet weak var phoneTokenField: ZFTokenField!
  
  
  @IBOutlet weak var brandPhoneDescriptionLabel: UILabel!
  @IBOutlet weak var phoneDescriptionLabel: UILabel!
  
  @IBOutlet weak var editButton: UIButton!
  @IBOutlet weak var deleteButton: UIButton!
  
  // MARK: Property
  
  var viewModel: BrandViewModelType!
  let disposeBag = DisposeBag()
  var brandPhoneNumbers: [String] = []
  var phoneNumbers: [String] = []
  var logoImage: UIImage?
  var brandLocation: CLLocationCoordinate2D?
  lazy var storage = Storage.storage(url:"gs://hive-world.appspot.com")
  var imageURL: String = ""
  var imageID: String = ""
  var isFromEdit: Bool = false
  var selectProvince = 0
  var selectDistrict = 0
  var selectSubDistrict = 0
  var textFieldOffSet: CGFloat = 16.0
  var isView: Bool = false
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupL10N()
    bindViewModel()
  }
  
  func setupUI() {
    self.setNavigationBarItem(titlePageName: "createbrand_title".localized(), isBackButton: true)
    brandBackgroundView.layer.applySketchShadow(color: .black,
                                                alpha: 0.16,
                                                x: 0,
                                                y: 3,
                                                blur: 6,
                                                spread: 0)
    
    brandPhoneTokenField.dataSource = self;
    brandPhoneTokenField.delegate = self;
    let numberlicKeyboard = MMNumberKeyboard(frame: .zero)
    numberlicKeyboard.allowsDecimalPoint = false
    brandPhoneTokenField.textField.inputView = numberlicKeyboard
    brandPhoneTokenField.textField.layer.masksToBounds = false
    brandPhoneTokenField.textField.font = UIFont(name: "Kanit-Regular", size: 15)
    brandPhoneTokenField.reloadData()
    
    phoneTokenField.dataSource = self;
    phoneTokenField.delegate = self;
    let phoneNumberlicKeyboard = MMNumberKeyboard(frame: .zero)
    phoneNumberlicKeyboard.allowsDecimalPoint = false
    phoneTokenField.textField.inputView = phoneNumberlicKeyboard
    phoneTokenField.textField.layer.masksToBounds = false
    phoneTokenField.textField.font = UIFont(name: "Kanit-Regular", size: 15)
    phoneTokenField.reloadData()
    
    brandNameTextField.layer.cornerRadius = 10
    brandNameTextField.layer.borderWidth = 1
    brandNameTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    brandNameTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: brandNameTextField.frame.height))
    brandNameTextField.leftViewMode = .always
    brandNameTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: brandNameTextField.frame.height))
    brandNameTextField.rightViewMode = .always
    
    brandPhoneView.layer.cornerRadius = 10
    brandPhoneView.layer.borderWidth = 1
    brandPhoneView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    brandPhoneView.layer.masksToBounds = true
    
    phoneView.layer.cornerRadius = 10
    phoneView.layer.borderWidth = 1
    phoneView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    phoneView.layer.masksToBounds = true
    
    websiteTextfield.layer.cornerRadius = 10
    websiteTextfield.layer.borderWidth = 1
    websiteTextfield.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    websiteTextfield.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: websiteTextfield.frame.height))
    websiteTextfield.leftViewMode = .always
    websiteTextfield.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: websiteTextfield.frame.height))
    websiteTextfield.rightViewMode = .always
    
    brandEmailTextfield.layer.cornerRadius = 10
    brandEmailTextfield.layer.borderWidth = 1
    brandEmailTextfield.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    brandEmailTextfield.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: brandEmailTextfield.frame.height))
    brandEmailTextfield.leftViewMode = .always
    brandEmailTextfield.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: brandEmailTextfield.frame.height))
    brandEmailTextfield.rightViewMode = .always
    
    facebookTextfield.layer.cornerRadius = 10
    facebookTextfield.layer.borderWidth = 1
    facebookTextfield.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    facebookTextfield.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: facebookTextfield.frame.height))
    facebookTextfield.leftViewMode = .always
    facebookTextfield.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: facebookTextfield.frame.height))
    facebookTextfield.rightViewMode = .always
    
    brandLineTextfield.layer.cornerRadius = 10
    brandLineTextfield.layer.borderWidth = 1
    brandLineTextfield.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    brandLineTextfield.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: facebookTextfield.frame.height))
    brandLineTextfield.leftViewMode = .always
    brandLineTextfield.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: facebookTextfield.frame.height))
    brandLineTextfield.rightViewMode = .always
    
    instagramTextfield.layer.cornerRadius = 10
    instagramTextfield.layer.borderWidth = 1
    instagramTextfield.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    instagramTextfield.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: instagramTextfield.frame.height))
    instagramTextfield.leftViewMode = .always
    instagramTextfield.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: instagramTextfield.frame.height))
    instagramTextfield.rightViewMode = .always
    
    brandDetailView.layer.cornerRadius = 10
    brandDetailView.layer.borderWidth = 1
    brandDetailView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    
    prefixView.layer.cornerRadius = 10
    prefixView.layer.borderWidth = 1
    prefixView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    prefixTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: prefixTextField.frame.height))
    prefixTextField.leftViewMode = .always
    prefixTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: prefixTextField.frame.height))
    prefixTextField.rightViewMode = .always
    
    nameTextField.layer.cornerRadius = 10
    nameTextField.layer.borderWidth = 1
    nameTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    nameTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: nameTextField.frame.height))
    nameTextField.leftViewMode = .always
    
    lastNameTextField.layer.cornerRadius = 10
    lastNameTextField.layer.borderWidth = 1
    lastNameTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    lastNameTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: lastNameTextField.frame.height))
    lastNameTextField.leftViewMode = .always
    
    emailTextField.layer.cornerRadius = 10
    emailTextField.layer.borderWidth = 1
    emailTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    emailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: emailTextField.frame.height))
    emailTextField.leftViewMode = .always
    
    buildingTextfield.layer.cornerRadius = 10
    buildingTextfield.layer.borderWidth = 1
    buildingTextfield.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    buildingTextfield.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: buildingTextfield.frame.height))
    buildingTextfield.leftViewMode = .always
    buildingTextfield.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: buildingTextfield.frame.height))
    buildingTextfield.rightViewMode = .always
    
    roomTextField.layer.cornerRadius = 10
    roomTextField.layer.borderWidth = 1
    roomTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    roomTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: roomTextField.frame.height))
    roomTextField.leftViewMode = .always
    roomTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: roomTextField.frame.height))
    roomTextField.rightViewMode = .always
    
    floorTextField.layer.cornerRadius = 10
    floorTextField.layer.borderWidth = 1
    floorTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    floorTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: floorTextField.frame.height))
    floorTextField.leftViewMode = .always
    floorTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: floorTextField.frame.height))
    floorTextField.rightViewMode = .always
    
    villageTextField.layer.cornerRadius = 10
    villageTextField.layer.borderWidth = 1
    villageTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    villageTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: villageTextField.frame.height))
    villageTextField.leftViewMode = .always
    villageTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: villageTextField.frame.height))
    villageTextField.rightViewMode = .always
    
    houseNoTextField.layer.cornerRadius = 10
    houseNoTextField.layer.borderWidth = 1
    houseNoTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    houseNoTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: houseNoTextField.frame.height))
    houseNoTextField.leftViewMode = .always
    houseNoTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: houseNoTextField.frame.height))
    houseNoTextField.rightViewMode = .always
    
    villageNoTextField.layer.cornerRadius = 10
    villageNoTextField.layer.borderWidth = 1
    villageNoTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    villageNoTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: villageNoTextField.frame.height))
    villageNoTextField.leftViewMode = .always
    villageNoTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: villageNoTextField.frame.height))
    villageNoTextField.rightViewMode = .always
    
    soiTextField.layer.cornerRadius = 10
    soiTextField.layer.borderWidth = 1
    soiTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    soiTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: soiTextField.frame.height))
    soiTextField.leftViewMode = .always
    soiTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: soiTextField.frame.height))
    soiTextField.rightViewMode = .always
    
    streetTextField.layer.cornerRadius = 10
    streetTextField.layer.borderWidth = 1
    streetTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    streetTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: streetTextField.frame.height))
    streetTextField.leftViewMode = .always
    streetTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: streetTextField.frame.height))
    streetTextField.rightViewMode = .always
    
    subDistrictTextField.layer.cornerRadius = 10
    subDistrictTextField.layer.borderWidth = 1
    subDistrictTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    subDistrictTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: subDistrictTextField.frame.height))
    subDistrictTextField.leftViewMode = .always
    subDistrictTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: subDistrictTextField.frame.height))
    subDistrictTextField.rightViewMode = .always
    
    districtTextField.layer.cornerRadius = 10
    districtTextField.layer.borderWidth = 1
    districtTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    districtTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: districtTextField.frame.height))
    districtTextField.leftViewMode = .always
    districtTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: districtTextField.frame.height))
    districtTextField.rightViewMode = .always
    
    provinceTextField.layer.cornerRadius = 10
    provinceTextField.layer.borderWidth = 1
    provinceTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    provinceTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: provinceTextField.frame.height))
    provinceTextField.leftViewMode = .always
    provinceTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: provinceTextField.frame.height))
    provinceTextField.rightViewMode = .always
    
    postCodeTextField.layer.cornerRadius = 10
    postCodeTextField.layer.borderWidth = 1
    postCodeTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    postCodeTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: postCodeTextField.frame.height))
    postCodeTextField.leftViewMode = .always
    postCodeTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: postCodeTextField.frame.height))
    postCodeTextField.rightViewMode = .always
    
    mapView.layer.cornerRadius = 10
    mapView.layer.borderWidth = 1
    mapView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    mapView.layer.masksToBounds = true
    
    logoImageView.layer.cornerRadius = logoImageView.frame.height / 2
    submitButton.layer.cornerRadius = submitButton.frame.height / 2
    
    let postCodeNumberlicKeyboard = MMNumberKeyboard(frame: .zero)
    postCodeNumberlicKeyboard.allowsDecimalPoint = false
    postCodeTextField.inputView = postCodeNumberlicKeyboard
    
    brandDetailTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)
    
    mapView.delegate = self
    mapView.showsUserLocation = true
  }
  
  func setupL10N() {
    
    logoImageLabel.text = "createbrand_logo".localized()
    brandNameLabel.text = "createbrand_brandname".localized()
    brandDetailButton.setTitle("createbrand_title".localized(), for: .normal)
    brandPhoneTitleLabel.text = "createbrand_brandphone".localized()
    brandEmailLabel.text = "createbrand_email".localized()
    websiteLabel.text = "createbrand_website".localized()
    facebookLabel.text = "createbrand_facebook".localized()
    instagramLabel.text = "createbrand_instagram".localized()
    brandLineLabel.text = "createbrand_line".localized()
    brandDetailLabel.text = "createbrand_branddetail".localized()
    
    brandNameTextField.placeholder = "createbrand_brandname".localized()
    brandPhoneTokenField.textField.placeholder = "createbrand_phone_placeholder".localized()
    websiteTextfield.placeholder = "createbrand_website_placeholder".localized()
    brandEmailTextfield.placeholder = "createbrand_email_placeholder".localized()
    facebookTextfield.placeholder = "createbrand_facebook_placeholder".localized()
    brandLineTextfield.placeholder = "createbrand_line_placeholder".localized()
    instagramTextfield.placeholder = "createbrand_instagram_placeholder".localized()
    
    phoneTokenField.textField.placeholder = "createbrand_phone_placeholder".localized()
    
    businessLabel.text = "createbrand_business_information".localized()
    addressLabel.text = "createbrand_address".localized()
    
    prefixLabel.text = "createbrand_prefix".localized()
    prefixTextField.placeholder = "createbrand_prefix_placeholder".localized()
    nameLabel.text = "createbrand_name".localized()
    nameTextField.placeholder = "createbrand_name".localized()
    lastNameLabel.text = "createbrand_lastname".localized()
    lastNameTextField.placeholder = "createbrand_lastname".localized()
    emailLabel.text = "createbrand_email".localized()
    emailTextField.placeholder = "createbrand_email_placeholder".localized()
    phoneLabel.text = "createbrand_brandphone".localized()
    
    buildingLabel.text = "createbrand_building".localized()
    buildingTextfield.placeholder = "createbrand_building".localized()
    roomNoLabel.text = "createbrand_room_no".localized()
    roomTextField.placeholder = "createbrand_room_no".localized()
    floorLabel.text = "createbrand_floor".localized()
    floorTextField.placeholder = "createbrand_floor".localized()
    villageLabel.text = "createbrand_village".localized()
    villageTextField.placeholder = "createbrand_village".localized()
    
    houseNoLabel.text = "createbrand_house_no".localized()
    houseNoTextField.placeholder = "createbrand_house_no".localized()
    
    villageNoLabel.text = "createbrand_village_no".localized()
    villageNoTextField.placeholder = "createbrand_village_no".localized()
    
    soiLabel.text = "createbrand_soi".localized()
    soiTextField.placeholder = "createbrand_soi".localized()
    
    streetLabel.text = "createbrand_street".localized()
    streetTextField.placeholder = "createbrand_street".localized()
    
    subDistrictLabel.text = "createbrand_sub_district".localized()
    subDistrictTextField.placeholder = "createbrand_sub_district".localized()
    
    districtLabel.text = "createbrand_district".localized()
    districtTextField.placeholder = "createbrand_district".localized()
    
    provinceLabel.text = "createbrand_province".localized()
    provinceTextField.placeholder = "createbrand_province".localized()
    
    postCodeLabel.text = "createbrand_postcode".localized()
    postCodeTextField.placeholder = "createbrand_postcode".localized()
    
    mapLabel.text = "createbrand_map".localized()
    
    phoneDescriptionLabel.text = "createbrand_phone_description".localized()
    brandPhoneDescriptionLabel.text = "createbrand_phone_description".localized()
    
    submitButton.setTitle("createbrand_save".localized(), for: .normal)
    brandDetailTextView.text = "createbrand_branddetail_placeholder".localized()
    
    let underline: [NSAttributedString.Key: Any] = [
      .underlineStyle: NSUnderlineStyle.single.rawValue,
      .font : UIFont(name: "Kanit-SemiBold", size: 15)!,
      .foregroundColor : UIColor(hex: 0xFF6722)]
    let attributeString = NSMutableAttributedString(string: "createbrand_edit".localized(),
                                                    attributes: underline)
    editButton.setAttributedTitle(attributeString, for: .normal)
    
    let deleteUnderline: [NSAttributedString.Key: Any] = [
      .underlineStyle: NSUnderlineStyle.single.rawValue,
      .font : UIFont(name: "Kanit-SemiBold", size: 15)!,
      .foregroundColor : UIColor(hex: 0xD7DAE2)]
    let deleteAttributeString = NSMutableAttributedString(string: "createbrand_delete".localized(),
                                                    attributes: deleteUnderline)
    deleteButton.setAttributedTitle(deleteAttributeString, for: .normal)
    deleteButton.isSelected = false
    deleteButton.isUserInteractionEnabled = false
    deleteButton.isHidden = false
  }
  
  func bindViewModel() {
    
    let buttonItems = navigationItem.leftBarButtonItems
    let backButton = buttonItems?.filter{ $0.tag == 33 }.first
    backButton?.rx.tap
      .bind(to: viewModel.coordinates.navigateBack)
      .disposed(by: disposeBag)
    
    submitButton.rx.tap
      .filter{ [unowned self] in self.validateField() }
      .filter{ [unowned self] in self.isValidEmailFormat(email: self.emailTextField.text!) }
      .filter{ [unowned self] in self.isValidEmailFormat(email: self.brandEmailTextfield.text!) }
      .subscribe(onNext: { [unowned self] _ in
        self.uploadImage()
        
      })
      .disposed(by: disposeBag)
    
    logoImageButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        let controller = UIImagePickerController()
        controller.sourceType = .photoLibrary
        controller.delegate = self
        self.present(controller, animated: true)})
      .disposed(by: disposeBag)
    
    prefixButton.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      var data: [String] = []
      switch LocalizationManager.share.getDefaultLanguage() {
      case .th:
        data = ["นาย", "น.ส.", "นาง"]
      case .en:
        data = ["Mr.", "Miss", "Mrs."]
      }
      let controller = ArrayChoiceTableViewController(data) { [unowned self] (name) in
        self.prefixTextField.text = name
      }
      controller.tableView.separatorStyle = .none
      controller.preferredContentSize = CGSize(width: 100, height: 150)
      self.presentPopOverView(controller, sourceView: self.prefixTextField)
    })
    .disposed(by: disposeBag)
    
    provinceButton.rx.tap
      .subscribe(onNext: { [unowned self] in
        guard let province = LocalDataService.getProvince()?.list.filter({$0.status == "Y"}) else { return }
        guard let district = LocalDataService.getDistrict()?.list.filter({$0.status == "Y"}) else { return }
        guard let subDistrict = LocalDataService.getSubDistrict()?.list.filter({$0.status == "Y"}) else { return }
        let controller = ArrayChoiceProvinceTableViewController<Any>(province) { [unowned self] (provinceModel) in
          self.provinceTextField.text = provinceModel.nameTH
          self.selectProvince = provinceModel.provinceID
          guard let selectDistrictModel = district.filter({$0.provinceID == self.selectProvince}).first else { return }
          self.districtTextField.text = selectDistrictModel.nameTH
          self.selectDistrict = selectDistrictModel.districtID
          guard let selectSubDistrictModel = subDistrict.filter({$0.districtID == self.selectDistrict}).first else { return }
          self.subDistrictTextField.text = selectSubDistrictModel.nameTH
          self.selectSubDistrict = selectSubDistrictModel.subDistricID
        }
        controller.tableView.separatorStyle = .none
        controller.preferredContentSize = CGSize(width: 200, height: 300)
        self.presentPopOverView(controller, sourceView: self.provinceTextField)
      })
      .disposed(by: disposeBag)
    
    districtButton.rx.tap
      .filter{self.provinceTextField.text != ""}
      .subscribe(onNext: { [unowned self] _ in
        guard let district = LocalDataService.getDistrict()?.list.filter({$0.status == "Y"}) else { return }
        guard let subDistrict = LocalDataService.getSubDistrict()?.list.filter({$0.status == "Y"}) else { return }
        let filterDistrict = district.filter({ $0.provinceID == self.selectProvince })
        if filterDistrict.count > 0 {
          let controller = ArrayChoiceDistrictTableViewController<Any>(filterDistrict) { [unowned self] (model) in
            self.districtTextField.text = model.nameTH
            self.selectDistrict = model.districtID
            guard let selectSubDistrictModel = subDistrict.filter({$0.districtID == self.selectDistrict}).first else { return }
            self.subDistrictTextField.text = selectSubDistrictModel.nameTH
            self.selectSubDistrict = selectSubDistrictModel.subDistricID
          }
          controller.tableView.separatorStyle = .none
          controller.preferredContentSize = CGSize(width: 200, height: 300)
          self.presentPopOverView(controller, sourceView: self.districtTextField)
        }
      })
      .disposed(by: disposeBag)
    
    subDistricButton.rx.tap
      .filter{self.provinceTextField.text != "" && self.districtTextField.text != ""}
      .subscribe(onNext: { [unowned self] _ in
        guard let subDistrict = LocalDataService.getSubDistrict()?.list.filter({$0.status == "Y"}) else { return }
        let filterSubDistrict = subDistrict.filter({ $0.districtID == self.selectDistrict })
        if filterSubDistrict.count > 0 {
          let controller = ArrayChoiceSubDistrictTableViewController<Any>(filterSubDistrict) { [unowned self] (model) in
            self.subDistrictTextField.text = model.nameTH
            self.selectSubDistrict = model.subDistricID
          }
          controller.tableView.separatorStyle = .none
          controller.preferredContentSize = CGSize(width: 200, height: 300)
          self.presentPopOverView(controller, sourceView: self.subDistrictTextField)}})
      .disposed(by: disposeBag)
    
    postCodeTextField.rx.text.orEmpty
    .scan("") { (previous, new) -> String in
        if new.count > 5 {
            return previous ?? String(new.prefix(5))
        } else {
            return new
        }
    }
    .subscribe(postCodeTextField.rx.text)
    .disposed(by: disposeBag)
    
    LocationService
      .shared
      .didUpdateLocationSingle()
      .filter{ [unowned self] _ in self.brandLocation == nil }
      .subscribe(onNext: { [unowned self] location in
        guard let location = location else { return }
        let region = MKCoordinateRegion( center: location.coordinate,
                                         latitudinalMeters: CLLocationDistance(exactly: 5000)!,
                                         longitudinalMeters: CLLocationDistance(exactly: 5000)!)
        self.mapView.setRegion(self.mapView.regionThatFits(region), animated: true)})
      .disposed(by: disposeBag)
    
    mapButton.rx.tap
      .bind(to: viewModel.inputs.selectLocationTrigger)
      .disposed(by: disposeBag)
    
    brandDetailTextView.rx
      .didBeginEditing.subscribe(onNext: { [unowned self] _ in
        if self.brandDetailTextView.textColor == UIColor(hex: 0x8E8E93).withAlphaComponent(0.5) {
          self.brandDetailTextView.text = nil
          self.brandDetailTextView.textColor = .black}})
      .disposed(by: disposeBag)

    brandDetailTextView.rx
      .didEndEditing
      .subscribe(onNext:{ _ in
        if self.brandDetailTextView.text.isEmpty {
          self.brandDetailTextView.text = "createbrand_branddetail_placeholder".localized()
          self.brandDetailTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)}})
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayCreateBrandSuccess
      .subscribe(onNext: { [unowned self] _ in
        Helper.Alert(view: self, title: "createbrand_success_title".localized(),
                     message: "createbrand_success_message".localized(),
                     confirmText: "ok".localized()) { [unowned self] (_) in
                      self.viewModel.coordinates.navigateBack.onNext(())
        }
      })
      .disposed(by: disposeBag)
    
    //MARK: Edit
    
    viewModel
      .outputs
      .isFromDetail
      .filterNil()
      .subscribe(onNext: { [unowned self] brandID in
        self.isFromEdit = true
        self.editButton.isHidden = false
        self.submitButton.isHidden = true
        self.disableTextField()
        self.viewModel.inputs.getBrandTrigger.onNext(brandID)})
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayBrandData
      .subscribe(onNext: { [unowned self] brandData in
        self.setupEditData(brandData: brandData)
      })
      .disposed(by: disposeBag)
    
    editButton.rx.tap
      .subscribe(onNext: {[unowned self] _ in
        self.editButton.isHidden = true
        self.enableTextField()
//        self.brandNameTextField.becomeFirstResponder()
        
      })
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayUpdateBrandSuccess.subscribe(onNext: { _ in
        Helper.Alert(view: self, title: "createbrand_update_success_title".localized(),
                     message: "createbrand_update_success_message".localized(),
                     confirmText: "ok".localized()) { [unowned self] (_) in
                      self.viewModel.coordinates.navigateBack.onNext(())
        }
      })
      .disposed(by: disposeBag)
    
    //MARK: Delete
    
    deleteButton.rx.tap
      .subscribe(onNext: { _ in
        Helper.AlertConfirm(view: self, title: "createbrand_delete_title".localized(), message: "createbrand_delete_message".localized(), confirmText: "ok".localized(), cancelText: "cancel".localized(), isInverseButtonPos: true, confirmCompletion: { [unowned self] (_) in
          self.viewModel.coordinates.navigateToVerifyPhoneNumber.onNext(())
        }, cancelCompletion: nil)
      })
      .disposed(by: disposeBag)
    
    viewModel.outputs
      .displayDeleteBrandSuccess
      .subscribe(onNext: { _ in
        Helper.Alert(view: self, title: "createbrand_delete_success_title".localized(), message: "createbrand_delete_success_message".localized(), confirmText: "ok".localized()) { (_) in
          self.viewModel.coordinates.navigateBack.onNext(())
        }
      })
      .disposed(by: disposeBag)
    
    //MARK: Bind loading
       
    viewModel.outputs.loading
      .subscribe(onNext: { [unowned self] isLoading in
        guard let tabBarController = self.tabBarController else { return }
        LoadingActivityView.action(in: tabBarController.view, isShow: isLoading)})
      .disposed(by: disposeBag)
    
    // MARK: Bind Error
          
    viewModel.error
      .subscribe(onNext: { [unowned self] error in
          Helper.Alert(
            view: self,
              title: error.title,
              message: error.message,
              confirmText: "done".localized())})
      .disposed(by: disposeBag)
    
  }
  
  func validateField() -> Bool {
    
    let isPassValidatedBrandName = validateTextField(text: brandNameTextField.text!)
    let isPassValidatedBrandPhone = validatePhoneField(phoneNumbers: brandPhoneNumbers)
    let isPassValidatedPhone = validatePhoneField(phoneNumbers: phoneNumbers)
    let isPassValidatedPrefix = validateTextField(text: prefixTextField.text!)
    let isPassValidatedName = validateTextField(text: nameTextField.text!)
    let isPassValidatedLastName = validateTextField(text: lastNameTextField.text!)
    let isPassValidatedHouseNo = validateTextField(text: houseNoTextField.text!)
    let isPassValidatedSubDistrict = validateTextField(text: subDistrictTextField.text!)
    let isPassValidatedDistrict = validateTextField(text: districtTextField.text!)
    let isPassValidatedProvince = validateTextField(text: provinceTextField.text!)
    let isPassValidatedPostCode = validateTextField(text: postCodeTextField.text!)
    let isPassValidatedMap = validateMapView(location: brandLocation)
    
    setUIRequireBrandName(isRequired: !isPassValidatedBrandName)
    setUIRequireBrandPhoneNumber(isRequired: !isPassValidatedBrandPhone)
    setUIRequirePrefix(isRequired: !isPassValidatedPrefix)
    setUIRequireName(isRequired: !isPassValidatedName)
    setUIRequireLastName(isRequired: !isPassValidatedLastName)
    setUIRequireHouseNo(isRequired: !isPassValidatedHouseNo)
    setUIRequireSubDistrict(isRequired: !isPassValidatedSubDistrict)
    setUIRequireDistrict(isRequired: !isPassValidatedDistrict)
    setUIRequirePostCode(isRequired: !isPassValidatedPostCode)
    setUIRequireProvince(isRequired: !isPassValidatedProvince)
    setUIRequireMap(isRequired: !isPassValidatedMap)
    setUIRequirePhoneNumber(isRequired: !isPassValidatedPhone)
    
    return isPassValidatedBrandName
      && isPassValidatedBrandPhone
      && isPassValidatedPrefix
      && isPassValidatedName
      && isPassValidatedLastName
      && isPassValidatedHouseNo
      && isPassValidatedSubDistrict
      && isPassValidatedDistrict
      && isPassValidatedProvince
      && isPassValidatedPostCode
      && isPassValidatedMap
      && isPassValidatedPhone
  }
  
  func validateTextField(text: String) -> Bool {
    return text.trimmingCharacters(in: .whitespacesAndNewlines) != ""
  }
  
  func validateMapView(location: CLLocationCoordinate2D?) -> Bool {
    return location != nil
  }
  
  func validatePhoneField(phoneNumbers: [String]) -> Bool {
    if phoneNumbers.count == 0 {
      return false
    } else {
      return true
    }
  }
  
  func setUIRequireBrandName(isRequired: Bool) {
    brandNameTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireBrandPhoneNumber(isRequired: Bool) {
    brandPhoneView.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequirePhoneNumber(isRequired: Bool) {
    phoneView.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequirePrefix(isRequired: Bool) {
    prefixView.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireName(isRequired: Bool) {
    nameTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireLastName(isRequired: Bool) {
    lastNameTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireHouseNo(isRequired: Bool) {
    houseNoTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireSubDistrict(isRequired: Bool) {
    subDistrictTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireDistrict(isRequired: Bool) {
    districtTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireProvince(isRequired: Bool) {
    provinceTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequirePostCode(isRequired: Bool) {
    postCodeTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireMap(isRequired: Bool) {
    mapView.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func isValidEmailFormat(email:String) -> Bool {
    if email == "" { return true }
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let validFormat = emailTest.evaluate(with: email)
    let appName = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String ?? ""
    if !validFormat {
      Helper.Alert(view: self, title: appName, message: "authentication_valid_email".localized(), confirmText: "submit".localized())
    }
    return validFormat
  }
  
  func presentPopOverView(_ controller: UIViewController, sourceView: UIView) {
    let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
    presentationController.sourceView = sourceView
    presentationController.sourceRect = CGRect(x: 50  , y: 44, width: 0, height: 0)
    presentationController.permittedArrowDirections = [.up]
    self.present(controller, animated: true)
  }
  
  func removeAnnotation() {
    for annotation in mapView.annotations {
      if annotation is MKUserLocation {
        continue
      } else {
        mapView.removeAnnotation(annotation)
      }
    }
  }
  
  func zoomTofit() {
    var zoomRect = MKMapRect.null
    for annotation in mapView.annotations {
      let mapPoint = MKMapPoint(annotation.coordinate)
      let pointRect = MKMapRect(x: mapPoint.x, y: mapPoint.y, width: 0.1, height: 0.1)
      zoomRect = zoomRect.union(pointRect)
    }
    mapView.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50), animated: true)
  }
  
  func setupAnnotation(location: CLLocationCoordinate2D) {
    removeAnnotation()
    let annotation = MKPointAnnotation()
    annotation.coordinate = location
    mapView.addAnnotations([annotation])
    zoomTofit()
  }
  
  func uploadImage() {
    if let logoImage = self.logoImage {
      let imageName = NSUUID().uuidString
      let filePath = "icon/\(imageName).png"
      let storageRef = self.storage.reference(withPath: filePath)
      guard let uploadData = logoImage.pngData() else { return }
      viewModel.outputs.loading.onNext(true)
      storageRef.putData(uploadData, metadata: nil) { [unowned self] (metadata, error) in
        self.viewModel.outputs.loading.onNext(false)
          if error != nil {
              Helper.Alert(view: self, title: "Error",
                           message: error?.localizedDescription ?? "",
                           confirmText: "ok".localized())
              return
          }
          print("GS link : ",storageRef)
        self.viewModel.outputs.loading.onNext(true)
          storageRef.downloadURL(completion: { [unowned self] (url, error) in
            self.viewModel.outputs.loading.onNext(false)
            if error != nil {
              Helper.Alert(view: self, title: "Error",
                           message: error?.localizedDescription ?? "",
                           confirmText: "ok".localized())
              return
            }
            guard let path = url else { return }
            self.imageID = "\(storageRef)"
            self.imageURL = "\(path)"
            self.createBrand()
          })
      }
    } else {
      createBrand()
    }
  }
  
  func createBrand() {
    let brandPhones = self.brandPhoneNumbers.map { ["phoneNumber" : $0] }
    let phoneNumbers = self.phoneNumbers.map{ ["phoneNumber" : $0] }
    var brandDetail = ""
    if brandDetailTextView.text != "createbrand_branddetail_placeholder".localized() {
      brandDetail = brandDetailTextView.text ?? ""
    }
    if isFromEdit {
      self.viewModel
      .inputs
      .updateBrandTrigger
      .onNext(BrandData(brandName: brandNameTextField.text ?? "",
                              brandEmail: brandEmailTextfield.text ?? "",
                              website: websiteTextfield.text ?? "",
                              lineID: brandLineTextfield.text ?? "",
                              facebook: facebookTextfield.text ?? "",
                              instagram: instagramTextfield.text ?? "",
                              imageURL: imageURL,
                              imageID: imageID,
                              brandDetail: brandDetail,
                              brandPhones: brandPhones,
                              building: buildingTextfield.text ?? "",
                              roomNO: roomTextField.text ?? "",
                              floor: floorTextField.text ?? "",
                              village: villageTextField.text ?? "",
                              houseNO: houseNoTextField.text ?? "",
                              villageNO: villageNoTextField.text ?? "",
                              soi: soiTextField.text ?? "",
                              street: streetTextField.text ?? "",
                              subDistrict: selectSubDistrict,
                              district: selectDistrict,
                              province: selectProvince,
                              postCode: postCodeTextField.text ?? "",
                              latitude: "\(brandLocation?.latitude ?? 0)",
                              longitude: "\(brandLocation?.longitude ?? 0)",
                              prefix: prefixTextField.text ?? "",
                              firstName: nameTextField.text ?? "",
                              lastName: lastNameTextField.text ?? "",
                              phoneNumbers: phoneNumbers,
                              email: emailTextField.text ?? ""))
    } else {
      self.viewModel
      .inputs
      .createBrandTrigger
      .onNext(BrandData(brandName: brandNameTextField.text ?? "",
                              brandEmail: brandEmailTextfield.text ?? "",
                              website: websiteTextfield.text ?? "",
                              lineID: brandLineTextfield.text ?? "",
                              facebook: facebookTextfield.text ?? "",
                              instagram: instagramTextfield.text ?? "",
                              imageURL: imageURL,
                              imageID: imageID,
                              brandDetail: brandDetail,
                              brandPhones: brandPhones,
                              building: buildingTextfield.text ?? "",
                              roomNO: roomTextField.text ?? "",
                              floor: floorTextField.text ?? "",
                              village: villageTextField.text ?? "",
                              houseNO: houseNoTextField.text ?? "",
                              villageNO: villageNoTextField.text ?? "",
                              soi: soiTextField.text ?? "",
                              street: streetTextField.text ?? "",
                              subDistrict: selectSubDistrict,
                              district: selectDistrict,
                              province: selectProvince,
                              postCode: postCodeTextField.text ?? "",
                              latitude: "\(brandLocation?.latitude ?? 0)",
                              longitude: "\(brandLocation?.longitude ?? 0)",
                              prefix: prefixTextField.text ?? "",
                              firstName: nameTextField.text ?? "",
                              lastName: lastNameTextField.text ?? "",
                              phoneNumbers: phoneNumbers,
                              email: emailTextField.text ?? ""))
    }
    
  }
  
  func disableTextField() {
    self.logoImageButton.isEnabled = false
    self.brandNameTextField.isEnabled = false
    self.brandPhoneTokenField.isEnabled = false
    self.brandEmailTextfield.isEnabled = false
    self.brandLineTextfield.isEnabled = false
    self.websiteTextfield.isEnabled  = false
    self.facebookTextfield.isEnabled = false
    self.brandDetailTextView.isUserInteractionEnabled = false
    self.prefixButton.isEnabled = false
    self.nameTextField.isEnabled = false
    self.lastNameTextField.isEnabled = false
    self.emailTextField.isEnabled = false
    self.phoneTokenField.isEnabled = false
    self.mapButton.isEnabled = false
    self.buildingTextfield.isEnabled = false
    self.roomTextField.isEnabled = false
    self.floorTextField.isEnabled = false
    self.villageTextField.isEnabled  = false
    self.houseNoTextField.isEnabled = false
    self.villageNoTextField.isEnabled = false
    self.soiTextField.isEnabled = false
    self.subDistrictTextField.isEnabled = false
    self.districtTextField.isEnabled = false
    self.provinceTextField.isEnabled = false
    self.postCodeTextField.isEnabled = false
    self.mapView.isUserInteractionEnabled = false
    self.prefixTextField.isEnabled = false
    self.provinceButton.isUserInteractionEnabled = false
    self.districtButton.isUserInteractionEnabled = false
    self.subDistricButton.isUserInteractionEnabled = false
    self.instagramTextfield.isUserInteractionEnabled = false
    self.streetTextField.isUserInteractionEnabled = false
    self.deleteButton.isUserInteractionEnabled = false
    
    self.brandNameTextField.layer.borderColor = UIColor.clear.cgColor
    self.brandPhoneTokenField.layer.borderColor = UIColor.clear.cgColor
    self.brandEmailTextfield.layer.borderColor = UIColor.clear.cgColor
    self.brandLineTextfield.layer.borderColor = UIColor.clear.cgColor
    self.websiteTextfield.layer.borderColor = UIColor.clear.cgColor
    self.facebookTextfield.layer.borderColor = UIColor.clear.cgColor
    self.brandDetailView.layer.borderColor = UIColor.clear.cgColor
    self.nameTextField.layer.borderColor = UIColor.clear.cgColor
    self.lastNameTextField.layer.borderColor = UIColor.clear.cgColor
    self.emailTextField.layer.borderColor = UIColor.clear.cgColor
    self.phoneTokenField.layer.borderColor = UIColor.clear.cgColor
    self.mapView.layer.borderColor = UIColor.clear.cgColor
    self.buildingTextfield.layer.borderColor = UIColor.clear.cgColor
    self.roomTextField.layer.borderColor = UIColor.clear.cgColor
    self.floorTextField.layer.borderColor = UIColor.clear.cgColor
    self.villageTextField.layer.borderColor = UIColor.clear.cgColor
    self.houseNoTextField.layer.borderColor = UIColor.clear.cgColor
    self.villageNoTextField.layer.borderColor = UIColor.clear.cgColor
    self.soiTextField.layer.borderColor = UIColor.clear.cgColor
    self.subDistrictTextField.layer.borderColor = UIColor.clear.cgColor
    self.districtTextField.layer.borderColor = UIColor.clear.cgColor
    self.provinceTextField.layer.borderColor = UIColor.clear.cgColor
    self.postCodeTextField.layer.borderColor = UIColor.clear.cgColor
    self.mapView.layer.borderColor = UIColor.clear.cgColor
    self.prefixView.layer.borderColor = UIColor.clear.cgColor
    self.instagramTextfield.layer.borderColor = UIColor.clear.cgColor
    self.brandPhoneView.layer.borderColor = UIColor.clear.cgColor
    self.streetTextField.layer.borderColor = UIColor.clear.cgColor
    self.phoneView.layer.borderColor = UIColor.clear.cgColor
    self.deleteButton.isUserInteractionEnabled = false
    self.deleteButton.isHidden = false
    
    self.brandNameTextField.placeholder = nil
    self.websiteTextfield.placeholder = nil
    self.brandEmailTextfield.placeholder = nil
    self.facebookTextfield.placeholder = nil
    self.brandLineTextfield.placeholder = nil
    self.instagramTextfield.placeholder = nil
    if self.brandDetailTextView.textColor == UIColor(hex: 0x8E8E93).withAlphaComponent(0.5) {
        self.brandDetailTextView.text = nil
    }
    self.buildingTextfield.placeholder = nil
    self.roomTextField.placeholder = nil
    self.floorTextField.placeholder = nil
    self.emailTextField.placeholder = nil
    self.villageTextField.placeholder = nil
    self.houseNoTextField.placeholder = nil
    self.villageNoTextField.placeholder = nil
    self.soiTextField.placeholder = nil
    self.streetTextField.placeholder = nil
    self.brandPhoneTokenField.textField.placeholder = nil
    self.phoneTokenField.textField.placeholder = nil
    self.starLabels.forEach{ $0.isHidden = true }
    self.dropDownImageViews.forEach{ $0.isHidden = true }
    self.phoneDescriptionLabel.isHidden = true
    self.brandPhoneDescriptionLabel.isHidden = true
    self.isView = true
    self.brandPhoneTokenField.reloadData()
    self.phoneTokenField.reloadData()
    self.deleteButton.isHidden = true
    self.submitTopContraint.constant = 0
    self.submitHeightConstraint.constant = 0
  }
  
  func enableTextField() {
    self.logoImageButton.isEnabled = true
    self.brandNameTextField.isEnabled = true
    self.brandPhoneTokenField.isEnabled = true
    self.brandEmailTextfield.isEnabled = true
    self.brandLineTextfield.isEnabled = true
    self.websiteTextfield.isEnabled  = true
    self.facebookTextfield.isEnabled = true
    self.brandDetailTextView.isUserInteractionEnabled = true
    self.prefixButton.isEnabled = true
    self.nameTextField.isEnabled = true
    self.lastNameTextField.isEnabled = true
    self.emailTextField.isEnabled = true
    self.phoneTokenField.isEnabled = true
    self.mapButton.isEnabled = true
    self.buildingTextfield.isEnabled = true
    self.roomTextField.isEnabled = true
    self.floorTextField.isEnabled = true
    self.villageTextField.isEnabled  = true
    self.houseNoTextField.isEnabled = true
    self.villageNoTextField.isEnabled = true
    self.soiTextField.isEnabled = true
    self.subDistrictTextField.isEnabled = true
    self.districtTextField.isEnabled = true
    self.provinceTextField.isEnabled = true
    self.postCodeTextField.isEnabled = true
    self.mapView.isUserInteractionEnabled = true
    self.prefixTextField.isEnabled = true
    self.submitButton.isHidden = false
    self.provinceButton.isUserInteractionEnabled = true
    self.districtButton.isUserInteractionEnabled = true
    self.subDistricButton.isUserInteractionEnabled = true
    self.instagramTextfield.isUserInteractionEnabled = true
    self.streetTextField.isUserInteractionEnabled = true
    
    self.brandNameTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.brandPhoneTokenField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.brandEmailTextfield.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.brandLineTextfield.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.websiteTextfield.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.facebookTextfield.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.brandDetailView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.nameTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.lastNameTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.emailTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.phoneTokenField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.mapView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.buildingTextfield.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.roomTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.floorTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.villageTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.houseNoTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.villageNoTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.soiTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.subDistrictTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.districtTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.provinceTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.postCodeTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.mapView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.prefixView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.instagramTextfield.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.brandPhoneView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.streetTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.phoneView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    
    let deleteUnderline: [NSAttributedString.Key: Any] = [
      .underlineStyle: NSUnderlineStyle.single.rawValue,
      .font : UIFont(name: "Kanit-SemiBold", size: 15)!,
      .foregroundColor : UIColor(hex: 0xF2503E)]
    let deleteAttributeString = NSMutableAttributedString(string: "createbrand_delete".localized(),
                                                    attributes: deleteUnderline)
    deleteButton.setAttributedTitle(deleteAttributeString, for: .normal)
    deleteButton.isSelected = true
    deleteButton.isUserInteractionEnabled = true
    deleteButton.isHidden = false
    
    self.setupPlaceholder()
    self.starLabels.forEach{ $0.isHidden = false }
    self.dropDownImageViews.forEach{ $0.isHidden = false }
    self.phoneDescriptionLabel.isHidden = false
    self.brandPhoneDescriptionLabel.isHidden = false
    self.prefixTextField.placeholder = nil
    self.nameTextField.placeholder = nil
    self.lastNameTextField.placeholder = nil
    self.isView = false
    self.brandPhoneTokenField.reloadData()
    self.phoneTokenField.reloadData()
    self.submitTopContraint.constant = 40
    self.submitHeightConstraint.constant = 44
  }
    
  func setupPlaceholder() {
    
    logoImageLabel.text = "createbrand_logo".localized()
    brandNameLabel.text = "createbrand_brandname".localized()
    brandDetailButton.setTitle("createbrand_title".localized(), for: .normal)
    brandPhoneTitleLabel.text = "createbrand_brandphone".localized()
    brandEmailLabel.text = "createbrand_email".localized()
    websiteLabel.text = "createbrand_website".localized()
    facebookLabel.text = "createbrand_facebook".localized()
    instagramLabel.text = "createbrand_instagram".localized()
    brandLineLabel.text = "createbrand_line".localized()
    brandDetailLabel.text = "createbrand_branddetail".localized()
    
    brandNameTextField.placeholder = "createbrand_brandname".localized()
    brandPhoneTokenField.textField.placeholder = "createbrand_phone_placeholder".localized()
    websiteTextfield.placeholder = "createbrand_website_placeholder".localized()
    brandEmailTextfield.placeholder = "createbrand_email_placeholder".localized()
    facebookTextfield.placeholder = "createbrand_facebook_placeholder".localized()
    brandLineTextfield.placeholder = "createbrand_line_placeholder".localized()
    instagramTextfield.placeholder = "createbrand_instagram_placeholder".localized()
    
    phoneTokenField.textField.placeholder = "createbrand_phone_placeholder".localized()
    
    businessLabel.text = "createbrand_business_information".localized()
    addressLabel.text = "createbrand_address".localized()
    
    prefixLabel.text = "createbrand_prefix".localized()
    prefixTextField.placeholder = "createbrand_prefix_placeholder".localized()
    nameLabel.text = "createbrand_name".localized()
    nameTextField.placeholder = "createbrand_name".localized()
    lastNameLabel.text = "createbrand_lastname".localized()
    lastNameTextField.placeholder = "createbrand_lastname".localized()
    emailLabel.text = "createbrand_email".localized()
    emailTextField.placeholder = "createbrand_email_placeholder".localized()
    phoneLabel.text = "createbrand_brandphone".localized()
    
    buildingLabel.text = "createbrand_building".localized()
    buildingTextfield.placeholder = "createbrand_building".localized()
    roomNoLabel.text = "createbrand_room_no".localized()
    roomTextField.placeholder = "createbrand_room_no".localized()
    floorLabel.text = "createbrand_floor".localized()
    floorTextField.placeholder = "createbrand_floor".localized()
    villageLabel.text = "createbrand_village".localized()
    villageTextField.placeholder = "createbrand_village".localized()
    
    houseNoLabel.text = "createbrand_house_no".localized()
    houseNoTextField.placeholder = "createbrand_house_no".localized()
    
    villageNoLabel.text = "createbrand_village_no".localized()
    villageNoTextField.placeholder = "createbrand_village_no".localized()
    
    soiLabel.text = "createbrand_soi".localized()
    soiTextField.placeholder = "createbrand_soi".localized()
    
    streetLabel.text = "createbrand_street".localized()
    streetTextField.placeholder = "createbrand_street".localized()
    
    subDistrictLabel.text = "createbrand_sub_district".localized()
    subDistrictTextField.placeholder = "createbrand_sub_district".localized()
    
    districtLabel.text = "createbrand_district".localized()
    districtTextField.placeholder = "createbrand_district".localized()
    
    provinceLabel.text = "createbrand_province".localized()
    provinceTextField.placeholder = "createbrand_province".localized()
    
    postCodeLabel.text = "createbrand_postcode".localized()
    postCodeTextField.placeholder = "createbrand_postcode".localized()
    
    mapLabel.text = "createbrand_map".localized()
    
    phoneDescriptionLabel.text = "createbrand_phone_description".localized()
    brandPhoneDescriptionLabel.text = "createbrand_phone_description".localized()
    
    submitButton.setTitle("createbrand_save".localized(), for: .normal)
    if brandDetailTextView.text == "" {
      brandDetailTextView.text = "createbrand_branddetail_placeholder".localized()
      brandDetailTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)
    }
    self.isView = false
    self.brandPhoneTokenField.reloadData()
    self.phoneTokenField.reloadData()
  }
  
  func setupEditData(brandData: BrandData) {
    if (brandData.imageURL == "") {
        self.logoImageView.image = UIImage(named: "ic_create_default_logo")
    } else {
        let url = URL(string: brandData.imageURL)
        self.logoImageView.kf.setImage(with: url)
    }
    self.brandNameTextField.text = brandData.brandName
    self.brandPhoneNumbers = brandData.brandPhones.map{ $0["phoneNumber"] as? String ?? "" }
    self.brandPhoneTokenField.reloadData()
    self.websiteTextfield.text = brandData.website
    self.brandEmailTextfield.text = brandData.brandEmail
    self.facebookTextfield.text = brandData.facebook
    self.brandLineTextfield.text = brandData.lineID
    self.instagramTextfield.text = brandData.instagram
    self.brandDetailTextView.text = brandData.brandDetail
    self.brandDetailTextView.textColor = .black
    self.prefixTextField.text = brandData.prefix
    self.nameTextField.text = brandData.firstName
    self.lastNameTextField.text = brandData.lastName
    self.emailTextField.text = brandData.email
    self.phoneNumbers = brandData.phoneNumbers.map{ $0["phoneNumber"] as? String ?? "" }
    self.phoneTokenField.reloadData()
    self.imageID = brandData.imageID
    self.imageURL = brandData.imageURL
    if brandData.latitude != "" && brandData.longitude != "" {
      let location = CLLocationCoordinate2D(latitude: Double(brandData.latitude) ?? 0,
                                            longitude: Double(brandData.longitude) ?? 0)
      self.brandLocation = location
      self.setupAnnotation(location: location)
    }
    self.roomTextField.text = brandData.roomNO
    self.floorTextField.text = brandData.floor
    self.villageTextField.text = brandData.village
    self.houseNoTextField.text = brandData.houseNO
    self.villageNoTextField.text = brandData.villageNO
    self.soiTextField.text = brandData.soi
    self.streetTextField.text = brandData.street
    self.selectProvince = brandData.province
    self.selectSubDistrict = brandData.subDistrict
    self.selectDistrict = brandData.district
    guard let province = LocalDataService.getProvince()?.list.filter({$0.provinceID == self.selectProvince}).first else { return }
    guard let district = LocalDataService.getDistrict()?.list.filter({$0.districtID == self.selectDistrict}).first else { return }
    guard let subDistrict = LocalDataService.getSubDistrict()?.list.filter({$0.subDistricID == self.selectSubDistrict}).first else { return }
    self.subDistrictTextField.text = subDistrict.nameTH
    self.districtTextField.text = district.nameTH
    self.provinceTextField.text = province.nameTH
    self.postCodeTextField.text = brandData.postCode
    self.buildingTextfield.text = brandData.building
  }
}

extension BrandViewController: ZFTokenFieldDelegate, ZFTokenFieldDataSource {
  func lineHeightForToken(in tokenField: ZFTokenField!) -> CGFloat {
    return 32
  }
  
  func numberOfToken(in tokenField: ZFTokenField!) -> UInt {
    if tokenField == brandPhoneTokenField {
      return UInt(brandPhoneNumbers.count)
    } else {
      return UInt(phoneNumbers.count)
    }
    
  }
  
  func tokenField(_ tokenField: ZFTokenField!, viewForTokenAt index: UInt) -> UIView! {
    
    if tokenField == brandPhoneTokenField {
      let nibs = Bundle.main.loadNibNamed("PhoneTokenView", owner: nil, options: nil)
      let view = nibs?[0] as! UIView
      let label = view.viewWithTag(2) as! UILabel
      let button = view.viewWithTag(3) as! UIButton
      
      button.addTarget(self, action: #selector(didClickOnClearPhoneBrandNumber(tokenButton:)), for: .touchUpInside)
      if isView {
        button.isHidden = true
        button.constraints.forEach{
          if $0.identifier == "widthConstraint" {
           $0.constant = 0
        }}
      } else {
        button.isHidden = false
        button.constraints.forEach{
          if $0.identifier == "widthConstraint" {
           $0.constant = 20
        }}
      }
      label.text = self.brandPhoneNumbers[Int(index)];
      view.frame = CGRect(x: 0, y: 0, width: 137, height: 32)
      view.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
      view.layer.borderWidth = 1
      return view
    } else {
      let nibs = Bundle.main.loadNibNamed("PhoneTokenView", owner: nil, options: nil)
      let view = nibs?[0] as! UIView
      let label = view.viewWithTag(2) as! UILabel
      let button = view.viewWithTag(3) as! UIButton
      
      button.addTarget(self, action: #selector(didClickOnClearPhoneNumber(tokenButton:)), for: .touchUpInside)
      if isView {
        button.isHidden = true
        button.constraints.forEach{
          if $0.identifier == "widthConstraint" {
           $0.constant = 0
        }}
      } else {
        button.isHidden = false
        button.constraints.forEach{
          if $0.identifier == "widthConstraint" {
           $0.constant = 20
        }}
      }
      label.text = self.phoneNumbers[Int(index)];
      view.frame = CGRect(x: 0, y: 0, width: 137, height: 32)
      view.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
      view.layer.borderWidth = 1
      return view
    }
  }
  
  @objc func didClickOnClearPhoneBrandNumber(tokenButton: UIButton) {
    let index = brandPhoneTokenField.index(ofTokenView: tokenButton.superview)
    if index != NSNotFound {
      self.brandPhoneNumbers.remove(at: Int(index))
      self.brandPhoneTokenField.reloadData()
    }
    
  }
  
  @objc func didClickOnClearPhoneNumber(tokenButton: UIButton) {
    let index = phoneTokenField.index(ofTokenView: tokenButton.superview)
    if index != NSNotFound {
      self.phoneNumbers.remove(at: Int(index))
      self.phoneTokenField.reloadData()
    }
    
  }
  
  func tokenMarginInToken(in tokenField: ZFTokenField!) -> CGFloat {
    return 8
  }
  
  func tokenField(_ tokenField: ZFTokenField!, didReturnWithText text: String!) {
    if tokenField == brandPhoneTokenField {
      self.brandPhoneNumbers.append(text)
      self.brandPhoneTokenField.reloadData()
    } else {
      phoneNumbers.append(text)
      phoneTokenField.reloadData()
    }
    
  }
  
  func tokenFieldShouldEndEditing(_ textField: ZFTokenField!) -> Bool {
    if textField == brandPhoneTokenField {
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [unowned self] in
        self.brandPhoneScrollView.setContentOffset(.zero, animated: true)
      }
      
      if textField.textField.text == "" { return true }
      self.brandPhoneNumbers.append(textField.textField.text!)
      self.brandPhoneTokenField.reloadData()
      self.brandPhoneTokenField.layoutIfNeeded()
      return true
    } else {
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [unowned self] in
        self.phoneScrollView.setContentOffset(.zero, animated: true)
      }
      
      if textField.textField.text == "" { return true }
      self.phoneNumbers.append(textField.textField.text!)
      self.phoneTokenField.reloadData()
      self.phoneTokenField.layoutIfNeeded()
      return true
    }
    
  }
}

extension BrandViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
      logoImage = image.resize(newWidth: 144)
      logoImageView.image = logoImage
    }
    dismiss(animated: true, completion: nil)
  }
}

extension BrandViewController: MKMapViewDelegate {
  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    guard !(annotation is MKUserLocation) else {
      return nil
    }
    
    let annotationIdentifier = "Identifier"
    var annotationView: MKAnnotationView?
    if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
      annotationView = dequeuedAnnotationView
      annotationView?.annotation = annotation
    }
    else {
      annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
      annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
    }
    
    if let annotationView = annotationView {
      annotationView.canShowCallout = true
      annotationView.image = UIImage(named: "ic_map_pin")
    }
    return annotationView
  }
}

extension BrandViewController: MapSelectViewControllerDelegate {
  func didSelectLocation(location: CLLocationCoordinate2D) {
    print(location)
    self.brandLocation = location
    setupAnnotation(location: location)
  }
}

extension BrandViewController: InAppVerifyPhoneNumberViewControllerDelegate {
  func didVerifySuccess() {
    viewModel.inputs.deleteBrandTrigger.onNext(())
  }
}
