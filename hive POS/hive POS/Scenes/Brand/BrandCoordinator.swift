//
//  BrandCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 28/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol BrandInputsParamsType {
  var brandID: Int? { get }
}

class BrandCoordinator: BaseCoordinator<Void>, BrandInputsParamsType {
  
  // MARK: Property
  
  var brandID: Int?

  // MARK: Public

  override func start() -> Observable<Void> { 
    let window = self.window
    let viewModel = BrandViewModel(brandID: brandID)
    let viewController = BrandViewController.initFromStoryboard(name: Storyboard.brandBranch.identifier)
    viewController.viewModel = viewModel
    viewController.hidesBottomBarWhenPushed = true
    
    viewModel
      .coordinates
      .presentSelectLocation
      .map{ _  -> MapSelectCoordinator in
        let coordinator = MapSelectCoordinator(window: window,
                                               baseViewController: viewController,
                                               transitionType: .push,
                                               animated: true)
        coordinator.viewController.delegate = viewController.self
        coordinator.viewController.navigationTitle = "createbrand_title".localized()
        return coordinator }
      .flatMapLatest(navigateToTarget)
      .subscribe()
      .disposed(by: disposeBag)
    
    viewModel
      .coordinates
      .navigateToVerifyPhoneNumber
      .map{ _ -> InAppVerifyPhoneNumberCoordinator in
        let coordinator = InAppVerifyPhoneNumberCoordinator(window: window,
                                                            baseViewController: viewController,
                                                            transitionType: .modal, animated: true)
        coordinator.viewController.delegate = viewController.self
        return coordinator }
      .flatMapLatest(navigateToTarget)
      .subscribe()
      .disposed(by: disposeBag)
    
    transition(to: viewController)
    return viewModel.coordinates.navigateBack
    .do(onNext: { [weak self] _ in self?.navigateBack()})
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: BrandInputsParamsType { return self }
}
