//
//  BrandViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 28/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa

struct BrandData {
  var brandName: String
  var brandEmail: String
  var website: String
  var lineID: String
  var facebook: String
  var instagram: String
  var imageURL: String
  var imageID: String
  var brandDetail: String
  var brandPhones: [[String : Any]]
  var building: String
  var roomNO: String
  var floor: String
  var village: String
  var houseNO: String
  var villageNO: String
  var soi : String
  var street: String
  var subDistrict: Int
  var district: Int
  var province: Int
  var postCode: String
  var latitude: String
  var longitude: String
  var prefix: String
  var firstName: String
  var lastName: String
  var phoneNumbers: [[String : Any]]
  var email: String
}

protocol BrandViewModelInputs {
  var selectLocationTrigger: PublishSubject<Void> { get }
  var createBrandTrigger: PublishSubject<BrandData> { get }
  var getBrandTrigger: PublishSubject<Int> { get }
  var updateBrandTrigger: PublishSubject<BrandData> { get }
  var verifyPhoneNumberTrigger: PublishSubject<Void> { get }
  var deleteBrandTrigger: PublishSubject<Void> { get }
  var selectProvinceTrigger: PublishSubject<Int> { get }
  var selectDistrictTrigger: PublishSubject<Int> { get }
  var selectSubDistrictTrigger: PublishSubject<Int> { get }
}

protocol BrandViewModelOutputs {
  var loading: BehaviorSubject<Bool> { get }
  var displayCreateBrandSuccess: PublishSubject<Void> { get }
  var isFromDetail: BehaviorRelay<Int?> { get }
  var displayBrandData: PublishSubject<BrandData> { get }
  var displayUpdateBrandSuccess: PublishSubject<Void> { get }
  var displayDeleteBrandSuccess: PublishSubject<Void> { get }
  var displaySelectProvince: PublishSubject<Int> { get }
  var displaySelectDistrict: PublishSubject<Int> { get }
  var displaySelectSubDistrict: PublishSubject<Int> { get }
}

protocol BrandViewModelCoordinates {
  var navigateBack: PublishSubject<Void> { get }
  var presentSelectLocation: PublishSubject<Void> { get }
  var navigateToVerifyPhoneNumber: PublishSubject<Void> { get }
}

protocol BrandViewModelType: ErrorAlertableViewModelType {
  var inputs: BrandViewModelInputs { get }
  var outputs: BrandViewModelOutputs { get }
  var coordinates: BrandViewModelCoordinates { get }
}

class BrandViewModel: BrandViewModelType, BrandViewModelInputs, BrandViewModelOutputs, BrandViewModelCoordinates {
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  
  // MARK: Init
    
  public init(apiService: APIService = APIService(), brandID: Int?) {
    
    guard var district = LocalDataService.getProvince()?.list.filter({$0.status == "Y"}) else { return }
    guard var subDistrict = LocalDataService.getProvince()?.list.filter({ $0.status == "Y"}) else { return }
    
    self.isFromDetail.accept(brandID)
    
    let createBrandTriggerSignal =
      createBrandTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ CreateBrandRequestModel(brandName: $0.brandName, brandEmail: $0.brandEmail,
                                      website: $0.website, lineID: $0.lineID,
                                      facebook: $0.facebook, instagram: $0.instagram,
                                      imageURL: $0.imageURL, imageID: $0.imageID,
                                      brandDetail: $0.brandDetail, brandPhones: $0.brandPhones,
                                      building: $0.building, roomNO: $0.roomNO,
                                      floor: $0.floor, village: $0.village,
                                      houseNO: $0.houseNO, villageNO: $0.villageNO,
                                      soi: $0.soi, street: $0.street,
                                      subDistrict: $0.subDistrict, district: $0.district,
                                      province: $0.province, postCode: $0.postCode,
                                      latitude: $0.latitude, longitude: $0.longitude,
                                      prefix: $0.prefix, firstName: $0.firstName,
                                      lastName: $0.lastName, phoneNumbers: $0.phoneNumbers,
                                      email: $0.email) }
        .flatMapLatest{ apiService.rx_createBrand(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    createBrandTriggerSignal
      .elements()
      .map{ _ in () }
      .bind(to:displayCreateBrandSuccess)
      .disposed(by: disposeBag)
    
    selectLocationTrigger
      .bind(to: presentSelectLocation)
      .disposed(by: disposeBag)
    
    //MARK: Edit
    
    let getBrandDataTriggerSignal =
      getBrandTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ GetBrandRequestModel(brandID: $0) }
        .flatMapLatest{ apiService.rx_getBrand(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    getBrandDataTriggerSignal
      .elements()
      .map{ BrandData(brandName: $0.brandName, brandEmail: $0.email,
                      website: $0.website, lineID: $0.lineName, facebook: $0.facebookUrl,
                      instagram: $0.instagramName, imageURL: $0.brandImageUrl, imageID: $0.brandImageID,
                      brandDetail: $0.brandDetail, brandPhones: $0.brandTelephones.map{ ["phoneNumber" : $0.phoneNumber] },
                      building: $0.building, roomNO: $0.roomNumber, floor: $0.floor, village: $0.village,
                      houseNO: $0.placeNumber, villageNO: $0.moo, soi: $0.alley, street: $0.road,
                      subDistrict: $0.area, district: $0.district, province: $0.province, postCode: $0.zipcode,
                      latitude: "\($0.placeLatitude)", longitude: "\($0.placeLongitude)", prefix: $0.contactPrefix,
                      firstName: $0.contactFirstname, lastName: $0.contactLastname,
                      phoneNumbers: $0.contactTelephones.map{ ["phoneNumber" : $0.phoneNumber] },
                      email: $0.contactEmail) }
      .bind(to: displayBrandData)
      .disposed(by: disposeBag)
    
    let updateBrandTriggerSignal =
      updateBrandTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ brandData -> UpdateBrandRequestModel? in
          guard let brandID = brandID else {
            self.outputs.loading.onNext(false)
            return nil }
          let requestModel =
            UpdateBrandRequestModel(brandID: brandID, brandName: brandData.brandName,
                                    brandEmail: brandData.brandEmail, website: brandData.website,
                                    lineID: brandData.lineID, facebook: brandData.facebook,
                                    instagram: brandData.instagram, imageURL: brandData.imageURL,
                                    imageID: brandData.imageID, brandDetail: brandData.brandDetail,
                                    brandPhones: brandData.brandPhones, building: brandData.building,
                                    roomNO: brandData.roomNO, floor: brandData.floor, village: brandData.village,
                                    houseNO: brandData.houseNO, villageNO: brandData.villageNO,
                                    soi: brandData.soi, street: brandData.street, subDistrict: brandData.subDistrict,
                                    district: brandData.district, province: brandData.province,
                                    postCode: brandData.postCode, latitude: brandData.latitude,
                                    longitude: brandData.longitude, prefix: brandData.prefix,
                                    firstName: brandData.firstName, lastName: brandData.lastName,
                                    phoneNumbers: brandData.phoneNumbers, email: brandData.email)
        return requestModel}
        .filterNil()
        .flatMapLatest{ apiService.rx_updateBrand(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    updateBrandTriggerSignal
      .elements().map{ _ in ()}
      .bind(to: displayUpdateBrandSuccess)
      .disposed(by: disposeBag)
    
    let deleteBrandTriggerSignal =
      deleteBrandTrigger
        .map { _ in brandID }
        .filterNil()
        .map { DeleteBrandRequestModel(brandID: $0) }
        .flatMapLatest{ apiService.rx_deleteBrand(requestModel: $0).materialize() }
        .share(replay: 1)
    
    deleteBrandTriggerSignal.elements()
      .bind(to: displayDeleteBrandSuccess)
      .disposed(by: disposeBag)
    
    verifyPhoneNumberTrigger
      .bind(to: navigateToVerifyPhoneNumber)
      .disposed(by: disposeBag)
    
    // Error ---------------------------------------------------------------

    let errorSignal = Observable
      .merge(createBrandTriggerSignal.errors(),
             getBrandDataTriggerSignal.errors(),
             updateBrandTriggerSignal.errors(),
             deleteBrandTriggerSignal.errors())
      .share(replay: 1)

    errorSignal
      .map(errorToAlertable)
      .bind(to: error)
      .disposed(by: disposeBag)
  }
  
  // MARK: Private
  
  // MARK: Input
  
  var selectLocationTrigger = PublishSubject<Void>()
  var createBrandTrigger = PublishSubject<BrandData>()
  var getBrandTrigger = PublishSubject<Int>()
  var updateBrandTrigger = PublishSubject<BrandData>()
  var verifyPhoneNumberTrigger = PublishSubject<Void>()
  var deleteBrandTrigger = PublishSubject<Void>()
  var selectDistrictTrigger = PublishSubject<Int>()
  var selectProvinceTrigger = PublishSubject<Int>()
  var selectSubDistrictTrigger = PublishSubject<Int>()
    
  // MARK: Output
  
  var error = PublishSubject<(title: String, message: String)>()
  var loading = BehaviorSubject<Bool>(value: false)
  var displayCreateBrandSuccess = PublishSubject<Void>()
  var isFromDetail = BehaviorRelay<Int?>(value: nil)
  var displayBrandData = PublishSubject<BrandData>()
  var displayUpdateBrandSuccess = PublishSubject<Void>()
  var navigateToVerifyPhoneNumber = PublishSubject<Void>()
  var displayDeleteBrandSuccess = PublishSubject<Void>()
  var displaySelectProvince = PublishSubject<Int>()
  var displaySelectSubDistrict = PublishSubject<Int>()
  var displaySelectDistrict = PublishSubject<Int>()

  // MARK: Coordinates
  
  var navigateBack = PublishSubject<Void>()
  var presentSelectLocation = PublishSubject<Void>()
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: BrandViewModelOutputs { return self }
  public var inputs: BrandViewModelInputs { return self }
  public var coordinates: BrandViewModelCoordinates { return self }
}
