//
//  AuthenticationCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol AuthenticationInputsParamsType {
    
}

class AuthenticationCoordinator: BaseCoordinator<Void>, AuthenticationInputsParamsType {
  
  // MARK: Property

  // MARK: Public

  override func start() -> Observable<Void> {
    let window = self.window
    let viewModel = AuthenticationViewModel()
    let viewController = AuthenticationViewController.initFromStoryboard(name: Storyboard.authentication.identifier)
    viewController.viewModel = viewModel
    
    viewModel
      .coordinates
      .navigateToForgotPassword
      .map{ ForgotPasswordCoordinator(window: window,
                                     baseViewController: viewController,
                                     transitionType: .modal,
                                     animated: true) }
      .flatMapLatest(navigateToTarget)
      .subscribe()
      .disposed(by: disposeBag)
    
    viewModel
    .coordinates
    .navigateToRegister
    .map{ RegisterCoordinator(window: window,
                              baseViewController: viewController,
                              transitionType: .modal,
                              animated: true) }
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
    viewModel.coordinates
      .navigateToMainTabBar
      .map{ MainTabBarCoordinator(window: window,
                                  baseViewController: viewController,
                                  transitionType: .rootWindow,
                                  animated: true) }
      .flatMapLatest(navigateToTarget)
      .subscribe()
      .disposed(by: disposeBag)
  
    transition(to: viewController)
    return .never()
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: AuthenticationInputsParamsType { return self }
}
