//
//  AuthenticationViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZSwiftKit
import Firebase
import FirebaseDatabase

class AuthenticationViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var wrongEmailLabel: UILabel!
  @IBOutlet weak var wrongEmailPasswordLabel: UILabel!
  @IBOutlet weak var signInBackgroundView: UIView!
  @IBOutlet weak var welcomeLabel: UILabel!
  @IBOutlet weak var descriptionLabel: UILabel!
  @IBOutlet weak var emailTitleLabel: UILabel!
  @IBOutlet weak var passwordTitleLabel: UILabel!
  @IBOutlet weak var signInLabel: UILabel!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var emailTextBackField: UITextField!
  @IBOutlet weak var passwordBackTextField: UITextField!
  @IBOutlet weak var forgotPasswordButton: UIButton!
  @IBOutlet weak var signInButton: UIButton!
  @IBOutlet weak var signInView: UIView!
  @IBOutlet weak var registerButton: UIButton!
  @IBOutlet weak var showHidePasswordButton: UIButton!
  @IBOutlet weak var rememberMeButton: UIButton!
  
  // MARK: Property 
  
  var viewModel: AuthenticationViewModelType!
  let disposeBag: DisposeBag = DisposeBag()
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupL10N()
    bindViewModel()
  }
  
  func bindViewModel() {
    
    showHidePasswordButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.showHidePasswordButton.isSelected = !self.showHidePasswordButton.isSelected
        if self.showHidePasswordButton.isSelected {
          self.passwordTextField.isSecureTextEntry = false
        } else {
          self.passwordTextField.isSecureTextEntry = true}})
      .disposed(by: disposeBag)
    
    forgotPasswordButton.rx.tap
      .bind(to: viewModel.inputs.forgotPasswordButtonTrigger)
      .disposed(by: disposeBag)
  
    rememberMeButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.rememberMeButton.isSelected = !self.rememberMeButton.isSelected
        if self.rememberMeButton.isSelected {
          LocalDataService.setRememberMe()
        } else {
          LocalDataService.deleteRememberMe()
        }
      })
      .disposed(by: disposeBag)
    
    signInButton.rx.tap
      .filter{ [unowned self] in self.validateField() }
      .filter{ [unowned self] in self.isValidEmailFormat(email: self.emailTextField.text!) }
      .do(onNext: { _ in LocalDataService.deleteUser(); LocalDataService.deleteUserToken(); LocalDataService.deleteSelectBrand() })
      .map{ [unowned self] in (email: self.emailTextField.text!,
                               password: self.passwordTextField.text!) }
      .bind(to: viewModel.inputs.signInTrigger)
      .disposed(by: disposeBag)
    
    registerButton.rx.tap
      .bind(to: viewModel.inputs.registerButtonTrigger)
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayWrongEmail
      .subscribe(onNext: { [unowned self] _ in
        self.wrongEmailLabel.text = "authentication_wrong_email".localized()
        self.wrongEmailLabel.isHidden = false
        self.wrongEmailPasswordLabel.isHidden = true
        self.emailTextBackField.layer.borderColor = UIColor(hex: 0xF2503E).cgColor
        self.passwordBackTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
      })
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayWrongEmailPassword
      .subscribe(onNext: { [unowned self] _ in
        self.wrongEmailPasswordLabel.text = "authentication_wrong_email_password".localized()
        self.wrongEmailLabel.isHidden = true
        self.wrongEmailPasswordLabel.isHidden = false
        self.emailTextBackField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
        self.passwordBackTextField.layer.borderColor = UIColor(hex: 0xF2503E).cgColor
      })
      .disposed(by: disposeBag)
    
    //MARK: Bind loading
    
    viewModel.outputs.loading
      .subscribe(onNext: { [unowned self] isLoading in
        LoadingActivityView.action(in: self.view, isShow: isLoading)})
      .disposed(by: disposeBag)
    
    // MARK: Bind Error
          
    viewModel.error
      .subscribe(onNext: { [unowned self] error in
          Helper.Alert(
              view: self,
              title: error.title,
              message: error.message,
              confirmText: "done".localized())})
      .disposed(by: disposeBag)
    
  }
  
  func setupUI() {
    signInBackgroundView.layer.cornerRadius = 10
    signInView.layer.cornerRadius = signInView.frame.height / 2
    emailTextBackField.layer.cornerRadius = 10
    passwordBackTextField.layer.cornerRadius = 10
    emailTextBackField.layer.borderWidth = 1
    passwordBackTextField.layer.borderWidth = 1
    emailTextBackField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    passwordBackTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    wrongEmailPasswordLabel.isHidden = true
    wrongEmailLabel.isHidden = true
    
  }
  
  func setupL10N() {
    
    signInLabel.text = "authentication_signin".localized()
    welcomeLabel.text = "authentication_welcome".localized()
    emailTitleLabel.text = "authentication_email".localized()
    passwordTitleLabel.text = "authentication_password".localized()
    descriptionLabel.text = "authentication_description".localized()
    emailTextField.placeholder = "authentication_email".localized()
    passwordTextField.placeholder = "authentication_password".localized()
    
    wrongEmailLabel.text = "authentication_wrong_email".localized()
    wrongEmailPasswordLabel.text = "authentication_wrong_email_password".localized()
    
    rememberMeButton.setTitle("authentication_remember_me".localized(), for: .normal)
    
    let underline: [NSAttributedString.Key: Any] = [
      .underlineStyle: NSUnderlineStyle.single.rawValue,
      .font : UIFont(name: "Kanit-Regular", size: 15)!,
      .foregroundColor : UIColor(hex: 0x454F63)]
    let attributeString = NSMutableAttributedString(string: "authentication_forgot_password".localized(),
                                                    attributes: underline)
    forgotPasswordButton.setAttributedTitle(attributeString, for: .normal)
    
    let attributeStringRegister = NSMutableAttributedString(string: "authentication_register".localized(),
                                                    attributes: underline)
    registerButton.setAttributedTitle(attributeStringRegister, for: .normal)
    
  }
  
  func validateField() -> Bool {
    
    wrongEmailPasswordLabel.isHidden = true
    wrongEmailLabel.isHidden = true
    
    let isPassValidatedUsername = validateTextField(text: emailTextField.text!)
    let isPassValidatedPassword = validateTextField(text: passwordTextField.text!)
    
    setUIRequireUsername(isRequired: !isPassValidatedUsername)
    setUIRequirePassword(isRequired: !isPassValidatedPassword)
    
    return isPassValidatedUsername && isPassValidatedPassword
  }
  
  func validateTextField(text: String) -> Bool {
    return text != ""
  }
  
  func setUIRequireUsername(isRequired: Bool) {
    emailTextBackField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
    wrongEmailLabel.text = isRequired ? "authentication_please_input".localized() : "authentication_wrong_email".localized()
    wrongEmailLabel.isHidden = !isRequired
  }
  
  func setUIRequirePassword(isRequired: Bool) {
    passwordBackTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
    wrongEmailPasswordLabel.text = isRequired ? "authentication_please_input".localized() : "authentication_wrong_email_password".localized()
    wrongEmailPasswordLabel.isHidden = !isRequired
  }
  
  func isValidEmailFormat(email:String) -> Bool {
    
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let validFormat = emailTest.evaluate(with: email)
    let appName = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as? String ?? ""
    if !validFormat {
      Helper.Alert(view: self, title: appName, message: "authentication_valid_email".localized(), confirmText: "submit".localized())
    }
    return validFormat
  }
  
}
