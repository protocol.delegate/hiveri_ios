//
//  AuthenticationViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 20/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa
import Firebase
import FirebaseDatabase
import RxSwiftExt
import RxOptional

protocol AuthenticationViewModelInputs {
  var signInTrigger: PublishSubject<(email: String, password: String)> { get }
  var forgotPasswordButtonTrigger: PublishSubject<Void> { get }
  var registerButtonTrigger: PublishSubject<Void> { get }
}

protocol AuthenticationViewModelOutputs {
  var loading: BehaviorSubject<Bool> { get }
  var displayWrongEmail: PublishSubject<Void> { get }
  var displayWrongEmailPassword: PublishSubject<Void> { get }
}

protocol AuthenticationViewModelCoordinates {
  var navigateToForgotPassword: PublishSubject<Void> { get }
  var navigateToRegister: PublishSubject<Void> { get }
  var navigateToMainTabBar: PublishSubject<Void> { get }
}

protocol AuthenticationViewModelType: ErrorAlertableViewModelType {
  var inputs: AuthenticationViewModelInputs { get }
  var outputs: AuthenticationViewModelOutputs { get }
  var coordinates: AuthenticationViewModelCoordinates { get }
}

class AuthenticationViewModel: AuthenticationViewModelType, AuthenticationViewModelInputs, AuthenticationViewModelOutputs, AuthenticationViewModelCoordinates {
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  var ref: DatabaseReference = Database.database().reference()
  
  // MARK: Init
    
  public init(apiService: APIServiceType = APIService()) {
    
    signInTrigger
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
      .do(onNext: {[unowned self] _ in self.signOut()})
      .do(onNext: { LocalDataService.setAuthen(email: $0.email, password: $0.password) })
      .subscribe(onNext: { data in
        Auth.auth().signIn(withEmail: data.email, password: data.password) { (result, error) in
          self.outputs.loading.onNext(false)
          switch error {
          case .none:
            if result?.user.isEmailVerified == false {
              self.error.onNext((title: "Email Not Verified",
                                 message: "authentication_verify_email".localized()))
              return
            }
            Auth.auth().currentUser?.getIDToken(completion: { (token, error) in
              if error == nil {
                guard let userToken = token else { return }
                LocalDataService.setUserToken(token: userToken)
                guard let userID = Auth.auth().currentUser?.uid else { return }
                self.ref.child("users").child(userID).observeSingleEvent(of: .value) { (snapshot) in
                  let value = snapshot.value as? NSDictionary
                  let firstName = value?["firstName"] as? String
                  let lastName = value?["lastName"] as? String
                  let prefix = value?["prefix"] as? String
                  let countryCode = value?["countryCode"] as? String
                  let phoneNumber = value?["phoneNumber"] as? String
                  let profileImageID = value?["profileImageId"] as? String
                  let profileImageURL = value?["profileImageUrl"] as? String
                  let requestModel = UpdateUserRequestModel(firstName: firstName ?? "",
                                                            lastName: lastName ?? "",
                                                            profileImagePath: profileImageURL ?? "",
                                                            profileImageID: profileImageID ?? "",
                                                            countryCode: countryCode ?? "",
                                                            phoneNumber: phoneNumber ?? "",
                                                            prefix: prefix ?? "")
                  self.requestModel.onNext(requestModel)
                }
              } else {
                self.error.onNext((title: "Error", message: error?.localizedDescription ?? ""))
              }
            })
            
          case  .some(let error as NSError) where error.code == AuthErrorCode.wrongPassword.rawValue:
            self.displayWrongEmailPassword.onNext(())
          case .some(let error as NSError) where error.code == AuthErrorCode.userNotFound.rawValue:
            self.displayWrongEmail.onNext(())
            
          case .some(let error):
            self.error.onNext((title: "Error", message: error.localizedDescription))
          }
        }
      })
      .disposed(by: disposeBag)
    
    let signInTriggerSignal =
      requestModel
        .filterNil()
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .flatMapLatest{ apiService
          .rx_updateUser(requestModel: $0)
          .materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    signInTriggerSignal
      .elements()
      .subscribe(onNext: { model in print("model------> ", model.firebaseUid) })
      .disposed(by: disposeBag)
    
    signInTriggerSignal
      .elements()
      .map{ LocalDataService.setUser($0)
        return () }
      .bind(to: navigateToMainTabBar)
      .disposed(by: disposeBag)
    
    signInTriggerSignal.errors()
      .subscribe(onNext: { [unowned self] _ in self.signOut() })
      .disposed(by: disposeBag)
    
    forgotPasswordButtonTrigger
      .bind(to: coordinates.navigateToForgotPassword)
      .disposed(by: disposeBag)
    
    registerButtonTrigger
      .bind(to: coordinates.navigateToRegister)
      .disposed(by: disposeBag)
    
    // Error ---------------------------------------------------------------
    
    let errorSignal = Observable
      .merge(signInTriggerSignal.errors())
      .share(replay: 1)
    
    errorSignal
      .map(errorToAlertable)
      .bind(to: error)
      .disposed(by: disposeBag)
    
  }
  
  // MARK: Private
  
  private func signOut() {
    do { try Auth.auth().signOut() } catch {}
  }
  
  // MARK: Input
  
  var signInTrigger = PublishSubject<(email: String, password: String)>()
  var forgotPasswordTrigger = PublishSubject<String>()
  var forgotPasswordButtonTrigger = PublishSubject<Void>()
  var registerButtonTrigger = PublishSubject<Void>()
    
  // MARK: Output

  var loading = BehaviorSubject<Bool>(value: false)
  var displayWrongEmail = PublishSubject<Void>()
  var displayWrongEmailPassword = PublishSubject<Void>()
  var displayForgotPasswordSuccess = PublishSubject<Void>()
  var error = PublishSubject<(title: String, message: String)>()
  var requestModel = BehaviorSubject<UpdateUserRequestModel?>(value: nil)

  // MARK: Coordinates
  
  var navigateToForgotPassword = PublishSubject<Void>()
  var navigateToRegister = PublishSubject<Void>()
  var navigateToMainTabBar = PublishSubject<Void>()
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: AuthenticationViewModelOutputs { return self }
  public var inputs: AuthenticationViewModelInputs { return self }
  public var coordinates: AuthenticationViewModelCoordinates { return self }
}
