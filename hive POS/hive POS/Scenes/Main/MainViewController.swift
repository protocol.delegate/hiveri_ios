//
//  MainViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 23/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class MainViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  // MARK: Property 
  
  var viewModel: MainViewModelType!
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
  }
  
  func bindViewModel() {
  }
}
