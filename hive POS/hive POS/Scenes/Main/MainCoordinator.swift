//
//  MainCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 23/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol MainInputsParamsType {
    
}

class MainCoordinator: BaseCoordinator<Void>, MainInputsParamsType {
  
  // MARK: Property

  // MARK: Public

  override func start() -> Observable<Void> { 
    let window = self.window
    let viewModel = MainViewModel()
    let viewController = MainViewController.initFromStoryboard(name: Storyboard.main.identifier)
    viewController.viewModel = viewModel
    let navigationController = UINavigationController(rootViewController: viewController)
    navigationController.setNavigationBarHidden(true, animated: false)
    
    transition(to: navigationController)
    
    return .never()
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: MainInputsParamsType { return self }
}
