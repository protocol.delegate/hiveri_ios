//
//  MainViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 23/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa

protocol MainViewModelInputs {
    
}

protocol MainViewModelOutputs {
    
}

protocol MainViewModelCoordinates {
    
}

protocol MainViewModelType {
  var inputs: MainViewModelInputs { get }
  var outputs: MainViewModelOutputs { get }
  var coordinates: MainViewModelCoordinates { get }
}

class MainViewModel: MainViewModelType, MainViewModelInputs, MainViewModelOutputs, MainViewModelCoordinates {
  
  // MARK: Property
  
  // MARK: Init
    
  public init() {
  }
  
  // MARK: Private
  
  // MARK: Input
    
  // MARK: Output

  // MARK: Coordinates
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: MainViewModelOutputs { return self }
  public var inputs: MainViewModelInputs { return self }
  public var coordinates: MainViewModelCoordinates { return self }
}
