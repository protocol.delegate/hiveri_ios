//
//  PaymentOrderManagementViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 12/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa

protocol PaymentOrderManagementViewModelInputs {
  var getPaymenTypeTrigger: PublishSubject<Void> { get }
  var getBindingCreditCardTrigger: PublishSubject<Void> { get }
  var getCreditCardInqueryTrigger: PublishSubject<Double> { get }
  var getTableListTrigger: PublishSubject<(spoonWalkID: String, brandID: Int)> { get }
}

protocol PaymentOrderManagementViewModelOutputs {
  var displayPaymentType: PublishSubject<[SectionOfDisplayPaymentType]> { get }
  var displayBindingCreditCard: PublishSubject<String> { get }
  var displayCreditCardTransectionSuccess: PublishSubject<Void> { get }
  var loading: BehaviorSubject<Bool> { get }
  var displayTableList: PublishSubject<[DisplayTableData]> { get }
}

protocol PaymentOrderManagementViewModelCoordinates {
  var navigateToPayment: PublishSubject<String> { get }
}

protocol PaymentOrderManagementViewModelType: ErrorAlertableViewModelType {
  var inputs: PaymentOrderManagementViewModelInputs { get }
  var outputs: PaymentOrderManagementViewModelOutputs { get }
  var coordinates: PaymentOrderManagementViewModelCoordinates { get }
}

class PaymentOrderManagementViewModel: PaymentOrderManagementViewModelType, PaymentOrderManagementViewModelInputs, PaymentOrderManagementViewModelOutputs, PaymentOrderManagementViewModelCoordinates {
  
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  
  // MARK: Init
    
  public init(apiService: APIServiceType = APIService()) {
    
    let paymentData = [DisplayPaymentType(paymentType: .cash),
                    DisplayPaymentType(paymentType: .creditCard)]
    
    var paymentUserID: String?
    var paymentCardID: String?
    var amount: Double?
    
    getPaymenTypeTrigger
      .map{[SectionOfDisplayPaymentType(header: "", items: paymentData)]}
      .bind(to: displayPaymentType)
      .disposed(by: disposeBag)
    
    let getBindingCreditCardTriggerSignal =
      getBindingCreditCardTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ _ -> BindingCreditCardRequestModel in
          paymentUserID = UUID().uuidString
          paymentCardID = UUID().uuidString
          print("userID ", paymentUserID ?? "")
          print("cardID ", paymentCardID ?? "")
          return BindingCreditCardRequestModel(platformID: "test_platform02",
                                               customerID: paymentUserID ?? "",
                                               cardID: paymentCardID ?? "",
                                               cardName: "Credit Card",
                                               callBackURL: "hiveri://paymentSuccess")}
        .flatMapLatest{ apiService.rx_bindingCreditCard(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    getBindingCreditCardTriggerSignal.elements()
      .map{ $0.deepLink }
      .bind(to: navigateToPayment)
      .disposed(by: disposeBag)
    
    let getCreditCardInqueryTriggerSignal =
      getCreditCardInqueryTrigger
        .do(onNext: { amount = $0 })
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ _ in InqueryCreditCardRequestModel(platformID: "test_platform02",
                                                 customerID: paymentUserID ?? "")}
        .flatMapLatest{ apiService.rx_inqueryCreditCard(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    let creditCardTranSectionTriggerSignal =
      getCreditCardInqueryTriggerSignal
        .elements()
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ data -> CreditCardTransectionRequestModel? in
          if data.count < 1 {
            self.outputs.loading.onNext(false)
            return nil
          }
          guard let price = amount else {
            self.outputs.loading.onNext(false)
            return nil
          }
          return CreditCardTransectionRequestModel(platformID: "test_platform02",
                                                   cardToken: data[0].cardToken,
                                                   cardID: paymentCardID ?? "",
                                                   customerID: paymentUserID ?? "",
                                                   currency: "THB",
                                                   amount: "\(price)",
                                                   transectionType: "NON3DS")}
        .filterNil()
        .flatMapLatest{ apiService.rx_creditCardTransection(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    creditCardTranSectionTriggerSignal
      .elements()
      .subscribe(onNext: { model in
        if model.status != "" {
          self.error.onNext((title: "Error", message: model.status))
        } else {
          self.displayCreditCardTransectionSuccess.onNext(())
        }})
      .disposed(by: disposeBag)
    
    let getTableListTriggerSignal =
      getTableListTrigger
        .map{ GetTableListRequestModel(spoobWalkID: $0.spoonWalkID,
                                       brandID: $0.brandID) }
        .flatMapLatest{ apiService.rx_getTableList(requestModel: $0).materialize() }
        .share(replay: 1)
    
    getTableListTriggerSignal
      .elements()
      .map{ $0.map { DisplayTableData(tableID: $0.tableID,
                                            tableName: $0.tableName,
                                            isEnable: $0.isEnable)}}
      .map { $0.filter{ $0.isEnable == false }}
      .bind(to: displayTableList)
      .disposed(by: disposeBag)
    
    // Error ---------------------------------------------------------------
    
    let errorSignal = Observable
      .merge(getBindingCreditCardTriggerSignal.errors(),
             creditCardTranSectionTriggerSignal.errors(),
             getCreditCardInqueryTriggerSignal.errors(),
             getCreditCardInqueryTriggerSignal.errors(),
             getTableListTriggerSignal.errors())
      .share(replay: 1)
    
    errorSignal
      .map(errorToAlertable)
      .bind(to: error)
      .disposed(by: disposeBag)
  }
  
  // MARK: Private
  
  // MARK: Input
  
  var getPaymenTypeTrigger = PublishSubject<Void>()
  var getBindingCreditCardTrigger = PublishSubject<Void>()
  var getCreditCardInqueryTrigger = PublishSubject<Double>()
  var getTableListTrigger = PublishSubject<(spoonWalkID: String, brandID: Int)>()
    
  // MARK: Output
  
  var displayPaymentType = PublishSubject<[SectionOfDisplayPaymentType]>()
  var displayBindingCreditCard = PublishSubject<String>()
  var displayCreditCardTransectionSuccess = PublishSubject<Void>()
  var error = PublishSubject<(title: String, message: String)>()
  var loading = BehaviorSubject<Bool>(value: false)
  var displayTableList = PublishSubject<[DisplayTableData]>()

  // MARK: Coordinates
  
  var navigateToPayment = PublishSubject<String>()
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: PaymentOrderManagementViewModelOutputs { return self }
  public var inputs: PaymentOrderManagementViewModelInputs { return self }
  public var coordinates: PaymentOrderManagementViewModelCoordinates { return self }
}
