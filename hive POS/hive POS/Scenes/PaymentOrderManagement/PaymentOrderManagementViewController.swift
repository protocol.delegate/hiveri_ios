//
//  PaymentOrderManagementViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 12/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources
import ZSwiftKit

class PaymentOrderManagementViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var printTaxView: UIView!
  @IBOutlet weak var printTaxLabel: UILabel!
  @IBOutlet weak var printTaxButton: UIButton!
  @IBOutlet weak var printBillButton: UIButton!
  @IBOutlet weak var printQuotationButton: UIButton!
  @IBOutlet weak var searchBillButton: UIButton!
  @IBOutlet weak var splitBillButton: UIButton!
  @IBOutlet weak var openDeckButton: UIButton!
  @IBOutlet weak var discountButton: UIButton!
  @IBOutlet weak var findButton: UIButton!
  @IBOutlet weak var memberButton: UIButton!
  @IBOutlet weak var widrawButton: UIButton!
  @IBOutlet weak var billTotalLabel: UILabel!
  @IBOutlet weak var billPriceTotalLabel: UILabel!
  @IBOutlet weak var calBackgroundView: UIView!
  @IBOutlet weak var priceView: UIView!
  @IBOutlet weak var orderView: UIView!
  @IBOutlet weak var orderTextField: UITextField!
  @IBOutlet weak var orderButton: UIButton!
  @IBOutlet weak var customerLabel: UILabel!
  @IBOutlet weak var customerTextField: UITextField!
  @IBOutlet weak var timeLabel: UILabel!
  @IBOutlet weak var oderListLabel: UILabel!
  @IBOutlet weak var orderTableView: UITableView!
  @IBOutlet weak var totalTitleLabel: UILabel!
  @IBOutlet weak var totalUnitLabel: UILabel!
  @IBOutlet weak var totalLabel: UILabel!
  
  @IBOutlet weak var paymentCollectionView: UICollectionView!
  @IBOutlet weak var priceLabel: UILabel!
  @IBOutlet weak var priceTextField: UITextField!
  @IBOutlet weak var priceTotalButton: UIButton!
  
  @IBOutlet weak var pay1000Button: UIButton!
  @IBOutlet weak var pay100Button: UIButton!
  @IBOutlet weak var pay20Button: UIButton!
  @IBOutlet weak var pay5Button: UIButton!
  @IBOutlet weak var pay1Button: UIButton!
  @IBOutlet weak var pay025Button: UIButton!
  
  @IBOutlet weak var pay5000Button: UIButton!
  @IBOutlet weak var pay500Button: UIButton!
  @IBOutlet weak var pay50Button: UIButton!
  @IBOutlet weak var pay10Button: UIButton!
  @IBOutlet weak var pay2Button: UIButton!
  @IBOutlet weak var pay05Button: UIButton!
  
  @IBOutlet weak var paymentButton: UIButton!
  
  @IBOutlet weak var cal7Button: UIButton!
  @IBOutlet weak var cal8Button: UIButton!
  @IBOutlet weak var cal9Button: UIButton!
  @IBOutlet weak var cal4Button: UIButton!
  @IBOutlet weak var cal5Button: UIButton!
  @IBOutlet weak var cal6Button: UIButton!
  @IBOutlet weak var cal1Button: UIButton!
  @IBOutlet weak var cal2Button: UIButton!
  @IBOutlet weak var cal3Button: UIButton!
  @IBOutlet weak var calDotButton: UIButton!
  @IBOutlet weak var cal0Button: UIButton!
  @IBOutlet weak var calDelButton: UIButton!
  
  @IBOutlet weak var calView: UIView!
  @IBOutlet weak var paymentTitleLabel: UILabel!
  
  @IBOutlet weak var calVerticalStackView: UIStackView!
  
  // MARK: Property
  
  var viewModel: PaymentOrderManagementViewModelType!
  let disposeBag = DisposeBag()
  var selectedPayment: PaymentType = .cash
  var timer: Disposable?
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    setupUI()
    setupL10N()
    bindViewModel()
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    
    timer?.dispose()
    timer = Observable<Int>.interval(0.1, scheduler: MainScheduler.instance)
      .subscribe(onNext: { [unowned self] _ in
        let dateFormatter : DateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let date = Date()
        let dateString = dateFormatter.string(from: date)
        self.timeLabel.text = dateString
      })
  }
  
  func bindViewModel() {
    
    let dataSource = RxCollectionViewSectionedReloadDataSource<SectionOfDisplayPaymentType>(
    configureCell: { dataSource, collectionView, indexPath, item in
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PaymentCollectionViewCell.className, for: indexPath) as! PaymentCollectionViewCell
      cell.configure(paymentType: item.paymentType, isSelect: item.paymentType == self.selectedPayment )
      return cell})
    
    viewModel.outputs.displayPaymentType
      .bind(to: paymentCollectionView.rx.items(dataSource: dataSource))
      .disposed(by: disposeBag)
    
    paymentCollectionView.rx
      .modelSelected(DisplayPaymentType.self)
      .subscribe(onNext: { [unowned self] model in
        switch model.paymentType {
        case .cash:
          self.pay1000Button.isUserInteractionEnabled = true
          self.pay100Button.isUserInteractionEnabled = true
          self.pay20Button.isUserInteractionEnabled = true
          self.pay5Button.isUserInteractionEnabled = true
          self.pay1Button.isUserInteractionEnabled = true
          self.pay025Button.isUserInteractionEnabled = true
          self.pay5000Button.isUserInteractionEnabled = true
          self.pay500Button.isUserInteractionEnabled = true
          self.pay50Button.isUserInteractionEnabled = true
          self.pay10Button.isUserInteractionEnabled = true
          self.pay20Button.isUserInteractionEnabled = true
          self.pay05Button.isUserInteractionEnabled = true
          self.calDelButton.isUserInteractionEnabled = true
          self.calDotButton.isUserInteractionEnabled = true
          self.cal0Button.isUserInteractionEnabled = true
          self.cal1Button.isUserInteractionEnabled = true
          self.cal2Button.isUserInteractionEnabled = true
          self.cal3Button.isUserInteractionEnabled = true
          self.cal4Button.isUserInteractionEnabled = true
          self.cal5Button.isUserInteractionEnabled = true
          self.cal6Button.isUserInteractionEnabled = true
          self.cal7Button.isUserInteractionEnabled = true
          self.cal8Button.isUserInteractionEnabled = true
          self.cal9Button.isUserInteractionEnabled = true
        case .creditCard:
          self.pay1000Button.isUserInteractionEnabled = false
          self.pay100Button.isUserInteractionEnabled = false
          self.pay20Button.isUserInteractionEnabled = false
          self.pay5Button.isUserInteractionEnabled = false
          self.pay1Button.isUserInteractionEnabled = false
          self.pay025Button.isUserInteractionEnabled = false
          self.pay5000Button.isUserInteractionEnabled = false
          self.pay500Button.isUserInteractionEnabled = false
          self.pay50Button.isUserInteractionEnabled = false
          self.pay10Button.isUserInteractionEnabled = false
          self.pay20Button.isUserInteractionEnabled = false
          self.pay05Button.isUserInteractionEnabled = false
          self.calDelButton.isUserInteractionEnabled = false
          self.calDotButton.isUserInteractionEnabled = false
          self.cal0Button.isUserInteractionEnabled = false
          self.cal1Button.isUserInteractionEnabled = false
          self.cal2Button.isUserInteractionEnabled = false
          self.cal3Button.isUserInteractionEnabled = false
          self.cal4Button.isUserInteractionEnabled = false
          self.cal5Button.isUserInteractionEnabled = false
          self.cal6Button.isUserInteractionEnabled = false
          self.cal7Button.isUserInteractionEnabled = false
          self.cal8Button.isUserInteractionEnabled = false
          self.cal9Button.isUserInteractionEnabled = false
        }
        self.viewModel.inputs.getPaymenTypeTrigger.onNext(())
        self.selectedPayment = model.paymentType})
      .disposed(by: disposeBag)
    
    viewModel
      .inputs
      .getPaymenTypeTrigger
      .onNext(())
    
    cal0Button.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        guard let text = self.priceTextField.text else { return }
        if text.count > 20 { return }
        if text.contains(".") {
          self.priceTextField.text! = self.priceTextField.text! + "0"
          return
        }
        let price = Double(self.priceTextField.text!) ?? 0
        if price == 0 {
          return
        }
        self.priceTextField.text! = self.priceTextField.text! + "0"})
      .disposed(by: disposeBag)
    
    calDotButton.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      guard let text = self.priceTextField.text else { return }
      if text.contains(".") { return }
      self.priceTextField.text = self.priceTextField.text! + "."})
    .disposed(by: disposeBag)
    
    calDelButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        guard var text = self.priceTextField.text else { return }
        if text.count > 0 {
          text.removeLast()
          if text.count == 0 {
            self.priceTextField.text = "0"
            return
          }
          self.priceTextField.text = text
        }
      })
      .disposed(by: disposeBag)
    
    cal1Button.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        guard let text = self.priceTextField.text else { return }
        if text.count > 20 { return }
        let price = Double(text) ?? 0
        if price == 0 {
          if text.contains(".") {
            self.priceTextField.text = self.priceTextField.text! + "1"
          } else {
            self.priceTextField.text = "1"
          }
        } else {
          self.priceTextField.text = self.priceTextField.text! + "1"
        }
      })
      .disposed(by: disposeBag)
    
    cal2Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      if price == 0 {
        if text.contains(".") {
          self.priceTextField.text = self.priceTextField.text! + "2"
        } else {
          self.priceTextField.text = "2"
        }
      } else {
        self.priceTextField.text = self.priceTextField.text! + "2"
      }
    })
    .disposed(by: disposeBag)
    
    cal3Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      if price == 0 {
        if text.contains(".") {
          self.priceTextField.text = self.priceTextField.text! + "3"
        } else {
          self.priceTextField.text = "3"
        }
      } else {
        self.priceTextField.text = self.priceTextField.text! + "3"
      }
    })
    .disposed(by: disposeBag)
    
    cal4Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      if price == 0 {
        if text.contains(".") {
          self.priceTextField.text = self.priceTextField.text! + "4"
        } else {
          self.priceTextField.text = "4"
        }
      } else {
        self.priceTextField.text = self.priceTextField.text! + "4"
      }
    })
    .disposed(by: disposeBag)
    
    cal5Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      if price == 0 {
        if text.contains(".") {
          self.priceTextField.text = self.priceTextField.text! + "5"
        } else {
          self.priceTextField.text = "5"
        }
      } else {
        self.priceTextField.text = self.priceTextField.text! + "5"
      }
    })
    .disposed(by: disposeBag)
    
    cal6Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      if price == 0 {
        if text.contains(".") {
          self.priceTextField.text = self.priceTextField.text! + "6"
        } else {
          self.priceTextField.text = "6"
        }
      } else {
        self.priceTextField.text = self.priceTextField.text! + "6"
      }
    })
    .disposed(by: disposeBag)
    
    cal7Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      if price == 0 {
        if text.contains(".") {
          self.priceTextField.text = self.priceTextField.text! + "7"
        } else {
          self.priceTextField.text = "7"
        }
      } else {
        self.priceTextField.text = self.priceTextField.text! + "7"
      }
    })
    .disposed(by: disposeBag)
    
    cal8Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      if price == 0 {
        if text.contains(".") {
          self.priceTextField.text = self.priceTextField.text! + "8"
        } else {
          self.priceTextField.text = "8"
        }
      } else {
        self.priceTextField.text = self.priceTextField.text! + "8"
      }
    })
    .disposed(by: disposeBag)
    
    cal9Button.rx.tap
    .subscribe(onNext: { [unowned self] _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      if price == 0 {
        if text.contains(".") {
          self.priceTextField.text = self.priceTextField.text! + "9"
        } else {
          self.priceTextField.text = "9"
        }
      } else {
        self.priceTextField.text = self.priceTextField.text! + "9"
      }
    })
    .disposed(by: disposeBag)
    
    pay025Button.rx.tap.subscribe(onNext: { _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      self.priceTextField.text = "\(price + 0.25)"})
      .disposed(by: disposeBag)
    
    pay1Button.rx.tap.subscribe(onNext: { _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      self.priceTextField.text = "\(price + 1)"})
      .disposed(by: disposeBag)
    
    pay5Button.rx.tap.subscribe(onNext: { _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      self.priceTextField.text = "\(price + 5)"
    }).disposed(by: disposeBag)
    
    pay20Button.rx.tap.subscribe(onNext: { _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      self.priceTextField.text = "\(price + 20)"
    }).disposed(by: disposeBag)
    
    pay100Button.rx.tap.subscribe(onNext: { _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      self.priceTextField.text = "\(price + 100)"
    }).disposed(by: disposeBag)
    
    pay1000Button.rx.tap.subscribe(onNext: { _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      self.priceTextField.text = "\(price + 1000)"
    }).disposed(by: disposeBag)
    
    pay05Button.rx.tap.subscribe(onNext: { _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      self.priceTextField.text = "\(price + 0.50)"
    }).disposed(by: disposeBag)
    
    pay2Button.rx.tap.subscribe(onNext: { _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      self.priceTextField.text = "\(price + 2)"
    }).disposed(by: disposeBag)
    
    pay10Button.rx.tap.subscribe(onNext: { _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      self.priceTextField.text = "\(price + 10)"
    }).disposed(by: disposeBag)
    
    pay50Button.rx.tap.subscribe(onNext: { _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      self.priceTextField.text = "\(price + 50)"
    }).disposed(by: disposeBag)

    pay500Button.rx.tap.subscribe(onNext: { _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      self.priceTextField.text = "\(price + 500)"})
      .disposed(by: disposeBag)
    
    pay5000Button.rx.tap.subscribe(onNext: { _ in
      guard let text = self.priceTextField.text else { return }
      if text.count > 20 { return }
      let price = Double(text) ?? 0
      self.priceTextField.text = "\(price + 5000)"})
    .disposed(by: disposeBag)
    
    paymentButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        switch self.selectedPayment {
        case .cash:
          break;
        case .creditCard:
          self.viewModel.inputs.getBindingCreditCardTrigger.onNext(())
        }
      })
      .disposed(by: disposeBag)
    
    viewModel.outputs
      .displayCreditCardTransectionSuccess
      .subscribe(onNext: { _ in
        print("Payment Success")})
      .disposed(by: disposeBag)
    
    //MARK: Bind loading
    
    viewModel.outputs.loading
      .subscribe(onNext: { [unowned self] isLoading in
        LoadingActivityView.action(in: self.view, isShow: isLoading)})
      .disposed(by: disposeBag)
    
    // MARK: Bind Error
          
    viewModel.error
      .subscribe(onNext: { [unowned self] error in
          Helper.Alert(
              view: self,
              title: error.title,
              message: error.message,
              confirmText: "done".localized())})
      .disposed(by: disposeBag)
  }
  
  func setupUI() {
  
    printTaxView.layer.cornerRadius = 10
    printTaxView.layer.applySketchShadow(color: .black,
                                           alpha: 0.16,
                                           x: 0,
                                           y: 2,
                                           blur: 6,
                                           spread: 0)
    printBillButton.layer.cornerRadius = 10
    printBillButton.layer.applySketchShadow(color: .black,
                                            alpha: 0.16,
                                            x: 0,
                                            y: 2,
                                            blur: 6,
                                            spread: 0)
    printQuotationButton.layer.cornerRadius = 10
    printQuotationButton.layer.applySketchShadow(color: .black,
                                                 alpha: 0.16,
                                                 x: 0,
                                                 y: 2,
                                                 blur: 6,
                                                 spread: 0)
    searchBillButton.layer.cornerRadius = 10
    searchBillButton.layer.applySketchShadow(color: .black,
                                             alpha: 0.16,
                                             x: 0,
                                             y: 2,
                                             blur: 6,
                                             spread: 0)
    splitBillButton.layer.cornerRadius = 10
    splitBillButton.layer.applySketchShadow(color: .black,
                                            alpha: 0.16,
                                            x: 0,
                                            y: 2,
                                            blur: 6,
                                            spread: 0)
    openDeckButton.layer.cornerRadius = 10
    openDeckButton.layer.applySketchShadow(color: .black,
                                           alpha: 0.16,
                                           x: 0,
                                           y: 2,
                                           blur: 6,
                                           spread: 0)
    discountButton.layer.cornerRadius = 10
    discountButton.layer.applySketchShadow(color: .black,
                                           alpha: 0.16,
                                           x: 0,
                                           y: 2,
                                           blur: 6,
                                           spread: 0)
    findButton.layer.cornerRadius = 10
    findButton.layer.applySketchShadow(color: .black,
                                       alpha: 0.16,
                                       x: 0,
                                       y: 2,
                                       blur: 6,
                                       spread: 0)
    memberButton.layer.cornerRadius = 10
    memberButton.layer.applySketchShadow(color: .black,
                                         alpha: 0.16,
                                         x: 0,
                                         y: 2,
                                         blur: 6,
                                         spread: 0)
    widrawButton.layer.cornerRadius = 10
    widrawButton.layer.applySketchShadow(color: .black,
                                         alpha: 0.16,
                                         x: 0,
                                         y: 2,
                                         blur: 6,
                                         spread: 0)
    
    orderTextField.layer.cornerRadius = 10
    orderTextField.layer.borderWidth = 1
    orderTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    orderTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: orderTextField.frame.height))
    orderTextField.leftViewMode = .always
    orderTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: orderTextField.frame.height))
    orderTextField.rightViewMode = .always
    
    customerTextField.layer.cornerRadius = 10
    customerTextField.layer.borderWidth = 1
    customerTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    customerTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: customerTextField.frame.height))
    customerTextField.leftViewMode = .always
    customerTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: customerTextField.frame.height))
    customerTextField.rightViewMode = .always
    
    priceTextField.layer.cornerRadius = 10
    priceTextField.layer.borderWidth = 0
    priceTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    priceTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: priceTextField.frame.height))
    priceTextField.leftViewMode = .always
    priceTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: priceTextField.frame.height))
    priceTextField.rightViewMode = .always
    
    pay1000Button.layer.cornerRadius = 10
    pay1000Button.layer.borderWidth = 1
    pay1000Button.layer.borderColor = UIColor(hex: 0xD7DAE2).cgColor
    
    pay100Button.layer.cornerRadius = 10
    pay100Button.layer.borderWidth = 1
    pay100Button.layer.borderColor = UIColor(hex: 0xD7DAE2).cgColor
    
    pay20Button.layer.cornerRadius = 10
    pay20Button.layer.borderWidth = 1
    pay20Button.layer.borderColor = UIColor(hex: 0xD7DAE2).cgColor
    
    pay5Button.layer.cornerRadius = 10
    pay5Button.layer.borderWidth = 1
    pay5Button.layer.borderColor = UIColor(hex: 0xD7DAE2).cgColor
    
    pay1Button.layer.cornerRadius = 10
    pay1Button.layer.borderWidth = 1
    pay1Button.layer.borderColor = UIColor(hex: 0xD7DAE2).cgColor
    
    pay025Button.layer.cornerRadius = 10
    pay025Button.layer.borderWidth = 1
    pay025Button.layer.borderColor = UIColor(hex: 0xD7DAE2).cgColor
    
    pay5000Button.layer.cornerRadius = 10
    pay5000Button.layer.borderWidth = 1
    pay5000Button.layer.borderColor = UIColor(hex: 0xD7DAE2).cgColor
    
    pay500Button.layer.cornerRadius = 10
    pay500Button.layer.borderWidth = 1
    pay500Button.layer.borderColor = UIColor(hex: 0xD7DAE2).cgColor
    
    pay50Button.layer.cornerRadius = 10
    pay50Button.layer.borderWidth = 1
    pay50Button.layer.borderColor = UIColor(hex: 0xD7DAE2).cgColor
    
    pay10Button.layer.cornerRadius = 10
    pay10Button.layer.borderWidth = 1
    pay10Button.layer.borderColor = UIColor(hex: 0xD7DAE2).cgColor
    
    pay2Button.layer.cornerRadius = 10
    pay2Button.layer.borderWidth = 1
    pay2Button.layer.borderColor = UIColor(hex: 0xD7DAE2).cgColor
    
    pay05Button.layer.cornerRadius = 10
    pay05Button.layer.borderWidth = 1
    pay05Button.layer.borderColor = UIColor(hex: 0xD7DAE2).cgColor
    
    priceTotalButton.layer.cornerRadius = priceTotalButton.frame.height / 2
    paymentButton.layer.cornerRadius = paymentButton.frame.height / 2
    
    calView.layer.cornerRadius = 10
    calView.layer.masksToBounds = true
    calBackgroundView.layer.cornerRadius = 10
    calBackgroundView.layer.masksToBounds = true
    
    orderView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
    priceView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
    
    if let collectionViewLayout = paymentCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      collectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      collectionViewLayout.scrollDirection = .horizontal
    }
    
    paymentCollectionView.register(UINib(nibName: PaymentCollectionViewCell.className, bundle: nil),
                                   forCellWithReuseIdentifier: PaymentCollectionViewCell.className)
  }
  
  func setupL10N() {
    self.setNavigationBarItem(titlePageName: "payment_title".localized())
    customerLabel.text = "payment_customer".localized()
    oderListLabel.text = "payment_foodlist".localized() + "(0)"
    totalTitleLabel.text = "payment_total".localized()
    totalLabel.text = "0"
    totalUnitLabel.text = "payment_total_unit".localized()
    paymentTitleLabel.text = "payment_type".localized()
    priceLabel.text = "payment_price".localized()
    priceTextField.text = "0"
    priceTotalButton.setTitle("payment_price_total".localized(), for: .normal)
    paymentButton.setTitle("payment_pay".localized(), for: .normal)
    billTotalLabel.text = "0"
    billPriceTotalLabel.text = "payment_sum".localized()
    printTaxLabel.text = "payment_tax_title".localized()
    printBillButton.setTitle("payment_bill_title".localized(), for: .normal)
    printQuotationButton.setTitle("payment_quotation_title".localized(), for: .normal)
    searchBillButton.setTitle("payment_searchbill_title".localized(), for: .normal)
    splitBillButton.setTitle("payment_splitbill_title".localized(), for: .normal)
    openDeckButton.setTitle("payment_opendeck_title".localized(), for: .normal)
    discountButton.setTitle("payment_discount_title".localized(), for: .normal)
    findButton.setTitle("payment_fine_title".localized(), for: .normal)
    memberButton.setTitle("payment_member_title".localized(), for: .normal)
    widrawButton.setTitle("payment_widraw_title".localized(), for: .normal)
  }
}

extension PaymentOrderManagementViewController: PaymentCreditCardViewControllerDelegate {
  func didCardBindingSuccess(refCode: String) {
    print("cardToken------> ", refCode)
    viewModel.inputs.getCreditCardInqueryTrigger.onNext(100)
  }
}
