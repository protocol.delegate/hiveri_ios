//
//  PaymentCollectionViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 14/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

class PaymentCollectionViewCell: UICollectionViewCell {
    
  @IBOutlet weak var paymentView: UIView!
  @IBOutlet weak var imageView: UIImageView!
  @IBOutlet weak var paymentLabel: UILabel!
  
  func configure(paymentType: PaymentType, isSelect: Bool) {
    switch paymentType {
    case .cash:
      imageView.image = UIImage(named: "ic_payment_cash")
      paymentLabel.text = "payment_cash".localized()
    case .creditCard:
      imageView.image = UIImage(named: "ic_payment_creditcard")
      paymentLabel.text = "payment_creditcard".localized()
    }
    
    paymentView.layer.cornerRadius = 10
    paymentView.layer.borderWidth = 1
    paymentView.layer.borderColor = isSelect ? UIColor(hex: 0x39D639).cgColor : UIColor.clear.cgColor
    paymentView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 2, blur: 6, spread: 0)
  }
}
