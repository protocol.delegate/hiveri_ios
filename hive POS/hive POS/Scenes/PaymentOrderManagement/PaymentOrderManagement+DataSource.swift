//
//  PaymentOrderManagement.swift
//  hive POS
//
//  Created by Space-C4ts on 14/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import RxDataSources

enum PaymentType {
  case cash
  case creditCard
}

struct DisplayPaymentType {
  var paymentType: PaymentType
}

struct SectionOfDisplayPaymentType {
  var header: String
  var items: [Item]
}

extension SectionOfDisplayPaymentType: SectionModelType {
  typealias Item = DisplayPaymentType
  
  init(original: SectionOfDisplayPaymentType, items: [Item]) {
    self = original
    self.items = items
  }
}
