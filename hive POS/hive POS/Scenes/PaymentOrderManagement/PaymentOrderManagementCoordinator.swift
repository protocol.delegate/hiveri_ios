//
//  PaymentOrderManagementCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 12/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol PaymentOrderManagementInputsParamsType {
    
}

class PaymentOrderManagementCoordinator: BaseCoordinator<Void>, PaymentOrderManagementInputsParamsType {
  
  // MARK: Property

  // MARK: Public

  override func start() -> Observable<Void> { 
    let window = self.window
    let viewModel = PaymentOrderManagementViewModel()
    let viewController = PaymentOrderManagementViewController.initFromStoryboard(name: Storyboard.order.identifier)
    viewController.viewModel = viewModel
    
    viewModel
      .coordinates
      .navigateToPayment
      .map{ path -> PaymentCreditCardCoordinator in
        let coordinator = PaymentCreditCardCoordinator(window: window,
                                                       baseViewController: viewController,
                                                       transitionType: .modal,
                                                       animated: true)
        coordinator.deepLink = path
        coordinator.viewController.delegate = viewController.self
        return coordinator}
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
    transition(to: viewController)
    return .never()
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: PaymentOrderManagementInputsParamsType { return self }
}
