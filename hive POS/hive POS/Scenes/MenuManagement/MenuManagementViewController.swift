//
//  MenuManagementViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 10/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources
import ZSwiftKit

class MenuManagementViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var deleteCategoryButton: UIButton!
  @IBOutlet weak var menuView: UIView!
  @IBOutlet weak var allMenuLabel: UILabel!
  @IBOutlet weak var allMenuUnitLabel: UILabel!
  @IBOutlet weak var categoryCollectionView: UICollectionView!
  @IBOutlet weak var menuCollectionView: UICollectionView!
  @IBOutlet weak var menuViewCollectionView: UIView!
  @IBOutlet weak var categoryEditButton: UIButton!
  @IBOutlet weak var createMenuButton: UIButton!
  // MARK: Property
  
  var viewModel: MenuManagementViewModelType!
  let disposeBag = DisposeBag()
  var selectCategoryID = ""
  var categoryData: [DisplayMenuCategoryData]?
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
  
    setupL10N()
    setupUI()
    bindViewModel()
    
  }
  
  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    self.setNavigationBarItem(titlePageName: "menumanagement_title".localized())
    if let branchData = BranchSignService.shared.getBranchData() {
      viewModel.inputs.getMenuCategoriesTrigger.onNext(branchData.spoonWalkID)
      if self.selectCategoryID != "" {
        viewModel.inputs.getMenuListTrigger.onNext(self.selectCategoryID)
      }
    }
    
  }
  
  func bindViewModel() {
    
    createMenuButton.rx.tap
      .filter{ [unowned self] in self.selectCategoryID != "" }
      .map{[unowned self] in self.selectCategoryID}
      .bind(to: viewModel.coordinates.navigateToCreateMenu)
      .disposed(by: disposeBag)
    
    let categoryDataSource = RxCollectionViewSectionedReloadDataSource<SectionOfDisplayMenuCategoryDataType>(
    configureCell: { [unowned self] dataSource, collectionView, indexPath, item in
      switch dataSource[indexPath] {
      case .categoryData(let items):
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuCategoryCollectionViewCell.className, for: indexPath) as! MenuCategoryCollectionViewCell
        cell.configure(categoryData: items, isSelect: items.categoryID == self.selectCategoryID)
        cell.delegate = self
        return cell
      case .create:
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuCategoryAddCollectionViewCell.className, for: indexPath) as! MenuCategoryAddCollectionViewCell
        cell.delegate = self
        cell.configure()
        return cell
      }
    })
    
    viewModel
    .outputs
    .displsyMenuCatagoriesData
    .subscribe(onNext: { [unowned self] model in
      if model.count == 0 {
        self.categoryEditButton.isUserInteractionEnabled = false
        self.deleteCategoryButton.isUserInteractionEnabled = false
      } else {
        self.categoryEditButton.isUserInteractionEnabled = true
        self.deleteCategoryButton.isUserInteractionEnabled = true
      }
      
      if self.selectCategoryID == "" && model.count > 0 {
        self.selectCategoryID = model[0].categoryID
        self.categoryData = model
        self.viewModel.inputs.getMenuListTrigger.onNext(self.selectCategoryID)
      }
      self.viewModel.inputs.bindCategoryDataTrigger.onNext(model)
    })
    .disposed(by: disposeBag)
    
    self.viewModel
    .outputs
    .displayMenuCategories
    .bind(to: self.categoryCollectionView.rx.items(dataSource: categoryDataSource))
    .disposed(by: self.disposeBag)
    
    viewModel.outputs
      .displayCreateCategorySuccess
      .subscribe(onNext: {[unowned self] _ in
        if let branchData = BranchSignService.shared.getBranchData() {
          self.viewModel.inputs.getMenuCategoriesTrigger.onNext(branchData.spoonWalkID)
        }})
      .disposed(by: disposeBag)
    
    categoryEditButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        if self.selectCategoryID == "" { return }
        guard let categoryData = self.categoryData else { return }
        guard let filterCategory = categoryData.filter({ $0.categoryID ==  self.selectCategoryID}).first else { return }
        Helper.AlertInput(view: self,
                          title: "menumanagement_edit_category_title".localized(),
                          message: "",
                          confirmText: "ok".localized(),
                          cancelText: "cancel".localized(),
                          isInverseButtonPos: true,
                          inputPlaceholder: "menumanagement_edit_category_title".localized(),
                          inputText: filterCategory.categoryName,
                          confirmCompletion: { [unowned self] (_, text) in
                            if text != filterCategory.categoryName{
                              self.viewModel
                                .inputs
                                .editCategoryTrigger
                                .onNext((categoryID: self.selectCategoryID,
                                         categoryName: text))
                            }
        },
                          cancelCompletion: nil)
      })
      .disposed(by: disposeBag)
    
    viewModel.outputs
      .displayEditCategorySuccess
      .subscribe(onNext: { [unowned self] _ in
        if let branchData = BranchSignService.shared.getBranchData() {
          self.viewModel.inputs.getMenuCategoriesTrigger.onNext(branchData.spoonWalkID)
        }
      })
      .disposed(by: disposeBag)
    
    deleteCategoryButton.rx.tap
      .filter{ self.selectCategoryID != "" }
      .subscribe(onNext: { [unowned self] _ in
        Helper.AlertConfirm(view: self, title: "menumanagement_delete_category_title".localized(),
                            message: "menumanagement_delete_category_message".localized(),
                            confirmText: "ok".localized(),
                            cancelText: "cancel".localized(), isInverseButtonPos: true,
                            confirmCompletion: { [unowned self] (_) in
                              let categoryID = self.selectCategoryID
                              self.selectCategoryID = ""
                              self.viewModel.inputs.deleteCategoryTrigger.onNext(categoryID)
        },
                            cancelCompletion: nil)
      })
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayDeleteCategorySuccess
      .subscribe(onNext: { [unowned self] _ in
        Helper.Alert(view: self,
                     title: "menumanagement_delete_success_title".localized(),
                     message: "menumanagement_delete_success_message".localized(),
                     confirmText: "ok".localized()) { (_) in
          if let branchData = BranchSignService.shared.getBranchData() {
            self.viewModel.inputs.getMenuCategoriesTrigger.onNext(branchData.spoonWalkID)
          }
        }
      })
      .disposed(by: disposeBag)
    
    let menuDataSource = RxCollectionViewSectionedReloadDataSource<SectionOfDisplayMenuData>(
    configureCell: { dataSource, collectionView, indexPath, item in
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MenuCollectionViewCell.className, for: indexPath) as! MenuCollectionViewCell
      cell.configure(name: item.nameTH, price: item.price, imagePath: item.imagePath)
      return cell})
    
    viewModel.outputs.displayMenuData
      .bind(to: menuCollectionView.rx.items(dataSource: menuDataSource))
      .disposed(by: disposeBag)
    
    menuCollectionView.rx.modelSelected(DisplayMenuData.self)
      .subscribe(onNext: { [unowned self] model in
        self.viewModel.coordinates.navigateToMenuDetail.onNext((categoryID: model.categoryID,
                                                                menuData: model))})
      .disposed(by: disposeBag)
    
    viewModel.outputs.displayMenuModel
      .subscribe(onNext: { [unowned self] model in
        self.allMenuUnitLabel.text = "\(model.count) "+"menumanagement_all_unit".localized()})
      .disposed(by: disposeBag)
    
    //MARK: Bind loading
       
    viewModel.outputs.loading
      .subscribe(onNext: { [unowned self] isLoading in
        guard let tabBarController = self.tabBarController else { return }
        LoadingActivityView.action(in: tabBarController.view, isShow: isLoading)})
      .disposed(by: disposeBag)
    
    // MARK: Bind Error
          
    viewModel.error
      .subscribe(onNext: { [unowned self] error in
          Helper.Alert(
            view: self,
              title: error.title,
              message: error.message,
              confirmText: "done".localized())})
      .disposed(by: disposeBag)
  }
  
  func setupUI() {
    if let collectionViewLayout = categoryCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      collectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      collectionViewLayout.scrollDirection = .horizontal
    }
    menuViewCollectionView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
    menuView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: -2, blur: 6, spread: 0)
    
    menuCollectionView.register(UINib(nibName: MenuCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: MenuCollectionViewCell.className)
  }
  
  func setupL10N() {
    
    allMenuLabel.text = "menumanagement_all_title".localized()
    allMenuUnitLabel.text = "0 "+"menumanagement_all_unit".localized()
    deleteCategoryButton.setTitle("menumanagement_delete_category_title".localized(), for: .normal)
    categoryEditButton.setTitle("menumanagement_edit".localized(), for: .normal)
  }
}

extension MenuManagementViewController: MenuCategoryAddCollectionViewCellDelegate {
  func didClickOnCreateMenuCategory() {
    Helper.AlertInput(view: self, title: "menumanagement_create_category_title".localized(), message: "", confirmText: "ok".localized(), cancelText: "cancel".localized(), isInverseButtonPos: true,
                      inputPlaceholder: "menumanagement_create_category_title".localized(),
                      inputText: "", confirmCompletion: { (_, text) in
                        if text.trimmingCharacters(in: .whitespacesAndNewlines) != "" {
                          self.viewModel.inputs.createMenuCategoryTrigger.onNext(text)
                        } else {
                          Helper.Alert(view: self,
                                       title: "menumanagement_validate_category_title".localized(),
                                       message: "menumanagement_validate_category_message".localized(),
                                       confirmText: "ok".localized())
                        }
    }, cancelCompletion: nil)
  }
}

extension MenuManagementViewController: MenuCategoryCollectionViewCellDelegate {
  func didSelectCategory(categoryID: String) {
    if let branchData = BranchSignService.shared.getBranchData() {
      self.selectCategoryID = categoryID
      viewModel.inputs.getMenuCategoriesTrigger.onNext(branchData.spoonWalkID)
      self.viewModel.inputs.getMenuListTrigger.onNext(self.selectCategoryID)
    }
  }
}
