//
//  MenuManagement+DataSource.swift.swift
//  hive POS
//
//  Created by Space-C4ts on 10/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import RxDataSources

struct DisplayMenuCategoryData {
  var categoryID: String
  var categoryName: String
}

enum DisplayMenuCategoryDataType {
  case categoryData(items: DisplayMenuCategoryData)
  case create
}

enum SectionOfDisplayMenuCategoryDataType {
  case Section(title: String, items: [DisplayMenuCategoryDataType])
}

extension SectionOfDisplayMenuCategoryDataType: SectionModelType {
  typealias Item = DisplayMenuCategoryDataType
  
  var items: [DisplayMenuCategoryDataType] {
    switch self {
    case .Section(_, items: let items):
      return items.map { $0 }
    }
  }
  
  init(original: SectionOfDisplayMenuCategoryDataType, items: [Item]) {
    switch original {
    case .Section(title: let title, _):
      self = .Section(title: title, items: items)
    }
  }
}

struct DisplayMenuData {
  var menuID: String
  var brandID: Int
  var branchID: Int
  var menuNumber: String
  var categoryID: String
  var imagePath: String
  var nameTH: String
  var nameEN: String
  var detailTH: String
  var detailEN: String
  var price: Double
  var priceType: Int
  var priceDetail: String
  var menuOption: [Int]
  var menuType: [Int]
  var timeToCook: Int
  var isPublish: Bool
}

struct SectionOfDisplayMenuData {
  var header: String
  var items: [Item]
}

extension SectionOfDisplayMenuData: SectionModelType {
  typealias Item = DisplayMenuData
  
  init(original: SectionOfDisplayMenuData, items: [Item]) {
    self = original
    self.items = items
  }
}


struct CreateMenuData {
  var brandID: Int
  var branchID: Int
  var categoryID: String
  var imagePath: String
  var nameTH: String
  var nameEN: String
  var detailTH: String
  var detailEN: String
  var price: Double
  var priceType: Int?
  var priceDetail: String
  var menuOption: Int?
  var menuType: Int?
  var timeToCook: Int
  var isPublish: Bool
}
