//
//  MenuManagementCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 10/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol MenuManagementInputsParamsType {
    
}

class MenuManagementCoordinator: BaseCoordinator<Void>, MenuManagementInputsParamsType {
  
  // MARK: Property

  // MARK: Public

  override func start() -> Observable<Void> {
    let window = self.window
    let viewModel = MenuManagementViewModel()
    let viewController = MenuManagementViewController.initFromStoryboard(name: Storyboard.menu.identifier)
    viewController.viewModel = viewModel
    
    viewModel
    .coordinates
    .navigateToCreateMenu
    .map{ categoryID -> MenuCoordinator in
      let coordinator = MenuCoordinator(window: window,
                                        baseViewController: viewController,
                                        transitionType: .push,
                                        animated: true)
      coordinator.categoryID = categoryID
      return coordinator
    }
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
    viewModel
    .coordinates
    .navigateToMenuDetail
    .map{ item -> MenuCoordinator in
      let coordinator = MenuCoordinator(window: window,
                                        baseViewController: viewController,
                                        transitionType: .push,
                                        animated: true)
      coordinator.categoryID = item.categoryID
      coordinator.menuData = item.menuData
      return coordinator
    }
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
    transition(to: viewController)
    return .never()
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: MenuManagementInputsParamsType { return self }
}
