//
//  MenuManagementViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 10/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa

protocol MenuManagementViewModelInputs {
  var getMenuCategoriesTrigger: PublishSubject<String> { get }
  var createMenuCategoryTrigger: PublishSubject<String> { get }
  var editCategoryTrigger: PublishSubject<(categoryID: String, categoryName: String)> { get }
  var deleteCategoryTrigger: PublishSubject<String> { get }
  var getMenuListTrigger: PublishSubject<String> { get }
  var bindCategoryDataTrigger: PublishSubject<[DisplayMenuCategoryData]> { get }
}

protocol MenuManagementViewModelOutputs {
  var displayMenuCategories: PublishSubject<[SectionOfDisplayMenuCategoryDataType]> { get }
  var displsyMenuCatagoriesData: PublishSubject<[DisplayMenuCategoryData]> { get }
  var displayCreateCategorySuccess: PublishSubject<Void> { get }
  var displayEditCategorySuccess: PublishSubject<Void> { get }
  var displayDeleteCategorySuccess: PublishSubject<Void> { get }
  var displayMenuData: PublishSubject<[SectionOfDisplayMenuData]> { get }
  var displayMenuModel: PublishSubject<[DisplayMenuData]> { get }
  var loading: BehaviorSubject<Bool> { get }
}

protocol MenuManagementViewModelCoordinates {
  var navigateToCreateMenu: PublishSubject<String> { get }
  var navigateToMenuDetail: PublishSubject<(categoryID: String, menuData: DisplayMenuData)> { get }
}

protocol MenuManagementViewModelType: ErrorAlertableViewModelType {
  var inputs: MenuManagementViewModelInputs { get }
  var outputs: MenuManagementViewModelOutputs { get }
  var coordinates: MenuManagementViewModelCoordinates { get }
}

class MenuManagementViewModel: MenuManagementViewModelType, MenuManagementViewModelInputs, MenuManagementViewModelOutputs, MenuManagementViewModelCoordinates {
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  
  // MARK: Init
    
  public init(apiService: APIServiceType = APIService()) {
    
    let getMenuCategoriesTriggerSignal =
      getMenuCategoriesTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ GetMenuCategoryRequestModel(shopID: $0) }
        .flatMapLatest{ apiService.rx_getMenuCategory(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    bindCategoryDataTrigger
      .map{ model -> [SectionOfDisplayMenuCategoryDataType] in
        let menuData = model.map{ DisplayMenuCategoryDataType
          .categoryData(items: DisplayMenuCategoryData(categoryID: $0.categoryID,
                                                       categoryName: $0.categoryName)) }
        var sectionData:[SectionOfDisplayMenuCategoryDataType] = []
        sectionData.append(SectionOfDisplayMenuCategoryDataType.Section(title: "", items: menuData))
        sectionData.append(SectionOfDisplayMenuCategoryDataType.Section(title: "", items: [.create]))
        return sectionData }
      .bind(to: displayMenuCategories)
      .disposed(by: disposeBag)
    
    getMenuCategoriesTriggerSignal
      .elements()
      .map{ $0.map{ DisplayMenuCategoryData(categoryID: $0.categoryID, categoryName: $0.categoryName) } }
      .bind(to: displsyMenuCatagoriesData)
      .disposed(by: disposeBag)
    
    let createMenuCategoryTriggerSignal =
      createMenuCategoryTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ name -> CreateMenuCategoryRequestModel? in
          guard let brandData = LocalDataService.getSelectBrand() else {
            self.outputs.loading.onNext(false)
            return nil
          }
          guard let branchData = BranchSignService.shared.getBranchData() else {
            self.outputs.loading.onNext(false)
            return nil
          }
          
          return CreateMenuCategoryRequestModel(brandID: brandData.ID,
                                                branchID: branchData.ID,
                                                shopID: branchData.spoonWalkID,
                                                categoryName: name) }
        .filterNil()
        .flatMapLatest{ apiService.rx_createMenuCategory(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    createMenuCategoryTriggerSignal
      .elements()
      .map{ _ in () }
      .bind(to: displayCreateCategorySuccess)
      .disposed(by: disposeBag)
    
    let editCategoryTriggerSignal =
      editCategoryTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ UpdateCategoryRequestModel(categoryID: $0.categoryID,
                                         categoryName: $0.categoryName) }
        .flatMapLatest{ apiService.rx_updateMenuCategory(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    editCategoryTriggerSignal.elements()
      .bind(to: displayEditCategorySuccess)
      .disposed(by: disposeBag)
    
    let deleteCategoryTriggerSignal =
      deleteCategoryTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ DeleteCategoryRequestModel(categoryID: $0) }
        .flatMapLatest{ apiService.rx_deleteCategory(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    deleteCategoryTriggerSignal
      .elements()
      .bind(to: displayDeleteCategorySuccess)
      .disposed(by: disposeBag)
    
    let getMenuListTriggerSignal =
      getMenuListTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{GetMenuListRequestModel(categoryID: $0)}
        .flatMapLatest{ apiService.rx_getMenuList(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    getMenuListTriggerSignal
      .elements()
      .map{ model -> [SectionOfDisplayMenuData] in
        let menuData = model.map{ DisplayMenuData(menuID: $0.menuID,
                                                  brandID: $0.brandID,
                                                  branchID: $0.branchID,
                                                  menuNumber: $0.menuNO,
                                                  categoryID: $0.categoryID,
                                                  imagePath: $0.imagePath,
                                                  nameTH: $0.nameTH,
                                                  nameEN: $0.nameEN,
                                                  detailTH: $0.detailTH,
                                                  detailEN: $0.detailEN,
                                                  price: $0.price,
                                                  priceType: $0.priceType,
                                                  priceDetail: "\($0.priceDetail)",
                                                  menuOption: $0.menuOption,
                                                  menuType: $0.menuType,
                                                  timeToCook: $0.timeToCook,
                                                  isPublish: $0.isPublish) }
        return [SectionOfDisplayMenuData(header: "", items: menuData)]}
      .bind(to: displayMenuData).disposed(by: disposeBag)
    
    getMenuListTriggerSignal
      .elements()
      .map { model in
        return model.map{ DisplayMenuData(menuID: $0.menuID,
                                          brandID: $0.brandID,
                                          branchID: $0.branchID,
                                          menuNumber: $0.menuNO,
                                          categoryID: $0.categoryID,
                                          imagePath: $0.imagePath,
                                          nameTH: $0.nameTH,
                                          nameEN: $0.nameEN,
                                          detailTH: $0.detailTH,
                                          detailEN: $0.detailEN,
                                          price: $0.price,
                                          priceType: $0.priceType,
                                          priceDetail: "\($0.priceDetail)",
                                          menuOption: $0.menuOption,
                                          menuType: $0.menuType,
                                          timeToCook: $0.timeToCook,
                                          isPublish: $0.isPublish) }}
      .bind(to: displayMenuModel)
      .disposed(by: disposeBag)
    
    // Error ---------------------------------------------------------------
    
    let errorSignal = Observable
      .merge(getMenuCategoriesTriggerSignal.errors(),
             createMenuCategoryTriggerSignal.errors(),
             editCategoryTriggerSignal.errors(),
             deleteCategoryTriggerSignal.errors(),
             getMenuListTriggerSignal.errors())
      .share(replay: 1)
    
    errorSignal
      .map(errorToAlertable)
      .bind(to: error)
      .disposed(by: disposeBag)
  }
  
  // MARK: Private
  
  // MARK: Input
  
  var getMenuCategoriesTrigger = PublishSubject<String>()
  var createMenuCategoryTrigger = PublishSubject<String>()
  var editCategoryTrigger = PublishSubject<(categoryID: String, categoryName: String)>()
  var deleteCategoryTrigger = PublishSubject<String>()
  var getMenuListTrigger = PublishSubject<String>()
  var bindCategoryDataTrigger = PublishSubject<[DisplayMenuCategoryData]>()
  
  // MARK: Output
  
  var displayMenuCategories = PublishSubject<[SectionOfDisplayMenuCategoryDataType]>()
  var error = PublishSubject<(title: String, message: String)>()
  var loading = BehaviorSubject<Bool>(value: false)
  var displsyMenuCatagoriesData = PublishSubject<[DisplayMenuCategoryData]>()
  var displayCreateCategorySuccess = PublishSubject<Void>()
  var displayEditCategorySuccess = PublishSubject<Void>()
  var displayDeleteCategorySuccess = PublishSubject<Void>()
  var displayMenuData = PublishSubject<[SectionOfDisplayMenuData]>()
  var displayMenuModel = PublishSubject<[DisplayMenuData]>()

  // MARK: Coordinates
  
  var navigateToCreateMenu = PublishSubject<String>()
  var navigateToMenuDetail = PublishSubject<(categoryID: String, menuData: DisplayMenuData)>()
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: MenuManagementViewModelOutputs { return self }
  public var inputs: MenuManagementViewModelInputs { return self }
  public var coordinates: MenuManagementViewModelCoordinates { return self }
}
