//
//  MenuCategoryAddCollectionViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 10/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

protocol MenuCategoryAddCollectionViewCellDelegate: class {
  func didClickOnCreateMenuCategory()
}

class MenuCategoryAddCollectionViewCell: UICollectionViewCell {
  
    @IBOutlet weak var createMenuCategoryBackgroundView: UIView!
    @IBOutlet weak var createMenuCategoryTitleLabel: UILabel!
    
    weak var delegate: MenuCategoryAddCollectionViewCellDelegate?
    
    @IBAction func didClickOnCreateMwnuCategory(_ sender: Any) {
      delegate?.didClickOnCreateMenuCategory()
    }
    
    func configure() {
      setupUI()
      setupL10N()
    }
    
    func setupUI() {
      createMenuCategoryBackgroundView.layer.cornerRadius = 10
    }
    
    func setupL10N() {
      createMenuCategoryTitleLabel.text = "menumanagement_add_title".localized()
    }
}
