//
//  MenuCategoryCollectionViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 10/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

protocol MenuCategoryCollectionViewCellDelegate: class {
  func didSelectCategory(categoryID: String)
}

class MenuCategoryCollectionViewCell: UICollectionViewCell {
    // MARK: - Instances
    
    var categoryToShow: Bool?
    var categoryID: String?
    var isSelect: Bool?
    var checkHidden: Bool = false
  weak var delegate: MenuCategoryCollectionViewCellDelegate?

    // MARK: - @IBOutlet
  
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var handleCategoryUIView: UIView!
    @IBOutlet weak var cellBackgroundView: UIView!
    
    //MARK: - IBAction
    
    
    // MARK: - ui
    
    func configure(categoryData: DisplayMenuCategoryData, isSelect: Bool) {
      self.categoryLabel.text = categoryData.categoryName
      self.categoryID = categoryData.categoryID
      self.isSelect = false
      handleCategoryUIView.isHidden = self.isSelect!
      if self.isSelect == isSelect {
        uiShowoffBrand()
      } else {
        uiShowOnBrand()
      }
      setupUI()
        
    }
    
  func uiShowOnBrand() {
    handleCategoryUIView.isHidden = false
    categoryLabel.alpha = 1
  }
    
  func uiShowoffBrand() {
    handleCategoryUIView.isHidden = true
    categoryLabel.alpha = 0.3
  }
    
  func checkDataRestaurantResponse(){
    print("Data not found")
  }
  
  func setupUI() {
    cellBackgroundView.layer.cornerRadius = 10
    cellBackgroundView.layer.applySketchShadow(color: .black, alpha: 0.5, x: 0, y: 2, blur: 6, spread: 0)
  }
  
  @IBAction func didClickOnCategory(_ sender: Any) {
    guard let categoryID = self.categoryID else {
      return
    }
    delegate?.didSelectCategory(categoryID: categoryID)
  }
}
