//
//  MapSelectViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 29/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa

protocol MapSelectViewModelInputs {
  
}

protocol MapSelectViewModelOutputs {
    
}

protocol MapSelectViewModelCoordinates {
  var dissmissView: PublishSubject<Void> { get }
}

protocol MapSelectViewModelType {
  var inputs: MapSelectViewModelInputs { get }
  var outputs: MapSelectViewModelOutputs { get }
  var coordinates: MapSelectViewModelCoordinates { get }
}

class MapSelectViewModel: MapSelectViewModelType, MapSelectViewModelInputs, MapSelectViewModelOutputs, MapSelectViewModelCoordinates {
  
  // MARK: Property
  
  // MARK: Init
    
  public init() {
    
  }
  
  // MARK: Private
  
  // MARK: Input
    
  // MARK: Output

  // MARK: Coordinates
  
  var dissmissView = PublishSubject<Void>()
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: MapSelectViewModelOutputs { return self }
  public var inputs: MapSelectViewModelInputs { return self }
  public var coordinates: MapSelectViewModelCoordinates { return self }
}
