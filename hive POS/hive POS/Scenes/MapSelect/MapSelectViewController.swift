//
//  MapSelectViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 29/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import MapKit

protocol MapSelectViewControllerDelegate: class {
  func didSelectLocation(location: CLLocationCoordinate2D)
}

class MapSelectViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var dissmissButton: UIButton!
  @IBOutlet weak var submitButton: UIButton!
  
  // MARK: Property
  
  var viewModel: MapSelectViewModelType!
  let disposeBag = DisposeBag()
  weak var delegate: MapSelectViewControllerDelegate?
  var navigationTitle: String = ""
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
    setupL10N()
    setupMapView()
    bindViewModel()
    
  }
  
  func bindViewModel() {
    
    let buttonItems = navigationItem.leftBarButtonItems
    let backButton = buttonItems?.filter{ $0.tag == 33 }.first
    backButton?.rx.tap
      .bind(to: viewModel.coordinates.dissmissView)
      .disposed(by: disposeBag)
    
    dissmissButton.rx.tap
      .bind(to: viewModel.coordinates.dissmissView)
      .disposed(by: disposeBag)
    
    submitButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        let location = CLLocationCoordinate2D(latitude: self.mapView.centerCoordinate.latitude,
                                              longitude: self.mapView.centerCoordinate.longitude)
        self.delegate?.didSelectLocation(location: location)
        self.viewModel.coordinates.dissmissView.onNext(())})
      .disposed(by: disposeBag)
    
  }
  
  func setupUI() {
    submitButton.layer.cornerRadius = submitButton.frame.height / 2
  }
  
  func setupL10N() {
    submitButton.setTitle("selectmap_submit".localized(), for: .normal)
    self.setNavigationBarItem(titlePageName: navigationTitle, isBackButton: true)
  }
  
  func setupMapView() {
    mapView.showsCompass = false
    mapView.delegate = self
    guard let location = LocationService.shared.shareLocation else { return }
    moveToCurrentLocation(userLocation: location, isZoom: true, animated: true)
  }
  
  func moveToCurrentLocation(userLocation: CLLocation, isZoom: Bool, animated: Bool) {
    let center = CLLocationCoordinate2D(latitude: userLocation.coordinate.latitude, longitude: userLocation.coordinate.longitude)
    
    if isZoom {
      var region = MKCoordinateRegion(
        center: center,
        span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
      region.center = center
      mapView.setRegion(region, animated: animated)
    } else {
      mapView.setCenter(center, animated: true)
    }
  }
}

extension MapSelectViewController: MKMapViewDelegate {
  
}
