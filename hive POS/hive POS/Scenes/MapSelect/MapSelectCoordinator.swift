//
//  MapSelectCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 29/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol MapSelectInputsParamsType {
    
}

class MapSelectCoordinator: BaseCoordinator<Void>, MapSelectInputsParamsType {
  
  // MARK: Property
  
  let viewController = MapSelectViewController.initFromStoryboard(name: Storyboard.brandBranch.identifier)
  
  // MARK: Public

  override func start() -> Observable<Void> { 
    let window = self.window
    let viewModel = MapSelectViewModel()
    viewController.viewModel = viewModel
    
    transition(to: viewController)
    
    return viewModel.coordinates.dissmissView
    .do(onNext: { [weak self] _ in self?.navigateBack()})
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: MapSelectInputsParamsType { return self }
}
