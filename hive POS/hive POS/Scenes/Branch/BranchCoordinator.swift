//
//  BranchCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 2/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol BranchInputsParamsType {
  
  var brandID: Int? { get }
  var branchID: Int? { get }
  
}

class BranchCoordinator: BaseCoordinator<Void>, BranchInputsParamsType {
  
  // MARK: Property
  
  var brandID: Int?
  var branchID: Int?

  // MARK: Public

  override func start() -> Observable<Void> {
    let window = self.window
    let viewModel = BranchViewModel(brandID: brandID, branchID: branchID)
    let viewController = BranchViewController.initFromStoryboard(name: Storyboard.brandBranch.identifier)
    viewController.viewModel = viewModel
    viewController.hidesBottomBarWhenPushed = true
    
    viewModel
    .coordinates
    .navigateToSelectLocation
    .map{ _  -> MapSelectCoordinator in
      let coordinator = MapSelectCoordinator(window: window,
                                             baseViewController: viewController,
                                             transitionType: .push,
                                             animated: true)
      coordinator.viewController.delegate = viewController.self
      coordinator.viewController.navigationTitle = "branch_title".localized()
      return coordinator }
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
    viewModel
    .coordinates
    .navigateToVerifyPhoneNumber
    .map{ _ -> InAppVerifyPhoneNumberCoordinator in
      let coordinator = InAppVerifyPhoneNumberCoordinator(window: window,
                                                          baseViewController: viewController,
                                                          transitionType: .modal, animated: true)
      coordinator.viewController.delegate = viewController.self
      return coordinator }
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
    transition(to: viewController)
    return viewModel.coordinates.navigateBack
    .do(onNext: { [weak self] _ in self?.navigateBack()})
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: BranchInputsParamsType { return self }
}
