//
//  BranchViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 2/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa

struct BranchData {
  var branchName: String
  var branchDetail: String
  var building: String
  var room: String
  var floor: String
  var village: String
  var houseNO: String
  var villageNO: String
  var soi: String
  var street: String
  var subDistrict: Int
  var district: Int
  var province: Int
  var postCode: String
  var isDelivery: Bool
  var isOnline: Bool
  var latitude: Double
  var longitude: Double
  var email: String
  var website: String
  var line: String
  var facebook: String
  var instagram: String
  var preOrderMinute: Int
  var priceSince: Int
  var priceTo: Int
  var branchNO: String
  var phoneNumbers: [[String:Any]]
  var opendays: [String : Any]
  var foodImage: [[String : Any]]
  var menuImages: [[String : Any]]
  var viewImages: [[String : Any]]
  var foodCategory: [[String : Any]]
  var holidayWeeks: [[String : Any]]
  var holidayDays: [[String : Any]]
  var branchImageLogoID: String
  var branchImageLogoURL: String
  var branchImageBanerID: String
  var branchImageBanerURL: String
  var spoonwalkID: String
}

protocol BranchViewModelInputs {
  var getMenuTrigger: PublishSubject<Void> { get }
  var getBrandDataTrigger: PublishSubject<Void> { get }
  var mapViewTrigger: PublishSubject<Void> { get }
  var openHoursEverydayTrigger: PublishSubject<[OpenHourData]> { get }
  var openHoursMondayTrigger: PublishSubject<[OpenHourData]> { get }
  var openHoursTuesdayTrigger: PublishSubject<[OpenHourData]> { get }
  var openHoursWednesdayTrigger: PublishSubject<[OpenHourData]> { get }
  var openHoursThursdayTrigger: PublishSubject<[OpenHourData]> { get }
  var openHoursFridayTrigger: PublishSubject<[OpenHourData]> { get }
  var openHoursSaturdayTrigger: PublishSubject<[OpenHourData]> { get }
  var openHoursSundayTrigger: PublishSubject<[OpenHourData]> { get }
  var openHoursPublicHolidayTrigger: PublishSubject<[OpenHourData]> { get }
  var holidayTrigger: PublishSubject<[HolidayData]> { get }
  var getFoodCategoryTableOne: PublishSubject<[FoodCategoryData]> { get }
  var getFoodCategoryTableTwo: PublishSubject<[FoodCategoryData]> { get }
  var getFoodCategoryTableThree: PublishSubject<[FoodCategoryData]> { get }
  var getBranchImageTrigger: PublishSubject<(imageDatas: [BranchImageData], isView: Bool)> { get }
  var getDrinkImageTrigger: PublishSubject<(imageDatas: [BranchImageData], isView: Bool)> { get }
  var getMenuImageTrigger: PublishSubject<(imageDatas: [BranchImageData], isView: Bool)> { get }
  var createBranchTrigger: PublishSubject<BranchData> { get }
  var getBranchTrigger: PublishSubject<Void> { get }
  var updateBranchTrigger: PublishSubject<BranchData> { get }
  var deleteBranchTrigger: PublishSubject<Void> { get }
}

protocol BranchViewModelOutputs {
  var displayMenuData: PublishSubject<[SectionOfDisplayBranchMenuData]>{ get }
  var displayBrandData: PublishSubject<Void> { get }
  var loading: BehaviorSubject<Bool> { get }
  var displayOpenHoursEveryday: PublishSubject<[SectionOfOpenHourData]> { get }
  var displayOpenHoursMonday: PublishSubject<[SectionOfOpenHourData]> { get }
  var displayOpenHoursTuesday: PublishSubject<[SectionOfOpenHourData]> { get }
  var displayOpenHoursWednesday: PublishSubject<[SectionOfOpenHourData]> { get }
  var displayOpenHoursThursday: PublishSubject<[SectionOfOpenHourData]> { get }
  var displayOpenHoursFriday: PublishSubject<[SectionOfOpenHourData]> { get }
  var displayOpenHoursSaturday: PublishSubject<[SectionOfOpenHourData]> { get }
  var displayOpenHoursSunday: PublishSubject<[SectionOfOpenHourData]> { get }
  var displayOpenHoursPublicHoliday: PublishSubject<[SectionOfOpenHourData]> { get }
  var displayHolidayData: PublishSubject<[SectionOfHolidayData]> { get }
  var displayFoodCategoryTableOne: PublishSubject<[SectionOfFoodCetegoryData]> { get }
  var displayFoodCategoryTableTwo: PublishSubject<[SectionOfFoodCetegoryData]> { get }
  var displayFoodCategoryTableThree: PublishSubject<[SectionOfFoodCetegoryData]> { get }
  var displayBranchImages: PublishSubject<[SectionOfBranchImageDataType]> { get }
  var displayDrinkImages: PublishSubject<[SectionOfBranchImageDataType]> { get }
  var displayMenuImages: PublishSubject<[SectionOfBranchImageDataType]> { get }
  var displayCreateBranchSuccess: PublishSubject<Void> { get }
  var isFromDetail: BehaviorRelay<Int?> { get }
  var displayBranchData: PublishSubject<BranchModel> { get }
  var displayUpdateBranchSuccess: PublishSubject<Void> { get }
  var displayDeleteBranchSuccess: PublishSubject<Void> { get }
}

protocol BranchViewModelCoordinates {
  var navigateBack: PublishSubject<Void> { get }
  var navigateToSelectLocation: PublishSubject<Void> { get }
  var navigateToVerifyPhoneNumber: PublishSubject<Void> { get }
}

protocol BranchViewModelType: ErrorAlertableViewModelType {
  var inputs: BranchViewModelInputs { get }
  var outputs: BranchViewModelOutputs { get }
  var coordinates: BranchViewModelCoordinates { get }
}

class BranchViewModel: BranchViewModelType, BranchViewModelInputs, BranchViewModelOutputs, BranchViewModelCoordinates {
  
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  
  // MARK: Init
    
  public init(apiService: APIService = APIService(), brandID: Int?, branchID: Int?) {
    
    guard let brandID = brandID else { return }
    isFromDetail.accept(branchID)
    
    let menuData = [DisplayBranchMenuData(title: "branch_menu_information".localized(), ID: 0),
                    DisplayBranchMenuData(title: "branch_menu_openhours".localized(), ID: 1)]
    
    getMenuTrigger
      .map{menuData}
      .map{[SectionOfDisplayBranchMenuData(header: "", items: $0)]}
      .bind(to: displayMenuData)
      .disposed(by: disposeBag)
    
    let getBrandDataTriggerSignal = getBrandDataTrigger
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
      .map{ GetBrandRequestModel(brandID: brandID) }
      .flatMapLatest{ apiService.rx_getBrand(requestModel: $0).materialize() }
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
      .share(replay: 1)
    
    getBrandDataTriggerSignal
      .elements()
      .do(onNext: { LocalDataService.setSelectBrand($0) }).map{ _ in () }
      .bind(to: displayBrandData)
      .disposed(by: disposeBag)
    
    openHoursEverydayTrigger
      .map{ return [SectionOfOpenHourData(header: "", items: $0)] }
      .bind(to: displayOpenHoursEveryday)
      .disposed(by: disposeBag)
    
    openHoursMondayTrigger
      .map{ return [SectionOfOpenHourData(header: "", items: $0)] }
      .bind(to: displayOpenHoursMonday)
      .disposed(by: disposeBag)
    
    openHoursTuesdayTrigger
      .map{ return [SectionOfOpenHourData(header: "", items: $0)] }
      .bind(to: displayOpenHoursTuesday)
      .disposed(by: disposeBag)
    
    openHoursWednesdayTrigger
      .map{ return [SectionOfOpenHourData(header: "", items: $0)] }
      .bind(to: displayOpenHoursWednesday)
      .disposed(by: disposeBag)
    
    openHoursThursdayTrigger
      .map{ return [SectionOfOpenHourData(header: "", items: $0)] }
      .bind(to: displayOpenHoursThursday)
      .disposed(by: disposeBag)
    
    openHoursFridayTrigger
      .map{ return [SectionOfOpenHourData(header: "", items: $0)] }
      .bind(to: displayOpenHoursFriday)
      .disposed(by: disposeBag)
    
    openHoursSaturdayTrigger
      .map{ return [SectionOfOpenHourData(header: "", items: $0)] }
      .bind(to: displayOpenHoursSaturday)
      .disposed(by: disposeBag)
    
    openHoursSundayTrigger
      .map{ return [SectionOfOpenHourData(header: "", items: $0)] }
      .bind(to: displayOpenHoursSunday)
      .disposed(by: disposeBag)
    
    openHoursPublicHolidayTrigger
      .map{ return [SectionOfOpenHourData(header: "", items: $0)] }
      .bind(to: displayOpenHoursPublicHoliday)
      .disposed(by: disposeBag)
    
    holidayTrigger.map { [SectionOfHolidayData(header: "", items: $0)] }
      .bind(to: displayHolidayData)
      .disposed(by: disposeBag)
    
    getFoodCategoryTableOne.map { [SectionOfFoodCetegoryData(header: "", items: $0)] }
    .bind(to: displayFoodCategoryTableOne)
    .disposed(by: disposeBag)
    
    getFoodCategoryTableTwo.map { [SectionOfFoodCetegoryData(header: "", items: $0)] }
    .bind(to: displayFoodCategoryTableTwo)
    .disposed(by: disposeBag)
    
    getFoodCategoryTableThree.map { [SectionOfFoodCetegoryData(header: "", items: $0)] }
    .bind(to: displayFoodCategoryTableThree)
    .disposed(by: disposeBag)
    
    getBranchImageTrigger
      .map{ data -> [SectionOfBranchImageDataType] in
        var section = data.imageDatas.map{ SectionOfBranchImageDataType.Section(title: "", items: [.branchImage(items: $0)]) }
        if !data.isView {
          section.append(SectionOfBranchImageDataType.Section(title: "", items: [.add]))
        }
        return section}
      .bind(to: displayBranchImages).disposed(by: disposeBag)
    
    getDrinkImageTrigger
    .map{ data -> [SectionOfBranchImageDataType] in
      var section = data.imageDatas.map{ SectionOfBranchImageDataType.Section(title: "", items: [.branchImage(items: $0)]) }
      if !data.isView {
        section.append(SectionOfBranchImageDataType.Section(title: "", items: [.add]))
      }
      return section}
    .bind(to: displayDrinkImages).disposed(by: disposeBag)
    
    getMenuImageTrigger
    .map{ data -> [SectionOfBranchImageDataType] in
      var section = data.imageDatas.map{ SectionOfBranchImageDataType.Section(title: "", items: [.branchImage(items: $0)]) }
      if !data.isView {
        section.append(SectionOfBranchImageDataType.Section(title: "", items: [.add]))
      }
      return section}
    .bind(to: displayMenuImages).disposed(by: disposeBag)
    
    var createBranchData: BranchData?
    
    let createBranchTriggerSignal =
      createBranchTrigger
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .do(onNext: { createBranchData = $0 })
        .map{ CreateBranchRequestModel(branchName: $0.branchName, branchDetail: $0.branchDetail,
                                       building: $0.building, room: $0.room, floor: $0.floor,
                                       village: $0.village, houseNO: $0.houseNO, villageNO: $0.villageNO,
                                       soi: $0.soi, street: $0.street, subDistrict: $0.subDistrict,
                                       district: $0.district, province: $0.province, postCode: $0.postCode,
                                       isDelivery: $0.isDelivery, isOnline: $0.isOnline, latitude: $0.latitude,
                                       longitude: $0.longitude, email: $0.email, website: $0.website,
                                       line: $0.line, facebook: $0.facebook, instagram: $0.instagram,
                                       preOrderMinute: $0.preOrderMinute, priceSince: $0.priceSince,
                                       priceTo: $0.priceTo, brandID: brandID, branchNO: $0.branchNO,
                                       phoneNumbers: $0.phoneNumbers, openDays: $0.opendays,
                                       foodImage: $0.foodImage, menuImages: $0.menuImages,
                                       viewImages: $0.viewImages, foodCategory: $0.foodCategory,
                                       holidayWeeks: $0.holidayWeeks, holidayDays: $0.holidayDays,
                                       branchImageLogoID: $0.branchImageLogoID,
                                       branchImageLogoURL: $0.branchImageLogoURL,
                                       branchImageBanerID: $0.branchImageBanerID,
                                       branchImageBanerURL: $0.branchImageBanerURL) }
        .flatMapLatest{ apiService.rx_createBranch(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    

    
    let createShopSignal =
      createBranchTriggerSignal
        .elements()
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ model -> CreateShopRequestModel in
          
          let phones = model.telephones.map{ $0.phoneNumber }
          let everyDay = model.openDays.everyDay.map{ ["startTime" : $0.timeStart,
                                                      "endTime" : $0.timeClose] }
          let monday = model.openDays.monday.map{ ["startTime" : $0.timeStart,
                                                  "endTime" : $0.timeClose] }
          let tuesday = model.openDays.tuesday.map{ ["startTime" : $0.timeStart,
                                                    "endTime" : $0.timeClose] }
          let wednesday = model.openDays.wednesday.map{ ["startTime" : $0.timeStart,
                                                        "endTime" : $0.timeClose] }
          let thursday = model.openDays.thursday.map{ ["startTime" : $0.timeStart,
                                                        "endTime" : $0.timeClose] }
          let friday = model.openDays.friday.map{ ["startTime" : $0.timeStart,
                                                     "endTime" : $0.timeClose] }
          let saturday = model.openDays.saturday.map{ ["startTime" : $0.timeStart,
                                                      "endTime" : $0.timeClose] }
          let sunday = model.openDays.sunday.map{ ["startTime" : $0.timeStart,
                                                  "endTime" : $0.timeClose] }
          let publicHoliday = model.openDays.specialHoliday.map{ ["startTime" : $0.timeStart,
                                                                   "endTime" : $0.timeClose] }
          
          let openHours: [[String: Any]] = [["everyday" : everyDay],
                                            ["monday" : monday],
                                            ["tuesday" : tuesday],
                                            ["wednesday" : wednesday],
                                            ["thursday" : thursday],
                                            ["friday" : friday],
                                            ["saturday" : saturday],
                                            ["sunday" : sunday],
                                            ["specialHoliday" : publicHoliday]]
          
          let holidayWeek = model.holidayDates.map{ ["day" : $0.day, "week" : $0.week] }
          let holidayDay = model.holidayDays.map{ ["date": $0.date] }
          let closeTimes = [["holidayDays" : holidayDay],
                            ["holidayDates" : holidayWeek]]
          var isDelivery = false
          if model.hasDelivery || model.onlineOnly {
            isDelivery = true
          } else {
            isDelivery = false
          }
          let address: [String: Any] = ["additional" : model.building,
                         "address_no" : model.roomNumber,
                         "lat" : model.placeLatitude,
                         "lng" : model.placeLongitude,
                         "name" : model.branchName,
                         "road" : model.placeRoad,
                         "room_no" : model.roomNumber,
                         "soi_no" : model.placeAlley,
                         "floor_no" : model.floor,
                         "postal_code" : model.zipcode,
                         "province_id" : model.placeProvince,
                         "district_id" : model.placeDistrict,
                         "sub_district_id" : model.placeArea,
                         "has_shipping" : isDelivery]
          let requestModel = CreateShopRequestModel(brandID: brandID,
                                                    branchID: model.ID,
                                                    logoImage: model.branchImageUrl,
                                                    bannerImage: model.branchImageCoverUrl,
                                                    name: model.branchName,
                                                    branch: model.branchNo,
                                                    description: model.branchDetail,
                                                    priceSince: model.priceSince,
                                                    priceTo: model.priceTo,
                                                    phone: phones,
                                                    website: model.websiteUrl,
                                                    facebook: model.facebookName,
                                                    line: model.lineId,
                                                    instagram: model.instagramName,
                                                    workTime: openHours,
                                                    closeTime: closeTimes,
                                                    orderBefore: Int(model.preOrderMinute) ?? 0,
                                                    address: address)
          return requestModel }
        .flatMapLatest{ apiService.rx_createShop(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    let updateBranchWithSpoonwalkTriggerSignal =
      createShopSignal
        .elements()
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ model -> UpdateBranchRequestModel? in
          
          guard let createData = createBranchData else {
            self.outputs.loading.onNext(false)
            return nil }
          
          return UpdateBranchRequestModel(branchID: model.branchID,
                                          branchName: createData.branchName,
                                          branchDetail: createData.branchDetail,
                                          building: createData.building,
                                          room: createData.room,
                                          floor: createData.floor,
                                          village: createData.village,
                                          houseNO: createData.houseNO,
                                          villageNO: createData.villageNO,
                                          soi: createData.soi,
                                          street: createData.street,
                                          subDistrict: createData.subDistrict,
                                          district: createData.district,
                                          province: createData.province,
                                          postCode: createData.postCode,
                                          isDelivery: createData.isDelivery,
                                          isOnline: createData.isOnline,
                                          latitude: createData.latitude,
                                          longitude: createData.longitude,
                                          email: createData.email,
                                          website: createData.website,
                                          line: createData.line,
                                          facebook: createData.facebook,
                                          instagram: createData.instagram,
                                          preOrderMinute: createData.preOrderMinute,
                                          priceSince: createData.priceSince,
                                          priceTo: createData.priceTo,
                                          brandID: model.brandID,
                                          branchNO: createData.branchNO,
                                          phoneNumbers: createData.phoneNumbers,
                                          openDays: createData.opendays,
                                          foodImage: createData.foodImage,
                                          menuImages: createData.menuImages,
                                          viewImages: createData.viewImages,
                                          foodCategory: createData.foodCategory,
                                          holidayWeeks: createData.holidayWeeks,
                                          holidayDays: createData.holidayDays,
                                          branchImageLogoID: createData.branchImageLogoID,
                                          branchImageLogoURL: createData.branchImageLogoURL,
                                          branchImageBanerID: createData.branchImageBanerID,
                                          branchImageBanerURL: createData.branchImageBanerURL,
                                          spoonwalkID: model.shopID) }
        .filterNil()
        .flatMapLatest{ apiService.rx_updateBranch(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    Observable.zip(createShopSignal.errors(),
                   createBranchTriggerSignal.elements())
      .map{ DeleteBranchRequestModel(branchID: $1.ID) }
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
      .flatMapLatest{ apiService.rx_deleteBranch(requestModel: $0).materialize()}
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
      .subscribe()
      .disposed(by: disposeBag)
    
    Observable.zip(updateBranchWithSpoonwalkTriggerSignal.errors(),
                   createBranchTriggerSignal.elements())
      .map{ DeleteBranchRequestModel(branchID: $1.ID) }
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
      .flatMapLatest{ apiService.rx_deleteBranch(requestModel: $0).materialize()}
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
      .subscribe()
      .disposed(by: disposeBag)
    
    
    updateBranchWithSpoonwalkTriggerSignal
      .elements()
      .map{ _ in ()}
      .bind(to: displayCreateBranchSuccess)
      .disposed(by: disposeBag)
    
    mapViewTrigger
      .bind(to: navigateToSelectLocation)
      .disposed(by: disposeBag)
    
    //MARK: Edit
    
    let getBranchTriggerSignal =
      getBranchTrigger
        .map{ _ -> GetBranchRequestModel? in
          guard let branchID = branchID else { return nil }
          return GetBranchRequestModel(branchID: branchID)}
        .filterNil()
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .flatMapLatest{ apiService.rx_getBranch(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    getBranchTriggerSignal
      .elements()
      .bind(to: displayBranchData)
      .disposed(by: disposeBag)
    
    let updateBranchTriggerSignal =
      updateBranchTrigger
        .map{ data -> UpdateBranchRequestModel? in
          guard let branchID = branchID else { return nil }
          let requestModel = UpdateBranchRequestModel(branchID: branchID,
                                                      branchName: data.branchName,
                                                      branchDetail: data.branchDetail,
                                                      building: data.building,
                                                      room: data.room,
                                                      floor: data.floor,
                                                      village: data.village,
                                                      houseNO: data.houseNO,
                                                      villageNO: data.villageNO,
                                                      soi: data.soi,
                                                      street: data.street,
                                                      subDistrict: data.subDistrict,
                                                      district: data.district,
                                                      province: data.province,
                                                      postCode: data.postCode,
                                                      isDelivery: data.isDelivery,
                                                      isOnline: data.isOnline,
                                                      latitude: data.latitude,
                                                      longitude: data.longitude,
                                                      email: data.email,
                                                      website: data.website,
                                                      line: data.line,
                                                      facebook: data.facebook,
                                                      instagram: data.instagram,
                                                      preOrderMinute: data.preOrderMinute,
                                                      priceSince: data.priceSince,
                                                      priceTo: data.priceTo,
                                                      brandID: brandID,
                                                      branchNO: data.branchNO,
                                                      phoneNumbers: data.phoneNumbers,
                                                      openDays: data.opendays,
                                                      foodImage: data.foodImage,
                                                      menuImages: data.menuImages,
                                                      viewImages: data.viewImages,
                                                      foodCategory: data.foodCategory,
                                                      holidayWeeks: data.holidayWeeks,
                                                      holidayDays: data.holidayDays,
                                                      branchImageLogoID: data.branchImageLogoID,
                                                      branchImageLogoURL: data.branchImageLogoURL,
                                                      branchImageBanerID: data.branchImageBanerID,
                                                      branchImageBanerURL: data.branchImageBanerURL,
                                                      spoonwalkID: data.spoonwalkID)
        return requestModel}
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .filterNil()
        .flatMapLatest{apiService.rx_updateBranch(requestModel: $0).materialize()}
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    updateBranchTriggerSignal
      .elements()
      .map{_ in ()}
      .bind(to: displayUpdateBranchSuccess)
      .disposed(by: disposeBag)
    
    let deleteBranchTriggerSignal =
      deleteBranchTrigger
        .map{ branchID }
        .filterNil()
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
        .map{ DeleteBranchRequestModel(branchID: $0) }
        .flatMapLatest{ apiService.rx_deleteBranch(requestModel: $0).materialize() }
        .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(false)})
        .share(replay: 1)
    
    deleteBranchTriggerSignal
    .elements()
    .bind(to: displayDeleteBranchSuccess)
    .disposed(by: disposeBag)
    
    // Error ---------------------------------------------------------------
    
    let errorSignal = Observable
      .merge(getBrandDataTriggerSignal.errors(),
             createBranchTriggerSignal.errors(),
             updateBranchTriggerSignal.errors(),
             deleteBranchTriggerSignal.errors(),
             createShopSignal.errors(),
             updateBranchTriggerSignal.errors())
      .share(replay: 1)

    errorSignal
      .map(errorToAlertable)
      .bind(to: error)
      .disposed(by: disposeBag)
    
  }
  
  // MARK: Private
  
  // MARK: Input
  
  var getMenuTrigger = PublishSubject<Void>()
  var getBrandDataTrigger = PublishSubject<Void>()
  var mapViewTrigger = PublishSubject<Void>()
  var openHoursEverydayTrigger = PublishSubject<[OpenHourData]>()
  var openHoursMondayTrigger = PublishSubject<[OpenHourData]>()
  var openHoursTuesdayTrigger = PublishSubject<[OpenHourData]>()
  var openHoursWednesdayTrigger = PublishSubject<[OpenHourData]>()
  var openHoursThursdayTrigger = PublishSubject<[OpenHourData]>()
  var openHoursFridayTrigger = PublishSubject<[OpenHourData]>()
  var openHoursSaturdayTrigger = PublishSubject<[OpenHourData]>()
  var openHoursSundayTrigger = PublishSubject<[OpenHourData]>()
  var openHoursPublicHolidayTrigger = PublishSubject<[OpenHourData]>()
  var holidayTrigger = PublishSubject<[HolidayData]>()
  var getFoodCategoryTableOne = PublishSubject<[FoodCategoryData]>()
  var getFoodCategoryTableTwo = PublishSubject<[FoodCategoryData]>()
  var getFoodCategoryTableThree = PublishSubject<[FoodCategoryData]>()
  var getBranchImageTrigger = PublishSubject<(imageDatas: [BranchImageData], isView: Bool)>()
  var createBranchTrigger = PublishSubject<BranchData>()
  var getBranchTrigger = PublishSubject<Void>()
  var updateBranchTrigger = PublishSubject<BranchData>()
  var deleteBranchTrigger = PublishSubject<Void>()
  var getDrinkImageTrigger = PublishSubject<(imageDatas: [BranchImageData], isView: Bool)>()
  var getMenuImageTrigger = PublishSubject<(imageDatas: [BranchImageData], isView: Bool)>()
    
  // MARK: Output
  
  var displayMenuData = PublishSubject<[SectionOfDisplayBranchMenuData]>()
  var displayBrandData = PublishSubject<Void>()
  var loading = BehaviorSubject<Bool>(value: false)
  var error = PublishSubject<(title: String, message: String)>()
  var displayOpenHoursEveryday = PublishSubject<[SectionOfOpenHourData]>()
  var displayOpenHoursMonday = PublishSubject<[SectionOfOpenHourData]>()
  var displayOpenHoursTuesday = PublishSubject<[SectionOfOpenHourData]>()
  var displayOpenHoursWednesday = PublishSubject<[SectionOfOpenHourData]>()
  var displayOpenHoursThursday = PublishSubject<[SectionOfOpenHourData]>()
  var displayOpenHoursFriday = PublishSubject<[SectionOfOpenHourData]>()
  var displayOpenHoursSaturday = PublishSubject<[SectionOfOpenHourData]>()
  var displayOpenHoursSunday = PublishSubject<[SectionOfOpenHourData]>()
  var displayOpenHoursPublicHoliday = PublishSubject<[SectionOfOpenHourData]>()
  var displayHolidayData = PublishSubject<[SectionOfHolidayData]>()
  var displayFoodCategoryTableOne = PublishSubject<[SectionOfFoodCetegoryData]>()
  var displayFoodCategoryTableTwo = PublishSubject<[SectionOfFoodCetegoryData]>()
  var displayFoodCategoryTableThree = PublishSubject<[SectionOfFoodCetegoryData]>()
  var displayBranchImages = PublishSubject<[SectionOfBranchImageDataType]>()
  var displayCreateBranchSuccess = PublishSubject<Void>()
  var isFromDetail = BehaviorRelay<Int?>(value: nil)
  var displayBranchData = PublishSubject<BranchModel>()
  var displayUpdateBranchSuccess = PublishSubject<Void>()
  var displayDeleteBranchSuccess = PublishSubject<Void>()
  var displayMenuImages = PublishSubject<[SectionOfBranchImageDataType]>()
  var displayDrinkImages = PublishSubject<[SectionOfBranchImageDataType]>()

  // MARK: Coordinates
  
  var navigateBack = PublishSubject<Void>()
  var navigateToSelectLocation = PublishSubject<Void>()
  var navigateToVerifyPhoneNumber = PublishSubject<Void>()
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: BranchViewModelOutputs { return self }
  public var inputs: BranchViewModelInputs { return self }
  public var coordinates: BranchViewModelCoordinates { return self }
}
