//
//  BranchViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 2/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources
import ZSwiftKit
import MapKit
import ZFTokenField
import MMNumberKeyboard
import Firebase

class BranchViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet

  @IBOutlet weak var submitButtonHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var selectTimeSubmitButtonTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var selectTimeSubmitButtonHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var submitButtonTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var selectTimeEditButton: UIButton!
  @IBOutlet weak var selectTimeDeleteButton: UIButton!
  @IBOutlet weak var selectTimeSubmitButton: UIButton!
  @IBOutlet var starLabels: [UILabel]!
  @IBOutlet var dropDownImageViews: [UIImageView]!
  @IBOutlet weak var categoryNoteLabel: UILabel!
  @IBOutlet weak var menuImageCollectionView: UICollectionView!
  @IBOutlet weak var drinkImageCollectionView: UICollectionView!
  
  @IBOutlet weak var provinceButton: UIButton!
  @IBOutlet weak var districtButton: UIButton!
  @IBOutlet weak var subDistrictButton: UIButton!
  
  @IBOutlet weak var deleteButton: UIButton!
  @IBOutlet weak var editButton: UIButton!
  @IBOutlet weak var branchImageLabel: UILabel!
  @IBOutlet weak var branchImageCollectionView: UICollectionView!
  
  @IBOutlet weak var menuImageLabel: UILabel!
  
  @IBOutlet weak var foodImageLabel: UILabel!
  
  @IBOutlet weak var foodCategoryDescriptionLabel: UILabel!
  @IBOutlet weak var foodCategoryView: UIView!
  @IBOutlet weak var foodCategoryTableThreeView: UIView!
  @IBOutlet weak var foodCategoryTableTwoView: UIView!
  @IBOutlet weak var foodCategoryTableFourView: UIView!
  @IBOutlet weak var foodCategoryTableOneView: UIView!
  @IBOutlet weak var foodCategoryTitleLabel: UILabel!
  @IBOutlet weak var foodCategoryTableOneTableView: UITableView!
  @IBOutlet weak var foodCategoryTableTwoTableView: UITableView!
  @IBOutlet weak var foodCategoryTableThreeTableView: UITableView!
  @IBOutlet weak var foodCategoryTableFourTableView: UITableView!
  
  @IBOutlet weak var pricePerPersonLabel: UILabel!
  @IBOutlet weak var priceStartTextField: UITextField!
  @IBOutlet weak var priceToLabel: UILabel!
  @IBOutlet weak var priceEndTextField: UITextField!
  @IBOutlet weak var priceUnitLabel: UILabel!
  
  @IBOutlet weak var orderTextField: UITextField!
  @IBOutlet weak var orderUnitLabel: UILabel!
  @IBOutlet weak var orderLabel: UILabel!
  @IBOutlet weak var holidayViewHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var holidayTitleLabel: UILabel!
  @IBOutlet weak var holidayDateButton: UIButton!
  @IBOutlet weak var holidayWeekButton: UIButton!
  @IBOutlet weak var noHolidayButton: UIButton!
  @IBOutlet weak var holidayAddButtonHeightConstraint: NSLayoutConstraint!
  @IBOutlet weak var holidayTableView: UITableView!
  @IBOutlet weak var holidayAddButton: UIButton!
  @IBOutlet weak var logoLabel: UILabel!
  @IBOutlet weak var logoDescriptionLabel: UILabel!
  @IBOutlet weak var logoImgaeView: UIImageView!
  @IBOutlet weak var logoImageButton: UIButton!
  @IBOutlet weak var openHoursBackgroundView: UIView!
  @IBOutlet weak var informationBackgroundView: UIView!
  @IBOutlet weak var openHoursView: UIView!
  @IBOutlet weak var informationView: UIView!
  @IBOutlet weak var menuCollectionView: UICollectionView!
  @IBOutlet weak var detailButton: UIButton!
  
  @IBOutlet weak var banerLabel: UILabel!
  @IBOutlet weak var banerButton: UIButton!
  @IBOutlet weak var banerDescriptionLabel: UILabel!
  @IBOutlet weak var banerImageView: UIImageView!
  
  @IBOutlet weak var isDeleveryButton: UIButton!
  
  @IBOutlet weak var brandAddressSwitch: UISwitch!
  @IBOutlet weak var brandAddressSwitchLabel: UILabel!
  
  @IBOutlet weak var addressLabel: UILabel!
  
  @IBOutlet weak var branchNameLabel: UILabel!
  @IBOutlet weak var branchNameTextField: UITextField!
  
  @IBOutlet weak var branchNumberLabel: UILabel!
  @IBOutlet weak var branchNumberTextField: UITextField!
  
  @IBOutlet weak var branchDetailLabel: UILabel!
  @IBOutlet weak var branchDetailTextView: UITextView!
  @IBOutlet weak var branchDetailView: UIView!
  
  @IBOutlet weak var mapLabel: UILabel!
  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var mapButton: UIButton!
  
  @IBOutlet weak var buildingLabel: UILabel!
  @IBOutlet weak var buildingTextField: UITextField!
  
  @IBOutlet weak var roomNOLabel: UILabel!
  @IBOutlet weak var roomNOTextField: UITextField!
  
  @IBOutlet weak var floorLabel: UILabel!
  @IBOutlet weak var floorTextField: UITextField!
  
  @IBOutlet weak var villageLabel: UILabel!
  @IBOutlet weak var villageTextField: UITextField!
  
  @IBOutlet weak var houseNOLabel: UILabel!
  @IBOutlet weak var houseNOTextField: UITextField!
  
  @IBOutlet weak var villageNOLabel: UILabel!
  @IBOutlet weak var villageNOTextField: UITextField!
  
  @IBOutlet weak var soiLabel: UILabel!
  @IBOutlet weak var soiTextField: UITextField!
  
  @IBOutlet weak var streetLabel: UILabel!
  @IBOutlet weak var streetTextField: UITextField!
  
  @IBOutlet weak var subDistrictLabel: UILabel!
  @IBOutlet weak var subDistrictTextField: UITextField!
  
  @IBOutlet weak var districtLabel: UILabel!
  @IBOutlet weak var districtTextField: UITextField!
  
  @IBOutlet weak var provinceLabel: UILabel!
  @IBOutlet weak var provinceTextField: UITextField!
  
  @IBOutlet weak var postCodeLabel: UILabel!
  @IBOutlet weak var postCodeTextField: UITextField!
  
  @IBOutlet weak var brandContactSwitch: UISwitch!
  @IBOutlet weak var brandContactSwitchLabel: UILabel!
  
  @IBOutlet weak var contactLabel: UILabel!
  
  @IBOutlet weak var phoneLabel: UILabel!
  @IBOutlet weak var phoneDescriptionLabel: UILabel!
  @IBOutlet weak var phoneView: UIView!
  @IBOutlet weak var phoneScrollView: UIScrollView!
  @IBOutlet weak var phoneTokenField: ZFTokenField!
  
  @IBOutlet weak var websiteLabel: UILabel!
  @IBOutlet weak var websiteTextField: UITextField!
  
  @IBOutlet weak var emailLabel: UILabel!
  @IBOutlet weak var emailTextField: UITextField!
  
  @IBOutlet weak var facebookLabel: UILabel!
  @IBOutlet weak var facebookTextField: UITextField!
  
  @IBOutlet weak var lineLabel: UILabel!
  @IBOutlet weak var lineTextField: UITextField!
  
  @IBOutlet weak var instagramLabel: UILabel!
  @IBOutlet weak var instagramTextField: UITextField!
  
  @IBOutlet weak var submitButton: UIButton!
  
  
  @IBOutlet weak var openHoursTitleLabel: UILabel!
  
  @IBOutlet weak var everydayButton: UIButton!
  @IBOutlet weak var everydayCollectionView: UICollectionView!
  @IBOutlet weak var everydayAddButton: UIButton!
  
  @IBOutlet weak var mondayButton: UIButton!
  @IBOutlet weak var mondayAddButton: UIButton!
  @IBOutlet weak var mondayCollectionVew: UICollectionView!
  
  @IBOutlet weak var tuesdayButton: UIButton!
  @IBOutlet weak var tuesdayAddButton: UIButton!
  @IBOutlet weak var tuesdayCollectionView: UICollectionView!
  
  @IBOutlet weak var wednesdayButton: UIButton!
  @IBOutlet weak var wednesdayCollectionView: UICollectionView!
  @IBOutlet weak var wednesdayAddButton: UIButton!
  
  @IBOutlet weak var thursdayButton: UIButton!
  @IBOutlet weak var thursdayAddButton: UIButton!
  @IBOutlet weak var thursdayCollectionView: UICollectionView!
  
  @IBOutlet weak var fridayAddButton: UIButton!
  @IBOutlet weak var fridayButton: UIButton!
  @IBOutlet weak var fridayCollectionView: UICollectionView!
  
  @IBOutlet weak var saturdayButton: UIButton!
  @IBOutlet weak var saturdayCollectionView: UICollectionView!
  @IBOutlet weak var saturdayAddButton: UIButton!
  
  @IBOutlet weak var sundayButton: UIButton!
  @IBOutlet weak var sundayAddButton: UIButton!
  @IBOutlet weak var sundayCollectionView: UICollectionView!
  
  @IBOutlet weak var publicHolidayButton: UIButton!
  @IBOutlet weak var publicHolidayAddButton: UIButton!
  @IBOutlet weak var publicHolidayCollectionView: UICollectionView!
  
  // MARK: Property
  
  var viewModel: BranchViewModelType!
  let disposeBag = DisposeBag()
  var selectBrandID = 0
  var phoneNumbers: [String] = []
  var logoImagePicker = UIImagePickerController()
  var logoImageURL: String = ""
  var logoImageID: String = ""
  var banerImagePicker = UIImagePickerController()
  var banerImageURL: String = ""
  var banerImageID: String = ""
  lazy var storage = Storage.storage(url:"gs://hive-world.appspot.com")
  var branchLocation: CLLocationCoordinate2D?
  
  var openHoursEveryday: [OpenHourData] = []
  var openHoursMonday: [OpenHourData] = []
  var openHoursTuesday: [OpenHourData] = []
  var openHoursWednesday: [OpenHourData] = []
  var openHoursThursday: [OpenHourData] = []
  var openHoursFridayday: [OpenHourData] = []
  var openHoursSaturday: [OpenHourData] = []
  var openHoursSunday: [OpenHourData] = []
  var openHoursPublicHoliday: [OpenHourData] = []
  var isView = false
  var holidayData:[HolidayData] = []
  var isWeek = false
  var selectFoodCategory: [[String:Any]] = []
  var categoryTwoData: [FoodCategoryModel] = []
  var categoryThreeData: [FoodCategoryModel] = []
//  var menuImageID: String = ""
//  var menuImageURL: String = ""
//  var foodImageID: String = ""
//  var foodImageURL: String = ""
  var menuImagePicker = UIImagePickerController()
  var foodImagePicker = UIImagePickerController()
  var branchImages: [BranchImageData] = []
  var branchImagePicker = UIImagePickerController()
  var isFromDetail = false
  var branchEditData: BranchModel?
  var selectProvince = 0
  var selectSubDistrict = 0
  var selectDistrict = 0
  var spoonwalkID = ""
  var branchDrinkImages: [BranchImageData] = []
  var branchMenuImages: [BranchImageData] = []
  var textFieldOffSet: CGFloat = 16
  let priceEndNumberlicKeyboard = MMNumberKeyboard(frame: .zero)
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
    setupL10N()
    setupBackButton()
    bindViewModel()
    
  }
  
  func bindViewModel() {
    
    let dataSource = RxCollectionViewSectionedReloadDataSource<SectionOfDisplayBranchMenuData>(
      configureCell: { [unowned self] dataSource, collectionView, indexPath, item in
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BranchMenuCollectionViewCell.className, for: indexPath) as! BranchMenuCollectionViewCell
        cell.configure(title: item.title, isSelect: self.selectBrandID == item.ID)
        return cell})
    
    viewModel
      .outputs
      .displayMenuData
      .bind(to: menuCollectionView.rx.items(dataSource: dataSource))
      .disposed(by: disposeBag)
    
    menuCollectionView.rx
      .modelSelected(DisplayBranchMenuData.self)
      .subscribe(onNext: { [unowned self] model in
        self.selectBrandID = model.ID
        if model.ID == 0 {
          self.informationView.isHidden = false
          self.openHoursView.isHidden = true
        } else {
          self.informationView.isHidden = true
          self.openHoursView.isHidden = false
        }
        self.menuCollectionView.reloadData()})
      .disposed(by: disposeBag)
    
    viewModel
      .inputs
      .getMenuTrigger
      .onNext(())
    
    viewModel
      .inputs
      .getBrandDataTrigger
      .onNext(())
    
    viewModel
      .outputs
      .displayBrandData
      .subscribe(onNext: { [unowned self] _ in
        self.setNavigationBarItem(titlePageName: "branch_title".localized(),
                                  isBackButton: true)
        self.setupBackButton()})
      .disposed(by: disposeBag)
    
    logoImageButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.present(self.logoImagePicker, animated: true, completion: nil)})
      .disposed(by: disposeBag)
    
    isDeleveryButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.isDeleveryButton.isSelected = !self.isDeleveryButton.isSelected})
      .disposed(by: disposeBag)
    
    mapButton.rx.tap
      .bind(to: viewModel.inputs.mapViewTrigger)
      .disposed(by: disposeBag)
    
    provinceButton.rx.tap
    .subscribe(onNext: { [unowned self] in
      guard let province = LocalDataService.getProvince()?.list.filter({$0.status == "Y"}) else { return }
      guard let district = LocalDataService.getDistrict()?.list.filter({$0.status == "Y"}) else { return }
      guard let subDistrict = LocalDataService.getSubDistrict()?.list.filter({$0.status == "Y"}) else { return }
      let controller = ArrayChoiceProvinceTableViewController<Any>(province) { [unowned self] (provinceModel) in
        self.provinceTextField.text = provinceModel.nameTH
        self.selectProvince = provinceModel.provinceID
        guard let selectDistrictModel = district.filter({$0.provinceID == self.selectProvince}).first else { return }
        self.districtTextField.text = selectDistrictModel.nameTH
        self.selectDistrict = selectDistrictModel.districtID
        guard let selectSubDistrictModel = subDistrict.filter({$0.districtID == self.selectDistrict}).first else { return }
        self.subDistrictTextField.text = selectSubDistrictModel.nameTH
        self.selectSubDistrict = selectSubDistrictModel.subDistricID
      }
      controller.tableView.separatorStyle = .none
      controller.preferredContentSize = CGSize(width: 200, height: 300)
      self.presentPopOverView(controller, sourceView: self.provinceTextField)
    })
    .disposed(by: disposeBag)
    
    districtButton.rx.tap
      .filter{self.provinceTextField.text != ""}
      .subscribe(onNext: { [unowned self] _ in
        guard let district = LocalDataService.getDistrict()?.list.filter({$0.status == "Y"}) else { return }
        guard let subDistrict = LocalDataService.getSubDistrict()?.list.filter({$0.status == "Y"}) else { return }
        let filterDistrict = district.filter({ $0.provinceID == self.selectProvince })
        if filterDistrict.count > 0 {
          let controller = ArrayChoiceDistrictTableViewController<Any>(filterDistrict) { [unowned self] (model) in
            self.districtTextField.text = model.nameTH
            self.selectDistrict = model.districtID
            guard let selectSubDistrictModel = subDistrict.filter({$0.districtID == self.selectDistrict}).first else { return }
            self.subDistrictTextField.text = selectSubDistrictModel.nameTH
            self.selectSubDistrict = selectSubDistrictModel.subDistricID
          }
          controller.tableView.separatorStyle = .none
          controller.preferredContentSize = CGSize(width: 200, height: 300)
          self.presentPopOverView(controller, sourceView: self.districtTextField)
        }
      })
      .disposed(by: disposeBag)
    
    subDistrictButton.rx.tap
      .filter{self.provinceTextField.text != "" && self.districtTextField.text != ""}
      .subscribe(onNext: { [unowned self] _ in
        guard let subDistrict = LocalDataService.getSubDistrict()?.list.filter({$0.status == "Y"}) else { return }
        let filterSubDistrict = subDistrict.filter({ $0.districtID == self.selectDistrict })
        if filterSubDistrict.count > 0 {
          let controller = ArrayChoiceSubDistrictTableViewController<Any>(filterSubDistrict) { [unowned self] (model) in
            self.subDistrictTextField.text = model.nameTH
            self.selectSubDistrict = model.subDistricID
          }
          controller.tableView.separatorStyle = .none
          controller.preferredContentSize = CGSize(width: 200, height: 300)
          self.presentPopOverView(controller, sourceView: self.subDistrictTextField)}})
      .disposed(by: disposeBag)
    
    brandAddressSwitch.rx
      .controlEvent(.valueChanged)
      .subscribe(onNext: { [unowned self] _ in
        if self.brandAddressSwitch.isOn {
          guard let brandData = LocalDataService.getSelectBrand() else { return }
          guard let province = LocalDataService.getProvince()?.list.filter({$0.provinceID == brandData.province}).first else { return }
          guard let district = LocalDataService.getDistrict()?.list.filter({$0.districtID == brandData.district}).first else { return }
          guard let subDistrict = LocalDataService.getSubDistrict()?.list.filter({$0.subDistricID == brandData.area}).first else { return }
          self.selectProvince = brandData.province
          self.selectDistrict = brandData.district
          self.selectSubDistrict = brandData.area
          self.buildingTextField.text = brandData.building
          self.roomNOTextField.text = brandData.roomNumber
          self.floorTextField.text = brandData.floor
          self.villageTextField.text = brandData.village
          self.houseNOTextField.text = brandData.placeNumber
          self.villageNOTextField.text = brandData.moo
          self.soiTextField.text = brandData.alley
          self.streetTextField.text = brandData.road
          self.subDistrictTextField.text = subDistrict.nameTH
          self.districtTextField.text = district.nameTH
          self.provinceTextField.text = province.nameTH
          self.postCodeTextField.text = brandData.zipcode
        } else {
          self.buildingTextField.text = ""
          self.roomNOTextField.text = ""
          self.floorTextField.text = ""
          self.villageTextField.text = ""
          self.houseNOTextField.text = ""
          self.villageNOTextField.text = ""
          self.soiTextField.text = ""
          self.streetTextField.text = ""
          self.subDistrictTextField.text = ""
          self.districtTextField.text = ""
          self.provinceTextField.text = ""
          self.postCodeTextField.text = ""
        }
      })
      .disposed(by: disposeBag)
    
    branchDetailTextView.rx
      .didBeginEditing.subscribe(onNext: { [unowned self] _ in
        if self.branchDetailTextView.textColor == UIColor(hex: 0x8E8E93).withAlphaComponent(0.5) {
          self.branchDetailTextView.text = nil
          self.branchDetailTextView.textColor = .black}})
      .disposed(by: disposeBag)

    branchDetailTextView.rx
      .didEndEditing
      .subscribe(onNext:{ _ in
        if self.branchDetailTextView.text.isEmpty {
          self.branchDetailTextView.text = "branch_detail_placeholder".localized()
          self.branchDetailTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)}})
      .disposed(by: disposeBag)
    
    brandContactSwitch.rx
      .controlEvent(.valueChanged)
      .subscribe(onNext: { _  in
        if self.brandContactSwitch.isOn {
          guard let brandData = LocalDataService.getSelectBrand() else { return }
          self.phoneNumbers = brandData.contactTelephones.map{ $0.phoneNumber }
          self.phoneTokenField.reloadData()
          self.websiteTextField.text = brandData.website
          self.emailTextField.text = brandData.email
          self.facebookTextField.text = brandData.facebookUrl
          self.lineTextField.text  = brandData.lineName
          self.instagramTextField.text = brandData.instagramName
        } else {
          self.phoneNumbers = []
          self.phoneTokenField.reloadData()
          self.websiteTextField.text = ""
          self.emailTextField.text = ""
          self.facebookTextField.text = ""
          self.lineTextField.text  = ""
          self.instagramTextField.text = ""
        }
      })
      .disposed(by: disposeBag)
    
    let openHoursDataSource = RxCollectionViewSectionedReloadDataSource<SectionOfOpenHourData>(
    configureCell: { [unowned self] dataSource, collectionView, indexPath, item in
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OpenHourCollectionViewCell.className, for: indexPath) as! OpenHourCollectionViewCell
      cell.configure(startTime: item.startTime, endTime: item.endTime, isView: self.isView, isEveryDay: self.everydayButton.isSelected)
      return cell})
    
    let openHoursEveryDayDataSource = RxCollectionViewSectionedReloadDataSource<SectionOfOpenHourData>(
    configureCell: { [unowned self] dataSource, collectionView, indexPath, item in
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OpenHourCollectionViewCell.className, for: indexPath) as! OpenHourCollectionViewCell
      cell.configure(startTime: item.startTime, endTime: item.endTime, isView: self.isView)
      return cell})
    
    
    viewModel
      .outputs
      .displayOpenHoursEveryday
      .bind(to: everydayCollectionView.rx.items(dataSource: openHoursEveryDayDataSource))
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayOpenHoursMonday
      .bind(to: mondayCollectionVew.rx.items(dataSource: openHoursDataSource))
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayOpenHoursTuesday
      .bind(to: tuesdayCollectionView.rx.items(dataSource: openHoursDataSource))
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayOpenHoursWednesday
      .bind(to: wednesdayCollectionView.rx.items(dataSource: openHoursDataSource))
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayOpenHoursThursday
      .bind(to: thursdayCollectionView.rx.items(dataSource: openHoursDataSource))
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayOpenHoursFriday
      .bind(to: fridayCollectionView.rx.items(dataSource: openHoursDataSource))
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayOpenHoursSaturday
      .bind(to: saturdayCollectionView.rx.items(dataSource: openHoursDataSource))
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayOpenHoursSunday
      .bind(to: sundayCollectionView.rx.items(dataSource: openHoursDataSource))
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayOpenHoursPublicHoliday
      .bind(to: publicHolidayCollectionView.rx.items(dataSource: openHoursDataSource))
      .disposed(by: disposeBag)
    
    everydayButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.everydayButton.isSelected = !self.everydayButton.isSelected
        if self.everydayButton.isSelected {
          self.everydayAddButton.isSelected = true
          self.everydayAddButton.isUserInteractionEnabled = true
          
          self.mondayButton.isSelected = true
          self.mondayButton.isUserInteractionEnabled = false
          self.mondayAddButton.isSelected = false
          self.mondayAddButton.isUserInteractionEnabled = false
          self.openHoursMonday = []
          self.viewModel.inputs.openHoursMondayTrigger.onNext(self.openHoursMonday)
          self.mondayCollectionVew.isUserInteractionEnabled = false
          
          self.tuesdayButton.isSelected = true
          self.tuesdayButton.isUserInteractionEnabled = false
          self.tuesdayAddButton.isSelected = false
          self.tuesdayAddButton.isUserInteractionEnabled = false
          self.openHoursTuesday = []
          self.viewModel.inputs.openHoursTuesdayTrigger.onNext(self.openHoursTuesday)
          self.tuesdayCollectionView.isUserInteractionEnabled = false
          
          self.wednesdayButton.isSelected = true
          self.wednesdayButton.isUserInteractionEnabled = false
          self.wednesdayAddButton.isSelected = false
          self.wednesdayAddButton.isUserInteractionEnabled = false
          self.openHoursWednesday = []
          self.viewModel.inputs.openHoursWednesdayTrigger.onNext(self.openHoursWednesday)
          self.wednesdayCollectionView.isUserInteractionEnabled = false
          
          self.thursdayButton.isSelected = true
          self.thursdayButton.isUserInteractionEnabled = false
          self.thursdayAddButton.isSelected = false
          self.thursdayAddButton.isUserInteractionEnabled = false
          self.openHoursThursday = []
          self.viewModel.inputs.openHoursThursdayTrigger.onNext(self.openHoursThursday)
          self.thursdayCollectionView.isUserInteractionEnabled = false
          
          self.fridayButton.isSelected = true
          self.fridayButton.isUserInteractionEnabled = false
          self.fridayAddButton.isSelected = false
          self.fridayAddButton.isUserInteractionEnabled = false
          self.openHoursFridayday = []
          self.viewModel.inputs.openHoursFridayTrigger.onNext(self.openHoursFridayday)
          self.fridayCollectionView.isUserInteractionEnabled = false
          
          self.saturdayButton.isSelected = true
          self.saturdayButton.isUserInteractionEnabled = false
          self.saturdayAddButton.isSelected = false
          self.saturdayAddButton.isUserInteractionEnabled = false
          self.openHoursSaturday = []
          self.viewModel.inputs.openHoursSaturdayTrigger.onNext(self.openHoursSaturday)
          self.saturdayCollectionView.isUserInteractionEnabled = false
          
          self.sundayButton.isSelected = true
          self.sundayButton.isUserInteractionEnabled = false
          self.sundayAddButton.isSelected = false
          self.sundayAddButton.isUserInteractionEnabled = false
          self.openHoursSunday = []
          self.viewModel.inputs.openHoursSundayTrigger.onNext(self.openHoursSunday)
          self.sundayCollectionView.isUserInteractionEnabled = false
          
          self.publicHolidayButton.isSelected = true
          self.publicHolidayButton.isUserInteractionEnabled = false
          self.publicHolidayAddButton.isSelected = false
          self.publicHolidayAddButton.isUserInteractionEnabled = false
          self.openHoursPublicHoliday = []
          self.viewModel.inputs.openHoursPublicHolidayTrigger.onNext(self.openHoursPublicHoliday)
          self.publicHolidayCollectionView.isUserInteractionEnabled = false
          
        } else {
          self.everydayAddButton.isSelected = false
          self.everydayAddButton.isUserInteractionEnabled = false
          self.openHoursEveryday = []
          self.viewModel.inputs.openHoursEverydayTrigger.onNext(self.openHoursEveryday)
          
          self.mondayButton.isSelected = true
          self.mondayButton.isUserInteractionEnabled = true
          self.mondayAddButton.isSelected = true
          self.mondayAddButton.isUserInteractionEnabled = true
          self.mondayCollectionVew.isUserInteractionEnabled = true
          
          self.tuesdayButton.isSelected = true
          self.tuesdayButton.isUserInteractionEnabled = true
          self.tuesdayAddButton.isSelected = true
          self.tuesdayAddButton.isUserInteractionEnabled = true
          self.tuesdayCollectionView.isUserInteractionEnabled = true
          
          self.wednesdayButton.isSelected = true
          self.wednesdayButton.isUserInteractionEnabled = true
          self.wednesdayAddButton.isSelected = true
          self.wednesdayAddButton.isUserInteractionEnabled = true
          self.wednesdayCollectionView.isUserInteractionEnabled = true
          
          self.thursdayButton.isSelected = true
          self.thursdayButton.isUserInteractionEnabled = true
          self.thursdayAddButton.isSelected = true
          self.thursdayAddButton.isUserInteractionEnabled = true
          self.thursdayCollectionView.isUserInteractionEnabled = true
          
          self.fridayButton.isSelected = true
          self.fridayButton.isUserInteractionEnabled = true
          self.fridayAddButton.isSelected = true
          self.fridayAddButton.isUserInteractionEnabled = true
          self.fridayCollectionView.isUserInteractionEnabled = true
          
          self.saturdayButton.isSelected = true
          self.saturdayButton.isUserInteractionEnabled = true
          self.saturdayAddButton.isSelected = true
          self.saturdayAddButton.isUserInteractionEnabled = true
          self.saturdayCollectionView.isUserInteractionEnabled = true
          
          self.sundayButton.isSelected = true
          self.sundayButton.isUserInteractionEnabled = true
          self.sundayAddButton.isSelected = true
          self.sundayAddButton.isUserInteractionEnabled = true
          self.sundayCollectionView.isUserInteractionEnabled = true
          
          self.publicHolidayButton.isSelected = true
          self.publicHolidayButton.isUserInteractionEnabled = true
          self.publicHolidayAddButton.isSelected = true
          self.publicHolidayAddButton.isUserInteractionEnabled = true
          self.publicHolidayCollectionView.isUserInteractionEnabled = true
          self.viewModel.inputs.openHoursEverydayTrigger.onNext(self.openHoursEveryday)
          self.viewModel.inputs.openHoursMondayTrigger.onNext(self.openHoursMonday)
          self.viewModel.inputs.openHoursTuesdayTrigger.onNext(self.openHoursTuesday)
          self.viewModel.inputs.openHoursWednesdayTrigger.onNext(self.openHoursWednesday)
          self.viewModel.inputs.openHoursThursdayTrigger.onNext(self.openHoursThursday)
          self.viewModel.inputs.openHoursFridayTrigger.onNext(self.openHoursFridayday)
          self.viewModel.inputs.openHoursSaturdayTrigger.onNext(self.openHoursSaturday)
          self.viewModel.inputs.openHoursSundayTrigger.onNext(self.openHoursSunday)
          self.viewModel.inputs.openHoursPublicHolidayTrigger.onNext(self.openHoursPublicHoliday)
        }
      })
      .disposed(by: disposeBag)
    
    everydayAddButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        Helper.AlertInput(view: self,
                          title: "", message: "",
                          confirmText: "ok".localized(),
                          cancelText: "cancel".localized(),
                          isInverseButtonPos: true,
                          inputPlaceholder: "08:00 - 10:00",
                          inputText: "",
                          confirmCompletion: { (_, text) in
                            if self.validateTime(inputTimes: text) {
                              let times = text.split(separator: "-")
                              let dateFormatter = DateFormatter()
                              dateFormatter.dateFormat = "HH:mm"
                              let startTime = dateFormatter.date(from: String(times[0]))
                              let endTime = dateFormatter.date(from: String(times[1]))
                              self.openHoursEveryday
                                .append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                                                     endTime: dateFormatter.string(from: endTime ?? Date())))
                              self.viewModel
                                .inputs
                                .openHoursEverydayTrigger
                                .onNext(self.openHoursEveryday)
                              
                              self.openHoursMonday.append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                              endTime: dateFormatter.string(from: endTime ?? Date())))
                              self.viewModel.inputs.openHoursMondayTrigger.onNext(self.openHoursMonday)
                              
                              self.openHoursTuesday.append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                              endTime: dateFormatter.string(from: endTime ?? Date())))
                              self.viewModel.inputs.openHoursTuesdayTrigger.onNext(self.openHoursTuesday)
                              
                              self.openHoursWednesday.append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                              endTime: dateFormatter.string(from: endTime ?? Date())))
                              self.viewModel.inputs.openHoursWednesdayTrigger.onNext(self.openHoursWednesday)
                              
                              self.openHoursThursday.append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                              endTime: dateFormatter.string(from: endTime ?? Date())))
                              self.viewModel.inputs.openHoursThursdayTrigger.onNext(self.openHoursThursday)
                              
                              self.openHoursFridayday.append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                              endTime: dateFormatter.string(from: endTime ?? Date())))
                              self.viewModel.inputs.openHoursFridayTrigger.onNext(self.openHoursFridayday)
                              
                              self.openHoursSaturday.append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                              endTime: dateFormatter.string(from: endTime ?? Date())))
                              self.viewModel.inputs.openHoursSaturdayTrigger.onNext(self.openHoursSaturday)
                              
                              self.openHoursSunday.append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                              endTime: dateFormatter.string(from: endTime ?? Date())))
                              self.viewModel.inputs.openHoursSundayTrigger.onNext(self.openHoursSunday)
                              
                              self.openHoursPublicHoliday.append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                              endTime: dateFormatter.string(from: endTime ?? Date())))
                              self.viewModel.inputs.openHoursPublicHolidayTrigger.onNext(self.openHoursPublicHoliday)
                              
                            } else {
                              Helper.Alert(view: self, title: "branch_alerttime_title".localized(), message: "branch_alerttime_message".localized(), confirmText: "ok".localized())
                            }
        }, cancelCompletion: nil)
      })
      .disposed(by: disposeBag)
    
    everydayCollectionView.rx.itemSelected
      .subscribe(onNext: { [unowned self] index in
        if index.section == 0 {
          self.openHoursEveryday.remove(at: index.row)
          self.openHoursMonday.remove(at: index.row)
          self.openHoursTuesday.remove(at: index.row)
          self.openHoursWednesday.remove(at: index.row)
          self.openHoursThursday.remove(at: index.row)
          self.openHoursFridayday.remove(at: index.row)
          self.openHoursSaturday.remove(at: index.row)
          self.openHoursSunday.remove(at: index.row)
          self.openHoursPublicHoliday.remove(at: index.row)
          self.viewModel
          .inputs
          .openHoursEverydayTrigger
          .onNext(self.openHoursEveryday)
          self.viewModel
          .inputs
          .openHoursMondayTrigger
          .onNext(self.openHoursMonday)
          self.viewModel
          .inputs
          .openHoursTuesdayTrigger
          .onNext(self.openHoursTuesday)
          self.viewModel
          .inputs
          .openHoursWednesdayTrigger
          .onNext(self.openHoursWednesday)
          self.viewModel
          .inputs
          .openHoursThursdayTrigger
          .onNext(self.openHoursThursday)
          self.viewModel
          .inputs
          .openHoursFridayTrigger
          .onNext(self.openHoursFridayday)
          self.viewModel
          .inputs
          .openHoursSaturdayTrigger
          .onNext(self.openHoursSaturday)
          self.viewModel
          .inputs
          .openHoursSundayTrigger
          .onNext(self.openHoursSunday)
          self.viewModel
          .inputs
          .openHoursPublicHolidayTrigger
          .onNext(self.openHoursPublicHoliday)
        }})
      .disposed(by: disposeBag)
    
    mondayButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.mondayButton.isSelected = !self.mondayButton.isSelected
        self.mondayAddButton.isSelected = self.mondayButton.isSelected
        if self.mondayButton.isSelected {
          self.mondayAddButton.isUserInteractionEnabled = true
          self.mondayCollectionVew.isUserInteractionEnabled = true
        } else {
          self.openHoursMonday = []
          self.viewModel.inputs.openHoursMondayTrigger.onNext(self.openHoursMonday)
          self.mondayAddButton.isUserInteractionEnabled = false
          self.mondayCollectionVew.isUserInteractionEnabled = false
        }
      })
      .disposed(by: disposeBag)
    
    tuesdayButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.tuesdayButton.isSelected = !self.tuesdayButton.isSelected
        self.tuesdayAddButton.isSelected = self.tuesdayButton.isSelected
        if self.tuesdayButton.isSelected {
          self.tuesdayAddButton.isUserInteractionEnabled = true
          self.tuesdayCollectionView.isUserInteractionEnabled = true
        } else {
          self.openHoursTuesday = []
          self.viewModel.inputs.openHoursTuesdayTrigger.onNext(self.openHoursTuesday)
          self.tuesdayAddButton.isUserInteractionEnabled = false
          self.tuesdayCollectionView.isUserInteractionEnabled = false
        }
      })
      .disposed(by: disposeBag)
    
    wednesdayButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.wednesdayButton.isSelected = !self.wednesdayButton.isSelected
        self.wednesdayAddButton.isSelected = self.wednesdayButton.isSelected
        if self.wednesdayButton.isSelected {
          self.wednesdayAddButton.isUserInteractionEnabled = true
          self.wednesdayCollectionView.isUserInteractionEnabled = true
        } else {
          self.openHoursWednesday = []
          self.viewModel.inputs.openHoursWednesdayTrigger.onNext(self.openHoursWednesday)
          self.wednesdayAddButton.isUserInteractionEnabled = false
          self.wednesdayCollectionView.isUserInteractionEnabled = false
        }
      })
      .disposed(by: disposeBag)
    
    thursdayButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.thursdayButton.isSelected = !self.thursdayButton.isSelected
        self.thursdayAddButton.isSelected = self.thursdayButton.isSelected
        if self.thursdayButton.isSelected {
          self.thursdayAddButton.isUserInteractionEnabled = true
          self.thursdayCollectionView.isUserInteractionEnabled = true
        } else {
          self.openHoursThursday = []
          self.viewModel.inputs.openHoursThursdayTrigger.onNext(self.openHoursThursday)
          self.thursdayAddButton.isUserInteractionEnabled = false
          self.thursdayCollectionView.isUserInteractionEnabled = false
        }
      })
      .disposed(by: disposeBag)
    
    fridayButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.fridayButton.isSelected = !self.fridayButton.isSelected
        self.fridayAddButton.isSelected = self.fridayButton.isSelected
        if self.fridayButton.isSelected {
          self.fridayAddButton.isUserInteractionEnabled = true
          self.fridayCollectionView.isUserInteractionEnabled = true
        } else {
          self.openHoursFridayday = []
          self.viewModel.inputs.openHoursFridayTrigger.onNext(self.openHoursFridayday)
          self.fridayAddButton.isUserInteractionEnabled = false
          self.fridayCollectionView.isUserInteractionEnabled = false
        }
      })
      .disposed(by: disposeBag)
    
    saturdayButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.saturdayButton.isSelected = !self.saturdayButton.isSelected
        self.saturdayAddButton.isSelected = self.saturdayButton.isSelected
        if self.saturdayButton.isSelected {
          self.saturdayAddButton.isUserInteractionEnabled = true
          self.saturdayCollectionView.isUserInteractionEnabled = true
        } else {
          self.openHoursSaturday = []
          self.viewModel.inputs.openHoursSaturdayTrigger.onNext(self.openHoursSaturday)
          self.saturdayAddButton.isUserInteractionEnabled = false
          self.saturdayCollectionView.isUserInteractionEnabled = false
        }
      })
      .disposed(by: disposeBag)
    
    sundayButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.sundayButton.isSelected = !self.sundayButton.isSelected
        self.sundayAddButton.isSelected = self.sundayButton.isSelected
        if self.sundayButton.isSelected {
          self.sundayAddButton.isUserInteractionEnabled = true
          self.sundayCollectionView.isUserInteractionEnabled = true
        } else {
          self.openHoursSunday = []
          self.viewModel.inputs.openHoursSundayTrigger.onNext(self.openHoursSunday)
          self.sundayAddButton.isUserInteractionEnabled = false
          self.sundayCollectionView.isUserInteractionEnabled = false
        }
      })
      .disposed(by: disposeBag)
    
    publicHolidayButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.publicHolidayButton.isSelected = !self.publicHolidayButton.isSelected
        self.publicHolidayAddButton.isSelected = self.publicHolidayButton.isSelected
        if self.publicHolidayButton.isSelected {
          self.publicHolidayAddButton.isUserInteractionEnabled = true
          self.publicHolidayCollectionView.isUserInteractionEnabled = true
        } else {
          self.openHoursPublicHoliday = []
          self.viewModel.inputs.openHoursPublicHolidayTrigger.onNext(self.openHoursPublicHoliday)
          self.publicHolidayAddButton.isUserInteractionEnabled = false
          self.publicHolidayCollectionView.isUserInteractionEnabled = false
        }
      })
      .disposed(by: disposeBag)
    
    mondayAddButton.rx.tap
      .subscribe(onNext: { _ in
        Helper.AlertInput(view: self,
                          title: "", message: "",
                          confirmText: "ok".localized(),
                          cancelText: "cancel".localized(),
                          isInverseButtonPos: true,
                          inputPlaceholder: "08:00 - 10:00",
                          inputText: "",
                          confirmCompletion: { (_, text) in
                            if self.validateTime(inputTimes: text) {
                              let times = text.split(separator: "-")
                              let dateFormatter = DateFormatter()
                              dateFormatter.dateFormat = "HH:mm"
                              let startTime = dateFormatter.date(from: String(times[0]))
                              let endTime = dateFormatter.date(from: String(times[1]))
                              self.openHoursMonday
                                .append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                                                     endTime: dateFormatter.string(from: endTime ?? Date())))
                              self.viewModel
                                .inputs
                                .openHoursMondayTrigger
                                .onNext(self.openHoursMonday)
                            } else {
                              Helper.Alert(view: self, title: "branch_alerttime_title".localized(), message: "branch_alerttime_message".localized(), confirmText: "ok".localized())
                            }
        }, cancelCompletion: nil)
      })
      .disposed(by: disposeBag)
    
    tuesdayAddButton.rx.tap
    .subscribe(onNext: { _ in
      Helper.AlertInput(view: self,
                        title: "", message: "",
                        confirmText: "ok".localized(),
                        cancelText: "cancel".localized(),
                        isInverseButtonPos: true,
                        inputPlaceholder: "08:00 - 10:00",
                        inputText: "",
                        confirmCompletion: { (_, text) in
                          if self.validateTime(inputTimes: text) {
                            let times = text.split(separator: "-")
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "HH:mm"
                            let startTime = dateFormatter.date(from: String(times[0]))
                            let endTime = dateFormatter.date(from: String(times[1]))
                            self.openHoursTuesday
                              .append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                                                   endTime: dateFormatter.string(from: endTime ?? Date())))
                            self.viewModel
                              .inputs
                              .openHoursTuesdayTrigger
                              .onNext(self.openHoursTuesday)
                          } else {
                            Helper.Alert(view: self, title: "branch_alerttime_title".localized(), message: "branch_alerttime_message".localized(), confirmText: "ok".localized())
                          }
      }, cancelCompletion: nil)
    })
    .disposed(by: disposeBag)
    
    wednesdayAddButton.rx.tap
    .subscribe(onNext: { _ in
      Helper.AlertInput(view: self,
                        title: "", message: "",
                        confirmText: "ok".localized(),
                        cancelText: "cancel".localized(),
                        isInverseButtonPos: true,
                        inputPlaceholder: "08:00 - 10:00",
                        inputText: "",
                        confirmCompletion: { (_, text) in
                          if self.validateTime(inputTimes: text) {
                            let times = text.split(separator: "-")
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "HH:mm"
                            let startTime = dateFormatter.date(from: String(times[0]))
                            let endTime = dateFormatter.date(from: String(times[1]))
                            self.openHoursWednesday
                              .append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                                                   endTime: dateFormatter.string(from: endTime ?? Date())))
                            self.viewModel
                              .inputs
                              .openHoursWednesdayTrigger
                              .onNext(self.openHoursWednesday)
                          } else {
                            Helper.Alert(view: self, title: "branch_alerttime_title".localized(), message: "branch_alerttime_message".localized(), confirmText: "ok".localized())
                          }
      }, cancelCompletion: nil)
    })
    .disposed(by: disposeBag)
    
    thursdayAddButton.rx.tap
    .subscribe(onNext: { _ in
      Helper.AlertInput(view: self,
                        title: "", message: "",
                        confirmText: "ok".localized(),
                        cancelText: "cancel".localized(),
                        isInverseButtonPos: true,
                        inputPlaceholder: "08:00 - 10:00",
                        inputText: "",
                        confirmCompletion: { (_, text) in
                          if self.validateTime(inputTimes: text) {
                            let times = text.split(separator: "-")
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "HH:mm"
                            let startTime = dateFormatter.date(from: String(times[0]))
                            let endTime = dateFormatter.date(from: String(times[1]))
                            self.openHoursThursday
                              .append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                                                   endTime: dateFormatter.string(from: endTime ?? Date())))
                            self.viewModel
                              .inputs
                              .openHoursThursdayTrigger
                              .onNext(self.openHoursThursday)
                          } else {
                            Helper.Alert(view: self, title: "branch_alerttime_title".localized(), message: "branch_alerttime_message".localized(), confirmText: "ok".localized())
                          }
      }, cancelCompletion: nil)
    })
    .disposed(by: disposeBag)
    
    fridayAddButton.rx.tap
    .subscribe(onNext: { _ in
      Helper.AlertInput(view: self,
                        title: "", message: "",
                        confirmText: "ok".localized(),
                        cancelText: "cancel".localized(),
                        isInverseButtonPos: true,
                        inputPlaceholder: "08:00 - 10:00",
                        inputText: "",
                        confirmCompletion: { (_, text) in
                          if self.validateTime(inputTimes: text) {
                            let times = text.split(separator: "-")
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "HH:mm"
                            let startTime = dateFormatter.date(from: String(times[0]))
                            let endTime = dateFormatter.date(from: String(times[1]))
                            self.openHoursFridayday
                              .append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                                                   endTime: dateFormatter.string(from: endTime ?? Date())))
                            self.viewModel
                              .inputs
                              .openHoursFridayTrigger
                              .onNext(self.openHoursFridayday)
                          } else {
                            Helper.Alert(view: self, title: "branch_alerttime_title".localized(), message: "branch_alerttime_message".localized(), confirmText: "ok".localized())
                          }
      }, cancelCompletion: nil)
    })
    .disposed(by: disposeBag)
    
    saturdayAddButton.rx.tap
    .subscribe(onNext: { _ in
      Helper.AlertInput(view: self,
                        title: "", message: "",
                        confirmText: "ok".localized(),
                        cancelText: "cancel".localized(),
                        isInverseButtonPos: true,
                        inputPlaceholder: "08:00 - 10:00",
                        inputText: "",
                        confirmCompletion: { (_, text) in
                          if self.validateTime(inputTimes: text) {
                            let times = text.split(separator: "-")
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "HH:mm"
                            let startTime = dateFormatter.date(from: String(times[0]))
                            let endTime = dateFormatter.date(from: String(times[1]))
                            self.openHoursSaturday
                              .append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                                                   endTime: dateFormatter.string(from: endTime ?? Date())))
                            self.viewModel
                              .inputs
                              .openHoursSaturdayTrigger
                              .onNext(self.openHoursSaturday)
                          } else {
                            Helper.Alert(view: self, title: "branch_alerttime_title".localized(), message: "branch_alerttime_message".localized(), confirmText: "ok".localized())
                          }
      }, cancelCompletion: nil)
    })
    .disposed(by: disposeBag)
    
    sundayAddButton.rx.tap
    .subscribe(onNext: { _ in
      Helper.AlertInput(view: self,
                        title: "", message: "",
                        confirmText: "ok".localized(),
                        cancelText: "cancel".localized(),
                        isInverseButtonPos: true,
                        inputPlaceholder: "08:00 - 10:00",
                        inputText: "",
                        confirmCompletion: { (_, text) in
                          if self.validateTime(inputTimes: text) {
                            let times = text.split(separator: "-")
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "HH:mm"
                            let startTime = dateFormatter.date(from: String(times[0]))
                            let endTime = dateFormatter.date(from: String(times[1]))
                            self.openHoursSunday
                              .append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                                                   endTime: dateFormatter.string(from: endTime ?? Date())))
                            self.viewModel
                              .inputs
                              .openHoursSundayTrigger
                              .onNext(self.openHoursSunday)
                          } else {
                            Helper.Alert(view: self, title: "branch_alerttime_title".localized(), message: "branch_alerttime_message".localized(), confirmText: "ok".localized())
                          }
      }, cancelCompletion: nil)
    })
    .disposed(by: disposeBag)
    
    publicHolidayAddButton.rx.tap
    .subscribe(onNext: { _ in
      Helper.AlertInput(view: self,
                        title: "", message: "",
                        confirmText: "ok".localized(),
                        cancelText: "cancel".localized(),
                        isInverseButtonPos: true,
                        inputPlaceholder: "08:00 - 10:00",
                        inputText: "",
                        confirmCompletion: { (_, text) in
                          if self.validateTime(inputTimes: text) {
                            let times = text.split(separator: "-")
                            let dateFormatter = DateFormatter()
                            dateFormatter.dateFormat = "HH:mm"
                            let startTime = dateFormatter.date(from: String(times[0]))
                            let endTime = dateFormatter.date(from: String(times[1]))
                            self.openHoursPublicHoliday
                              .append(OpenHourData(startTime: dateFormatter.string(from: startTime ?? Date()),
                                                   endTime: dateFormatter.string(from: endTime ?? Date())))
                            self.viewModel
                              .inputs
                              .openHoursPublicHolidayTrigger
                              .onNext(self.openHoursPublicHoliday)
                          } else {
                            Helper.Alert(view: self, title: "branch_alerttime_title".localized(), message: "branch_alerttime_message".localized(), confirmText: "ok".localized())
                          }
      }, cancelCompletion: nil)
    })
    .disposed(by: disposeBag)
    
    mondayCollectionVew.rx.itemSelected
      .subscribe(onNext: { [unowned self] index in
        if index.section == 0 {
          self.openHoursMonday.remove(at: index.row)
          self.viewModel.inputs.openHoursMondayTrigger.onNext(self.openHoursMonday)
        }})
      .disposed(by: disposeBag)
    
    tuesdayCollectionView.rx.itemSelected
    .subscribe(onNext: { [unowned self] index in
      if index.section == 0 {
        self.openHoursTuesday.remove(at: index.row)
        self.viewModel.inputs.openHoursTuesdayTrigger.onNext(self.openHoursTuesday)
      }})
    .disposed(by: disposeBag)
    
    wednesdayCollectionView.rx.itemSelected
    .subscribe(onNext: { [unowned self] index in
      if index.section == 0 {
        self.openHoursWednesday.remove(at: index.row)
        self.viewModel.inputs.openHoursWednesdayTrigger.onNext(self.openHoursWednesday)
      }})
    .disposed(by: disposeBag)
    
    thursdayCollectionView.rx.itemSelected
    .subscribe(onNext: { [unowned self] index in
      if index.section == 0 {
        self.openHoursThursday.remove(at: index.row)
        self.viewModel.inputs.openHoursThursdayTrigger.onNext(self.openHoursThursday)
      }})
    .disposed(by: disposeBag)
    
    fridayCollectionView.rx.itemSelected
    .subscribe(onNext: { [unowned self] index in
      if index.section == 0 {
        self.openHoursFridayday.remove(at: index.row)
        self.viewModel.inputs.openHoursFridayTrigger.onNext(self.openHoursFridayday)
      }})
    .disposed(by: disposeBag)
    
    saturdayCollectionView.rx.itemSelected
    .subscribe(onNext: { [unowned self] index in
      if index.section == 0 {
        self.openHoursSaturday.remove(at: index.row)
        self.viewModel.inputs.openHoursSaturdayTrigger.onNext(self.openHoursSaturday)
      }})
    .disposed(by: disposeBag)
    
    sundayCollectionView.rx.itemSelected
    .subscribe(onNext: { [unowned self] index in
      if index.section == 0 {
        self.openHoursSunday.remove(at: index.row)
        self.viewModel.inputs.openHoursSundayTrigger.onNext(self.openHoursSunday)
      }})
    .disposed(by: disposeBag)
    
    publicHolidayCollectionView.rx.itemSelected
    .subscribe(onNext: { [unowned self] index in
      if index.section == 0 {
        self.openHoursPublicHoliday.remove(at: index.row)
        self.viewModel.inputs.openHoursPublicHolidayTrigger.onNext(self.openHoursPublicHoliday)
      }})
    .disposed(by: disposeBag)
    
    let holidayDataSource = RxTableViewSectionedReloadDataSource<SectionOfHolidayData>(
    configureCell: { [unowned self] dataSource, tableView, indexPath, item in
      switch item.type {
      case .week:
        let cell = tableView.dequeueReusableCell(withIdentifier: HolidayTableViewCell.className, for: indexPath) as! HolidayTableViewCell
        cell.configure(day: item.day, week: item.week,index: indexPath.row, viewController: self, isView: self.isView)
        cell.delegate = self
        return cell
      case .day:
        let cell = tableView.dequeueReusableCell(withIdentifier: HolidayDateTableViewCell.className, for: indexPath) as! HolidayDateTableViewCell
        cell.configure(day: item.day,index: indexPath.row, viewController: self, isView: self.isView)
        cell.delegate = self
        return cell
      }
      
      
    })
    
    viewModel.outputs
      .displayHolidayData
      .bind(to: holidayTableView.rx.items(dataSource: holidayDataSource))
      .disposed(by: disposeBag)
    
    noHolidayButton.rx.tap
      .subscribe(onNext: { _ in
        self.noHolidayButton.isSelected = !self.noHolidayButton.isSelected
        if self.noHolidayButton.isSelected {
          self.isWeek = false
          self.holidayAddButtonHeightConstraint.constant = 0
          self.holidayDateButton.isSelected = false
          self.holidayWeekButton.isSelected = false
          self.holidayData = []
          self.holidayViewHeightConstraint.constant = CGFloat(self.holidayData.count) * 80.0
          self.viewModel.inputs.holidayTrigger.onNext(self.holidayData)
        }
      })
      .disposed(by: disposeBag)
    
    holidayAddButton.rx.tap
      .subscribe(onNext: { _ in
        let holiday = HolidayData(day: "", week: "", type: self.isWeek ? .week : .day)
        self.holidayData.append(holiday)
        self.holidayViewHeightConstraint.constant = CGFloat(self.holidayData.count) * 80.0
        self.viewModel.inputs.holidayTrigger.onNext(self.holidayData)
      })
      .disposed(by: disposeBag)
    
    holidayWeekButton.rx.tap
      .subscribe(onNext: { _ in
        self.holidayWeekButton.isSelected = !self.holidayWeekButton.isSelected
        if self.holidayWeekButton.isSelected {
          self.isWeek = true
          self.holidayAddButtonHeightConstraint.constant = 44
          self.holidayDateButton.isSelected = false
          self.noHolidayButton.isSelected = false
          self.holidayData = []
          self.holidayViewHeightConstraint.constant = CGFloat(self.holidayData.count) * 80.0
          self.viewModel.inputs.holidayTrigger.onNext(self.holidayData)
        }
      })
      .disposed(by: disposeBag)
    
    holidayDateButton.rx.tap
      .subscribe(onNext: { _ in
        self.holidayDateButton.isSelected = !self.holidayDateButton.isSelected
        if self.holidayDateButton.isSelected {
          self.isWeek = false
          self.holidayAddButtonHeightConstraint.constant = 44
          self.holidayWeekButton.isSelected = false
          self.noHolidayButton.isSelected = false
          self.holidayData = []
          self.holidayViewHeightConstraint.constant = CGFloat(self.holidayData.count) * 80.0
          self.viewModel.inputs.holidayTrigger.onNext(self.holidayData)
        }
      })
      .disposed(by: disposeBag)
    
    let foodCategoryGenreDataSource = RxTableViewSectionedReloadDataSource<SectionOfFoodCetegoryData>(
    configureCell: { dataSource, tableView, indexPath, item in
      
      let cell = tableView.dequeueReusableCell(withIdentifier: FoodCategoryGenreTableViewCell.className, for: indexPath) as! FoodCategoryGenreTableViewCell
      cell.configure(title: item.name)
      cell.setSelected(item.isSelect, animated: true)
      return cell
    })
    
    let foodCategoryDataSource = RxTableViewSectionedReloadDataSource<SectionOfFoodCetegoryData>(
    configureCell: { dataSource, tableView, indexPath, item in
      
      let cell = tableView.dequeueReusableCell(withIdentifier: FoodCategoryTableViewCell.className, for: indexPath) as! FoodCategoryTableViewCell
      cell.configure(title: item.name, isSelect: item.isSelect)
//      cell.setSelected(item.isSelect, animated: true)
      return cell
    })
    
    viewModel.outputs
      .displayFoodCategoryTableOne
      .bind(to: foodCategoryTableOneTableView.rx.items(dataSource: foodCategoryGenreDataSource))
      .disposed(by: disposeBag)
    
    viewModel.outputs
      .displayFoodCategoryTableTwo
      .bind(to: foodCategoryTableTwoTableView.rx.items(dataSource: foodCategoryDataSource))
      .disposed(by: disposeBag)
    
    viewModel.outputs
    .displayFoodCategoryTableThree
    .bind(to: foodCategoryTableThreeTableView.rx.items(dataSource: foodCategoryDataSource))
    .disposed(by: disposeBag)
    
    Observable
      .just(FoodCategoryManagement.getFoodCategoryAndSpecial())
      .map{ data ->[FoodCategoryData] in
        let categoryData = data.map{ [unowned self] category -> FoodCategoryData in
          let selectCategory = self.selectFoodCategory.filter({ $0["categoryName"] as? String ?? "" == category.thaiName })
          return FoodCategoryData(ID: category.id, name: category.thaiName, isSelect: selectCategory.count > 0) }
        return categoryData }
      .bind(to: viewModel.inputs.getFoodCategoryTableOne)
      .disposed(by: disposeBag)
    
    foodCategoryTableOneTableView.rx
      .modelSelected(FoodCategoryData.self)
      .subscribe(onNext: { [unowned self] model in
        let categoryData = FoodCategoryManagement.getSubFoodCategoryAndSpecial()
        let categoryTwoData = categoryData.filter({ $0.forendKey == model.ID })
        self.categoryTwoData = categoryTwoData
        let foodCategoryData = categoryTwoData.map{ [unowned self] data -> FoodCategoryData in
          let selectData = self.selectFoodCategory.filter({ $0["categoryName"] as? String == data.thaiName })
          return FoodCategoryData(ID: data.id, name: data.thaiName, isSelect: selectData.count > 0)
        }
        self.viewModel.inputs.getFoodCategoryTableTwo.onNext(foodCategoryData)
        self.foodCategoryTableTwoView.isHidden = false
      })
      .disposed(by: disposeBag)
    
    foodCategoryTableTwoTableView.rx
      .modelSelected(FoodCategoryData.self)
      .subscribe(onNext: { [unowned self] model in
        if model.isSelect {
          for (index, selectData) in self.selectFoodCategory.enumerated() {
            if selectData["categoryName"] as? String == model.name {
              self.selectFoodCategory.remove(at: index)
            }
          }
        } else {
          self.selectFoodCategory.append(["categoryName" : model.name])
        }
        let foodCategoryData = self.categoryTwoData.map{ [unowned self] data -> FoodCategoryData in
          let selectData = self.selectFoodCategory.filter({ $0["categoryName"] as? String == data.thaiName })
          return FoodCategoryData(ID: data.id, name: data.thaiName, isSelect: selectData.count > 0)
        }
        
        self.viewModel.inputs.getFoodCategoryTableTwo.onNext(foodCategoryData)
      })
      .disposed(by: disposeBag)
    
    let branchImageDataSource = RxCollectionViewSectionedReloadDataSource<SectionOfBranchImageDataType>(
    configureCell: { dataSource, collectionView, indexPath, item in
      switch dataSource[indexPath] {
      case .branchImage(let item):
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BranchImageCollectionViewCell.className, for: indexPath) as! BranchImageCollectionViewCell
        cell.delegate = self
        cell.configure(imagePath: item.imageURL, index: indexPath.section, isView: self.isView)
        return cell
      case .add:
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BranchAddImageCollectionViewCell.className, for: indexPath) as! BranchAddImageCollectionViewCell
        cell.delegate = self
        return cell
      }
    })
    
    viewModel.outputs.displayBranchImages
      .bind(to: branchImageCollectionView.rx.items(dataSource: branchImageDataSource))
      .disposed(by: disposeBag)
    
    viewModel
      .inputs
      .getBranchImageTrigger.onNext((imageDatas: self.branchImages, isView: self.isView))
    
    let branchDrinkImageDataSource = RxCollectionViewSectionedReloadDataSource<SectionOfBranchImageDataType>(
    configureCell: { dataSource, collectionView, indexPath, item in
      switch dataSource[indexPath] {
      case .branchImage(let item):
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BranchDrinkImageCollectionViewCell.className, for: indexPath) as! BranchDrinkImageCollectionViewCell
        cell.delegate = self
        cell.configure(imagePath: item.imageURL, index: indexPath.section, isView: self.isView)
        return cell
      case .add:
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BranchDrinkAddImageCollectionViewCell.className, for: indexPath) as! BranchDrinkAddImageCollectionViewCell
        cell.delegate = self
        return cell
      }
    })
    
    viewModel.outputs.displayDrinkImages
      .bind(to: drinkImageCollectionView.rx.items(dataSource: branchDrinkImageDataSource))
      .disposed(by: disposeBag)
    
    viewModel
      .inputs
      .getDrinkImageTrigger.onNext((imageDatas: self.branchDrinkImages, isView: self.isView))
    
    let branchMenuImageDataSource = RxCollectionViewSectionedReloadDataSource<SectionOfBranchImageDataType>(
    configureCell: { dataSource, collectionView, indexPath, item in
      switch dataSource[indexPath] {
      case .branchImage(let item):
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BranchMenuImageCollectionViewCell.className, for: indexPath) as! BranchMenuImageCollectionViewCell
        cell.delegate = self
        cell.configure(imagePath: item.imageURL, index: indexPath.section, isView: self.isView)
        return cell
      case .add:
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BranchMenuAddImageCollectionViewCell.className, for: indexPath) as! BranchMenuAddImageCollectionViewCell
        cell.delegate = self
        return cell
      }
    })
    
    viewModel.outputs.displayMenuImages
      .bind(to: menuImageCollectionView.rx.items(dataSource: branchMenuImageDataSource))
      .disposed(by: disposeBag)
    
    viewModel
      .inputs
      .getMenuImageTrigger.onNext((imageDatas: self.branchDrinkImages, isView: self.isView))

    LocationService
    .shared
    .didUpdateLocationSingle()
    .filter{ [unowned self] _ in self.branchLocation == nil }
    .subscribe(onNext: { [unowned self] location in
      guard let location = location else { return }
      let region = MKCoordinateRegion( center: location.coordinate,
                                       latitudinalMeters: CLLocationDistance(exactly: 5000)!,
                                       longitudinalMeters: CLLocationDistance(exactly: 5000)!)
      self.mapView.setRegion(self.mapView.regionThatFits(region), animated: true)})
    .disposed(by: disposeBag)
    
    submitButton.rx.tap
      .filter{ [unowned self] in self.validateField() }
      .subscribe(onNext: { [unowned self] _ in
        
        var branchDetail = ""
        if self.branchDetailTextView.text != "branch_detail_placeholder".localized() {
          branchDetail = self.branchDetailTextView.text
        }
        
        let contactPhoneNumbers = self.phoneNumbers.map { ["phoneNumber" : $0] }
        let foodImages = self.branchDrinkImages.map{["imageId" : $0.imageID,
                                                "imageUrl" : $0.imageURL] }
        let menuImages = self.branchMenuImages.map{["imageId" : $0.imageID,
                                               "imageUrl" : $0.imageURL] }
        let viewImages = self.branchImages.map { ["imageId" : $0.imageID,
                                                  "imageUrl" : $0.imageURL] }
        
        
        
        let everyDay = self.openHoursEveryday.map{ ["startTime" : $0.startTime,
                                                    "endTime" : $0.endTime] }
        let monday = self.openHoursMonday.map{ ["startTime" : $0.startTime,
                                                "endTime" : $0.endTime] }
        let tuesday = self.openHoursTuesday.map{ ["startTime" : $0.startTime,
                                                  "endTime" : $0.endTime] }
        let wednesday = self.openHoursWednesday.map{ ["startTime" : $0.startTime,
                                                      "endTime" : $0.endTime] }
        let thursday = self.openHoursThursday.map{ ["startTime" : $0.startTime,
                                                      "endTime" : $0.endTime] }
        let friday = self.openHoursFridayday.map{ ["startTime" : $0.startTime,
                                                   "endTime" : $0.endTime] }
        let saturday = self.openHoursSaturday.map{ ["startTime" : $0.startTime,
                                                    "endTime" : $0.endTime] }
        let sunday = self.openHoursSunday.map{ ["startTime" : $0.startTime,
                                                "endTime" : $0.endTime] }
        let publicHoliday = self.openHoursPublicHoliday.map{ ["startTime" : $0.startTime,
                                                                 "endTime" : $0.endTime] }
        
        let openHours: [String: Any] = ["everyDay" : everyDay,
                                        "monday" : monday,
                                        "tuesday" : tuesday,
                                        "wednesday" : wednesday,
                                        "thursday" : thursday,
                                        "friday" : friday,
                                        "saturday" : saturday,
                                        "sunday" : sunday,
                                        "specialHoliday" : publicHoliday]
        let holidayWeekData = self.holidayData.filter({$0.type == .week})
        let holidayDayData = self.holidayData.filter({ $0.type == .day })
        
        let holidayWeek = holidayWeekData.map{ ["day" : $0.day, "week" : $0.week] }
        let holidayDay = holidayDayData.map{ ["date" : $0.day] }
        
        let branchData = BranchData(branchName: self.branchNameTextField.text ?? "",
                                    branchDetail: branchDetail,
                                    building: self.buildingTextField.text ?? "",
                                    room: self.roomNOTextField.text ?? "",
                                    floor: self.floorTextField.text ?? "",
                                    village: self.villageTextField.text ?? "",
                                    houseNO: self.houseNOTextField.text ?? "",
                                    villageNO: self.villageNOTextField.text ?? "",
                                    soi: self.soiTextField.text ?? "",
                                    street: self.streetTextField.text ?? "",
                                    subDistrict: self.selectSubDistrict,
                                    district: self.selectDistrict,
                                    province: self.selectProvince,
                                    postCode: self.postCodeTextField.text ?? "",
                                    isDelivery: self.isDeleveryButton.isSelected,
                                    isOnline: self.isDeleveryButton.isSelected,
                                    latitude: self.branchLocation?.latitude ?? 0,
                                    longitude: self.branchLocation?.longitude ?? 0,
                                    email: self.emailTextField.text ?? "",
                                    website: self.websiteTextField.text ?? "",
                                    line: self.lineTextField.text ?? "",
                                    facebook: self.facebookTextField.text ?? "",
                                    instagram: self.instagramTextField.text ?? "",
                                    preOrderMinute: Int(self.orderTextField.text ?? "0") ?? 0,
                                    priceSince: Int(self.priceStartTextField.text ?? "0") ?? 0,
                                    priceTo: Int(self.priceEndTextField.text ?? "0") ?? 0,
                                    branchNO: self.branchNumberTextField.text ?? "",
                                    phoneNumbers: contactPhoneNumbers,
                                    opendays: openHours,
                                    foodImage: foodImages,
                                    menuImages: menuImages,
                                    viewImages: viewImages,
                                    foodCategory: self.selectFoodCategory,
                                    holidayWeeks: holidayWeek,
                                    holidayDays: holidayDay,
                                    branchImageLogoID: self.logoImageID,
                                    branchImageLogoURL: self.logoImageURL,
                                    branchImageBanerID: self.banerImageID,
                                    branchImageBanerURL: self.banerImageURL,
                                    spoonwalkID: self.spoonwalkID)
        if self.isFromDetail {
          self.viewModel.inputs.updateBranchTrigger.onNext(branchData)
        } else {
          self.viewModel.inputs.createBranchTrigger.onNext(branchData)
        }})
      .disposed(by: disposeBag)
    
    selectTimeSubmitButton.rx.tap
    .filter{ [unowned self] in self.validateField() }
    .subscribe(onNext: { [unowned self] _ in
      
      var branchDetail = ""
      if self.branchDetailTextView.text != "branch_detail_placeholder".localized() {
        branchDetail = self.branchDetailTextView.text
      }
      
      let contactPhoneNumbers = self.phoneNumbers.map { ["phoneNumber" : $0] }
      let foodImages = self.branchDrinkImages.map{["imageId" : $0.imageID,
                                              "imageUrl" : $0.imageURL] }
      let menuImages = self.branchMenuImages.map{["imageId" : $0.imageID,
                                             "imageUrl" : $0.imageURL] }
      let viewImages = self.branchImages.map { ["imageId" : $0.imageID,
                                                "imageUrl" : $0.imageURL] }
      
      
      
      let everyDay = self.openHoursEveryday.map{ ["startTime" : $0.startTime,
                                                  "endTime" : $0.endTime] }
      let monday = self.openHoursMonday.map{ ["startTime" : $0.startTime,
                                              "endTime" : $0.endTime] }
      let tuesday = self.openHoursTuesday.map{ ["startTime" : $0.startTime,
                                                "endTime" : $0.endTime] }
      let wednesday = self.openHoursWednesday.map{ ["startTime" : $0.startTime,
                                                    "endTime" : $0.endTime] }
      let thursday = self.openHoursThursday.map{ ["startTime" : $0.startTime,
                                                    "endTime" : $0.endTime] }
      let friday = self.openHoursFridayday.map{ ["startTime" : $0.startTime,
                                                 "endTime" : $0.endTime] }
      let saturday = self.openHoursSaturday.map{ ["startTime" : $0.startTime,
                                                  "endTime" : $0.endTime] }
      let sunday = self.openHoursSunday.map{ ["startTime" : $0.startTime,
                                              "endTime" : $0.endTime] }
      let publicHoliday = self.openHoursPublicHoliday.map{ ["startTime" : $0.startTime,
                                                               "endTime" : $0.endTime] }
      
      let openHours: [String: Any] = ["everyDay" : everyDay,
                                      "monday" : monday,
                                      "tuesday" : tuesday,
                                      "wednesday" : wednesday,
                                      "thursday" : thursday,
                                      "friday" : friday,
                                      "saturday" : saturday,
                                      "sunday" : sunday,
                                      "specialHoliday" : publicHoliday]
      let holidayWeekData = self.holidayData.filter({$0.type == .week})
      let holidayDayData = self.holidayData.filter({ $0.type == .day })
      
      let holidayWeek = holidayWeekData.map{ ["day" : $0.day, "week" : $0.week] }
      let holidayDay = holidayDayData.map{ ["date" : $0.day] }
      
      let branchData = BranchData(branchName: self.branchNameTextField.text ?? "",
                                  branchDetail: branchDetail,
                                  building: self.buildingTextField.text ?? "",
                                  room: self.roomNOTextField.text ?? "",
                                  floor: self.floorTextField.text ?? "",
                                  village: self.villageTextField.text ?? "",
                                  houseNO: self.houseNOTextField.text ?? "",
                                  villageNO: self.villageNOTextField.text ?? "",
                                  soi: self.soiTextField.text ?? "",
                                  street: self.streetTextField.text ?? "",
                                  subDistrict: self.selectSubDistrict,
                                  district: self.selectDistrict,
                                  province: self.selectProvince,
                                  postCode: self.postCodeTextField.text ?? "",
                                  isDelivery: self.isDeleveryButton.isSelected,
                                  isOnline: self.isDeleveryButton.isSelected,
                                  latitude: self.branchLocation?.latitude ?? 0,
                                  longitude: self.branchLocation?.longitude ?? 0,
                                  email: self.emailTextField.text ?? "",
                                  website: self.websiteTextField.text ?? "",
                                  line: self.lineTextField.text ?? "",
                                  facebook: self.facebookTextField.text ?? "",
                                  instagram: self.instagramTextField.text ?? "",
                                  preOrderMinute: Int(self.orderTextField.text ?? "0") ?? 0,
                                  priceSince: Int(self.priceStartTextField.text ?? "0") ?? 0,
                                  priceTo: Int(self.priceEndTextField.text ?? "0") ?? 0,
                                  branchNO: self.branchNumberTextField.text ?? "",
                                  phoneNumbers: contactPhoneNumbers,
                                  opendays: openHours,
                                  foodImage: foodImages,
                                  menuImages: menuImages,
                                  viewImages: viewImages,
                                  foodCategory: self.selectFoodCategory,
                                  holidayWeeks: holidayWeek,
                                  holidayDays: holidayDay,
                                  branchImageLogoID: self.logoImageID,
                                  branchImageLogoURL: self.logoImageURL,
                                  branchImageBanerID: self.banerImageID,
                                  branchImageBanerURL: self.banerImageURL,
                                  spoonwalkID: self.spoonwalkID)
      if self.isFromDetail {
        self.viewModel.inputs.updateBranchTrigger.onNext(branchData)
      } else {
        self.viewModel.inputs.createBranchTrigger.onNext(branchData)
      }})
    .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayCreateBranchSuccess
      .subscribe(onNext: { [unowned self] _ in
        Helper.Alert(view: self, title: "branch_create_success_title".localized(),
                     message: "branch_create_success_message".localized(),
                     confirmText: "ok".localized()) { [unowned self] (_) in
                      self.viewModel.coordinates.navigateBack.onNext(())
        }
      })
      .disposed(by: disposeBag)
    
    banerButton.rx.tap.subscribe(onNext: { [unowned self] _ in
      self.present(self.banerImagePicker, animated: true, completion: nil)})
      .disposed(by: disposeBag)
    
    //MARK: Edit
    
    viewModel.outputs
      .isFromDetail
      .map{$0}
      .filterNil()
      .subscribe(onNext: { _ in
        self.isFromDetail = true
        self.isView = true
        self.submitButton.isHidden = true
        self.selectTimeSubmitButton.isHidden = true
        self.brandAddressSwitch.isUserInteractionEnabled = false
        self.brandContactSwitch.isUserInteractionEnabled = false
        self.editButton.isHidden = false
        self.selectTimeEditButton.isHidden = false
//        self.deleteButton.isHidden = false
//        self.deleteButton.isUserInteractionEnabled = false
//        let deleteUnderline: [NSAttributedString.Key: Any] = [
//          .underlineStyle: NSUnderlineStyle.single.rawValue,
//          .font : UIFont(name: "Kanit-SemiBold", size: 15)!,
//          .foregroundColor : UIColor(hex: 0xD7DAE2)]
//        let deleteAttributeString = NSMutableAttributedString(string: "branch_delete".localized(),
//                                                        attributes: deleteUnderline)
//        self.deleteButton.setAttributedTitle(deleteAttributeString, for: .normal)
        self.deleteButton.isHidden = true
        self.selectTimeDeleteButton.isHidden = true
//        self.selectTimeDeleteButton.isUserInteractionEnabled = false
//        self.selectTimeDeleteButton.setAttributedTitle(deleteAttributeString, for: .normal)
        self.disableField()
        self.viewModel.inputs.getBranchTrigger.onNext(())})
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayBranchData
      .subscribe(onNext: { [unowned self] model in
        self.spoonwalkID = model.spoonWalkID
        self.branchEditData = model
        self.logoImageID = model.branchImageProfileID
        self.logoImageURL = model.branchImageProfileUrl
        self.banerImageID = model.branchImageCoverID
        self.banerImageURL = model.branchImageCoverUrl
        let logoImageURL = URL(string: model.branchImageProfileUrl)
        let banerImageURL = URL(string: model.branchImageCoverUrl)
        self.logoImgaeView.kf.setImage(with: logoImageURL, placeholder: UIImage(named: "ic_default_image"))
        self.banerImageView.kf.setImage(with: banerImageURL, placeholder: UIImage(named: "ic_default_banner"))
        if model.hasDelivery || model.onlineOnly {
          self.isDeleveryButton.isSelected = true
        } else {
          self.isDeleveryButton.isSelected = false
        }
        self.branchNameTextField.text = model.branchName
        self.branchNumberTextField.text = model.branchNo
        if model.branchDetail != "" {
          self.branchDetailTextView.text = model.branchDetail
          self.branchDetailTextView.textColor = .black
        }
        
        self.buildingTextField.text = model.building
        self.roomNOTextField.text = model.roomNumber
        self.floorTextField.text = model.floor
        self.villageTextField.text = model.villageName
        self.houseNOTextField.text = model.placeNumber
        self.villageNOTextField.text = model.moo
        self.soiTextField.text = model.placeAlley
        self.streetTextField.text = model.placeRoad
        self.postCodeTextField.text = model.zipcode
        
        let location = CLLocationCoordinate2D(latitude: model.placeLatitude,
                                              longitude: model.placeLongitude)
        self.branchLocation = location
        self.setupAnnotation(location: location)
        
        self.phoneNumbers = model.telephones.map{ $0.phoneNumber }
        self.phoneTokenField.reloadData()
        
        self.emailTextField.text = model.email
        self.websiteTextField.text = model.websiteUrl
        self.lineTextField.text = model.lineId
        self.facebookTextField.text = model.facebookName
        self.instagramTextField.text = model.instagramName
        
        if model.openDays.everyDay.count > 0 {
          self.everydayButton.isSelected = true
          self.openHoursEveryday = model.openDays.everyDay.map{ OpenHourData(startTime: $0.timeStart,
                                                                             endTime: $0.timeClose) }
          self.openHoursMonday = self.openHoursEveryday
          self.openHoursTuesday = self.openHoursEveryday
          self.openHoursWednesday = self.openHoursEveryday
          self.openHoursThursday = self.openHoursEveryday
          self.openHoursFridayday = self.openHoursEveryday
          self.openHoursSaturday = self.openHoursEveryday
          self.openHoursSunday = self.openHoursEveryday
          self.openHoursPublicHoliday = self.openHoursEveryday
          
          self.everydayAddButton.isSelected = true
          self.everydayAddButton.isUserInteractionEnabled = false
          self.viewModel.inputs.openHoursEverydayTrigger.onNext(self.openHoursEveryday)
          
          self.mondayButton.isSelected = true
          self.mondayButton.isUserInteractionEnabled = false
          self.mondayAddButton.isSelected = false
          self.mondayAddButton.isUserInteractionEnabled = false
          self.viewModel.inputs.openHoursMondayTrigger.onNext(self.openHoursMonday)
          self.mondayCollectionVew.isUserInteractionEnabled = false
          
          self.tuesdayButton.isSelected = true
          self.tuesdayButton.isUserInteractionEnabled = false
          self.tuesdayAddButton.isSelected = false
          self.tuesdayAddButton.isUserInteractionEnabled = false
          self.viewModel.inputs.openHoursTuesdayTrigger.onNext(self.openHoursTuesday)
          self.tuesdayCollectionView.isUserInteractionEnabled = false
          
          self.wednesdayButton.isSelected = true
          self.wednesdayButton.isUserInteractionEnabled = false
          self.wednesdayAddButton.isSelected = false
          self.wednesdayAddButton.isUserInteractionEnabled = false
          self.viewModel.inputs.openHoursWednesdayTrigger.onNext(self.openHoursWednesday)
          self.wednesdayCollectionView.isUserInteractionEnabled = false
          
          self.thursdayButton.isSelected = true
          self.thursdayButton.isUserInteractionEnabled = false
          self.thursdayAddButton.isSelected = false
          self.thursdayAddButton.isUserInteractionEnabled = false
          self.viewModel.inputs.openHoursThursdayTrigger.onNext(self.openHoursThursday)
          self.thursdayCollectionView.isUserInteractionEnabled = false
          
          self.fridayButton.isSelected = true
          self.fridayButton.isUserInteractionEnabled = false
          self.fridayAddButton.isSelected = false
          self.fridayAddButton.isUserInteractionEnabled = false
          self.viewModel.inputs.openHoursFridayTrigger.onNext(self.openHoursFridayday)
          self.fridayCollectionView.isUserInteractionEnabled = false
          
          self.saturdayButton.isSelected = true
          self.saturdayButton.isUserInteractionEnabled = false
          self.saturdayAddButton.isSelected = false
          self.saturdayAddButton.isUserInteractionEnabled = false
          self.viewModel.inputs.openHoursSaturdayTrigger.onNext(self.openHoursSaturday)
          self.saturdayCollectionView.isUserInteractionEnabled = false
          
          self.sundayButton.isSelected = true
          self.sundayButton.isUserInteractionEnabled = false
          self.sundayAddButton.isSelected = false
          self.sundayAddButton.isUserInteractionEnabled = false
          self.viewModel.inputs.openHoursSundayTrigger.onNext(self.openHoursSunday)
          self.sundayCollectionView.isUserInteractionEnabled = false
          
          self.publicHolidayButton.isSelected = true
          self.publicHolidayButton.isUserInteractionEnabled = false
          self.publicHolidayAddButton.isSelected = false
          self.publicHolidayAddButton.isUserInteractionEnabled = false
          self.viewModel.inputs.openHoursPublicHolidayTrigger.onNext(self.openHoursPublicHoliday)
          self.publicHolidayCollectionView.isUserInteractionEnabled = false
        } else {
          if model.openDays.monday.count > 0 {
            self.openHoursMonday = model.openDays.monday.map{OpenHourData(startTime: $0.timeStart,
                                                                          endTime: $0.timeClose)}
            self.mondayButton.isSelected = true
            self.mondayAddButton.isSelected = true
            self.viewModel.inputs.openHoursMondayTrigger.onNext(self.openHoursMonday)
          }
          
          if model.openDays.tuesday.count > 0 {
            self.openHoursTuesday = model.openDays.tuesday.map{OpenHourData(startTime: $0.timeStart,
                                                                          endTime: $0.timeClose)}
            self.tuesdayButton.isSelected = true
            self.tuesdayAddButton.isSelected = true
            self.viewModel.inputs.openHoursTuesdayTrigger.onNext(self.openHoursTuesday)
          }
          
          if model.openDays.wednesday.count > 0 {
            self.openHoursWednesday = model.openDays.wednesday.map{OpenHourData(startTime: $0.timeStart,
                                                                                endTime: $0.timeClose)}
            self.wednesdayButton.isSelected = true
            self.wednesdayAddButton.isSelected = true
            self.viewModel.inputs.openHoursWednesdayTrigger.onNext(self.openHoursWednesday)
          }
          
          if model.openDays.thursday.count > 0 {
            self.openHoursThursday = model.openDays.thursday.map{OpenHourData(startTime: $0.timeStart,
                                                                                endTime: $0.timeClose)}
            self.thursdayButton.isSelected = true
            self.thursdayAddButton.isSelected = true
            self.viewModel.inputs.openHoursThursdayTrigger.onNext(self.openHoursThursday)
          }
          
          if model.openDays.friday.count > 0 {
            self.openHoursFridayday = model.openDays.friday.map{OpenHourData(startTime: $0.timeStart,
                                                                                endTime: $0.timeClose)}
            self.fridayButton.isSelected = true
            self.fridayAddButton.isSelected = true
            self.viewModel.inputs.openHoursFridayTrigger.onNext(self.openHoursFridayday)
          }
          
          if model.openDays.saturday.count > 0 {
            self.openHoursSaturday = model.openDays.saturday.map{OpenHourData(startTime: $0.timeStart,
                                                                                endTime: $0.timeClose)}
            self.saturdayButton.isSelected = true
            self.saturdayAddButton.isSelected = true
            self.viewModel.inputs.openHoursSaturdayTrigger.onNext(self.openHoursSaturday)
          }
          
          if model.openDays.sunday.count > 0 {
            self.openHoursSunday = model.openDays.sunday.map{OpenHourData(startTime: $0.timeStart,
                                                                          endTime: $0.timeClose)}
            self.sundayButton.isSelected = true
            self.sundayAddButton.isSelected = true
            self.viewModel.inputs.openHoursSundayTrigger.onNext(self.openHoursSunday)
          }
          
          if model.openDays.specialHoliday.count > 0 {
            self.openHoursPublicHoliday = model.openDays.specialHoliday.map{OpenHourData(startTime: $0.timeStart,
                                                                                         endTime: $0.timeClose)}
            self.publicHolidayButton.isSelected = true
            self.publicHolidayAddButton.isSelected = true
            self.viewModel.inputs.openHoursPublicHolidayTrigger.onNext(self.openHoursPublicHoliday)
          }
        }
        
        self.selectFoodCategory = model.foodCategorys.map{ ["categoryName" : $0.categoryName] }
        self.orderTextField.text = model.preOrderMinute
        self.priceStartTextField.text = "\(model.priceSince)"
        self.priceEndTextField.text = "\(model.priceTo)"
        
        self.branchDrinkImages = model.foodImgs.map{BranchImageData(imageID: $0.branchImageId,
                                                                    imageURL: $0.branchImageUrl)}
        self.viewModel.inputs.getDrinkImageTrigger.onNext((imageDatas: self.branchDrinkImages, isView: self.isView))
        
        self.branchMenuImages = model.menuImgs.map{ BranchImageData(imageID: $0.branchImageId,
                                                                    imageURL: $0.branchImageUrl) }
        self.viewModel.inputs.getMenuImageTrigger.onNext((imageDatas: self.branchMenuImages, isView: self.isView))
        self.branchImages = model.viewImgs.map { BranchImageData(imageID: $0.branchImageId,
                                                                 imageURL: $0.branchImageUrl) }
        self.viewModel.inputs.getBranchImageTrigger.onNext((imageDatas: self.branchImages, isView: self.isView))
        
        if model.holidayDates.count > 0 {
          self.holidayWeekButton.isSelected = true
          self.noHolidayButton.isSelected = false
          self.holidayData = model.holidayDates.map{ HolidayData(day: $0.day, week: $0.week, type: .week) }
          self.viewModel.inputs.holidayTrigger.onNext(self.holidayData)
          self.holidayAddButtonHeightConstraint.constant = 44
          self.holidayViewHeightConstraint.constant = CGFloat(model.holidayDates.count * 80)
        }
        if model.holidayDays.count > 0 {
          self.holidayDateButton.isSelected = true
          self.noHolidayButton.isSelected = false
          self.holidayData = model.holidayDays.map{ HolidayData(day: $0.date, week: "", type: .day) }
          self.holidayAddButtonHeightConstraint.constant = 44
          self.viewModel.inputs.holidayTrigger.onNext(self.holidayData)
          self.holidayViewHeightConstraint.constant = CGFloat(model.holidayDays.count * 80)
        }
        
        guard let province = LocalDataService.getProvince()?.list.filter({$0.provinceID == model.placeProvince}).first else { return }
        guard let district = LocalDataService.getDistrict()?.list.filter({$0.districtID == model.placeDistrict}).first else { return }
        guard let subDistrict = LocalDataService.getSubDistrict()?.list.filter({$0.subDistricID == model.placeArea}).first else { return }
        self.selectProvince = model.placeProvince
        self.selectDistrict = model.placeDistrict
        self.selectSubDistrict = model.placeArea
        self.subDistrictTextField.text = subDistrict.nameTH
        self.districtTextField.text = district.nameTH
        self.provinceTextField.text = province.nameTH

      })
      .disposed(by: disposeBag)
    
    editButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.isView = false
        self.editButton.isHidden = true
        self.selectTimeEditButton.isHidden = true
        let deleteUnderline: [NSAttributedString.Key: Any] = [
          .underlineStyle: NSUnderlineStyle.single.rawValue,
          .font : UIFont(name: "Kanit-SemiBold", size: 15)!,
          .foregroundColor : UIColor(hex: 0xF2503E)]
        let deleteAttributeString = NSMutableAttributedString(string: "branch_delete".localized(),
                                                        attributes: deleteUnderline)
        self.deleteButton.setAttributedTitle(deleteAttributeString, for: .normal)
        self.deleteButton.isUserInteractionEnabled = true
        self.deleteButton.isSelected = true
        self.deleteButton.isHidden = false
        
        let selectTimeDeleteButton: [NSAttributedString.Key: Any] = [
          .underlineStyle: NSUnderlineStyle.single.rawValue,
          .font : UIFont(name: "Kanit-SemiBold", size: 15)!,
          .foregroundColor : UIColor(hex: 0xF2503E)]
        let selectTimedeleteAttributeString = NSMutableAttributedString(string: "branch_delete".localized(),
                                                        attributes: selectTimeDeleteButton)
        self.selectTimeDeleteButton.setAttributedTitle(selectTimedeleteAttributeString, for: .normal)
        self.selectTimeDeleteButton.isUserInteractionEnabled = true
        self.selectTimeDeleteButton.isSelected = true
        self.selectTimeDeleteButton.isHidden = false
        self.enableField()
//        self.branchNameTextField.becomeFirstResponder()
        self.submitButton.isHidden = false
        self.selectTimeSubmitButton.isHidden = false
      })
      .disposed(by: disposeBag)
    
    selectTimeEditButton.rx.tap
          .subscribe(onNext: { [unowned self] _ in
            self.isView = false
            self.editButton.isHidden = true
            self.selectTimeEditButton.isHidden = true
            let deleteUnderline: [NSAttributedString.Key: Any] = [
              .underlineStyle: NSUnderlineStyle.single.rawValue,
              .font : UIFont(name: "Kanit-SemiBold", size: 15)!,
              .foregroundColor : UIColor(hex: 0xF2503E)]
            let deleteAttributeString = NSMutableAttributedString(string: "branch_delete".localized(),
                                                            attributes: deleteUnderline)
            self.deleteButton.setAttributedTitle(deleteAttributeString, for: .normal)
            self.deleteButton.isUserInteractionEnabled = true
            self.deleteButton.isSelected = true
            self.deleteButton.isHidden = false
            
            let selectTimeDeleteButton: [NSAttributedString.Key: Any] = [
              .underlineStyle: NSUnderlineStyle.single.rawValue,
              .font : UIFont(name: "Kanit-SemiBold", size: 15)!,
              .foregroundColor : UIColor(hex: 0xF2503E)]
            let selectTimedeleteAttributeString = NSMutableAttributedString(string: "branch_delete".localized(),
                                                            attributes: selectTimeDeleteButton)
            self.selectTimeDeleteButton.setAttributedTitle(selectTimedeleteAttributeString, for: .normal)
            self.selectTimeDeleteButton.isUserInteractionEnabled = true
            self.selectTimeDeleteButton.isSelected = true
            self.selectTimeDeleteButton.isHidden = false
            self.enableField()
    //        self.branchNameTextField.becomeFirstResponder()
            self.submitButton.isHidden = false
            self.selectTimeSubmitButton.isHidden = false
          })
          .disposed(by: disposeBag)
    
    viewModel.outputs
      .displayUpdateBranchSuccess
      .subscribe(onNext: { [unowned self] _ in
        Helper.Alert(view: self,
                     title: "branch_update_success_title".localized(),
                     message: "branch_update_success_message".localized(),
                     confirmText: "ok".localized()) { (_) in
                      self.viewModel.coordinates.navigateBack.onNext(())
        }
      })
      .disposed(by: disposeBag)
    
    //MARK: Delete
    
    deleteButton.rx.tap
      .subscribe(onNext: { _ in
        Helper.AlertConfirm(view: self, title: "branch_delete_title".localized(), message: "branch_delete_message".localized(), confirmText: "ok".localized(), cancelText: "cancel".localized(), isInverseButtonPos: true, confirmCompletion: { [unowned self] (_) in
          self.viewModel.coordinates.navigateToVerifyPhoneNumber.onNext(())
        }, cancelCompletion: nil)
      })
      .disposed(by: disposeBag)
    
    selectTimeDeleteButton.rx.tap
    .subscribe(onNext: { _ in
      Helper.AlertConfirm(view: self, title: "branch_delete_title".localized(), message: "branch_delete_message".localized(), confirmText: "ok".localized(), cancelText: "cancel".localized(), isInverseButtonPos: true, confirmCompletion: { [unowned self] (_) in
        self.viewModel.coordinates.navigateToVerifyPhoneNumber.onNext(())
      }, cancelCompletion: nil)
    })
    .disposed(by: disposeBag)
    
    viewModel.outputs
      .displayDeleteBranchSuccess
      .subscribe(onNext: { _ in
        Helper.Alert(view: self, title: "branch_delete_success_title".localized(), message: "branch_delete_success_message".localized(), confirmText: "ok".localized()) { (_) in
          self.viewModel.coordinates.navigateBack.onNext(())
        }
      })
      .disposed(by: disposeBag)
    
    //MARK: Bind loading
       
    viewModel.outputs.loading
      .subscribe(onNext: { [unowned self] isLoading in
        guard let tabBarController = self.tabBarController else { return }
        LoadingActivityView.action(in: tabBarController.view, isShow: isLoading)})
      .disposed(by: disposeBag)
    
    // MARK: Bind Error
          
    viewModel.error
      .subscribe(onNext: { [unowned self] error in
          Helper.Alert(
            view: self,
              title: error.title,
              message: error.message,
              confirmText: "done".localized())})
      .disposed(by: disposeBag)
  }
  
  func setupUI() {
    if let collectionViewLayout = menuCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      collectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      collectionViewLayout.scrollDirection = .horizontal
    }
    informationBackgroundView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
    openHoursBackgroundView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
    
    logoImagePicker.sourceType = .photoLibrary
    logoImagePicker.delegate = self
    
    banerImagePicker.sourceType = .photoLibrary
    banerImagePicker.delegate = self
    
    foodImagePicker.sourceType = .photoLibrary
    foodImagePicker.delegate = self
    
    menuImagePicker.sourceType = .photoLibrary
    menuImagePicker.delegate = self
    
    phoneTokenField.dataSource = self;
    phoneTokenField.delegate = self;
    let phoneNumberlicKeyboard = MMNumberKeyboard(frame: .zero)
    phoneNumberlicKeyboard.allowsDecimalPoint = false
    phoneTokenField.textField.inputView = phoneNumberlicKeyboard
    phoneTokenField.textField.layer.masksToBounds = false
    phoneTokenField.textField.font = UIFont(name: "Kanit-Regular", size: 15)
    phoneTokenField.reloadData()
    
    branchNameTextField.layer.cornerRadius = 10
    branchNameTextField.layer.borderWidth = 1
    branchNameTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    branchNameTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: branchNameTextField.frame.height))
    branchNameTextField.leftViewMode = .always
    branchNameTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: branchNameTextField.frame.height))
    branchNameTextField.rightViewMode = .always
    
    branchNumberTextField.layer.cornerRadius = 10
    branchNumberTextField.layer.borderWidth = 1
    branchNumberTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    branchNumberTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: branchNumberTextField.frame.height))
    branchNumberTextField.leftViewMode = .always
    branchNumberTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: branchNumberTextField.frame.height))
    branchNumberTextField.rightViewMode = .always
    
    branchDetailView.layer.cornerRadius = 10
    branchDetailView.layer.borderWidth = 1
    branchDetailView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    
    mapView.layer.cornerRadius = 10
    mapView.layer.borderWidth = 1
    mapView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    
    phoneView.layer.cornerRadius = 10
    phoneView.layer.borderWidth = 1
    phoneView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    
    buildingTextField.layer.cornerRadius = 10
    buildingTextField.layer.borderWidth = 1
    buildingTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    buildingTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: buildingTextField.frame.height))
    buildingTextField.leftViewMode = .always
    buildingTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: buildingTextField.frame.height))
    buildingTextField.rightViewMode = .always
    
    roomNOTextField.layer.cornerRadius = 10
    roomNOTextField.layer.borderWidth = 1
    roomNOTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    roomNOTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: roomNOTextField.frame.height))
    roomNOTextField.leftViewMode = .always
    roomNOTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: roomNOTextField.frame.height))
    roomNOTextField.rightViewMode = .always
    
    floorTextField.layer.cornerRadius = 10
    floorTextField.layer.borderWidth = 1
    floorTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    floorTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: floorTextField.frame.height))
    floorTextField.leftViewMode = .always
    floorTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: floorTextField.frame.height))
    floorTextField.rightViewMode = .always
    
    villageTextField.layer.cornerRadius = 10
    villageTextField.layer.borderWidth = 1
    villageTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    villageTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: villageTextField.frame.height))
    villageTextField.leftViewMode = .always
    villageTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: villageTextField.frame.height))
    villageTextField.rightViewMode = .always
    
    houseNOTextField.layer.cornerRadius = 10
    houseNOTextField.layer.borderWidth = 1
    houseNOTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    houseNOTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: houseNOTextField.frame.height))
    houseNOTextField.leftViewMode = .always
    houseNOTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: houseNOTextField.frame.height))
    houseNOTextField.rightViewMode = .always
    
    villageNOTextField.layer.cornerRadius = 10
    villageNOTextField.layer.borderWidth = 1
    villageNOTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    villageNOTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: villageNOTextField.frame.height))
    villageNOTextField.leftViewMode = .always
    villageNOTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: villageNOTextField.frame.height))
    villageNOTextField.rightViewMode = .always
    
    soiTextField.layer.cornerRadius = 10
    soiTextField.layer.borderWidth = 1
    soiTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    soiTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: soiTextField.frame.height))
    soiTextField.leftViewMode = .always
    soiTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: soiTextField.frame.height))
    soiTextField.rightViewMode = .always
    
    streetTextField.layer.cornerRadius = 10
    streetTextField.layer.borderWidth = 1
    streetTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    streetTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: streetTextField.frame.height))
    streetTextField.leftViewMode = .always
    streetTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: streetTextField.frame.height))
    streetTextField.rightViewMode = .always
    
    subDistrictTextField.layer.cornerRadius = 10
    subDistrictTextField.layer.borderWidth = 1
    subDistrictTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    subDistrictTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: subDistrictTextField.frame.height))
    subDistrictTextField.leftViewMode = .always
    subDistrictTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: subDistrictTextField.frame.height))
    subDistrictTextField.rightViewMode = .always
    
    districtTextField.layer.cornerRadius = 10
    districtTextField.layer.borderWidth = 1
    districtTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    districtTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: districtTextField.frame.height))
    districtTextField.leftViewMode = .always
    districtTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: districtTextField.frame.height))
    districtTextField.rightViewMode = .always
    
    provinceTextField.layer.cornerRadius = 10
    provinceTextField.layer.borderWidth = 1
    provinceTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    provinceTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: provinceTextField.frame.height))
    provinceTextField.leftViewMode = .always
    provinceTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: provinceTextField.frame.height))
    provinceTextField.rightViewMode = .always
    
    postCodeTextField.layer.cornerRadius = 10
    postCodeTextField.layer.borderWidth = 1
    postCodeTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    postCodeTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: postCodeTextField.frame.height))
    postCodeTextField.leftViewMode = .always
    postCodeTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: postCodeTextField.frame.height))
    postCodeTextField.rightViewMode = .always
    
    websiteTextField.layer.cornerRadius = 10
    websiteTextField.layer.borderWidth = 1
    websiteTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    websiteTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: websiteTextField.frame.height))
    websiteTextField.leftViewMode = .always
    websiteTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: websiteTextField.frame.height))
    websiteTextField.rightViewMode = .always
    
    emailTextField.layer.cornerRadius = 10
    emailTextField.layer.borderWidth = 1
    emailTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    emailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: emailTextField.frame.height))
    emailTextField.leftViewMode = .always
    emailTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: emailTextField.frame.height))
    emailTextField.rightViewMode = .always
    
    facebookTextField.layer.cornerRadius = 10
    facebookTextField.layer.borderWidth = 1
    facebookTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    facebookTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: facebookTextField.frame.height))
    facebookTextField.leftViewMode = .always
    facebookTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: facebookTextField.frame.height))
    facebookTextField.rightViewMode = .always
    
    lineTextField.layer.cornerRadius = 10
    lineTextField.layer.borderWidth = 1
    lineTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    lineTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: lineTextField.frame.height))
    lineTextField.leftViewMode = .always
    lineTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: lineTextField.frame.height))
    lineTextField.rightViewMode = .always
    
    instagramTextField.layer.cornerRadius = 10
    instagramTextField.layer.borderWidth = 1
    instagramTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    instagramTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: instagramTextField.frame.height))
    instagramTextField.leftViewMode = .always
    instagramTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet, height: instagramTextField.frame.height))
    instagramTextField.rightViewMode = .always
    
    orderTextField.layer.cornerRadius = 10
    orderTextField.layer.borderWidth = 1
    orderTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    orderTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet - 6, height: orderTextField.frame.height))
    orderTextField.leftViewMode = .always
    orderTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet - 6, height: orderTextField.frame.height))
    orderTextField.rightViewMode = .always
    let orderNumberlicKeyboard = MMNumberKeyboard(frame: .zero)
    orderNumberlicKeyboard.allowsDecimalPoint = false
    orderTextField.inputView = orderNumberlicKeyboard
    
    priceStartTextField.layer.cornerRadius = 10
    priceStartTextField.layer.borderWidth = 1
    priceStartTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    priceStartTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet - 6, height: priceStartTextField.frame.height))
    priceStartTextField.leftViewMode = .always
    priceStartTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet - 6, height: priceStartTextField.frame.height))
    priceStartTextField.rightViewMode = .always
    let priceStartNumberlicKeyboard = MMNumberKeyboard(frame: .zero)
    priceStartNumberlicKeyboard.allowsDecimalPoint = false
    priceStartTextField.inputView = priceStartNumberlicKeyboard
    
    priceEndTextField.layer.cornerRadius = 10
    priceEndTextField.layer.borderWidth = 1
    priceEndTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    priceEndTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet - 6, height: priceEndTextField.frame.height))
    priceEndTextField.leftViewMode = .always
    priceEndTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: textFieldOffSet - 6, height: priceEndTextField.frame.height))
    priceEndTextField.rightViewMode = .always

//    priceEndNumberlicKeyboard.delegate = self
    priceEndNumberlicKeyboard.allowsDecimalPoint = false
    priceEndTextField.inputView = priceEndNumberlicKeyboard
    priceEndTextField.delegate = self
    
    submitButton.layer.cornerRadius = submitButton.frame.height / 2
    selectTimeSubmitButton.layer.cornerRadius = submitButton.frame.height / 2
    mapView.delegate = self
    mapView.showsUserLocation = true
    
    everydayCollectionView.register(UINib(nibName: OpenHourCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: OpenHourCollectionViewCell.className)
    if let everyDayCollectionViewLayout = everydayCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      everyDayCollectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      everyDayCollectionViewLayout.scrollDirection = .horizontal
    }
    
    mondayCollectionVew.register(UINib(nibName: OpenHourCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: OpenHourCollectionViewCell.className)
    if let everyDayCollectionViewLayout = mondayCollectionVew.collectionViewLayout as? UICollectionViewFlowLayout {
      everyDayCollectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      everyDayCollectionViewLayout.scrollDirection = .horizontal
    }
    
    tuesdayCollectionView.register(UINib(nibName: OpenHourCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: OpenHourCollectionViewCell.className)
    if let everyDayCollectionViewLayout = tuesdayCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      everyDayCollectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      everyDayCollectionViewLayout.scrollDirection = .horizontal
    }
    
    wednesdayCollectionView.register(UINib(nibName: OpenHourCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: OpenHourCollectionViewCell.className)
    if let everyDayCollectionViewLayout = wednesdayCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      everyDayCollectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      everyDayCollectionViewLayout.scrollDirection = .horizontal
    }
    
    thursdayCollectionView.register(UINib(nibName: OpenHourCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: OpenHourCollectionViewCell.className)
    if let everyDayCollectionViewLayout = thursdayCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      everyDayCollectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      everyDayCollectionViewLayout.scrollDirection = .horizontal
    }
    
    fridayCollectionView.register(UINib(nibName: OpenHourCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: OpenHourCollectionViewCell.className)
    if let everyDayCollectionViewLayout = fridayCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      everyDayCollectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      everyDayCollectionViewLayout.scrollDirection = .horizontal
    }
    
    saturdayCollectionView.register(UINib(nibName: OpenHourCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: OpenHourCollectionViewCell.className)
    if let everyDayCollectionViewLayout = saturdayCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      everyDayCollectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      everyDayCollectionViewLayout.scrollDirection = .horizontal
    }
    
    sundayCollectionView.register(UINib(nibName: OpenHourCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: OpenHourCollectionViewCell.className)
    if let everyDayCollectionViewLayout = sundayCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      everyDayCollectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      everyDayCollectionViewLayout.scrollDirection = .horizontal
    }
    
    publicHolidayCollectionView.register(UINib(nibName: OpenHourCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: OpenHourCollectionViewCell.className)
    if let everyDayCollectionViewLayout = publicHolidayCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      everyDayCollectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      everyDayCollectionViewLayout.scrollDirection = .horizontal
    }
    
    holidayTableView.register(UINib(nibName: HolidayTableViewCell.className, bundle: nil),
                              forCellReuseIdentifier: HolidayTableViewCell.className)
    holidayTableView.register(UINib(nibName: HolidayDateTableViewCell.className, bundle: nil),
                              forCellReuseIdentifier: HolidayDateTableViewCell.className)
    
    noHolidayButton.isSelected = true
    
    foodCategoryView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
    foodCategoryTableOneTableView.register(UINib(nibName: FoodCategoryGenreTableViewCell.className, bundle: nil), forCellReuseIdentifier: FoodCategoryGenreTableViewCell.className)
    foodCategoryTableOneView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
    foodCategoryTableTwoTableView.register(UINib(nibName: FoodCategoryTableViewCell.className, bundle: nil), forCellReuseIdentifier: FoodCategoryTableViewCell.className)
    foodCategoryTableTwoView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
    
    foodCategoryTableThreeTableView.register(UINib(nibName: FoodCategoryTableViewCell.className, bundle: nil), forCellReuseIdentifier: FoodCategoryTableViewCell.className)
    foodCategoryTableThreeView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
    
    branchImageCollectionView.register(UINib(nibName: BranchImageCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: BranchImageCollectionViewCell.className)
    branchImageCollectionView.register(UINib(nibName: BranchAddImageCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: BranchAddImageCollectionViewCell.className)
    
    drinkImageCollectionView.register(UINib(nibName: BranchDrinkImageCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: BranchDrinkImageCollectionViewCell.className)
    drinkImageCollectionView.register(UINib(nibName: BranchDrinkAddImageCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: BranchDrinkAddImageCollectionViewCell.className)
    
    menuImageCollectionView.register(UINib(nibName: BranchMenuImageCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: BranchMenuImageCollectionViewCell.className)
    menuImageCollectionView.register(UINib(nibName: BranchMenuAddImageCollectionViewCell.className, bundle: nil), forCellWithReuseIdentifier: BranchMenuAddImageCollectionViewCell.className)
    
    if let branchImageCollectionViewLayout = branchImageCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      branchImageCollectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      branchImageCollectionViewLayout.scrollDirection = .horizontal
    }
    
    if let branchMenuImageCollectionViewLayout = menuImageCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      branchMenuImageCollectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      branchMenuImageCollectionViewLayout.scrollDirection = .horizontal
    }
    
    if let branchDrinkImageCollectionViewLayout = drinkImageCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      branchDrinkImageCollectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      branchDrinkImageCollectionViewLayout.scrollDirection = .horizontal
    }
    
    branchImagePicker.sourceType = .photoLibrary
    branchImagePicker.delegate = self
    
    brandAddressSwitch.onTintColor = UIColor(hex: 0x4DD43B)
    brandAddressSwitch.tintColor = UIColor(hex: 0x8E8E93)
    brandAddressSwitch.layer.cornerRadius = brandAddressSwitch.frame.height / 2
    brandAddressSwitch.backgroundColor = UIColor(hex: 0x8E8E93)
    brandContactSwitch.onTintColor = UIColor(hex: 0x4DD43B)
    brandContactSwitch.tintColor = UIColor(hex: 0x8E8E93)
    brandContactSwitch.layer.cornerRadius = brandAddressSwitch.frame.height / 2
    brandContactSwitch.backgroundColor = UIColor(hex: 0x8E8E93)
    
    brandAddressSwitch.layer.applySketchShadow(color: .black, alpha: 0.5, x: 0, y: 3, blur: 6, spread: 0)
    brandContactSwitch.layer.applySketchShadow(color: .black, alpha: 0.5, x: 0, y: 3, blur: 6, spread: 0)
  }
  
  func validateTime(inputTimes: String) -> Bool {
    let times = inputTimes.split(separator: "-")
    if times.count != 2 {
      return false
    } else {
      let dateFormatter = DateFormatter()
      dateFormatter.dateFormat = "HH:mm"
      if (dateFormatter.date(from: String(times[0])) != nil) && (dateFormatter.date(from: String(times[0])) != nil) {
        return true
      } else {
        return false
      }
    }
  }
  
  func setupL10N() {
    self.setNavigationBarItem(titlePageName: "branch_title".localized(),
                              isBackButton: true)
    detailButton.setTitle("branch_detail".localized(), for: .normal)
    
    logoLabel.text = "branch_logo_title".localized()
    logoDescriptionLabel.text = "branch_logo_description".localized()
    banerLabel.text = "branch_baner_title".localized()
    banerDescriptionLabel.text = "branch_baner_description".localized()
    isDeleveryButton.setTitle("branch_delevery".localized(), for: .normal)
    brandAddressSwitchLabel.text = "branch_address_switch".localized()
    addressLabel.text = "branch_address".localized()
    
    branchNameLabel.text = "branch_name".localized()
    branchNameTextField.placeholder = "branch_name".localized()
    branchNumberLabel.text = "branch_number".localized()
    branchNumberTextField.placeholder = "000000"
    branchDetailLabel.text = "branch_detail".localized()
    branchDetailTextView.text = "branch_detail_placeholder".localized()
    branchDetailTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)
    mapLabel.text = "branch_map".localized()
    
    buildingLabel.text = "branch_building".localized()
    buildingTextField.placeholder = "branch_building".localized()
    
    roomNOLabel.text = "branch_roomno".localized()
    roomNOTextField.placeholder = "branch_roomno".localized()
    
    floorLabel.text = "branch_floor".localized()
    floorTextField.placeholder = "branch_floor".localized()
    
    villageLabel.text = "branch_village".localized()
    villageTextField.placeholder = "branch_village".localized()
    
    houseNOLabel.text = "branch_houseno".localized()
    houseNOTextField.placeholder = "branch_houseno".localized()
    
    villageNOLabel.text = "branch_villageno".localized()
    villageNOTextField.placeholder = "branch_villageno".localized()
    
    soiLabel.text = "branch_soi".localized()
    soiTextField.placeholder = "branch_soi".localized()
    
    streetLabel.text = "branch_street".localized()
    streetTextField.placeholder = "branch_street".localized()
    
    subDistrictLabel.text = "branch_subdistrict".localized()
    subDistrictTextField.placeholder = "branch_subdistrict".localized()
    
    districtLabel.text = "branch_district".localized()
    districtTextField.placeholder = "branch_district".localized()
    
    provinceLabel.text = "branch_province".localized()
    provinceTextField.placeholder = "branch_province".localized()
    
    postCodeLabel.text = "branch_postcode".localized()
    postCodeTextField.placeholder = "branch_postcode".localized()
    
    brandContactSwitchLabel.text = "branch_contact_switch".localized()
    contactLabel.text = "branch_contract".localized()
    
    phoneLabel.text = "branch_phone".localized()
    phoneDescriptionLabel.text = "branch_phone_description".localized()
    phoneTokenField.textField.placeholder = "branch_phone_placeholder".localized()
    
    emailLabel.text = "branch_email".localized()
    emailTextField.placeholder = "branch_email_placeholder".localized()
    
    lineLabel.text = "branch_line".localized()
    lineTextField.placeholder = "branch_line_placeholder".localized()
    
    websiteLabel.text = "branch_website".localized()
    websiteTextField.placeholder = "branch_website_placeholder".localized()
    
    facebookLabel.text = "branch_facebook".localized()
    facebookTextField.placeholder = "branch_facebook_placeholder".localized()
    
    instagramLabel.text = "branch_instagram".localized()
    instagramTextField.placeholder = "branch_instagram_placeholder".localized()
    
    submitButton.setTitle("branch_save".localized(), for: .normal)
    selectTimeSubmitButton.setTitle("branch_save".localized(), for: .normal)
    
    openHoursTitleLabel.text = "branch_menu_openhours".localized()
    
    everydayButton.setTitle("branch_everyday".localized(), for: .normal)
    mondayButton.setTitle("branch_monday".localized(), for: .normal)
    tuesdayButton.setTitle("branch_tuesday".localized(), for: .normal)
    wednesdayButton.setTitle("branch_wednesday".localized(), for: .normal)
    thursdayButton.setTitle("branch_thursday".localized(), for: .normal)
    fridayButton.setTitle("branch_friday".localized(), for: .normal)
    saturdayButton.setTitle("branch_saturday".localized(), for: .normal)
    sundayButton.setTitle("branch_sunday".localized(), for: .normal)
    publicHolidayButton.setTitle("branch_publicholiday".localized(), for: .normal)
    holidayTitleLabel.text = "branch_holiday_title".localized()
    noHolidayButton.setTitle("branch_noholiday".localized(), for: .normal)
    holidayWeekButton.setTitle("branch_holiday_week".localized(), for: .normal)
    holidayDateButton.setTitle("branch_holiday_day".localized(), for: .normal)
    
    orderLabel.text = "branch_order_label".localized()
    orderUnitLabel.text = "branch_order_unit".localized()
    orderTextField.placeholder = "30"
    
    pricePerPersonLabel.text = "branch_price_title".localized()
    priceToLabel.text = "branch_price_to".localized()
    priceUnitLabel.text = "branch_price_unit".localized()
    priceStartTextField.placeholder = "100"
    priceEndTextField.placeholder = "500"
    
    foodCategoryTitleLabel.text = "branch_category_title".localized()
    
    foodImageLabel.text = "branch_food_image_title".localized()
    menuImageLabel.text = "branch_menu_title".localized()
    
    branchImageLabel.text = "branch_image_title".localized()
    foodCategoryDescriptionLabel.text = "branch_category_description".localized()
    
    let underline: [NSAttributedString.Key: Any] = [
      .underlineStyle: NSUnderlineStyle.single.rawValue,
      .font : UIFont(name: "Kanit-SemiBold", size: 15)!,
      .foregroundColor : UIColor(hex: 0xFF6722)]
    let attributeString = NSMutableAttributedString(string: "branch_edit".localized(),
                                                    attributes: underline)
    editButton.setAttributedTitle(attributeString, for: .normal)
    selectTimeEditButton.setAttributedTitle(attributeString, for: .normal)
    
    let deleteUnderline: [NSAttributedString.Key: Any] = [
      .underlineStyle: NSUnderlineStyle.single.rawValue,
      .font : UIFont(name: "Kanit-SemiBold", size: 15)!,
      .foregroundColor : UIColor(hex: 0xD7DAE2)]
    let deleteAttributeString = NSMutableAttributedString(string: "branch_delete".localized(),
                                                    attributes: deleteUnderline)
    deleteButton.setAttributedTitle(deleteAttributeString, for: .normal)
    deleteButton.isHidden = false
    deleteButton.isUserInteractionEnabled = false
    
    selectTimeDeleteButton.setAttributedTitle(deleteAttributeString, for: .normal)
    selectTimeDeleteButton.isHidden = false
    selectTimeDeleteButton.isUserInteractionEnabled = false
  }
  
  func setupBackButton() {
    let buttonItems = navigationItem.leftBarButtonItems
    let backButton = buttonItems?.filter{ $0.tag == 33 }.last
    
    backButton?.rx.tap.subscribe(onNext: { [unowned self] _ in
      self.viewModel.coordinates.navigateBack.onNext(())})
    .disposed(by: disposeBag)
  }
  
  func validateField() -> Bool {
    let isPassValidatedBranchName = validateTextField(text: branchNameTextField.text!)
    let isPassValidatedBranchNO = validateTextField(text: branchNumberTextField.text!)
    let isPassValidateHouseNO = validateTextField(text: houseNOTextField.text!)
    let isPassValidatePhoneNumber = validatePhonenumbers()
    let isPassValidateOpenHours = validateOpenDays()
    let isPassValidatePriceSince = validateTextField(text: priceStartTextField.text!)
    let isPassValidatePriceTo = validateTextField(text: priceEndTextField.text!)
    let isPassValidatedCategory = validateFoodCategory()
    let isPassValidateLocation = validateLocation()
    
    setUIRequireBrandName(isRequired: !isPassValidatedBranchName)
    setUIRequireBranchNumber(isRequired: !isPassValidatedBranchNO)
    setUIRequireHouseNumber(isRequired: !isPassValidateHouseNO)
    setUIRequirePhoneNumber(isRequired: !isPassValidatePhoneNumber)
    setUIRequireOpenHours(isRequire: !isPassValidateOpenHours)
    setUIRequirePriceSince(isRequired: !isPassValidatePriceSince)
    setUIRequirePriceTo(isRequired: !isPassValidatePriceTo)
    setUIRequireFoodCategory(isRequire: !isPassValidatedCategory)
    setUIRequireLocation(isRequired: !isPassValidateLocation)
    
    return isPassValidatedBranchName
      && isPassValidatedBranchNO
      && isPassValidateHouseNO
      && isPassValidatePhoneNumber
      && isPassValidateOpenHours
      && isPassValidatePriceSince
      && isPassValidatePriceTo
      && isPassValidatedCategory
      && isPassValidateLocation
  }
  
  func validatePhonenumbers() -> Bool {
    return phoneNumbers.count > 0
  }
  
  func validateOpenDays() -> Bool {
    return (openHoursEveryday.count
      + openHoursMonday.count
      + openHoursTuesday.count
      + openHoursWednesday.count
      + openHoursThursday.count
      + openHoursFridayday.count
      + openHoursSaturday.count
      + openHoursSunday.count
      + openHoursPublicHoliday.count) > 0
  }
  
  func validateFoodCategory() -> Bool {
    return selectFoodCategory.count > 0
  }
  
  func setUIRequireOpenHours(isRequire: Bool) {
    if isRequire {
      Helper.Alert(view: self, title: "branch_title".localized(),
                   message: "branch_openhours_message".localized(),
                   confirmText: "ok".localized())
    }
  }
  
  func setUIRequireFoodCategory(isRequire: Bool) {
    if isRequire {
      Helper.Alert(view: self, title: "branch_title".localized(),
                   message: "branch_category_message".localized(),
                   confirmText: "ok".localized())
    }
  }
  
  func validateTextField(text: String) -> Bool {
    return text.trimmingCharacters(in: .whitespacesAndNewlines) != ""
  }
  
  func validateLocation() -> Bool {
    return branchLocation != nil
  }
  
  func setUIRequireLocation(isRequired: Bool) {
    mapView.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequirePriceSince(isRequired: Bool) {
    priceStartTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequirePriceTo(isRequired: Bool) {
    priceEndTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireBrandName(isRequired: Bool) {
    branchNameTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireBranchNumber(isRequired: Bool) {
    branchNumberTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireHouseNumber(isRequired: Bool) {
    houseNOTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireSubDistrict(isRequired: Bool) {
    subDistrictTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireDistrict(isRequired: Bool) {
    districtTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequireProvince(isRequired: Bool) {
    provinceTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequirePostCode(isRequired: Bool) {
    postCodeTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func setUIRequirePhoneNumber(isRequired: Bool) {
    phoneView.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
  }
  
  func disableField() {
    logoImageButton.isUserInteractionEnabled = false
    banerButton.isUserInteractionEnabled = false
    isDeleveryButton.isUserInteractionEnabled = false
    branchNameTextField.isUserInteractionEnabled = false
    branchNumberTextField.isUserInteractionEnabled = false
    branchDetailTextView.isUserInteractionEnabled = false
    mapView.isUserInteractionEnabled = false
    mapButton.isUserInteractionEnabled = false
    buildingTextField.isUserInteractionEnabled = false
    roomNOTextField.isUserInteractionEnabled = false
    floorTextField.isUserInteractionEnabled = false
    villageTextField.isUserInteractionEnabled = false
    houseNOTextField.isUserInteractionEnabled = false
    villageNOTextField.isUserInteractionEnabled = false
    soiTextField.isUserInteractionEnabled = false
    streetTextField.isUserInteractionEnabled = false
    subDistrictTextField.isUserInteractionEnabled = false
    districtTextField.isUserInteractionEnabled = false
    provinceTextField.isUserInteractionEnabled = false
    postCodeTextField.isUserInteractionEnabled = false
    phoneTokenField.isUserInteractionEnabled = false
    websiteTextField.isUserInteractionEnabled = false
    emailTextField.isUserInteractionEnabled = false
    facebookTextField.isUserInteractionEnabled = false
    lineTextField.isUserInteractionEnabled = false
    instagramTextField.isUserInteractionEnabled = false
    everydayButton.isUserInteractionEnabled = false
    everydayCollectionView.isUserInteractionEnabled = false
    everydayAddButton.isUserInteractionEnabled = false
    mondayButton.isUserInteractionEnabled = false
    mondayCollectionVew.isUserInteractionEnabled = false
    mondayAddButton.isUserInteractionEnabled = false
    tuesdayButton.isUserInteractionEnabled = false
    tuesdayAddButton.isUserInteractionEnabled = false
    tuesdayCollectionView.isUserInteractionEnabled = false
    wednesdayButton.isUserInteractionEnabled = false
    wednesdayAddButton.isUserInteractionEnabled = false
    wednesdayCollectionView.isUserInteractionEnabled = false
    thursdayButton.isUserInteractionEnabled = false
    thursdayAddButton.isUserInteractionEnabled = false
    thursdayCollectionView.isUserInteractionEnabled = false
    fridayButton.isUserInteractionEnabled = false
    fridayAddButton.isUserInteractionEnabled = false
    fridayCollectionView.isUserInteractionEnabled = false
    saturdayButton.isUserInteractionEnabled = false
    saturdayAddButton.isUserInteractionEnabled = false
    saturdayCollectionView.isUserInteractionEnabled = false
    sundayButton.isUserInteractionEnabled = false
    sundayAddButton.isUserInteractionEnabled = false
    sundayCollectionView.isUserInteractionEnabled = false
    publicHolidayButton.isUserInteractionEnabled = false
    publicHolidayAddButton.isUserInteractionEnabled = false
    publicHolidayCollectionView.isUserInteractionEnabled = false
    noHolidayButton.isUserInteractionEnabled = false
    holidayAddButton.isUserInteractionEnabled = false
    holidayWeekButton.isUserInteractionEnabled = false
    holidayDateButton.isUserInteractionEnabled = false
    holidayTableView.isUserInteractionEnabled = false
    orderTextField.isUserInteractionEnabled = false
    priceStartTextField.isUserInteractionEnabled = false
    priceEndTextField.isUserInteractionEnabled = false
    foodCategoryTableTwoTableView.allowsSelection = false
    drinkImageCollectionView.isUserInteractionEnabled = false
    menuImageCollectionView.isUserInteractionEnabled = false
    branchImageCollectionView.isUserInteractionEnabled = false
    provinceButton.isUserInteractionEnabled = false
    districtButton.isUserInteractionEnabled = false
    subDistrictButton.isUserInteractionEnabled = false
    
    categoryNoteLabel.isHidden = true
    logoDescriptionLabel.isHidden = true
    banerDescriptionLabel.isHidden = true
    
    self.branchNameTextField.layer.borderColor = UIColor.clear.cgColor
    self.branchNumberTextField.layer.borderColor = UIColor.clear.cgColor
    self.branchDetailView.layer.borderColor = UIColor.clear.cgColor
    self.mapView.layer.borderColor = UIColor.clear.cgColor
    self.phoneView.layer.borderColor = UIColor.clear.cgColor
    self.emailTextField.layer.borderColor = UIColor.clear.cgColor
    self.lineTextField.layer.borderColor = UIColor.clear.cgColor
    self.buildingTextField.layer.borderColor = UIColor.clear.cgColor
    self.roomNOTextField.layer.borderColor = UIColor.clear.cgColor
    self.floorTextField.layer.borderColor = UIColor.clear.cgColor
    self.villageTextField.layer.borderColor = UIColor.clear.cgColor
    self.houseNOTextField.layer.borderColor = UIColor.clear.cgColor
    self.villageNOTextField.layer.borderColor = UIColor.clear.cgColor
    self.soiTextField.layer.borderColor = UIColor.clear.cgColor
    self.streetTextField.layer.borderColor = UIColor.clear.cgColor
    self.subDistrictTextField.layer.borderColor = UIColor.clear.cgColor
    self.districtTextField.layer.borderColor = UIColor.clear.cgColor
    self.provinceTextField.layer.borderColor = UIColor.clear.cgColor
    self.postCodeTextField.layer.borderColor = UIColor.clear.cgColor
    self.websiteTextField.layer.borderColor = UIColor.clear.cgColor
    self.facebookTextField.layer.borderColor = UIColor.clear.cgColor
    self.instagramTextField.layer.borderColor = UIColor.clear.cgColor
    self.orderTextField.layer.borderColor = UIColor.clear.cgColor
    self.priceStartTextField.layer.borderColor = UIColor.clear.cgColor
    self.priceEndTextField.layer.borderColor = UIColor.clear.cgColor
    self.starLabels.forEach{ $0.isHidden = true }
    self.dropDownImageViews.forEach{ $0.isHidden = true }
    self.removePlaceholder()
    self.phoneDescriptionLabel.isHidden = true
    self.everydayAddButton.isHidden = true
    self.mondayAddButton.isHidden = true
    self.tuesdayAddButton.isHidden = true
    self.wednesdayAddButton.isHidden = true
    self.thursdayAddButton.isHidden = true
    self.fridayAddButton.isHidden = true
    self.saturdayAddButton.isHidden = true
    self.sundayAddButton.isHidden = true
    self.publicHolidayAddButton.isHidden = true
    self.holidayAddButton.isHidden = true
    viewModel.inputs.getMenuImageTrigger.onNext((imageDatas: self.branchMenuImages, isView: self.isView))
    viewModel.inputs.getBranchImageTrigger.onNext((imageDatas: self.branchImages, isView: self.isView))
    viewModel.inputs.getDrinkImageTrigger.onNext((imageDatas: self.branchDrinkImages, isView: self.isView))
    submitButtonTopConstraint.constant = 0
    submitButtonHeightConstraint.constant = 0
    selectTimeSubmitButtonTopConstraint.constant = 0
    selectTimeSubmitButtonHeightConstraint.constant = 0
  }
  
  func enableField() {
    guard let branchEditData = self.branchEditData else { return }
    logoImageButton.isUserInteractionEnabled = true
    banerButton.isUserInteractionEnabled = true
    isDeleveryButton.isUserInteractionEnabled = true
    branchNameTextField.isUserInteractionEnabled = true
    branchNumberTextField.isUserInteractionEnabled = true
    branchDetailTextView.isUserInteractionEnabled = true
    mapView.isUserInteractionEnabled = true
    mapButton.isUserInteractionEnabled = true
    buildingTextField.isUserInteractionEnabled = true
    roomNOTextField.isUserInteractionEnabled = true
    floorTextField.isUserInteractionEnabled = true
    villageTextField.isUserInteractionEnabled = true
    houseNOTextField.isUserInteractionEnabled = true
    villageNOTextField.isUserInteractionEnabled = true
    soiTextField.isUserInteractionEnabled = true
    streetTextField.isUserInteractionEnabled = true
    subDistrictTextField.isUserInteractionEnabled = true
    districtTextField.isUserInteractionEnabled = true
    provinceTextField.isUserInteractionEnabled = true
    postCodeTextField.isUserInteractionEnabled = true
    phoneTokenField.isUserInteractionEnabled = true
    websiteTextField.isUserInteractionEnabled = true
    emailTextField.isUserInteractionEnabled = true
    facebookTextField.isUserInteractionEnabled = true
    lineTextField.isUserInteractionEnabled = true
    instagramTextField.isUserInteractionEnabled = true
    foodCategoryTableTwoTableView.allowsSelection = true
    categoryNoteLabel.isHidden = false
    logoDescriptionLabel.isHidden = false
    banerDescriptionLabel.isHidden = false
    
    if branchEditData.openDays.everyDay.count > 0 {
      everydayButton.isUserInteractionEnabled = true
      self.everydayAddButton.isUserInteractionEnabled = true
      self.mondayButton.isUserInteractionEnabled = false
      self.mondayAddButton.isUserInteractionEnabled = false
      self.mondayCollectionVew.isUserInteractionEnabled = false
      
      self.tuesdayButton.isUserInteractionEnabled = false
      self.tuesdayAddButton.isUserInteractionEnabled = false
      self.tuesdayCollectionView.isUserInteractionEnabled = false
      
      self.wednesdayButton.isUserInteractionEnabled = false
      self.wednesdayAddButton.isUserInteractionEnabled = false
      self.wednesdayCollectionView.isUserInteractionEnabled = false
      
      self.thursdayButton.isUserInteractionEnabled = false
      self.thursdayAddButton.isUserInteractionEnabled = false
      self.thursdayCollectionView.isUserInteractionEnabled = false
      
      self.fridayButton.isUserInteractionEnabled = false
      self.fridayAddButton.isUserInteractionEnabled = false
      self.fridayCollectionView.isUserInteractionEnabled = false
      
      self.saturdayButton.isUserInteractionEnabled = false
      self.saturdayAddButton.isUserInteractionEnabled = false
      self.saturdayCollectionView.isUserInteractionEnabled = false
      
      self.sundayButton.isUserInteractionEnabled = false
      self.sundayAddButton.isUserInteractionEnabled = false
      self.sundayCollectionView.isUserInteractionEnabled = false
      
      self.publicHolidayButton.isUserInteractionEnabled = false
      self.publicHolidayAddButton.isUserInteractionEnabled = false
      self.publicHolidayCollectionView.isUserInteractionEnabled = false
    } else {
        everydayButton.isUserInteractionEnabled = true
        everydayCollectionView.isUserInteractionEnabled = true
        everydayAddButton.isUserInteractionEnabled = true
        mondayButton.isUserInteractionEnabled = true
        mondayCollectionVew.isUserInteractionEnabled = true
        mondayAddButton.isUserInteractionEnabled = true
        tuesdayButton.isUserInteractionEnabled = true
        tuesdayAddButton.isUserInteractionEnabled = true
        tuesdayCollectionView.isUserInteractionEnabled = true
        wednesdayButton.isUserInteractionEnabled = true
        wednesdayAddButton.isUserInteractionEnabled = true
        wednesdayCollectionView.isUserInteractionEnabled = true
        thursdayButton.isUserInteractionEnabled = true
        thursdayAddButton.isUserInteractionEnabled = true
        thursdayCollectionView.isUserInteractionEnabled = true
        fridayButton.isUserInteractionEnabled = true
        fridayAddButton.isUserInteractionEnabled = true
        fridayCollectionView.isUserInteractionEnabled = true
        saturdayButton.isUserInteractionEnabled = true
        saturdayAddButton.isUserInteractionEnabled = true
        saturdayCollectionView.isUserInteractionEnabled = true
        sundayButton.isUserInteractionEnabled = true
        sundayAddButton.isUserInteractionEnabled = true
        sundayCollectionView.isUserInteractionEnabled = true
        publicHolidayButton.isUserInteractionEnabled = true
        publicHolidayAddButton.isUserInteractionEnabled = true
        publicHolidayCollectionView.isUserInteractionEnabled = true
    }
    noHolidayButton.isUserInteractionEnabled = true
    holidayAddButton.isUserInteractionEnabled = true
    holidayWeekButton.isUserInteractionEnabled = true
    holidayDateButton.isUserInteractionEnabled = true
    holidayTableView.isUserInteractionEnabled = true
    orderTextField.isUserInteractionEnabled = true
    priceStartTextField.isUserInteractionEnabled = true
    priceEndTextField.isUserInteractionEnabled = true
    foodCategoryTableOneView.isUserInteractionEnabled = true
    drinkImageCollectionView.isUserInteractionEnabled = true
    menuImageCollectionView.isUserInteractionEnabled = true
    branchImageCollectionView.isUserInteractionEnabled = true
    provinceButton.isUserInteractionEnabled = true
    districtButton.isUserInteractionEnabled = true
    subDistrictButton.isUserInteractionEnabled = true
    self.viewModel.inputs.openHoursEverydayTrigger.onNext(self.openHoursEveryday)
    self.viewModel.inputs.openHoursMondayTrigger.onNext(self.openHoursMonday)
    self.viewModel.inputs.openHoursTuesdayTrigger.onNext(self.openHoursTuesday)
    self.viewModel.inputs.openHoursWednesdayTrigger.onNext(self.openHoursWednesday)
    self.viewModel.inputs.openHoursThursdayTrigger.onNext(self.openHoursThursday)
    self.viewModel.inputs.openHoursFridayTrigger.onNext(self.openHoursFridayday)
    self.viewModel.inputs.openHoursSaturdayTrigger.onNext(self.openHoursSaturday)
    self.viewModel.inputs.openHoursSundayTrigger.onNext(self.openHoursSunday)
    self.viewModel.inputs.openHoursPublicHolidayTrigger.onNext(self.openHoursPublicHoliday)
    self.viewModel.inputs.holidayTrigger.onNext(self.holidayData)
    
    self.branchNameTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.branchNumberTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.branchDetailView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.mapView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.phoneView.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.emailTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.lineTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.buildingTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.roomNOTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.floorTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.villageTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.houseNOTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.villageNOTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.soiTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.streetTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.subDistrictTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.districtTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.provinceTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.postCodeTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.websiteTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.facebookTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.instagramTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.orderTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.priceStartTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    self.priceEndTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    
    self.starLabels.forEach{ $0.isHidden = false }
    self.dropDownImageViews.forEach{ $0.isHidden = false }
    self.phoneTokenField.reloadData()
    self.addPlaceholder()
    self.phoneDescriptionLabel.isHidden = false
    self.everydayAddButton.isHidden = false
    self.mondayAddButton.isHidden = false
    self.tuesdayAddButton.isHidden = false
    self.wednesdayAddButton.isHidden = false
    self.thursdayAddButton.isHidden = false
    self.fridayAddButton.isHidden = false
    self.saturdayAddButton.isHidden = false
    self.sundayAddButton.isHidden = false
    self.publicHolidayAddButton.isHidden = false
    self.holidayAddButton.isHidden = false
    self.banerImageView.kf.setImage(with: URL(string: banerImageURL),
                                    placeholder: UIImage(named: "ic_branch_default_banner"))
    self.logoImgaeView.kf.setImage(with: URL(string: logoImageURL),
                                   placeholder: UIImage(named: "ic_branch_default_logo"))
    viewModel.inputs.getMenuImageTrigger.onNext((imageDatas: self.branchMenuImages, isView: self.isView))
    viewModel.inputs.getBranchImageTrigger.onNext((imageDatas: self.branchImages, isView: self.isView))
    viewModel.inputs.getDrinkImageTrigger.onNext((imageDatas: self.branchDrinkImages, isView: self.isView))
    submitButtonHeightConstraint.constant = 44
    submitButtonTopConstraint.constant = 40
    selectTimeSubmitButtonTopConstraint.constant = 40
    selectTimeSubmitButtonHeightConstraint.constant = 44
  }
  
  func removePlaceholder(){
    branchNameTextField.placeholder = nil
    branchNumberTextField.placeholder = nil
    if branchDetailTextView.text == "branch_detail_placeholder".localized() {
      branchDetailTextView.text = nil
    }
    buildingTextField.placeholder = nil
    roomNOTextField.placeholder = nil
    floorTextField.placeholder = nil
    villageTextField.placeholder = nil
    houseNOTextField.placeholder = nil
    villageNOTextField.placeholder = nil
    soiTextField.placeholder = nil
    streetTextField.placeholder = nil
    subDistrictTextField.placeholder = nil
    districtTextField.placeholder = nil
    provinceTextField.placeholder = nil
    postCodeTextField.placeholder = nil
    phoneTokenField.textField.placeholder = nil
    websiteTextField.placeholder = nil
    emailTextField.placeholder = nil
    facebookTextField.placeholder = nil
    lineTextField.placeholder = nil
    instagramTextField.placeholder = nil
    orderTextField.placeholder = nil
    priceStartTextField.placeholder = nil
    priceEndTextField.placeholder = nil
  }
  
  func addPlaceholder() {
    
    detailButton.setTitle("branch_detail".localized(), for: .normal)
    
    logoLabel.text = "branch_logo_title".localized()
    logoDescriptionLabel.text = "branch_logo_description".localized()
    banerLabel.text = "branch_baner_title".localized()
    banerDescriptionLabel.text = "branch_baner_description".localized()
    isDeleveryButton.setTitle("branch_delevery".localized(), for: .normal)
    brandAddressSwitchLabel.text = "branch_address_switch".localized()
    addressLabel.text = "branch_address".localized()
    
    branchNameLabel.text = "branch_name".localized()
    branchNameTextField.placeholder = "branch_name".localized()
    branchNumberLabel.text = "branch_number".localized()
    branchNumberTextField.placeholder = "000000"
    branchDetailLabel.text = "branch_detail".localized()
    if branchDetailTextView.text == "" {
      branchDetailTextView.text = "branch_detail_placeholder".localized()
      branchDetailTextView.textColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.5)
    }

    mapLabel.text = "branch_map".localized()
    
    buildingLabel.text = "branch_building".localized()
    buildingTextField.placeholder = "branch_building".localized()
    
    roomNOLabel.text = "branch_roomno".localized()
    roomNOTextField.placeholder = "branch_roomno".localized()
    
    floorLabel.text = "branch_floor".localized()
    floorTextField.placeholder = "branch_floor".localized()
    
    villageLabel.text = "branch_village".localized()
    villageTextField.placeholder = "branch_village".localized()
    
    houseNOLabel.text = "branch_houseno".localized()
    houseNOTextField.placeholder = "branch_houseno".localized()
    
    villageNOLabel.text = "branch_villageno".localized()
    villageNOTextField.placeholder = "branch_villageno".localized()
    
    soiLabel.text = "branch_soi".localized()
    soiTextField.placeholder = "branch_soi".localized()
    
    streetLabel.text = "branch_street".localized()
    streetTextField.placeholder = "branch_street".localized()
    
    subDistrictLabel.text = "branch_subdistrict".localized()
    subDistrictTextField.placeholder = "branch_subdistrict".localized()
    
    districtLabel.text = "branch_district".localized()
    districtTextField.placeholder = "branch_district".localized()
    
    provinceLabel.text = "branch_province".localized()
    provinceTextField.placeholder = "branch_province".localized()
    
    postCodeLabel.text = "branch_postcode".localized()
    postCodeTextField.placeholder = "branch_postcode".localized()
    
    brandContactSwitchLabel.text = "branch_contact_switch".localized()
    contactLabel.text = "branch_contract".localized()
    
    phoneLabel.text = "branch_phone".localized()
    phoneDescriptionLabel.text = "branch_phone_description".localized()
    phoneTokenField.textField.placeholder = "branch_phone_placeholder".localized()
    
    emailLabel.text = "branch_email".localized()
    emailTextField.placeholder = "branch_email_placeholder".localized()
    
    lineLabel.text = "branch_line".localized()
    lineTextField.placeholder = "branch_line_placeholder".localized()
    
    websiteLabel.text = "branch_website".localized()
    websiteTextField.placeholder = "branch_website_placeholder".localized()
    
    facebookLabel.text = "branch_facebook".localized()
    facebookTextField.placeholder = "branch_facebook_placeholder".localized()
    
    instagramLabel.text = "branch_instagram".localized()
    instagramTextField.placeholder = "branch_instagram_placeholder".localized()
    
    submitButton.setTitle("branch_save".localized(), for: .normal)
    selectTimeSubmitButton.setTitle("branch_save".localized(), for: .normal)
    
    openHoursTitleLabel.text = "branch_menu_openhours".localized()
    
    everydayButton.setTitle("branch_everyday".localized(), for: .normal)
    mondayButton.setTitle("branch_monday".localized(), for: .normal)
    tuesdayButton.setTitle("branch_tuesday".localized(), for: .normal)
    wednesdayButton.setTitle("branch_wednesday".localized(), for: .normal)
    thursdayButton.setTitle("branch_thursday".localized(), for: .normal)
    fridayButton.setTitle("branch_friday".localized(), for: .normal)
    saturdayButton.setTitle("branch_saturday".localized(), for: .normal)
    sundayButton.setTitle("branch_sunday".localized(), for: .normal)
    publicHolidayButton.setTitle("branch_publicholiday".localized(), for: .normal)
    holidayTitleLabel.text = "branch_holiday_title".localized()
    noHolidayButton.setTitle("branch_noholiday".localized(), for: .normal)
    holidayWeekButton.setTitle("branch_holiday_week".localized(), for: .normal)
    holidayDateButton.setTitle("branch_holiday_day".localized(), for: .normal)
    
    orderLabel.text = "branch_order_label".localized()
    orderUnitLabel.text = "branch_order_unit".localized()
    orderTextField.placeholder = "30"
    
    pricePerPersonLabel.text = "branch_price_title".localized()
    priceToLabel.text = "branch_price_to".localized()
    priceUnitLabel.text = "branch_price_unit".localized()
    priceStartTextField.placeholder = "100"
    priceEndTextField.placeholder = "500"
    
    foodCategoryTitleLabel.text = "branch_category_title".localized()
    
    foodImageLabel.text = "branch_food_image_title".localized()
    menuImageLabel.text = "branch_menu_title".localized()
    
    branchImageLabel.text = "branch_image_title".localized()
    foodCategoryDescriptionLabel.text = "branch_category_description".localized()
  }
  
  func presentPopOverView(_ controller: UIViewController, sourceView: UIView) {
    let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
    presentationController.sourceView = sourceView
    presentationController.sourceRect = CGRect(x: 50  , y: 44, width: 0, height: 0)
    presentationController.permittedArrowDirections = [.up]
    self.present(controller, animated: true)
  }
}

extension BranchViewController: ZFTokenFieldDelegate, ZFTokenFieldDataSource {
  func lineHeightForToken(in tokenField: ZFTokenField!) -> CGFloat {
    return 32
  }
  
  func numberOfToken(in tokenField: ZFTokenField!) -> UInt {
    return UInt(phoneNumbers.count)
  }
  
  func tokenField(_ tokenField: ZFTokenField!, viewForTokenAt index: UInt) -> UIView! {
    let nibs = Bundle.main.loadNibNamed("PhoneTokenView", owner: nil, options: nil)
    let view = nibs?[0] as! UIView
    let label = view.viewWithTag(2) as! UILabel
    let button = view.viewWithTag(3) as! UIButton
    
    button.addTarget(self, action: #selector(didClickOnClearPhoneNumber(tokenButton:)), for: .touchUpInside)
    if isView {
      button.isHidden = true
      button.constraints.forEach{
        if $0.identifier == "widthConstraint" {
         $0.constant = 0
      }}
    } else {
      button.isHidden = false
      button.constraints.forEach{
        if $0.identifier == "widthConstraint" {
         $0.constant = 20
      }}
    }
    label.text = self.phoneNumbers[Int(index)];
    view.frame = CGRect(x: 0, y: 0, width: 137, height: 32)
    view.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    view.layer.borderWidth = 1
    return view
  }
  
  @objc func didClickOnClearPhoneNumber(tokenButton: UIButton) {
    let index = phoneTokenField.index(ofTokenView: tokenButton.superview)
    if index != NSNotFound {
      self.phoneNumbers.remove(at: Int(index))
      self.phoneTokenField.reloadData()
    }
  }
  
  func tokenMarginInToken(in tokenField: ZFTokenField!) -> CGFloat {
    return 8
  }
  
  func tokenFieldShouldEndEditing(_ textField: ZFTokenField!) -> Bool {
    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [unowned self] in
      self.phoneScrollView.setContentOffset(.zero, animated: true)
    }
      
    if textField.textField.text == "" { return true }
    self.phoneNumbers.append(textField.textField.text!)
    self.phoneTokenField.reloadData()
    self.phoneTokenField.layoutIfNeeded()
    return true
  }
  
  func setupAnnotation(location: CLLocationCoordinate2D) {
    removeAnnotation()
    let annotation = MKPointAnnotation()
    annotation.coordinate = location
    mapView.addAnnotations([annotation])
    zoomTofit()
  }
  
  func removeAnnotation() {
    for annotation in mapView.annotations {
      if annotation is MKUserLocation {
        continue
      } else {
        mapView.removeAnnotation(annotation)
      }
    }
  }
  
  func zoomTofit() {
    var zoomRect = MKMapRect.null
    for annotation in mapView.annotations {
      let mapPoint = MKMapPoint(annotation.coordinate)
      let pointRect = MKMapRect(x: mapPoint.x, y: mapPoint.y, width: 0.1, height: 0.1)
      zoomRect = zoomRect.union(pointRect)
    }
    mapView.setVisibleMapRect(zoomRect, edgePadding: UIEdgeInsets(top: 50, left: 50, bottom: 50, right: 50), animated: true)
  }
}

extension BranchViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
    guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
    dismiss(animated: true, completion: nil)
    if picker == logoImagePicker {
      guard let resize = image.resize(newWidth: 200) else { return }
      self.view.layoutIfNeeded()
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [unowned self] in
        self.uploadLogoImage(image: resize)
      }
    } else if picker == banerImagePicker {
      guard let resize = image.resize(newWidth: 1920) else { return }
      self.view.layoutIfNeeded()
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [unowned self] in
        self.uploadBanerImage(image: resize)
      }
    } else if picker == foodImagePicker {
      guard let resize = image.resize(newWidth: 400) else { return }
      self.view.layoutIfNeeded()
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [unowned self] in
        self.uploadFoodImage(image: resize)
      }
    } else if picker == menuImagePicker {
      guard let resize = image.resize(newWidth: 400) else { return }
      self.view.layoutIfNeeded()
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [unowned self] in
        self.uploadMenuImage(image: resize)
      }
      
    } else if picker == branchImagePicker {
      guard let resize = image.resize(newWidth: 400) else { return }
      self.view.layoutIfNeeded()
      DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [unowned self] in
        self.uploadBranchImage(image: resize)
      }
    }
  }
  
  func checkImageReccommend(image: UIImage, size: CGSize) -> Bool {
    let imageSize = image.size
    if imageSize == size {
      return true
    } else {
      Helper.Alert(view: self, title: "branch_title".localized(),
                   message: "branch_image_size_message".localized(), confirmText: "ok".localized())
      return false
    }
  }
  
  func uploadLogoImage(image: UIImage) {
    let storageRef = self.storage.reference(withPath: "branch/\(NSUUID().uuidString).png")
    if let imageData = image.pngData() {
      viewModel.outputs.loading.onNext(true)
      storageRef.putData(imageData, metadata: nil) { [unowned self] (response, error) in
        self.viewModel.outputs.loading.onNext(false)
        if let error = error {
          Helper.Alert(view: self, title: "Error",
                       message: error.localizedDescription,
                       confirmText: "ok".localized())
          return
        }
        self.viewModel.outputs.loading.onNext(true)
        storageRef.downloadURL { [unowned self] (url, error) in
          self.viewModel.outputs.loading.onNext(false)
          if let error = error {
            Helper.Alert(view: self, title: "Error",
                         message: error.localizedDescription,
                         confirmText: "ok".localized())
            return
          }
          
          guard let url = url else { return }
          self.logoImageID = "\(storageRef)"
          self.logoImageURL = "\(url)"
          self.logoImgaeView.kf.setImage(with: url)
          print("logoImage-----> ", self.logoImageURL)
        }
      }
    }
  }
  
  func uploadBanerImage(image: UIImage) {
    let storageRef = self.storage.reference(withPath: "branch/\(NSUUID().uuidString).png")
    if let imageData = image.pngData() {
      viewModel.outputs.loading.onNext(true)
      storageRef.putData(imageData, metadata: nil) { [unowned self] (response, error) in
        self.viewModel.outputs.loading.onNext(false)
        if let error = error {
          Helper.Alert(view: self, title: "Error",
                       message: error.localizedDescription,
                       confirmText: "ok".localized())
          return
        }
        self.viewModel.outputs.loading.onNext(true)
        storageRef.downloadURL { [unowned self] (url, error) in
          self.viewModel.outputs.loading.onNext(false)
          if let error = error {
            Helper.Alert(view: self, title: "Error",
                         message: error.localizedDescription,
                         confirmText: "ok".localized())
            return
          }
          
          guard let url = url else { return }
          self.banerImageID = "\(storageRef)"
          self.banerImageURL = "\(url)"
          self.banerImageView.kf.setImage(with: url)
        }
      }
    }
  }
  
  func uploadFoodImage(image: UIImage) {
    let storageRef = self.storage.reference(withPath: "branch/\(NSUUID().uuidString).png")
    if let imageData = image.pngData() {
      viewModel.outputs.loading.onNext(true)
      storageRef.putData(imageData, metadata: nil) { [unowned self] (response, error) in
        self.viewModel.outputs.loading.onNext(false)
        if let error = error {
          Helper.Alert(view: self, title: "Error",
                       message: error.localizedDescription,
                       confirmText: "ok".localized())
          return
        }
        self.viewModel.outputs.loading.onNext(true)
        storageRef.downloadURL { [unowned self] (url, error) in
          self.viewModel.outputs.loading.onNext(false)
          if let error = error {
            Helper.Alert(view: self, title: "Error",
                         message: error.localizedDescription,
                         confirmText: "ok".localized())
            return
          }
          
          guard let url = url else { return }
          self.branchDrinkImages.append(BranchImageData(imageID: "\(storageRef)", imageURL: "\(url)"))
          self.viewModel.inputs.getDrinkImageTrigger.onNext((imageDatas: self.branchDrinkImages, isView: self.isView))
        }
      }
    }
  }
  
  func uploadMenuImage(image: UIImage) {
    let storageRef = self.storage.reference(withPath: "branch/\(NSUUID().uuidString).png")
    if let imageData = image.pngData() {
      viewModel.outputs.loading.onNext(true)
      storageRef.putData(imageData, metadata: nil) { [unowned self] (response, error) in
        self.viewModel.outputs.loading.onNext(false)
        if let error = error {
          Helper.Alert(view: self, title: "Error",
                       message: error.localizedDescription,
                       confirmText: "ok".localized())
          return
        }
        self.viewModel.outputs.loading.onNext(true)
        storageRef.downloadURL { [unowned self] (url, error) in
          self.viewModel.outputs.loading.onNext(false)
          if let error = error {
            Helper.Alert(view: self, title: "Error",
                         message: error.localizedDescription,
                         confirmText: "ok".localized())
            return
          }
          
          guard let url = url else { return }
          self.branchMenuImages.append(BranchImageData(imageID: "\(storageRef)", imageURL: "\(url)"))
          self.viewModel.inputs.getMenuImageTrigger.onNext((imageDatas: self.branchMenuImages, isView: self.isView))
        }
      }
    }
  }
}

extension BranchViewController: MKMapViewDelegate {
  func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
    guard !(annotation is MKUserLocation) else {
      return nil
    }
    
    let annotationIdentifier = "Identifier"
    var annotationView: MKAnnotationView?
    if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
      annotationView = dequeuedAnnotationView
      annotationView?.annotation = annotation
    }
    else {
      annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
      annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
    }
    
    if let annotationView = annotationView {
      annotationView.canShowCallout = true
      annotationView.image = UIImage(named: "ic_map_pin")
    }
    return annotationView
  }
}

extension BranchViewController: MapSelectViewControllerDelegate {
  func didSelectLocation(location: CLLocationCoordinate2D) {
    print(location)
    self.branchLocation = location
    setupAnnotation(location: location)
  }
}

extension BranchViewController: HolidayTableViewCellDelegate {
  func didEditHolidaySuccess(day: String, week: String, index: Int) {
    let data = holidayData[index]
    self.holidayData[index] = HolidayData(day: day, week: week, type: data.type)
    viewModel.inputs.holidayTrigger.onNext(self.holidayData)
  }
}

extension BranchViewController: HolidayDateTableViewCellDelegate {
  func didSelectedDateSuccess(day: String, index: Int) {
    let data = holidayData[index]
    self.holidayData[index] = HolidayData(day: day, week: "", type: data.type)
    viewModel.inputs.holidayTrigger.onNext(self.holidayData)
    print("holidayData-------> ", self.holidayData)
  }
}

extension BranchViewController: BranchImageCollectionViewCellDelegate, BranchAddImageCollectionViewCellDelegate {
  func didClearBranchImage(index: Int) {
    print("clearIndex----> ", index)
    self.branchImages.remove(at: index)
    viewModel.inputs.getBranchImageTrigger.onNext((imageDatas: self.branchImages, isView: self.isView))
  }
  
  func didAddBranchImage() {
    self.present(branchImagePicker, animated: true, completion: nil)
  }
  
  func uploadBranchImage(image: UIImage) {
    let storageRef = self.storage.reference(withPath: "branch/\(NSUUID().uuidString).png")
    if let imageData = image.pngData() {
      viewModel.outputs.loading.onNext(true)
      storageRef.putData(imageData, metadata: nil) { [unowned self] (response, error) in
        self.viewModel.outputs.loading.onNext(false)
        if let error = error {
          Helper.Alert(view: self, title: "Error",
                       message: error.localizedDescription,
                       confirmText: "ok".localized())
          return
        }
        self.viewModel.outputs.loading.onNext(true)
        storageRef.downloadURL { [unowned self] (url, error) in
          self.viewModel.outputs.loading.onNext(false)
          if let error = error {
            Helper.Alert(view: self, title: "Error",
                         message: error.localizedDescription,
                         confirmText: "ok".localized())
            return
          }
          
          guard let url = url else { return }
          self.branchImages.append(BranchImageData(imageID: "\(storageRef)", imageURL: "\(url)"))
          self.viewModel.inputs.getBranchImageTrigger.onNext((imageDatas: self.branchImages, isView: self.isView))
        }
      }
    }
  }
}

extension BranchViewController: BranchDrinkImageCollectionViewCellDelegate, BranchDrinkAddImageCollectionViewCellDelegate {
  
  func didClearBranchDrinkImage(index: Int) {
    print("clearIndex----> ", index)
    self.branchDrinkImages.remove(at: index)
    viewModel.inputs.getDrinkImageTrigger.onNext((imageDatas: self.branchDrinkImages, isView: self.isView))
  }
  
  func didAddBranchDrinkImage() {
    self.present(foodImagePicker, animated: true, completion: nil)
  }
}

extension BranchViewController: BranchMenuImageCollectionViewCellDelegate, BranchMenuAddImageCollectionViewCellDelegate {
  
  func didClearBranchMenuImage(index: Int) {
    print("clearIndex----> ", index)
    self.branchMenuImages.remove(at: index)
    viewModel.inputs.getMenuImageTrigger.onNext((imageDatas: self.branchMenuImages, isView: self.isView))
  }
  
  func didAddBranchMenuImage() {
    self.present(menuImagePicker, animated: true, completion: nil)
  }
}

extension BranchViewController: InAppVerifyPhoneNumberViewControllerDelegate {
  func didVerifySuccess() {
    viewModel.inputs.deleteBranchTrigger.onNext(())
  }
}

extension BranchViewController: MMNumberKeyboardDelegate {
  func numberKeyboardShouldReturn(_ numberKeyboard: MMNumberKeyboard) -> Bool {
    if numberKeyboard == priceEndNumberlicKeyboard {
      let priceStart = Int(priceStartTextField.text ?? "0") ?? 0
      let priceEnd = Int(priceEndTextField.text ?? "0") ?? 0
      
      if priceEnd < priceStart {
        priceEndTextField.text = nil
      }
    }
    return true
  }
}

extension BranchViewController: UITextFieldDelegate {
  func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
    if textField == priceEndTextField {
      let priceStart = Int(priceStartTextField.text ?? "0") ?? 0
      let priceEnd = Int(priceEndTextField.text ?? "0") ?? 0
      
      if priceEnd < priceStart {
        priceEndTextField.text = nil
      }
    }
    return true
  }
}
