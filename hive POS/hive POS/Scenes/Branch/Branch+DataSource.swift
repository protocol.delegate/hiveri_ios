//
//  Brand+DataSource.swift
//  hive POS
//
//  Created by Space-C4ts on 2/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import RxDataSources

struct DisplayBranchMenuData {
  var title: String
  var ID: Int
}

struct SectionOfDisplayBranchMenuData {
  var header: String
  var items: [Item]
}

extension SectionOfDisplayBranchMenuData: SectionModelType {
  typealias Item = DisplayBranchMenuData
  
  init(original: SectionOfDisplayBranchMenuData, items: [Item]) {
    self = original
    self.items = items
  }
}

struct OpenHourData {
  var startTime: String
  var endTime: String
}

struct SectionOfOpenHourData {
  var header: String
  var items: [Item]
}

extension SectionOfOpenHourData: SectionModelType {
  typealias Item = OpenHourData
  
  init(original: SectionOfOpenHourData, items: [Item]) {
    self = original
    self.items = items
  }
}

enum HolidayType {
  case week
  case day
}

struct HolidayData {
  var day: String
  var week: String
  var type: HolidayType
}

struct SectionOfHolidayData{
  var header: String
  var items: [Item]
}

extension SectionOfHolidayData: SectionModelType {
  typealias Item = HolidayData
  
  init(original: SectionOfHolidayData, items: [Item]) {
    self = original
    self.items = items
  }
}

struct FoodCategoryData {
  var ID: String
  var name: String
  var isSelect: Bool
}

struct SectionOfFoodCetegoryData{
  var header: String
  var items: [Item]
}

extension SectionOfFoodCetegoryData: SectionModelType {
  typealias Item = FoodCategoryData
  
  init(original: SectionOfFoodCetegoryData, items: [Item]) {
    self = original
    self.items = items
  }
}

struct BranchImageData {
  var imageID: String
  var imageURL: String
}


enum BranchImageDataType {
  case branchImage(items: BranchImageData)
  case add
}

enum SectionOfBranchImageDataType {
  case Section(title: String, items: [BranchImageDataType])
}

extension SectionOfBranchImageDataType: SectionModelType {
  typealias Item = BranchImageDataType
  
  var items: [BranchImageDataType] {
    switch self {
    case .Section(_, items: let items):
      return items.map { $0 }
    }
  }
  
  init(original: SectionOfBranchImageDataType, items: [Item]) {
    switch original {
    case .Section(title: let title, _):
      self = .Section(title: title, items: items)
    }
  }
}

extension SectionOfBranchImageDataType {
  var title: String {
    switch self {
    case .Section(title: let title, _):
      return title
    }
  }
}
