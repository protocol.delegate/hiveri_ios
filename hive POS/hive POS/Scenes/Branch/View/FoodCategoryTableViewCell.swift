//
//  FoodCategoryTableViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 7/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

class FoodCategoryTableViewCell: UITableViewCell {

  @IBOutlet weak var categoryButton: UIButton!
  
  override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  func configure(title: String, isSelect: Bool) {
    categoryButton.setTitle(title, for: .normal)
    categoryButton.isSelected = isSelect
  }

}
