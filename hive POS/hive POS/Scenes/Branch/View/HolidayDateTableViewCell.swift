//
//  HolidayDateTableViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 5/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

protocol HolidayDateTableViewCellDelegate: class {
  func didSelectedDateSuccess(day: String, index: Int)
}

class HolidayDateTableViewCell: UITableViewCell {

  @IBOutlet weak var holidayTitleLabel: UILabel!
  @IBOutlet weak var dayTextField: UITextField!
  @IBOutlet weak var dropdownImageView: UIButton!
  
  var index: Int = 0
  var viewController: UIViewController?
  weak var delegate: HolidayDateTableViewCellDelegate?
  
  override func awakeFromNib() {
        super.awakeFromNib()
        
    }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    dayTextField.layer.cornerRadius = 10
    dayTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    dayTextField.layer.borderWidth = 1
    dayTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: dayTextField.frame.height))
    dayTextField.leftViewMode = .always
    dayTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: dayTextField.frame.height))
    dayTextField.rightViewMode = .always
  }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  func configure(day: String, index: Int, viewController: UIViewController, isView: Bool){
    self.index = index
    
    holidayTitleLabel.text = "branch_holiday_day".localized()
    dayTextField.placeholder = "branch_select_date_placeholder".localized()
    dayTextField.text = day
    self.viewController = viewController
    if isView {
      dayTextField.layer.borderColor = UIColor.clear.cgColor
      dropdownImageView.isHidden = true
    } else {
      dayTextField.layer.borderColor = UIColor.black.cgColor
      dropdownImageView.isHidden = false
    }
  }

  @IBAction func didClickOnSelectDay(_ sender: Any) {
    let data = ["วันที่ 1", "วันที่ 2", "วันที่ 3", "วันที่ 4", "วันที่ 5", "วันที่ 6", "วันที่ 7", "วันที่ 8", "วันที่ 9", "วันที่ 10", "วันที่ 11",
    "วันที่ 12", "วันที่ 13", "วันที่ 14", "วันที่ 15", "วันที่ 16", "วันที่ 17", "วันที่ 18", "วันที่ 19", "วันที่ 20", "วันที่ 21", "วันที่ 22",
    "วันที่ 23", "วันที่ 24", "วันที่ 25", "วันที่ 26", "วันที่ 27", "วันที่ 28", "วันที่ 29", "วันที่ 30", "วันที่ 31"]
    let controller = ArrayChoiceTableViewController(data) { [unowned self] (text) in
      self.dayTextField.text = text
      self.delegate?.didSelectedDateSuccess(day: text, index: self.index)
    }
    controller.preferredContentSize = CGSize(width: 130, height: 300)
    self.presentPopOverView(controller, sourceView: self.dayTextField)
  }
  
  func presentPopOverView(_ controller: UIViewController, sourceView: UIView) {
    let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
    presentationController.sourceView = sourceView
    presentationController.sourceRect = CGRect(x: 50  , y: 44, width: 0, height: 0)
    presentationController.permittedArrowDirections = [.up]
    self.viewController?.present(controller, animated: true, completion: nil)
  }
}
