//
//  FoodCategoryGenreTableViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 7/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

class FoodCategoryGenreTableViewCell: UITableViewCell {

  
  @IBOutlet weak var categoryTitleLabel: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
      let colorView = UIView()
      colorView.backgroundColor = UIColor(hex: 0x8E8E93).withAlphaComponent(0.2)
      self.selectedBackgroundView = colorView
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  func configure(title: String) {
    categoryTitleLabel.text = title
  }

}
