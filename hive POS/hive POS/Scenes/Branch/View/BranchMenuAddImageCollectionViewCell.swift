//
//  BranchMenuAddImageCollectionViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 29/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

protocol BranchMenuAddImageCollectionViewCellDelegate: class {
  func didAddBranchMenuImage()
}

class BranchMenuAddImageCollectionViewCell: UICollectionViewCell {
  var delegate: BranchMenuAddImageCollectionViewCellDelegate?
  
  @IBAction func didClickOnAdd(_ sender: Any) {
    delegate?.didAddBranchMenuImage()
  }
}
