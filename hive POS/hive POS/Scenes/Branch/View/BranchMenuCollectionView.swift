//
//  BranchMenuCollectionView.swift
//  hive POS
//
//  Created by Space-C4ts on 2/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import UIKit

class BranchMenuCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Instances
  
    var brandIsSelect: Bool?
  
    // MARK: - @IBOutlet
  
    @IBOutlet weak var menuTitleLabel: UILabel!
    @IBOutlet weak var handleUIView: UIView!
    @IBOutlet weak var cellBackgroundView: UIView!
    
    //MARK: - IBAction
    
    
    // MARK: - ui
    
  func configure(title: String, isSelect: Bool) {
      menuTitleLabel.text = title
      self.brandIsSelect = false
      handleUIView.isHidden = isSelect
      if brandIsSelect == isSelect {
          uiShowoffBrand()
      } else {
          uiShowOnBrand()
      }
      
      setupUI()
        
  }
    
    func uiShowOnBrand() {
        handleUIView.isHidden = false
        menuTitleLabel.alpha = 1
    }
    
    func uiShowoffBrand() {
        handleUIView.isHidden = true
        menuTitleLabel.alpha = 0.3
    }
    
    func checkDataRestaurantResponse(){
        print("Data not found")
    }
  
  func setupUI() {
    cellBackgroundView.layer.cornerRadius = 5
    cellBackgroundView.layer.applySketchShadow(color: .black, alpha: 0.5, x: 0, y: 2, blur: 6, spread: 0)
  }
    
}
