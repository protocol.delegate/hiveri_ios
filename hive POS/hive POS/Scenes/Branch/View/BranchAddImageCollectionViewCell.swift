//
//  BranchAddImageCollectionViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 7/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

protocol BranchAddImageCollectionViewCellDelegate: class {
  func didAddBranchImage()
}

class BranchAddImageCollectionViewCell: UICollectionViewCell {
  
  var delegate: BranchAddImageCollectionViewCellDelegate?
  
  @IBAction func didClickOnAdd(_ sender: Any) {
    delegate?.didAddBranchImage()
  }
}
