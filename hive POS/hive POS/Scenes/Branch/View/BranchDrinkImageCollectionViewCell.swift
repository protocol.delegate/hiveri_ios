//
//  BranchDrinkImageCollectionViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 29/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

protocol BranchDrinkImageCollectionViewCellDelegate: class {
  func didClearBranchDrinkImage(index: Int)
}

class BranchDrinkImageCollectionViewCell: UICollectionViewCell {
  @IBOutlet weak var branchImageView: UIImageView!
  @IBOutlet weak var clearButton: UIButton!
  
  weak var delegate: BranchDrinkImageCollectionViewCellDelegate?
  var index = 0
  
    func configure(imagePath: String, index: Int, isView: Bool) {
    self.index = index
    let imageURL = URL(string: imagePath)
    branchImageView.kf.setImage(with: imageURL,
                                placeholder: UIImage(named: "ic_default_image"))
      if isView {
        clearButton.isHidden = true
      } else {
        clearButton.isHidden = false
      }
  }
    
  
  @IBAction func didClickOnClear(_ sender: Any) {
    delegate?.didClearBranchDrinkImage(index: self.index)
  }
}
