//
//  HolidayTableViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 5/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

protocol HolidayTableViewCellDelegate: class {
  func didEditHolidaySuccess(day: String, week: String, index: Int)
}

class HolidayTableViewCell: UITableViewCell {
  
  @IBOutlet weak var dayTitleLabel: UILabel!
  @IBOutlet weak var weekTitleLabel: UILabel!
  @IBOutlet weak var dayTextField: UITextField!
  @IBOutlet weak var weekTextField: UITextField!
  @IBOutlet var dropdownImageViews: [UIButton]!
  weak var delegate: HolidayTableViewCellDelegate?
  var viewController: UIViewController?
  var index = 0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    dayTextField.layer.cornerRadius = 10
    dayTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    dayTextField.layer.borderWidth = 1
    dayTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: dayTextField.frame.height))
    dayTextField.leftViewMode = .always
    dayTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: dayTextField.frame.height))
    dayTextField.rightViewMode = .always
    
    weekTextField.layer.cornerRadius = 10
    weekTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    weekTextField.layer.borderWidth = 1
    weekTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: weekTextField.frame.height))
    weekTextField.leftViewMode = .always
    weekTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: weekTextField.frame.height))
    weekTextField.rightViewMode = .always
  }
  
  func configure(day: String, week: String, index: Int, viewController: UIViewController, isView: Bool) {
    self.index = index
    
    
    dayTitleLabel.text = "branch_holiday_day_title".localized()
    weekTitleLabel.text = "branch_holiday_week_title".localized()
    dayTextField.placeholder = "branch_select_date_placeholder".localized()
    weekTextField.placeholder = "branch_select_week_placeholder".localized()
    dayTextField.text = day
    weekTextField.text = week
    self.viewController = viewController
    if isView {
      dayTextField.layer.borderColor = UIColor.clear.cgColor
      weekTextField.layer.borderColor = UIColor.clear.cgColor
      dropdownImageViews.forEach{ $0.isHidden = true }
    } else {
      dayTextField.layer.borderColor = UIColor.black.cgColor
      weekTextField.layer.borderColor = UIColor.black.cgColor
      dropdownImageViews.forEach{ $0.isHidden = false }
    }
  }
  
  func presentPopOverView(_ controller: UIViewController, sourceView: UIView) {
    let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
    presentationController.sourceView = sourceView
    presentationController.sourceRect = CGRect(x: 50  , y: 44, width: 0, height: 0)
    presentationController.permittedArrowDirections = [.up]
    self.viewController?.present(controller, animated: true, completion: nil)
  }
  
  
  @IBAction func didClickOnday(_ sender: Any) {
    let data = ["วันอาทิตย์", "วันจันทร์", "วันอังคาร", "วันพุธ", "วันพฤหัสบดี", "วันศุกร์", "วันเสาร์"]
    let controller = ArrayChoiceTableViewController(data) { [unowned self] (text) in
      self.dayTextField.text = text
    }
    controller.preferredContentSize = CGSize(width: 130, height: 300)
    self.presentPopOverView(controller, sourceView: self.dayTextField)
  }
  
  @IBAction func didClickOnWeek(_ sender: Any) {
    if dayTextField.text == "" {
      dayTextField.layer.borderColor = UIColor(hex: 0xF2503E).cgColor
      return
    } else {
      dayTextField.layer.borderColor = UIColor.black.cgColor
    }
    let data = ["สัปดาห์ที่ 1", "สัปดาห์ที่ 2", "สัปดาห์ที่ 3", "สัปดาห์ที่ 4", "สัปดาห์ที่ 5", "สัปดาห์สุดท้ายของเดือน"]
    let controller = ArrayChoiceTableViewController(data) { [unowned self] (text) in
      self.weekTextField.text = text
      self.delegate?.didEditHolidaySuccess(day: self.dayTextField.text ?? "", week: text, index: self.index)
    }
    controller.preferredContentSize = CGSize(width: 200, height: 280)
    self.presentPopOverView(controller, sourceView: self.weekTextField)
  }
}
