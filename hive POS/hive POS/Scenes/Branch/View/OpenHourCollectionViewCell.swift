//
//  OpenHourCollectionViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 4/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

class OpenHourCollectionViewCell: UICollectionViewCell {
    
  @IBOutlet weak var openHourBackgroundView: UIView!
  @IBOutlet weak var openHoursLabel: UILabel!
  @IBOutlet weak var clearButtonWidthConstraint: NSLayoutConstraint!
  
  func configure(startTime: String, endTime: String, isView: Bool, isEveryDay: Bool = false) {
    
    openHourBackgroundView.layer.borderColor = UIColor.black.cgColor
    openHourBackgroundView.layer.borderWidth = 1
    openHourBackgroundView.layer.cornerRadius = 10
    
    openHoursLabel.text = "\(startTime) - \(endTime)"
    
    if isView {
      clearButtonWidthConstraint.constant = 0
    } else {
      clearButtonWidthConstraint.constant = 44
    }
    
    if isEveryDay {
      openHourBackgroundView.layer.borderColor = UIColor.clear.cgColor
      openHoursLabel.textColor = UIColor(hex: 0xC5CFE0)
      clearButtonWidthConstraint.constant = 0
    } else {
      if isView {
        clearButtonWidthConstraint.constant = 0
        openHourBackgroundView.layer.borderColor = UIColor.clear.cgColor
        openHoursLabel.textColor = UIColor(hex: 0xC5CFE0)
      } else {
        clearButtonWidthConstraint.constant = 44
        openHourBackgroundView.layer.borderColor = UIColor.black.cgColor
        openHoursLabel.textColor = .black
      }
    }
  }
}
