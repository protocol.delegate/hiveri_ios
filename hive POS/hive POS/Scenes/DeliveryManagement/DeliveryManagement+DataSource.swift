//
//  DeliveryManagement+DataSource.swift
//  hive POS
//
//  Created by Space-C4ts on 14/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import Foundation
import RxDataSources

enum DeliveryType {
  case all
  case wait
  case findDriver
  case kitchen
  case isDelivery
  case deliverySuccess
  case problem
}

struct DisplayDeliveryMenuData {
  var type: DeliveryType
}

struct SectionOfDisplayDeliveryMenuData {
  var header: String
  var items: [Item]
}

extension SectionOfDisplayDeliveryMenuData: SectionModelType {
  typealias Item = DisplayDeliveryMenuData
  
  init(original: SectionOfDisplayDeliveryMenuData, items: [Item]) {
    self = original
    self.items = items
  }
}
