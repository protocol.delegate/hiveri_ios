//
//  DeliveryManagementCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 14/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol DeliveryManagementInputsParamsType {
    
}

class DeliveryManagementCoordinator: BaseCoordinator<Void>, DeliveryManagementInputsParamsType {
  
  // MARK: Property

  // MARK: Public

  override func start() -> Observable<Void> {
    let window = self.window
    let viewModel = DeliveryManagementViewModel()
    let viewController = DeliveryManagementViewController.initFromStoryboard(name: Storyboard.delivery.identifier)
    viewController.viewModel = viewModel
    
    transition(to: viewController)
    return .never()
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: DeliveryManagementInputsParamsType { return self }
}
