//
//  DeliveryMenuCollectionViewCell.swift
//  hive POS
//
//  Created by Space-C4ts on 14/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit

class DeliveryMenuCollectionViewCell: UICollectionViewCell {
    
  @IBOutlet weak var cellBackGroundView: UIView!
  @IBOutlet weak var handleUIVIew: UIView!
  @IBOutlet weak var menuTitleLabel: UILabel!
  
  var isSelect = false
  
  func configure(deliveryType: DeliveryType, isSelect: Bool) {
      self.isSelect = false
      handleUIVIew.isHidden = isSelect
    if self.isSelect == isSelect {
          uiShowoffBrand()
      } else {
          uiShowOnBrand()
      }
      
    setupUI()
    switch deliveryType {
    case .all:
      menuTitleLabel.text = "delivery_all".localized()
    case .wait:
      menuTitleLabel.text = "delivery_wait".localized()
    case .findDriver:
      menuTitleLabel.text = "delivery_finddriver".localized()
    case .kitchen:
      menuTitleLabel.text = "delivery_kitchen".localized()
    case .isDelivery:
      menuTitleLabel.text = "delivery_isdelivery".localized()
    case .deliverySuccess:
      menuTitleLabel.text = "delivery_deliverysuccess".localized()
    case .problem:
      menuTitleLabel.text = "delivery_problem".localized()
    }
        
  }
    
    func uiShowOnBrand() {
        handleUIVIew.isHidden = false
        menuTitleLabel.alpha = 1
    }
    
    func uiShowoffBrand() {
        handleUIVIew.isHidden = true
        menuTitleLabel.alpha = 0.8
    }
    
    func checkDataRestaurantResponse(){
        print("Data not found")
    }
  
  func setupUI() {
    cellBackGroundView.layer.cornerRadius = 5
    cellBackGroundView.layer.applySketchShadow(color: .black, alpha: 0.5, x: 0, y: 2, blur: 6, spread: 0)
  }
}
