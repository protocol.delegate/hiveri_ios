//
//  DeliveryManagementViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 14/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa

protocol DeliveryManagementViewModelInputs {
  var getDeliveryMenuData: PublishSubject<Void> { get }
  var getDeliveryOrders: PublishSubject<Void> { get }
}

protocol DeliveryManagementViewModelOutputs {
  var displayDeliveryMenuData: PublishSubject<[SectionOfDisplayDeliveryMenuData]> { get }
  var loading: BehaviorSubject<Bool> { get }
}

protocol DeliveryManagementViewModelCoordinates {
    
}

protocol DeliveryManagementViewModelType: ErrorAlertableViewModelType {
  var inputs: DeliveryManagementViewModelInputs { get }
  var outputs: DeliveryManagementViewModelOutputs { get }
  var coordinates: DeliveryManagementViewModelCoordinates { get }
}

class DeliveryManagementViewModel: DeliveryManagementViewModelType, DeliveryManagementViewModelInputs, DeliveryManagementViewModelOutputs, DeliveryManagementViewModelCoordinates {
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  
  // MARK: Init
    
  public init(apiService: APIServiceType = APIService()) {
    
    let menuData = [DisplayDeliveryMenuData(type: .all),
                    DisplayDeliveryMenuData(type: .wait),
                    DisplayDeliveryMenuData(type: .findDriver),
                    DisplayDeliveryMenuData(type: .kitchen),
                    DisplayDeliveryMenuData(type: .isDelivery),
                    DisplayDeliveryMenuData(type: .deliverySuccess),
                    DisplayDeliveryMenuData(type: .problem)]
    
    let getDeliveryOrdersSignal = getDeliveryOrders
      .map{ BranchSignService.shared.getBranchData() }
      .filterNil()
      .map{ DeliveryOrdersRequestModel(token: "\($0.ID)") }
      .flatMapLatest{ apiService.rx_deliveryOrders(requestModel: $0).materialize() }
      .share(replay: 1)
    
    getDeliveryMenuData
      .map{ [SectionOfDisplayDeliveryMenuData(header: "", items: menuData)] }
      .bind(to: displayDeliveryMenuData)
      .disposed(by: disposeBag)
    
    // Error ---------------------------------------------------------------
    
    let errorSignal = Observable
      .merge(getDeliveryOrdersSignal.errors())
      .share(replay: 1)
    
    errorSignal
      .map(errorToAlertable)
      .bind(to: error)
      .disposed(by: disposeBag)
    
  }
  
  // MARK: Private
  
  // MARK: Input
  
  var getDeliveryMenuData = PublishSubject<Void>()
  var getDeliveryOrders = PublishSubject<Void>()
    
  // MARK: Output
  
  var displayDeliveryMenuData = PublishSubject<[SectionOfDisplayDeliveryMenuData]>()
  var error = PublishSubject<(title: String, message: String)>()
  var loading = BehaviorSubject<Bool>(value: false)

  // MARK: Coordinates
  
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: DeliveryManagementViewModelOutputs { return self }
  public var inputs: DeliveryManagementViewModelInputs { return self }
  public var coordinates: DeliveryManagementViewModelCoordinates { return self }
}
