//
//  DeliveryManagementViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 14/5/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxDataSources
import ZSwiftKit

class DeliveryManagementViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var testSubmit: UIButton!
  @IBOutlet weak var testTextView: UITextView!
  @IBOutlet weak var listView: UIView!
  @IBOutlet weak var deliveryView: UIView!
  @IBOutlet weak var tabMenuCollectionView: UICollectionView!
  @IBOutlet weak var listTitleLabel: UILabel!
  @IBOutlet weak var listLabel: UILabel!
  
  // MARK: Property 
  
  var viewModel: DeliveryManagementViewModelType!
  var selectMenu: DeliveryType = .all
  let disposeBag = DisposeBag()
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
    setupL10N()
    bindViewModel()
    
  }
  
  func bindViewModel() {
    
    let dataSource = RxCollectionViewSectionedReloadDataSource<SectionOfDisplayDeliveryMenuData>(
    configureCell: { [unowned self] dataSource, collectionView, indexPath, item in
      let cell = collectionView.dequeueReusableCell(withReuseIdentifier: DeliveryMenuCollectionViewCell.className, for: indexPath) as! DeliveryMenuCollectionViewCell
      cell.configure(deliveryType: item.type, isSelect: item.type == self.selectMenu)
      return cell
    })
    
    viewModel
      .outputs
      .displayDeliveryMenuData
      .bind(to: tabMenuCollectionView.rx.items(dataSource: dataSource))
      .disposed(by: disposeBag)
    
    tabMenuCollectionView.rx
      .modelSelected(DisplayDeliveryMenuData.self)
      .subscribe(onNext: { [unowned self] model in
        self.selectMenu = model.type
        self.viewModel.inputs.getDeliveryMenuData.onNext(())})
      .disposed(by: disposeBag)
    
    viewModel
    .inputs
    .getDeliveryMenuData
    .onNext(())
    
    testSubmit.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        WebSocketService.shared.write(text: self.testTextView.text)
      })
      .disposed(by: disposeBag)
    
    //MARK: Bind loading
       
    viewModel.outputs.loading
      .subscribe(onNext: { [unowned self] isLoading in
        guard let tabBarController = self.tabBarController else { return }
        LoadingActivityView.action(in: tabBarController.view, isShow: isLoading)})
      .disposed(by: disposeBag)
    
    // MARK: Bind Error
          
    viewModel.error
      .subscribe(onNext: { [unowned self] error in
          Helper.Alert(
            view: self,
              title: error.title,
              message: error.message,
              confirmText: "done".localized())})
      .disposed(by: disposeBag)
    
  }
  
  func setupL10N() {
    self.setNavigationBarItem(titlePageName: "delivery_title".localized())
    self.listTitleLabel.text = "delivery_orderlist".localized()
    self.listLabel.text = "\(0)" + "delivery_orderlist_unit".localized()
  }
  
  func setupUI() {
    if let collectionViewLayout = tabMenuCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
      collectionViewLayout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
      collectionViewLayout.scrollDirection = .horizontal
    }
    deliveryView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: -2, blur: 6, spread: 0)
    listView.layer.applySketchShadow(color: .black, alpha: 0.16, x: 0, y: 3, blur: 6, spread: 0)
  }
}
