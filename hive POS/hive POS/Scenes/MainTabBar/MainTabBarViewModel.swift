//
//  MainTabBarViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 24/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa

protocol MainTabBarViewModelInputs {
    
}

protocol MainTabBarViewModelOutputs {
    
}

protocol MainTabBarViewModelCoordinates {
  var navigateToAuthen: PublishSubject<Void> { get }
}

protocol MainTabBarViewModelType {
  var inputs: MainTabBarViewModelInputs { get }
  var outputs: MainTabBarViewModelOutputs { get }
  var coordinates: MainTabBarViewModelCoordinates { get }
}

class MainTabBarViewModel: MainTabBarViewModelType, MainTabBarViewModelInputs, MainTabBarViewModelOutputs, MainTabBarViewModelCoordinates {
  
  // MARK: Property
  
  // MARK: Init
    
  public init() {
  }
  
  // MARK: Private
  
  // MARK: Input
    
  // MARK: Output

  // MARK: Coordinates
  
  var navigateToAuthen = PublishSubject<Void>()
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: MainTabBarViewModelOutputs { return self }
  public var inputs: MainTabBarViewModelInputs { return self }
  public var coordinates: MainTabBarViewModelCoordinates { return self }
}
