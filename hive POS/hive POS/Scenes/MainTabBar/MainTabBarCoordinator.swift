//
//  MainTabBarCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 24/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol MainTabBarInputsParamsType {
    
}

class MainTabBarCoordinator: BaseCoordinator<Void>, MainTabBarInputsParamsType {
  
  // MARK: Property
  
  var brandBranchCoordinator: BrandBranchManagementCoordinator!
  var menuCoordinator: MenuManagementCoordinator!
  var orderCoordinator: OrderManagementCoordinator!
  var paymentCoordinator: PaymentOrderManagementCoordinator!
  var deliveryCoordinator: DeliveryManagementCoordinator!

  // MARK: Public

  override func start() -> Observable<Void> { 
    let window = self.window
    let viewModel = MainTabBarViewModel()
    let viewController = MainTabBarViewController.initFromStoryboard(name: Storyboard.main.identifier)
    viewController.viewModel = viewModel
    
    // Setup BrandBranch
    let brandBranchNav = UINavigationController()
    brandBranchNav.tabBarItem = UITabBarItem(title: "",
                                             image: UIImage(named: "ic_tab_brand_branch"),
                                             selectedImage: nil)
    
    brandBranchNav.setNavigationBarHidden(false, animated: false)
    
    brandBranchCoordinator = BrandBranchManagementCoordinator(window: window,
                                                       baseViewController: brandBranchNav,
                                                       transitionType: .rootNavigation,
                                                       animated: false)
    coordinate(to: brandBranchCoordinator)
      .subscribe()
      .disposed(by: disposeBag)
    
    // Setup BrandBranch
    let menuNav = UINavigationController()
    menuNav.tabBarItem = UITabBarItem(title: "",
                                      image: UIImage(named: "ic_tap_menu"),
                                      tag: 333)
    menuNav.setNavigationBarHidden(false, animated: false)
    
    menuCoordinator = MenuManagementCoordinator(window: window,
                                                baseViewController: menuNav,
                                                transitionType: .rootNavigation,
                                                animated: false)
    coordinate(to: menuCoordinator)
      .subscribe()
      .disposed(by: disposeBag)

    // Setup CreateOrder
    let orderNav = UINavigationController()
    orderNav.tabBarItem = UITabBarItem(title: "",
                                             image: UIImage(named: "ic_tab_order"),
                                             selectedImage: nil)
    orderNav.setNavigationBarHidden(false, animated: false)
    
    orderCoordinator = OrderManagementCoordinator(window: window,
                                                baseViewController: orderNav,
                                                transitionType: .rootNavigation,
                                                animated: false)
    coordinate(to: orderCoordinator)
      .subscribe()
      .disposed(by: disposeBag)
    
    // Setup Payment
    let paymentNav = UINavigationController()
    paymentNav.tabBarItem = UITabBarItem(title: "",
                                             image: UIImage(named: "ic_tab_payment"),
                                             selectedImage: nil)
    paymentNav.setNavigationBarHidden(false, animated: false)
    
    paymentCoordinator = PaymentOrderManagementCoordinator(window: window,
                                                           baseViewController: paymentNav,
                                                           transitionType: .rootNavigation,
                                                           animated: false)
    coordinate(to: paymentCoordinator)
      .subscribe()
      .disposed(by: disposeBag)
    
    // Setup Payment
    let deliveryNav = UINavigationController()
    deliveryNav.tabBarItem = UITabBarItem(title: "",
                                          image: UIImage(named: "ic_tab_delivery"),
                                          selectedImage: nil)
    deliveryNav.setNavigationBarHidden(false, animated: false)
    
    deliveryCoordinator = DeliveryManagementCoordinator(window: window,
                                                        baseViewController: deliveryNav,
                                                        transitionType: .rootNavigation,
                                                        animated: false)
    coordinate(to: deliveryCoordinator)
      .subscribe()
      .disposed(by: disposeBag)

    
    viewController.viewControllers = [UIViewController(), brandBranchNav, menuNav, orderNav, paymentNav, deliveryNav, UIViewController()]
    viewController.selectedIndex = 1
    
    
    viewModel.coordinates
    .navigateToAuthen
    .map{ AuthenticationCoordinator(window: window,
                                    baseViewController: viewController,
                                    transitionType: .rootWindow,
                                    animated: true) }
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
    transition(to: viewController)
    
    return .never()
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: MainTabBarInputsParamsType { return self }
}
