//
//  MainTabBarViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 24/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZSwiftKit
import Firebase

class MainTabBarViewController: UITabBarController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  // MARK: Property 
  
  var viewModel: MainTabBarViewModelType!
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
    setupListener()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
   
  }
  
  func bindViewModel() {
  }
  
  func setupUI(){
    self.tabBar.tintColor = UIColor(hex: 0x45D62C)
    delegate = self
//    guard let items = self.tabBar.items?.filter({$0.tag == 333}) else { return }
//    guard let item = items.first else { return }
//    self.tabBar.selectedItem = item
  }
  
  func setupListener() {
    NotificationCenter.default.addObserver(self,
                                           selector: #selector(self.signoutNotificationHandler(notification:)),
                                           name: .didClickOnSignout,
                                           object: nil)
  }
  
  @objc func signoutNotificationHandler(notification: Notification) {
    do { try Auth.auth().signOut() } catch {}
    LocalDataService.deleteUser()
    LocalDataService.deleteAuthen()
    LocalDataService.deleteRememberMe()
    LocalDataService.deleteSelectBrand()
    BranchSignService.shared.deleteBranchData()
    viewModel.coordinates.navigateToAuthen.onNext(())
  }
}

extension MainTabBarViewController: UITabBarControllerDelegate {
  
  func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
    guard let selectIndex = tabBarController.viewControllers?.enumerated().filter({ $0.element == viewController }).first?.offset else { return true }
    if selectIndex == 0 || selectIndex == 6 {
      return false
    }
    if selectIndex == 2 || selectIndex == 4 || selectIndex == 5 || selectIndex == 3 {
      if let _ = BranchSignService.shared.getBranchData(){
        return true
      } else {
        Helper.Alert(view: self, title: "maintab_alert_title".localized(),
                     message: "maintab_alert_message".localized(),
                     confirmText: "ok".localized())
        return false
      }
    } else {
      return true
    }
  }
  
}
