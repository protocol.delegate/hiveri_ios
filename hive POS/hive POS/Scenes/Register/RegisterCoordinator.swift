//
//  RegisterCoordinator.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxSwift

protocol RegisterInputsParamsType {
    
}

class RegisterCoordinator: BaseCoordinator<Void>, RegisterInputsParamsType {
  
  // MARK: Property

  // MARK: Public

  override func start() -> Observable<Void> {
    
    let window = self.window
    let viewModel = RegisterViewModel()
    let viewController = RegisterViewController.initFromStoryboard(name: Storyboard.authentication.identifier)
    viewController.viewModel = viewModel
    let navigationController = UINavigationController(rootViewController: viewController)
    navigationController.setNavigationBarHidden(true, animated: false)
    
    viewModel
    .coordinates
    .navigateToVerifyPhoneNumer
    .map{ registerData -> VerifyPhoneNumberCoordinator in
      let coordinator = VerifyPhoneNumberCoordinator(window: window,
                                                     baseViewController: viewController,
                                                     transitionType: .push,
                                                     animated: true)
      coordinator.registerData = registerData
      return coordinator}
    .flatMapLatest(navigateToTarget)
    .subscribe()
    .disposed(by: disposeBag)
    
      transition(to: navigationController)
    return viewModel.coordinates.dissmissView
      .do(onNext: { [weak self] _ in self?.navigateBack()})
  }

  // MARK: Private
    
  // MARK: InputParams

  public var inputsParams: RegisterInputsParamsType { return self }
}
