//
//  RegisterViewModel.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import RxSwift
import RxCocoa
import Firebase

struct RegisterData {
  var prefix: String
  var firstName: String
  var lastName: String
  var email: String
  var password: String
  var countryCode: String
  var phoneNumber: String
}

protocol RegisterViewModelInputs {
  var dissmissViewTrigger: PublishSubject<Void> { get }
  var submitButtonTrigger: PublishSubject<RegisterData> { get }
}

protocol RegisterViewModelOutputs {
  var displayCheckEmailError: PublishSubject<(Void)> { get }
  var loading: BehaviorSubject<Bool> { get }
}

protocol RegisterViewModelCoordinates {
  var navigateToVerifyPhoneNumer: PublishSubject<RegisterData> { get }
  var dissmissView: PublishSubject<Void> { get }
}

protocol RegisterViewModelType: ErrorAlertableViewModelType {
  var inputs: RegisterViewModelInputs { get }
  var outputs: RegisterViewModelOutputs { get }
  var coordinates: RegisterViewModelCoordinates { get }
}

class RegisterViewModel: RegisterViewModelType, RegisterViewModelInputs, RegisterViewModelOutputs, RegisterViewModelCoordinates {
  
  // MARK: Property
  
  let disposeBag = DisposeBag()
  
  // MARK: Init
    
  public init(apiService: APIServiceType = APIService()) {
    
    dissmissViewTrigger
      .bind(to: dissmissView)
      .disposed(by: disposeBag)
    
    submitButtonTrigger
      .do(onNext: {[unowned self] _ in self.outputs.loading.onNext(true)})
      .subscribe(onNext: { [unowned self] data in
        Auth.auth().fetchSignInMethods(forEmail: data.email) { [unowned self] (response, error) in
          if let error = error {
            self.error.onNext((title: "Error", message: error.localizedDescription))
            return
          }
          if let _ = response {
            self.outputs.displayCheckEmailError.onNext(())
          } else {
            self.coordinates.navigateToVerifyPhoneNumer.onNext(data)
          }
          self.outputs.loading.onNext(false)
        }
      })
      .disposed(by: disposeBag)
  }
  
  // MARK: Private
  
  // MARK: Input
  
  var dissmissViewTrigger = PublishSubject<Void>()
  var submitButtonTrigger = PublishSubject<RegisterData>()
    
  // MARK: Output
  
  var navigateToVerifyPhoneNumer = PublishSubject<RegisterData>()
  var displayCheckEmailError = PublishSubject<(Void)>()
  var error = PublishSubject<(title: String, message: String)>()
  var loading = BehaviorSubject<Bool>(value: false)

  // MARK: Coordinates
  
  var dissmissView = PublishSubject<Void>()
    
  // MARK: Input&Output&Coordinates
    
  public var outputs: RegisterViewModelOutputs { return self }
  public var inputs: RegisterViewModelInputs { return self }
  public var coordinates: RegisterViewModelCoordinates { return self }
}
