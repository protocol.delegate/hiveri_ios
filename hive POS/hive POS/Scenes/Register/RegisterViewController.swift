//
//  RegisterViewController.swift
//  hive POS
//
//  Created by Space-C4ts on 22/4/2563 BE.
//  Copyright © 2563 Space-C4ts. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import ZSwiftKit

class RegisterViewController: UIViewController, StoryboardInitializable {
  
  // MARK: IBOutlet
  
  @IBOutlet weak var registerTitleLabel: UILabel!
  @IBOutlet weak var registerDescriptionLabel: UILabel!
  @IBOutlet weak var prefixTitleLabel: UILabel!
  @IBOutlet weak var nameTitleLabel: UILabel!
  @IBOutlet weak var lastNameTitleLabel: UILabel!
  
  @IBOutlet weak var prefixButton: UIButton!
  @IBOutlet weak var prefixTextField: UITextField!
  @IBOutlet weak var nameTextField: UITextField!
  @IBOutlet weak var lastNameTextField: UITextField!
  
  @IBOutlet weak var emailTitleLabel: UILabel!
  @IBOutlet weak var emailTextField: UITextField!
  @IBOutlet weak var wrongEmailLabel: UILabel!
  
  @IBOutlet weak var passwordTitleLabel: UILabel!
  @IBOutlet weak var passwordTextField: UITextField!
  @IBOutlet weak var passwordDescriptionLabel: UILabel!
  
  @IBOutlet weak var confirmPasswordTitleLabel: UILabel!
  @IBOutlet weak var confirmPasswordTextField: UITextField!
  @IBOutlet weak var wrongConfirmPasswordLabel: UILabel!
  
  @IBOutlet weak var wrongPrefixLabel: UILabel!
  @IBOutlet weak var wrongNameLabel: UILabel!
  @IBOutlet weak var wrongLastNameLabel: UILabel!
  
  @IBOutlet weak var showHidePasswordButton: UIButton!
  @IBOutlet weak var showHideConfirmPasswordButton: UIButton!
  
  @IBOutlet weak var submitButtonView: UIView!
  @IBOutlet weak var submitButtonLabel: UILabel!
  @IBOutlet weak var submitButton: UIButton!
  
  @IBOutlet weak var dismissButton: UIButton!
  
  // MARK: Property
  
  var viewModel: RegisterViewModelType!
  let disposeBag: DisposeBag = DisposeBag()
  
  // MARK: View Life Cycle
    
  override func viewDidLoad() {
    super.viewDidLoad()
    
    setupUI()
    setupL10N()
    bindViewModel()
    
  }
  
  func bindViewModel() {
    
    prefixButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        var data: [String] = []
        switch LocalizationManager.share.getDefaultLanguage() {
        case .th:
          data = ["นาย", "น.ส.", "นาง"]
        case .en:
          data = ["Mr.", "Miss", "Mrs."]
        }
        let controller = ArrayChoiceTableViewController(data) { [unowned self] (name) in
          self.prefixTextField.text = name
        }
        controller.preferredContentSize = CGSize(width: 100, height: 150)
        self.presentPopOverView(controller, sourceView: self.prefixTextField)
      })
      .disposed(by: disposeBag)
    
    submitButton.rx.tap
      .filter{ [unowned self] in self.validateField()}
      .filter{ [unowned self] in self.isValidEmailFormat(email: self.emailTextField.text!)}
      .map{ RegisterData(prefix: self.prefixTextField.text!,
                         firstName: self.nameTextField.text!,
                         lastName: self.lastNameTextField.text!,
                         email: self.emailTextField.text!,
                         password: self.passwordTextField.text!,
                         countryCode: "",
                         phoneNumber: "") }
      .bind(to: viewModel.inputs.submitButtonTrigger)
      .disposed(by: disposeBag)
    
    showHidePasswordButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.showHidePasswordButton.isSelected = !self.showHidePasswordButton.isSelected
        if self.showHidePasswordButton.isSelected {
          self.passwordTextField.isSecureTextEntry = false
        } else {
          self.passwordTextField.isSecureTextEntry = true
        }})
      .disposed(by: disposeBag)
    
    dismissButton.rx.tap
      .bind(to: viewModel.inputs.dissmissViewTrigger)
      .disposed(by: disposeBag)
    
    showHideConfirmPasswordButton.rx.tap
      .subscribe(onNext: { [unowned self] _ in
        self.showHideConfirmPasswordButton.isSelected = !self.showHideConfirmPasswordButton.isSelected
        if self.showHideConfirmPasswordButton.isSelected {
          self.confirmPasswordTextField.isSecureTextEntry = false
        } else {
          self.confirmPasswordTextField.isSecureTextEntry = true
        }})
      .disposed(by: disposeBag)
    
    viewModel
      .outputs
      .displayCheckEmailError
      .subscribe(onNext: { [unowned self] _ in
        self.wrongEmailLabel.text =  "register_wrong_email".localized()
        self.emailTextField.layer.borderColor = UIColor(hex: 0xF2503E).cgColor
        self.wrongEmailLabel.isHidden = false})
      .disposed(by: disposeBag)
    
    //MARK: Bind loading
       
    viewModel.outputs.loading
      .subscribe(onNext: { [unowned self] isLoading in
        LoadingActivityView.action(in: self.view, isShow: isLoading)})
      .disposed(by: disposeBag)
    
    // MARK: Bind Error
          
    viewModel.error
      .subscribe(onNext: { [unowned self] error in
          Helper.Alert(
              view: self,
              title: error.title,
              message: error.message,
              confirmText: "done".localized())})
      .disposed(by: disposeBag)
  }
  
  func setupUI() {
    
    prefixTextField.layer.cornerRadius = 10
    prefixTextField.layer.borderWidth = 1
    prefixTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    prefixTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: prefixTextField.frame.height))
    prefixTextField.leftViewMode = .always
    prefixTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: prefixTextField.frame.height))
    prefixTextField.rightViewMode = .always
    
    nameTextField.layer.cornerRadius = 10
    nameTextField.layer.borderWidth = 1
    nameTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    nameTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: nameTextField.frame.height))
    nameTextField.leftViewMode = .always
    
    lastNameTextField.layer.cornerRadius = 10
    lastNameTextField.layer.borderWidth = 1
    lastNameTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    lastNameTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: lastNameTextField.frame.height))
    lastNameTextField.leftViewMode = .always
    
    emailTextField.layer.cornerRadius = 10
    emailTextField.layer.borderWidth = 1
    emailTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    emailTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: emailTextField.frame.height))
    emailTextField.leftViewMode = .always
    
    passwordTextField.layer.cornerRadius = 10
    passwordTextField.layer.borderWidth = 1
    passwordTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    passwordTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: passwordTextField.frame.height))
    passwordTextField.leftViewMode = .always
    passwordTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: passwordTextField.frame.height))
    passwordTextField.rightViewMode = .always
    
    confirmPasswordTextField.layer.cornerRadius = 10
    confirmPasswordTextField.layer.borderWidth = 1
    confirmPasswordTextField.layer.borderColor = UIColor(hex: 0x8E8E93).cgColor
    confirmPasswordTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: confirmPasswordTextField.frame.height))
    confirmPasswordTextField.leftViewMode = .always
    confirmPasswordTextField.rightView = UIView(frame: CGRect(x: 0, y: 0, width: 44, height: confirmPasswordTextField.frame.height))
    confirmPasswordTextField.rightViewMode = .always
    
    submitButtonView.layer.cornerRadius = submitButtonView.frame.height / 2
    
    passwordDescriptionLabel.isHidden = false
    passwordDescriptionLabel.textColor = UIColor(hex: 0x454F63)
    
  }
  
  func setupL10N() {
    
    registerTitleLabel.text = "register_title".localized()
    registerDescriptionLabel.text = "reister_description".localized()
    prefixTitleLabel.text = "register_prefix".localized()
    nameTitleLabel.text = "regster_name".localized()
    lastNameTitleLabel.text = "regster_lastname".localized()
    emailTitleLabel.text = "register_email".localized()
    passwordTitleLabel.text = "register_password".localized()
    confirmPasswordTitleLabel.text = "register_confirm_password".localized()
    submitButtonLabel.text = "register_submit".localized()
    
    prefixTextField.placeholder = "register_prefix_placeholder".localized()
    nameTextField.placeholder = "regster_name".localized()
    lastNameTextField.placeholder = "regster_lastname".localized()
    emailTextField.placeholder = "register_email".localized()
    passwordTextField.placeholder = "register_password".localized()
    confirmPasswordTextField.placeholder = "register_confirm_password".localized()
    
    wrongPrefixLabel.text = "register_please_input".localized()
    wrongNameLabel.text = "register_please_input".localized()
    wrongEmailLabel.text = "register_please_input".localized()
    wrongLastNameLabel.text = "register_please_input".localized()
    passwordDescriptionLabel.text = "register_password_desc".localized()
    wrongConfirmPasswordLabel.text = "register_password_not_match".localized()
    
  }
  
  func presentPopOverView(_ controller: UIViewController, sourceView: UIView) {
    let presentationController = AlwaysPresentAsPopover.configurePresentation(forController: controller)
    presentationController.sourceView = sourceView
    presentationController.sourceRect = CGRect(x: 50  , y: 44, width: 0, height: 0)
    presentationController.permittedArrowDirections = [.up]
    self.present(controller, animated: true)
  }
  
  func validateField() -> Bool {
    
    wrongPrefixLabel.isHidden = true
    wrongNameLabel.isHidden = true
    wrongLastNameLabel.isHidden = true
    wrongEmailLabel.isHidden = true
    wrongConfirmPasswordLabel.isHidden = true
    
    let isPassValidatedPrefix = validateTextField(text: prefixTextField.text!)
    let isPassValidatedName = validateTextField(text: nameTextField.text!)
    let isPassValidatedLastName = validateTextField(text: lastNameTextField.text!)
    let isPassValidatedEmail = validateTextField(text: emailTextField.text!)
    let isPassValidatePassword = validatePassword(password: passwordTextField.text!)
    let isPassValidateConfirmPassword = validateConfirmPassword(password: confirmPasswordTextField.text!)
    
    setUIRequirePrefix(isRequired: !isPassValidatedPrefix)
    setUIRequireName(isRequired: !isPassValidatedName)
    setUIRequireLastName(isRequired: !isPassValidatedLastName)
    setUIRequireEmail(isRequired: !isPassValidatedEmail)
    setUIRequirePassword(isRequired: !isPassValidatePassword)
    setUIRequireConfirmPassword(isRequired: !isPassValidateConfirmPassword)
    
    return isPassValidatedPrefix &&
      isPassValidatedName &&
      isPassValidatedLastName &&
      isPassValidatedEmail &&
      isPassValidatePassword &&
      isPassValidateConfirmPassword
  }
  
  func setUIRequirePrefix(isRequired: Bool) {
    prefixTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
    wrongPrefixLabel.isHidden = !isRequired
  }
  
  func setUIRequireName(isRequired: Bool) {
    nameTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
    wrongNameLabel.isHidden = !isRequired
  }
  
  func setUIRequireLastName(isRequired: Bool) {
    lastNameTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
    wrongLastNameLabel.isHidden = !isRequired
  }
  
  func setUIRequireEmail(isRequired: Bool) {
    emailTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
    wrongEmailLabel.text = "register_please_input".localized()
    wrongEmailLabel.isHidden = !isRequired
  }
  
  func setUIRequirePassword(isRequired: Bool) {
    passwordTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
    passwordDescriptionLabel.textColor = isRequired ? UIColor(hex: 0xF2503E) : UIColor(hex: 0x8E8E93)
  }
  
  func setUIRequireConfirmPassword(isRequired: Bool) {
    confirmPasswordTextField.layer.borderColor = isRequired ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
    wrongConfirmPasswordLabel.isHidden = !isRequired
  }
  
  func validateTextField(text: String) -> Bool {
    return text != ""
  }
  
  func validatePassword(password: String) -> Bool {
    if password == "" { return false }
    let predicate = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[A-Za-z])(?=.*[0-9])(?=.*[A-Z]).{8,}$")
    return predicate.evaluate(with: password)
  }
  
  func validateConfirmPassword(password: String) -> Bool {
    guard let passwordText = passwordTextField.text else { return false }
    if password == "" { return false }
    if password != passwordText { return false }
    return true
  }
  
  func isValidEmailFormat(email:String) -> Bool {
    let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
    let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
    let validFormat = emailTest.evaluate(with: email)
    if !validFormat {
      wrongEmailLabel.text = "register_invalid_email".localized()
      wrongEmailLabel.isHidden = false
    } else {
      wrongEmailLabel.isHidden = true
    }
    emailTextField.layer.borderColor = !validFormat ? UIColor(hex: 0xF2503E).cgColor : UIColor(hex: 0x8E8E93).cgColor
    return validFormat
  }
  
}
